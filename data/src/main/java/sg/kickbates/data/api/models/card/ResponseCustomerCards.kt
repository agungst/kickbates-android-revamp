package sg.kickbates.data.api.models.card


import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize
import sg.kickbates.data.BuildConfig
import sg.kickbates.data.api.models.common.Links
import sg.kickbates.data.api.models.common.Meta
import sg.kickbates.data.helper.CommonTools
import java.text.SimpleDateFormat
import java.util.*


@Parcelize
data class ResponseCustomerCards(

	@field:SerializedName("data") val data: List<DataItem>? = null,
	@field:SerializedName("meta") val meta: Meta? = null,
	@field:SerializedName("links") val links: Links? = null,
	@field:SerializedName("message") val message: String? = null,
	@field:SerializedName("status") val status: Int? = null
) : Parcelable {
	@Parcelize
	data class DataItem(
		@field:SerializedName("activation_date") val activationDate: String? = null,
		@field:SerializedName("due_date") val dueDate: String? = null,
		@field:SerializedName("statement_date") val statementDate: String? = null,
		@field:SerializedName("card_exp") val cardExp: String? = null,
		@field:SerializedName("active_date") val activeDate: String? = null,
		@field:SerializedName("current_balance") val currentBalance: String? = null,
		@field:SerializedName("card_type") val cardType: String? = null,
		@field:SerializedName("is_primary_card") val isPrimaryCard: String? = null,
		@field:SerializedName("users") val users: Users? = null,
		@field:SerializedName("card_id") val cardId: String? = null,
		@field:SerializedName("linked_accounts") val linkedAccounts: Boolean? = null,
		@field:SerializedName("card_status") val cardStatus: String? = null,
		@field:SerializedName("card_face") val cardFace: String? = null,
		@field:SerializedName("overseas_atm") val overseasAtm: String? = null,
		@field:SerializedName("issue_date") val issueDate: String? = null,
		@field:SerializedName("card_no") val cardNo: String? = null,
		@field:SerializedName("remaning_limit") val remaningLimit: String? = null,
		@field:SerializedName("users_id") val usersId: Int? = null,
		@field:SerializedName("id") val id: Int? = null,
		@field:SerializedName("kick_banks_id") val kickBanksId: Int? = null,
		@field:SerializedName("kick_banks") val kickBanks: KickBanks? = null,
		@field:SerializedName("card_desc") val cardDesc: String? = null,
		@field:SerializedName("kick_customer_card_transactions") val kickCustomerCardTransactions: List<DataCardTransactions>? = null,
		@field:SerializedName("kick_customer_card_statements") val kickCustomerCardStatements: List<DataCardStatements>? = null,
		@field:SerializedName("name_on_card") val nameOnCard: String? = null
	) : Parcelable {

		fun balance():String {
			return if (currentBalance.isNullOrBlank()){
				"0"
			}else {
				currentBalance
			}
		}
		fun paidStatusName():String {
			return  if (kickCustomerCardStatements?.isEmpty()!!) "PAID" else return if (haveStatementPaid(kickCustomerCardStatements)) "PAID" else "UNPAID"
		}

		fun haveStatementPaid(datas: List<DataCardStatements>?): Boolean {
			var havePaid = false
			if (datas?.isEmpty()!!) havePaid = true else {
				for (i in datas.indices){
					havePaid = datas[i].status == 2
				}
			}

			return havePaid
		}

		fun paidStatusNameVisible(): Boolean? {
			return  kickCustomerCardStatements !=null
		}

		fun isPaid():Boolean {
			return (cardStatus.isNullOrBlank() || cardStatus == "01")
		}

		fun summaryIsPaid():Boolean {
			return if (kickCustomerCardStatements.isNullOrEmpty()) false
			else !haveStatementPaid(kickCustomerCardStatements)
		}

		fun dueAmount():String? {
			return  if (kickCustomerCardStatements.isNullOrEmpty()) CommonTools.formatDollarCurrency(0.0)
			else kickCustomerCardStatements[0].amountDue?.let { CommonTools.formatDollarCurrency(it) }
		}

		fun dueDate():String {
			val sdfDefault = SimpleDateFormat("yyyy-MM-dd", Locale.US)
			val sdf = SimpleDateFormat("dd-MM-yyyy", Locale.US)
			return if (dueDate.isNullOrEmpty()) "-"
			else sdf.format(sdfDefault.parse(dueDate)!!)
		}
		fun statementDate():String {
			val sdfDefault = SimpleDateFormat("yyyy-MM-dd", Locale.US)
			val sdf = SimpleDateFormat("dd-MM-yyyy", Locale.US)
			return if (statementDate.isNullOrEmpty()) "-"
			else sdf.format(sdfDefault.parse(statementDate)!!)
		}

		fun visiblestatus():Boolean {
			return kickCustomerCardStatements.isNullOrEmpty()
		}

		@Parcelize
		data class DataCardStatements(
			@field:SerializedName("id") val id: Int? = null,
			@field:SerializedName("amountDue") val amountDue: Double? = null,
			@field:SerializedName("paidAmount") val paidAmount: Double? = null,
			@field:SerializedName("kick_customer_cards_id") val kickCustomerCardsId: String? = null,
			@field:SerializedName("statementDate") val statementDate: String? = null,
			@field:SerializedName("dueDate") val dueDate: String? = null,
			@field:SerializedName("status") val status: Int? = null
		) : Parcelable{
			fun amount():String? {
				return amountDue?.toString() ?: "0"
			}
			fun amountString():String? {
				return if (amountDue != null) CommonTools.formatDollarCurrency(amountDue) else "-"
			}
		}

		@Parcelize
		data class DataCardTransactions(
			@field:SerializedName("transactionCurrency") val transactionCurrency: String? = null,
			@field:SerializedName("transactionAmount") val transactionAmount: Double? = null,
			@field:SerializedName("transactionDescription") val transactionDescription: String? = null,
			@field:SerializedName("id") val id: Int? = null,
			@field:SerializedName("kick_customer_cards_id") val kickCustomerCardsId: String? = null,
			@field:SerializedName("transactionCode") val transactionCode: String? = null,
			@field:SerializedName("transactionDate") val transactionDate: String? = null,
			@field:SerializedName("status") val status: Int? = null
		) : Parcelable

		@Parcelize
		data class KickBanks(
			@field:SerializedName("bank_code") val bankCode: String? = null,
			@field:SerializedName("bank_is_delete") val bankIsDelete: Int? = null,
			@field:SerializedName("bank_image") val bankImage: String? = null,
			@field:SerializedName("bank_status") val bankStatus: Int? = null,
			@field:SerializedName("bank_slug") val bankSlug: String? = null,
			@field:SerializedName("bank_card_color") val bankCardColor: String? = null,
			@field:SerializedName("bank_name") val bankName: String? = null,
			@field:SerializedName("bank_updated_date") val bankUpdatedDate: String? = null,
			@field:SerializedName("bank_deleted_date") val bankDeletedDate: String? = null,
			@field:SerializedName("id") val id: Int? = null,
			@field:SerializedName("bank_created_date") val bankCreatedDate: String? = null,
			@field:SerializedName("bank_card_available") val bankCardAvailable: Int? = null
		) : Parcelable {
			fun bankIcon():String {
				return if (bankImage.isNullOrBlank()){
					BuildConfig.BASE_URL_IMAGE_PLACEHOLDER
				}else {
					"${BuildConfig.BASE_URL_IMAGE_BANK}$bankImage"
				}
			}

		}

		@Parcelize
		data class Users(
			@field:SerializedName("party_token") val partyToken: String? = null,
			@field:SerializedName("account_token") val accountToken: String? = null,
			@field:SerializedName("email_verified_at") val emailVerifiedAt: String? = null,
			@field:SerializedName("phone") val phone: String? = null,
			@field:SerializedName("party_id") val partyId: String? = null,
			@field:SerializedName("name") val name: String? = null,
			@field:SerializedName("id") val id: Int? = null,
			@field:SerializedName("email") val email: String? = null,
			@field:SerializedName("stripe_id") val stripeId: String? = null,
			@field:SerializedName("username") val username: String? = null,
			@field:SerializedName("account_token_fb") val accountTokenFb: String? = null
		) : Parcelable
	}
}