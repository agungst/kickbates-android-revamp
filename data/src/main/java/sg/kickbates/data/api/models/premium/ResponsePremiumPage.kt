package sg.kickbates.data.api.models.premium


import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize
import sg.kickbates.data.api.models.common.Links
import sg.kickbates.data.api.models.common.Meta

@Parcelize
data class ResponsePremiumPage(

	@field:SerializedName("data") val data: List<DataItem>? = null,
	@field:SerializedName("meta") val meta: Meta? = null,
	@field:SerializedName("links") val links: Links? = null,
	@field:SerializedName("message") val message: String? = null,
	@field:SerializedName("status") val status: Int? = null
) : Parcelable {
	fun titleDec():String? {
		return if (data.isNullOrEmpty()){
			""
		} else {
			data[0].settingField
		}
	}
	fun dec():String? {
		return if (data.isNullOrEmpty()){
			""
		} else {
			data[0].settingContent
		}
	}

	fun titleDuration():String? {
		return if (data.isNullOrEmpty()){
			""
		} else {
			data[1].settingField
		}
	}
	fun duration():String? {
		return if (data.isNullOrEmpty()){
			""
		} else {
			data[1].settingContent
		}
	}

	fun titleCash():String? {
		return if (data.isNullOrEmpty()){
			""
		} else {
			data[2].settingField
		}
	}
	fun cash():String? {
		return if (data.isNullOrEmpty()){
			""
		} else {
			"$ ${data[2].settingContent}"
		}
	}
	fun titlePrice():String? {
		return if (data.isNullOrEmpty()){
			""
		} else {
			data[3].settingField
		}
	}
	fun price():String? {
		return if (data.isNullOrEmpty()){
			""
		} else {
			"$ ${data[3].settingContent}"
		}
	}


	@Parcelize
	data class DataItem(
		@field:SerializedName("setting_type") val settingType: Int? = null,
		@field:SerializedName("deleted_by") val deletedBy: Int? = null,
		@field:SerializedName("setting_key") val settingKey: String? = null,
		@field:SerializedName("created_by") val createdBy: Int? = null,
		@field:SerializedName("is_delete") val isDelete: Int? = null,
		@field:SerializedName("setting_content") val settingContent: String? = null,
		@field:SerializedName("setting_field") val settingField: String? = null,
		@field:SerializedName("setting_content_start_date") val settingContentStartDate: String? = null,
		@field:SerializedName("deleted_date") val deletedDate: String? = null,
		@field:SerializedName("display_in_list") val displayInList: Int? = null,
		@field:SerializedName("setting_content_end_date") val settingContentEndDate: String? = null,
		@field:SerializedName("updated_by") val updatedBy: Int? = null,
		@field:SerializedName("id") val id: Int? = null,
		@field:SerializedName("created_date") val createdDate: String? = null,
		@field:SerializedName("updated_date") val updatedDate: String? = null,
		@field:SerializedName("status") val status: Int? = null
	) : Parcelable
}