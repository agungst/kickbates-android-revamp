package sg.kickbates.data.api.models.common

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

/**
 * Created by simx on 23,November,2019
 */
@Parcelize
data class KickMerchant (
    @field:SerializedName("merchant_email") val merchantEmail: String? = null,
    @field:SerializedName("merchant_code") val merchantCode: String? = null,
    @field:SerializedName("merchant_gender") val merchantGender: String? = null,
    @field:SerializedName("merchant_device_type") val merchantDeviceType: Int? = null,
    @field:SerializedName("merchant_dob") val merchantDob: String? = null,
    @field:SerializedName("merchant_created_by") val merchantCreatedBy: Int? = null,
    @field:SerializedName("kick_merchant_category_id") val kickMerchantCategoryId: String? = null,
    @field:SerializedName("merchant_logo") val merchantLogo: String? = null,
    @field:SerializedName("merchant_device_id") val merchantDeviceId: String? = null,
    @field:SerializedName("merchant_service_charge_percentage") val merchantServiceChargePercentage: Int? = null,
    @field:SerializedName("merchant_is_featured") val merchantIsFeatured: Int? = null,
    @field:SerializedName("merchant_gst_percentage") val merchantGstPercentage: Int? = null,
    @field:SerializedName("merchant_designation") val merchantDesignation: String? = null,
    @field:SerializedName("merchant_forgot_count") val merchantForgotCount: Int? = null,
    @field:SerializedName("merchant_deleted_date") val merchantDeletedDate: String? = null,
    @field:SerializedName("merchant_updated_by") val merchantUpdatedBy: Int? = null,
    @field:SerializedName("merchant_fb_link") val merchantFbLink: String? = null,
    @field:SerializedName("merchant_forgot_link_date") val merchantForgotLinkDate: String? = null,
    @field:SerializedName("id") val id: Int? = null,
    @field:SerializedName("merchant_address") val merchantAddress: String? = null,
    @field:SerializedName("merchant_forgot_link") val merchantForgotLink: String? = null,
    @field:SerializedName("lat") val lat: String? = null,
    @field:SerializedName("merchant_is_delete") val merchantIsDelete: Int? = null,
    @field:SerializedName("merchant_is_new") val merchantIsNew: Int? = null,
    @field:SerializedName("lng") val lng: String? = null,
    @field:SerializedName("merchant_deleted_by") val merchantDeletedBy: Int? = null,
    @field:SerializedName("merchant_name") val merchantName: String? = null,
    @field:SerializedName("merchant_ig_link") val merchantIgLink: String? = null,
    @field:SerializedName("merchant_shop_country_code") val merchantShopCountryCode: String? = null,
    @field:SerializedName("merchant_shop_number") val merchantShopNumber: String? = null,
    @field:SerializedName("merchant_status") val merchantStatus: Int? = null,
    @field:SerializedName("merchant_password") val merchantPassword: String? = null,
    @field:SerializedName("merchant_contact_person") val merchantContactPerson: String? = null,
    @field:SerializedName("merchant_desc") val merchantDesc: String? = null,
    @field:SerializedName("merchant_contact") val merchantContact: String? = null,
    @field:SerializedName("merchant_created_date") val merchantCreatedDate: String? = null,
    @field:SerializedName("merchant_updated_date") val merchantUpdatedDate: String? = null,
    @field:SerializedName("merchant_country_code") val merchantCountryCode: String? = null,
    @field:SerializedName("merchant_operating_hour") val merchantOperatingHour: String? = null,
    @field:SerializedName("merchant_image") val merchantImage: String? = null,
    @field:SerializedName("merchant_token") val merchantToken: String? = null
) : Parcelable {
    fun name():String {
        return if (merchantName.isNullOrEmpty()) "-" else merchantName
    }
}