package sg.kickbates.data.api.models.card.token


import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class RetailDemographic(

	@field:SerializedName("residenceCountry")
	val residenceCountry: String? = null,

	@field:SerializedName("partyDoc")
	val partyDoc: List<PartyDocItem>? = null,

	@field:SerializedName("gender")
	val gender: String? = null,

	@field:SerializedName("nationality")
	val nationality: String? = null,

	@field:SerializedName("partyName")
	val partyName: PartyName? = null,

	@field:SerializedName("dateOfBirth")
	val dateOfBirth: String? = null,

	@field:SerializedName("ethnicGroup")
	val ethnicGroup: String? = null,

	@field:SerializedName("maritalStatus")
	val maritalStatus: String? = null
) : Parcelable {
	@Parcelize
	data class PartyName(

		@field:SerializedName("fullName")
		val fullName: String? = null,

		@field:SerializedName("alias")
		val alias: String? = null,

		@field:SerializedName("salutation")
		val salutation: String? = null
	) : Parcelable

	@Parcelize
	data class PartyDocItem(

		@field:SerializedName("docNumber")
		val docNumber: String? = null,

		@field:SerializedName("docType")
		val docType: String? = null
	) : Parcelable
}