package sg.kickbates.data.api.models.card.token


import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class ResponseAuthCardToken(

	@field:SerializedName("data")
	val data: Data? = null,

	@field:SerializedName("status")
	val status: Int? = null
) : Parcelable {
	@Parcelize
	data class Data(

		@field:SerializedName("party")
		val party: Party? = null,

		@field:SerializedName("token")
		val token: Token? = null
	) : Parcelable {
		@Parcelize
		data class Token(

			@field:SerializedName("access_token")
			val accessToken: String? = null,

			@field:SerializedName("refresh_token")
			val refreshToken: String? = null,

			@field:SerializedName("party_id")
			val partyId: String? = null,

			@field:SerializedName("expire_in")
			val expireIn: String? = null,

			@field:SerializedName("token_type")
			val tokenType: String? = null
		) : Parcelable
	}
}