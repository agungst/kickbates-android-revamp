package sg.kickbates.data.api.models.card


import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class KickCustomerEarning(
	@field:SerializedName("earn_cashback") val earnCashback: Double? = null,
	@field:SerializedName("earn_remark") val earnRemark: String? = null,
	@field:SerializedName("earn_kick") val earnKick: Double? = null,
	@field:SerializedName("users_id") val usersId: Int? = null,
	@field:SerializedName("id") val id: Int? = null,
	@field:SerializedName("kick_invoice_id") val kickInvoiceId: Int? = null,
	@field:SerializedName("earn_status") val earnStatus: Int? = null
) : Parcelable {
	fun cashBackAmount(): Double? {
		return earnCashback?.let { earnKick?.plus(it) }
	}
}