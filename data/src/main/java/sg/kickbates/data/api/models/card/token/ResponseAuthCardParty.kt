package sg.kickbates.data.api.models.card.token


import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class ResponseAuthCardParty(

	@field:SerializedName("data")
	val data: Data? = null,

	@field:SerializedName("status")
	val status: Int? = null
) : Parcelable {
	@Parcelize
	data class Data(

		@field:SerializedName("retailParty")
		val party: Party.RetailParty? = null
	) : Parcelable
}