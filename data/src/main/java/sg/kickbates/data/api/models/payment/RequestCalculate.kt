package sg.kickbates.data.api.models.payment

import com.google.gson.annotations.SerializedName

data class RequestCalculate(
	@field:SerializedName("promo_amount") var promoAmount: Double? = null,
	@field:SerializedName("pay_by_kick") var payByKick: Double? = null,
	@field:SerializedName("payable") var payable: Double? = null,
	@field:SerializedName("promo_percentage") var promoPercentage: Double? = null,
	@field:SerializedName("merchant_id") var merchantI: Int? = null
)