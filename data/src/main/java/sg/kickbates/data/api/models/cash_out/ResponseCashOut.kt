package sg.kickbates.data.api.models.cash_out


import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize
import sg.kickbates.data.api.models.common.CreatedAt
import sg.kickbates.data.api.models.common.KickBanks
@Parcelize
data class ResponseCashOut(
	@field:SerializedName("data") val data: Data? = null,
	@field:SerializedName("message") val message: String? = null,
	@field:SerializedName("status") val status: Int? = null
) : Parcelable {
	@Parcelize
	data class Data(
		@field:SerializedName("request_status") val requestStatus: Int? = null,
		@field:SerializedName("request_account_name") val requestAccountName: String? = null,
		@field:SerializedName("request_cash") val requestCash: Double? = null,
		@field:SerializedName("created_at") val createdAt: CreatedAt? = null,
		@field:SerializedName("users_id") val usersId: String? = null,
		@field:SerializedName("request_account_number") val requestAccountNumber: String? = null,
		@field:SerializedName("id") val id: Int? = null,
		@field:SerializedName("kick_banks") val kickBanks: KickBanks? = null
	) : Parcelable
}