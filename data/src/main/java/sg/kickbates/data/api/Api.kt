package sg.kickbates.data.api

import com.google.gson.FieldNamingPolicy
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.jakewharton.retrofit2.adapter.kotlin.coroutines.CoroutineCallAdapterFactory
import kotlinx.coroutines.Deferred
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.*
import sg.kickbates.data.api.models.*
import sg.kickbates.data.api.models.booking.RequestBooking
import sg.kickbates.data.api.models.booking.ResponseBooking
import sg.kickbates.data.api.models.card.*
import sg.kickbates.data.api.models.card.token.ResponseAuthCardParty
import sg.kickbates.data.api.models.card.token.ResponseAuthCardToken
import sg.kickbates.data.api.models.card.token.ResponseAuthCardTokenCiti
import sg.kickbates.data.api.models.cash_back.ResponseCashBackHistory
import sg.kickbates.data.api.models.cash_out.ResponseCashOut
import sg.kickbates.data.api.models.cash_out.ResponseCashOutHistory
import sg.kickbates.data.api.models.cms.ResponseCms
import sg.kickbates.data.api.models.notification.ResponseNotificationHistory
import sg.kickbates.data.api.models.payment.*
import sg.kickbates.data.api.models.premium.ResponsePremiumPage
import sg.kickbates.data.api.models.voucher.ResponseVouchers
import sg.kickbates.data.api.models.voucher.ResponseVouchersDetail
import java.util.concurrent.TimeUnit

/**
 * Created by simx on 21,September,2019
 */
interface Api {

    @Headers("Accept: application/json", "Content-type: application/json")
    @POST("v1/ng_user/register")
    fun registerFacebookAsync(@Body requestRegisterFacebook: RequestRegisterFacebook): Deferred<ResponseRegister>
    @Headers("Accept: application/json", "Content-type: application/json")

    @POST("v1/ng_user/register")
    fun registerGoogleAsync(@Body requestRegister: RequestRegisterGoogle): Deferred<ResponseRegister>


    @Headers("Accept: application/json", "Content-type: application/json")
    @POST("v1/auth/login_social")
    fun loginGoogleAsync(@Body requestLoginGoogle: RequestLoginGoogle): Deferred<ResponseLogin>

    @Headers("Accept: application/json", "Content-type: application/json")
    @POST("v1/auth/login_social")
    fun loginFbAsync(@Body requestLoginFacebook: RequestLoginFacebook): Deferred<ResponseLogin>

    @Headers("Accept: application/json", "Content-type: application/json")
    @POST("v1/payment/generate_invoice")
    fun updateUserAsyc(@Header("Authorization") token:String): Deferred<ResponseDeals>

    @Headers("Accept: application/json", "Content-type: application/json")
    @GET("v1/auth/me")
    fun meAsync(@Header("Authorization") token:String?): Deferred<ResponseMeToken>

    @Headers("Accept: application/json", "Content-type: application/json")
    @GET("v1/ng_user/show/{id}")
    fun meFromIdAsync(@Header("Authorization") token:String?,
                      @Path("id")id:Int?): Deferred<ResponseMe>

    @Headers("Accept: application/json", "Content-type: application/json")
    @POST("v1/ng_user/update/{id}")
    fun updateProfileAsync(@Header("Authorization") token:String?,
                           @Path("id")id:Int?,
                           @Body requestUpdateProfile: RequestUpdateProfile?): Deferred<ResponseMe>

    @Headers("Accept: application/json", "Content-type: application/json")
    @POST("v1/ng_user/update/{id}")
    fun updateFcmTokenAsync(@Header("Authorization") token:String?,
                           @Path("id")id:Int?,
                           @Body requestFcmToken: RequestFcmToken?): Deferred<ResponseMe>

    @Headers("Accept: application/json", "Content-type: application/json")
    @POST("v1/ng_user/update/{id}")
    fun updateSubscribeAsync(@Header("Authorization") token:String?,
                           @Path("id")id:Int?,
                           @Query("is_subscribe_premium") isSubscribePremium:Boolean?): Deferred<ResponseMe>


    @Headers("Accept: application/json", "Content-type: application/json")
    @GET("v1/kick_deals")
    fun dealsAsync(@Header("Authorization") token:String?): Deferred<ResponseDeals>

    @Headers("Accept: application/json", "Content-type: application/json")
    @GET("v1/kick_deals")
    fun dealsAsync(
        @Header("Authorization") token:String?,
        @Query("page") page:Int?,
        @Query("lat") lat:String?,
        @Query("lng") lng:String?,
        @Query("filter[deal_name]") name:String?,
        @Query("filter[deal_code]") code:String?
    ): Deferred<ResponseDeals>


    @Headers("Accept: application/json", "Content-type: application/json")
    @GET("v1/kick_deals")
    fun dealsByCodeAsync(
        @Header("Authorization") token:String?,
        @Query("filter[merchant_code]") code:String?
    ): Deferred<ResponseDeals>

    @Headers("Accept: application/json", "Content-type: application/json")
    @GET("v1/kick_deals?filter_adv=1&filter[eq][is_banner]=1")
    fun dealsBannerAsync(
        @Header("Authorization") token:String?
    ): Deferred<ResponseDeals>

    @Headers("Accept: application/json", "Content-type: application/json")
    @GET("v1/kick_deals?filter_adv=true&filter[eq][deal_merchant_id]=13")
    fun dealsByMerchantIdAsync(
        @Header("Authorization") token:String?,
        @Query("filter[eq][deal_merchant_id]") merchantId:Int?
    ): Deferred<ResponseDeals>

    @Headers("Accept: application/json", "Content-type: application/json")
    @GET("v1/kick_merchant")
    fun merchantsAsync(@Header("Authorization") token:String?): Deferred<ResponseMerchants>

    @Headers("Accept: application/json", "Content-type: application/json")
    @POST("v1/kick_booking/book_now")
    fun bookMerchantsAsync(@Header("Authorization") token:String?, @Body requestBooking: RequestBooking?): Deferred<ResponseBooking>


    @Headers("Accept: application/json", "Content-type: application/json")
    @GET("v1/kick_merchant")
    fun merchantsAsync(
        @Header("Authorization") token:String?,
        @Query("page") page:Int?,
        @Query("lat") lat:String?,
        @Query("lng") lng:String?,
        @Query("filter[merchant_name]") name:String?,
        @Query("filter[merchant_code]") code:String?
    ): Deferred<ResponseMerchants>
    @Headers("Accept: application/json", "Content-type: application/json")
    @GET("v1/kick_merchant")
    fun merchantsByCodeAsync(
        @Header("Authorization") token:String?,
        @Query("filter[merchant_code]") code:String?
    ): Deferred<ResponseMerchants>


    /**
     * Banners
     */
    @Headers("Accept: application/json", "Content-type: application/json")
    @GET("v1/kick_banner")
    fun bannersAsync(@Header("Authorization") token:String?): Deferred<ResponseBanners>


    /**
     * Cards
     */

    @Headers("Accept: application/json", "Content-type: application/json")
    @GET("v1/kick_banks?filter[bank_card_available]=1")
    fun cardsAvailableAsync(@Header("Authorization") token:String?): Deferred<ResponseCardAvaiable>

    @Headers("Accept: application/json", "Content-type: application/json")
    @GET("v1/kick_banks?filter_adv=1&filter[eq][bank_is_delete]=0")
    fun allBankAvailableAsync(@Header("Authorization") token:String?): Deferred<ResponseCardAvaiable>

    @Headers("Accept: application/json", "Content-type: application/json")
    @GET("v1/kick_banks?filter[bank_card_available]=1")
    fun cardsHistoryAsync(@Header("Authorization") token:String?): Deferred<ResponseCardAvaiable>



    /**
     * Cash Back
     */
    @Headers("Accept: application/json", "Content-type: application/json")
    @GET("v1/kick_customer_earning/summary")
    fun cashBackSummaryAsync(@Header("Authorization") token:String?): Deferred<ResponseCashbackSummary>

    @Headers("Accept: application/json", "Content-type: application/json")
    @GET("v1/kick_customer_earning?filter_adv=1")
    fun cashBackHistoryAsync(@Header("Authorization") token:String?,
                             @Query("filter[eq][users_id]") userId: Int?,
                             @Query("page") page: Int?
    ): Deferred<ResponseCashBackHistory>

    @Headers("Accept: application/json", "Content-type: application/json")
    @GET("v1/kick_request_cashout?filter_adv=1")
    fun cashOutHistoryAsync(
        @Header("Authorization") token:String?,
        @Query("filter[eq][users_id]")userId: Int?,
        @Query("page") page: Int?
    ): Deferred<ResponseCashOutHistory>

    @Headers("Accept: application/json", "Content-type: application/json")
    @POST("v1/kick_request_cashout/store")
    fun cashOutStoreAsync(
        @Header("Authorization") token:String?,
        @Body requestCashOut: RequestCashOut?
    ): Deferred<ResponseCashOut>

    /**
     * Auth Card
     */

    @Headers("Accept: application/json", "Content-type: application/json")
    @GET("v1/kick_customer_cards/auth2f")
    fun auth2fCardAsync(
        @Header("Authorization") token:String?,
        @Query("code") code:String?,
        @Query("issuer") issuer:String?
    ): Deferred<ResponseAuthCard>


    @Headers("Accept: application/json", "Content-type: application/json")
    @GET("v1/kick_customer_cards/auth")
    fun authCardAsync(
        @Header("Authorization") token:String?,
        @Query("issuer") issuer:String?
    ): Deferred<ResponseAuthCard>

    /**
     * Get Token Card
     */
    @Headers("Accept: application/json", "Content-type: application/json")
    @GET("v1/kick_customer_cards/token")
    fun tokenCardAsync(
        @Header("Authorization") bearerToken:String?,
        @Query("issuer") issuer:String?,
        @Query("code") code:String?
    ): Deferred<ResponseAuthCardToken>

    @Headers("Accept: application/json", "Content-type: application/json")
    @GET("v1/kick_customer_cards/token")
    fun tokenCardCitiAsync(
        @Header("Authorization") bearerToken:String?,
        @Query("issuer") issuer:String?,
        @Query("code") code:String?
    ): Deferred<ResponseAuthCardTokenCiti>

    @Headers("Accept: application/json", "Content-type: application/json")
    @GET("v1/kick_customer_cards/party")
    fun partyCardAsync(
        @Header("Authorization") bearerToken:String?,
        @Query("issuer") issuer:String?,
        @Query("party_id") partyId:String?,
        @Query("accessToken") accessToken:String?
    ): Deferred<ResponseAuthCardParty>

    @Headers("Accept: application/json", "Content-type: application/json")
    @GET("v1/kick_customer_cards/cards")
    fun addCardAsync(
        @Header("Authorization") bearerToken:String?,
        @Query("issuer") issuer:String?,
        @Query("party_token") partyToken:String?,
        @Query("token") token:String?
    ): Deferred<ResponseAddCard>

    @Headers("Accept: application/json", "Content-type: application/json")
    @GET("v1/kick_customer_cards/cards")
    fun addCardAsync(
        @Header("Authorization") token:String?,
        @Query("issuer") issuer:String?,
        @Query("session_token") sessionToken:String?
    ): Deferred<ResponseAddCard>

    @FormUrlEncoded
    @Headers("Accept: application/json", "Content-type: application/x-www-form-urlencoded")
    @POST("v1/kick_payment_method/add_card")
    fun addPaymentMethodAsync(
        @Header("Authorization") bearer:String?,
        @Field("token") token:String?
    ): Deferred<ResponsePaymentMethodAddCard>

    @Headers("Accept: application/json", "Content-type: application/json")
    @GET("v1/kick_customer_cards")
    fun customerCardsAsync(
        @Header("Authorization") token:String?,
        @Query("filter[users_id]") userId:Int?
    ): Deferred<ResponseCustomerCards>

    @FormUrlEncoded
    @Headers("Accept: application/json", "Content-type: application/x-www-form-urlencoded")
    @POST("v1/tokens")
    fun stripeTokenAsync(
        @Header("Authorization") token:String?,
        @Field("card[number]") number:String?,
        @Field("card[exp_month]") exp_month:Int?,
        @Field("card[exp_year]") exp_year:Int?,
        @Field("card[cvc]") cvc:String?
    ): Deferred<ResponseCreateTokenStripe>

    /**
     * Notification
     */
    @Headers("Accept: application/json", "Content-type: application/json")
    @GET("v1/kick_notification?filter_adv=1")
    fun notificationsAsync(
        @Header("Authorization") token:String?,
        @Query("filter[eq][users_id]") userId:Int?
    ): Deferred<ResponseNotificationHistory>

    /**
     * Voucher
     */

    @Headers("Accept: application/json", "Content-type: application/json")
    @GET("v1/kick_promocode?filter_adv=1")
    fun vouchersAsync(
        @Header("Authorization") token:String?,
        @Query("filter[eq][users_id]") userId:Int?,
        @Query("page") page:Int?

    ): Deferred<ResponseVouchers>

    @Headers("Accept: application/json", "Content-type: application/json")
    @GET("v1/kick_promocode/code")
    fun vouchersDetailsAsync(
        @Header("Authorization") token:String?,
        @Query("code") code:String?
    ): Deferred<ResponseVouchersDetail>

    /**
     * Payment
     */

    @Headers("Accept: application/json", "Content-type: application/json")
    @GET("v1/kick_invoice/?filter_adv=1&filter[eq][invoice_status]=1")
    fun paymentHistoryAsync(
        @Header("Authorization") token:String?,
        @Query("filter[eq][users_id]") userId: Int?,
        @Query("page") page: Int?
    ): Deferred<ResponseTransactionsHistory>

    @Headers("Accept: application/json", "Content-type: application/json")
    @GET("v1/kick_payment_method")
    fun paymentChannelAsync(
        @Header("Authorization") token:String?
    ): Deferred<ResponsePaymentChannel>

    @Headers("Accept: application/json", "Content-type: application/json")
    @GET("v1/kick_payment_method/cards")
    fun paymentChannelCardAsync(
        @Header("Authorization") token:String?
    ): Deferred<ResponsePaymentChannelCard>

    @Headers("Accept: application/json", "Content-type: application/json")
    @POST("v1/kick_invoice/generate")
    fun generateInvoiceAsync(
        @Header("Authorization") token:String?,
        @Body requestInvoiceCard: RequestInvoiceCard
    ): Deferred<ResponseGenerateInvoice>
    @Headers("Accept: application/json", "Content-type: application/json")
    @POST("v1/kick_invoice/generate")
    fun generateInvoiceAsync(
        @Header("Authorization") token:String?,
        @Body requestInvoiceDeal: RequestInvoiceDeal
    ): Deferred<ResponseGenerateInvoice>

    @Headers("Accept: application/json", "Content-type: application/json")
    @POST("v1/kick_invoice/generate")
    fun generateInvoiceAsync(
        @Header("Authorization") token:String?,
        @Body requestInvoiceMerchant: RequestInvoiceMerchant
    ): Deferred<ResponseGenerateInvoice>

    @Headers("Accept: application/json", "Content-type: application/json")
    @POST("v1/kick_invoice/generate")
    fun generateInvoiceAsync(
        @Header("Authorization") token:String?,
        @Body requestInvoicePremium: RequestInvoicePremium
    ): Deferred<ResponseGenerateInvoice>

    @FormUrlEncoded
    @Headers("Accept: application/json", "Content-type: application/x-www-form-urlencoded")
    @POST("v1/kick_payment_method/charge_card")
    fun chargeCardAsync(
        @Header("Authorization") token:String?,
        @Field("payment_card_id") payment_card_id:String?,
        @Field("kick_invoice_id") kick_invoice_id:Int?
    ): Deferred<ResponseChargeCard>

    @Headers("Accept: application/json", "Content-type: application/x-www-form-urlencoded")
    @GET("v1/kick_payment_method/accounts")
    fun accountCardAsync(
        @Header("Authorization") token:String?,
        @Query("issuer") issuer:String?,
        @Query("party_token") party_token:String?,
        @Query("session_token") session_token:String?
    ): Deferred<ResponseAccountCard>


    @FormUrlEncoded
    @Headers("Accept: application/json", "Content-type: application/x-www-form-urlencoded")
    @POST("v1/kick_payment_method/charge_card")
    fun payCardDealAsync(
        @Header("Authorization") token:String?,
        @Field("payment_card_id") payment_card_id:String?,
        @Field("kick_invoice_id") kick_invoice_id:Int?
    ): Deferred<ResponseChargeCard>


    @Headers("Accept: application/json","Content-type: application/json")
    @POST("v1/kick_payment_method/paynow")
    fun payNowAsync(
        @Header("Authorization") token:String?,
        @Body requestPayNow: RequestPayNow?
    ): Deferred<ResponseChargeCard>

    @Headers("Accept: application/json","Content-type: application/json")
    @POST("/api/v1/kick_invoice/preview_amount")
    fun calculateAsync(
        @Header("Authorization") token:String?,
        @Body requestCalculate: RequestCalculate?
    ): Deferred<ResponseCalculatePromo>


    @FormUrlEncoded
    @Headers("Accept: application/json", "Content-type: application/x-www-form-urlencoded")
    @POST("v1/kick_payment_method/charge_card")
    fun payCardMerchantAsync(
        @Header("Authorization") token:String?,
        @Field("payment_card_id") payment_card_id:String?,
        @Field("kick_invoice_id") kick_invoice_id:Int?
    ): Deferred<ResponseChargeCard>


    @Headers("Accept: application/json", "Content-type: application/json")
    @POST("v1/kick_customer_earning/store")
    fun convertCashAsync(
        @Header("Authorization") token:String?,
        @Query("users_id") usersId:Int?,
        @Query("earn_kick") earnKick:Double?,
        @Query("earn_cashback") earnCashBack:Double?,
        @Query("earn_status") earnStatus:Int?
    ): Deferred<ResponseCorverKick>

    @Headers("Accept: application/json", "Content-type: application/json")
    @POST("v1/kick_setting?filter[setting_key]=premium")
    fun premiumInfoAsync(
        @Header("Authorization") token:String?
    ): Deferred<ResponsePremiumPage>


    /**
     * CMS
     */

    @Headers("Accept: application/json", "Content-type: application/json")
    @GET("v1/kick_cms")
    fun cmsAsync(
        @Header("Authorization") token:String?,
        @Query("filter[cms_key]") key:String?
    ): Deferred<ResponseCms>


    object Factory {
        fun create(base_url: String): Api {
            return getRetrofitConfig(base_url).create(Api::class.java)
        }

        /**
         * Config GSON
         * @return
         */
        private val gson: Gson
            get() {
                val gsonBuilder = GsonBuilder()
                gsonBuilder.setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES)
                return gsonBuilder.create()
            }


        private fun getRetrofitConfig(base_url: String): Retrofit {
            return Retrofit.Builder()
                .baseUrl(base_url)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .addCallAdapterFactory(CoroutineCallAdapterFactory())
                .client(client())
                .build()
        }


        /**
         * Config OkhttpClient and interceptions
         * @return
         */
        private fun client(): OkHttpClient {
            return OkHttpClient.Builder()
                .readTimeout(60, TimeUnit.SECONDS)
                .connectTimeout(60, TimeUnit.SECONDS)
                .addInterceptor(
                    HttpLoggingInterceptor()
                        .setLevel(HttpLoggingInterceptor.Level.BODY)
                )

                .build()
        }

    }
}