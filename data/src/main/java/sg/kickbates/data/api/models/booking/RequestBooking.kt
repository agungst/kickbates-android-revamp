package sg.kickbates.data.api.models.booking

import android.os.Parcelable

import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class RequestBooking(

	@field:SerializedName("date")
	var date: String? = null,

	@field:SerializedName("no_of_pax")
	var noOfPax: Int? = null,

	@field:SerializedName("users_id")
	var usersId: Int? = null,

	@field:SerializedName("cellphone")
	var cellphone: String? = null,

	@field:SerializedName("time")
	var time: String? = null,

	@field:SerializedName("kick_merchant_id")
	var kickMerchantId: Int? = null
) : Parcelable