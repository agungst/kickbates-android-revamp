package sg.kickbates.data.api.models.cash_out


import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize
import sg.kickbates.data.api.models.common.CreatedAt
import sg.kickbates.data.api.models.common.KickBanks
import sg.kickbates.data.api.models.common.Links
import sg.kickbates.data.api.models.common.Meta
import sg.kickbates.data.helper.CommonTools

@Parcelize
data class ResponseCashOutHistory(
	@field:SerializedName("data") val data: List<DataItem>? = null,
	@field:SerializedName("meta") val meta: Meta? = null,
	@field:SerializedName("links") val links: Links? = null,
	@field:SerializedName("message") val message: String? = null,
	@field:SerializedName("status") val status: Int? = null
) : Parcelable {
	@Parcelize
	data class DataItem(
		@field:SerializedName("request_status") val requestStatus: Int? = null,
		@field:SerializedName("request_account_name") val requestAccountName: String? = null,
		@field:SerializedName("request_cash") val requestCash: Double? = null,
		@field:SerializedName("users_id") val usersId: Int? = null,
		@field:SerializedName("request_account_number") val requestAccountNumber: String? = null,
		@field:SerializedName("id") val id: Int? = null,
		@field:SerializedName("created_at") val createdAt: CreatedAt? = null,
		@field:SerializedName("kick_banks") val kickBanks: KickBanks? = null
	) : Parcelable {
		fun status():String? {
			return when (requestStatus) {
				0 -> "Processing"
				1 -> "Approved"
				2 -> "Declined"
				else -> "Undefine"
			}
		}
		fun amount():String {
			return requestCash?.let { CommonTools.formatDollarCurrency(it) } ?: "-"
		}
	}
}