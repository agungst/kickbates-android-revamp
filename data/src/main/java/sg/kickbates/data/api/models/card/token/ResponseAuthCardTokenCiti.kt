package sg.kickbates.data.api.models.card.token


import com.google.gson.annotations.SerializedName

data class ResponseAuthCardTokenCiti(

	@field:SerializedName("data")
	val data: String? = null,

	@field:SerializedName("status")
	val status: Int? = null
)