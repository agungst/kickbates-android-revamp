package sg.kickbates.data.api.models.voucher


import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize
import sg.kickbates.data.BuildConfig
import sg.kickbates.data.api.models.common.Links
import sg.kickbates.data.api.models.common.Meta

@Parcelize
data class ResponseVouchers(

	@field:SerializedName("data") val data: List<DataItem>? = null,
	@field:SerializedName("meta") val meta: Meta? = null,
	@field:SerializedName("links") val links: Links? = null,
	@field:SerializedName("message") val message: String? = null,
	@field:SerializedName("status") val status: Int? = null
) : Parcelable {
	@Parcelize
	data class DataItem(
		@field:SerializedName("image") val image: String? = null,
		@field:SerializedName("promocode_merchant_id") val promocodeMerchantId: Int? = null,
		@field:SerializedName("product_status") val productStatus: Int? = null,
		@field:SerializedName("kick_invoice_id") val kickInvoiceId: Int? = null,
		@field:SerializedName("promocode_type") val promocodeType: Int? = null,
		@field:SerializedName("promocode_benifit") val promoCodeBenefit: Double? = null,
		@field:SerializedName("promocode_start_date") val promocodeStartDate: String? = null,
		@field:SerializedName("promocode_end_date") val promocodeEndDate: String? = null,
		@field:SerializedName("promocode_usage_limit") val promocodeUsageLimit: Int? = null,
		@field:SerializedName("users_id") val usersId: Int? = null,
		@field:SerializedName("id") var id: Int? = null,
		@field:SerializedName("promocode_code") val promocodeCode: String? = null,
		@field:SerializedName("promocode_name") val promocodeName: String? = null,
		@field:SerializedName("promocode_price") val promocodePrice: String? = null,
		@field:SerializedName("promocode_desc") val promocodeDesc: String? = null,
		@field:SerializedName("promocode_exclude_merchant") val promocodeExcludeMerchant: String? = null
	) : Parcelable {

		fun all():Boolean {
			return promocodeMerchantId == null
		}
		fun urlImage():String {
			return "${BuildConfig.BASE_URL_IMAGE_DEAL}$image"
		}
		fun promoName():String? {
			return "$promocodeCode - $promocodeDesc"
		}

		fun promoAmount(): Double? {
			return if (promocodeType == 2) promoCodeBenefit else 0.0
		}

		fun promoPercentage(): Double? {
			return if (promocodeType == 1) (promoCodeBenefit?.div(100)) else 0.0
		}
	}

}