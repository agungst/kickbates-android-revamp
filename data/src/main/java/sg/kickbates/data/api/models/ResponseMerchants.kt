package sg.kickbates.data.api.models


import android.os.Parcelable
import android.text.Spanned
import androidx.core.text.HtmlCompat
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize
import sg.kickbates.data.BuildConfig
import sg.kickbates.data.api.models.common.KickCategory
import sg.kickbates.data.api.models.common.KickTag
import sg.kickbates.data.api.models.common.Links
import sg.kickbates.data.api.models.common.Meta

@Parcelize
data class ResponseMerchants(
	@field:SerializedName("data") val data: List<DataItem>? = null,
	@field:SerializedName("meta") val meta: Meta? = null,
	@field:SerializedName("links") val links: Links? = null,
	@field:SerializedName("message") val message: String? = null,
	@field:SerializedName("status") val status: Int? = null
) : Parcelable {
	@Parcelize
	data class DataItem(
		@field:SerializedName("merchant_email") val merchantEmail: String? = null,
		@field:SerializedName("merchant_code") val merchantCode: String? = null,
		@field:SerializedName("distance") val distance: Double? = null,
		@field:SerializedName("kick_merchant_category_id") val kickMerchantCategoryId: Int? = null,
		@field:SerializedName("merchant_logo") val merchantLogo: String? = null,
		@field:SerializedName("merchant_is_featured") val merchantIsFeatured: Int? = null,
		@field:SerializedName("merchant_designation") val merchantDesignation: String? = null,
		@field:SerializedName("merchant_deleted_date") val merchantDeletedDate: String? = null,
		@field:SerializedName("merchant_fb_link") val merchantFbLink: String? = null,
		@field:SerializedName("id") val id: Int? = null,
		@field:SerializedName("merchant_address") val merchantAddress: String? = null,
		@field:SerializedName("lat") val lat: Double? = null,
		@field:SerializedName("merchant_is_delete") val merchantIsDelete: Int? = null,
		@field:SerializedName("merchant_is_new") val merchantIsNew: Int? = null,
		@field:SerializedName("lng") val lng: Double? = null,
		@field:SerializedName("merchant_name") val merchantName: String? = null,
		@field:SerializedName("merchant_ig_link") val merchantIgLink: String? = null,
		@field:SerializedName("merchant_shop_country_code") val merchantShopCountryCode: String? = null,
		@field:SerializedName("merchant_shop_number") val merchantShopNumber: String? = null,
		@field:SerializedName("merchant_status") val merchantStatus: Int? = null,
		@field:SerializedName("merchant_has_booking") val merchantHasBooking: Int? = null,
		@field:SerializedName("merchant_contact_person") val merchantContactPerson: String? = null,
		@field:SerializedName("merchant_contact") val merchantContact: String? = null,
		@field:SerializedName("merchant_desc") val merchantDesc: String? = null,
		@field:SerializedName("merchant_created_date") val merchantCreatedDate: String? = null,
		@field:SerializedName("merchant_updated_date") val merchantUpdatedDate: String? = null,
		@field:SerializedName("merchant_country_code") val merchantCountryCode: String? = null,
		@field:SerializedName("merchant_operating_hour") val merchantOperatingHour: String? = null,
		@field:SerializedName("kick_merchant_tag") val kickMerchantTag: List<KickTag>? = null,
		@field:SerializedName("kick_merchant_category") val kickMerchantCategory: KickCategory? = null,
		@field:SerializedName("merchant_image") val merchantImage: String? = null

	) : Parcelable {
		fun image():String {
			return  if (merchantImage.isNullOrBlank()){
				BuildConfig.BASE_URL_IMAGE_PLACEHOLDER
			}else {
				"${BuildConfig.BASE_URL_IMAGE_MERCHANT}${this.merchantImage}"
			}
		}
		fun cat():String? {
			return  if (kickMerchantCategory == null){
				"-"
			}else {
				kickMerchantCategory.name
			}
		}

		fun sortTitle():String{
			return if (!merchantName.isNullOrEmpty() )  cutTitle(merchantName) else merchantName.toString()
		}

		private fun cutTitle(value:String): String {
			return if (value.length <= 30) value else "${value.subSequence(0, 30)}..."
		}

		fun phone():String {
			return "${phoneCountryCode()}-$merchantContact"
		}
		private fun phoneCountryCode():String {
			return if (merchantCountryCode.isNullOrEmpty()) " " else "+$merchantCountryCode"
		}

		fun desc(): Spanned? {
			return merchantDesc?.let { HtmlCompat.fromHtml(it, HtmlCompat.FROM_HTML_MODE_COMPACT) }
		}

		fun notBooking():Boolean {
			return if (merchantHasBooking == null) false else merchantHasBooking == 1
		}
	}

}