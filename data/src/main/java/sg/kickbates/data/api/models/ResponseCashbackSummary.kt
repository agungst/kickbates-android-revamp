package sg.kickbates.data.api.models


import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class ResponseCashbackSummary(
	@field:SerializedName("data") val data: Data? = null,
	@field:SerializedName("status") val status: Int? = null
) : Parcelable {
	@Parcelize
	data class Data(
		@field:SerializedName("kick_balance") val kickBalance: Double? = null,
		@field:SerializedName("cash_processed") val cashProcessed: Double? = null,
		@field:SerializedName("cash_processing") val cashProcessing: Double? = null,
		@field:SerializedName("kick_spent") val kickSpent: Double? = null,
		@field:SerializedName("cash_pending") val cashPending: Double? = null,
		@field:SerializedName("cash_available") var cashAvailable: Double? = null
	) : Parcelable
}