package sg.kickbates.data.api.models


import com.google.gson.annotations.SerializedName


data class ResponseLogin(

	@field:SerializedName("access_token") val accessToken: String? = null,
	@field:SerializedName("message") val message: String? = null,
	@field:SerializedName("status") val status: Int? = null
) {
	fun token():String? {
		return "Bearer $accessToken"
	}
}