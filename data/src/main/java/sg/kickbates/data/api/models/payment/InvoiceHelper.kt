package sg.kickbates.data.api.models.payment

import sg.kickbates.data.helper.ProductType

/**
 * Created by simx on 23,November,2019
 */
object InvoiceHelper {
    fun generateInvoice(type: ProductType?, id:Int?, payable:Double?, payByKick:Double?, promoCodeId:Int?, name:String?, image:String?):Any{
        return when(type) {
            ProductType.DEALS -> RequestInvoiceDeal(name, image,id,payable, payByKick, promoCodeId)
            ProductType.MERCHANT -> RequestInvoiceMerchant(name, image,id,payable, payByKick, promoCodeId)
            ProductType.CARD -> RequestInvoiceCard(name, image,id,payable, payByKick, promoCodeId)
            else -> {}
        }
    }
}