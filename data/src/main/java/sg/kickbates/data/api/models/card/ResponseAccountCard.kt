package sg.kickbates.data.api.models.card


import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class ResponseAccountCard(
	@field:SerializedName("data") val data: List<DataItem>? = null,
	@field:SerializedName("status") val status: Int? = null
) : Parcelable {
	@Parcelize
	data class DataItem(
		@field:SerializedName("accountName") val accountName: String? = null,
		@field:SerializedName("accountNo") val accountNo: String? = null,
		@field:SerializedName("id") val id: String? = null
	) : Parcelable
}