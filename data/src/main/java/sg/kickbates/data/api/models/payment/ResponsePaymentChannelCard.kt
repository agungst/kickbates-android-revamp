package sg.kickbates.data.api.models.payment


import com.google.gson.annotations.SerializedName
import sg.kickbates.data.BuildConfig


data class ResponsePaymentChannelCard(

	@field:SerializedName("data")
	val data: List<DataItem>? = null,

	@field:SerializedName("status")
	val status: Int? = null
){
	data class DataItem(

		@field:SerializedName("address_zip_check")
		val addressZipCheck: String? = null,

		@field:SerializedName("country")
		val country: String? = null,

		@field:SerializedName("last4")
		val last4: String? = null,

		@field:SerializedName("funding")
		val funding: String? = null,

		@field:SerializedName("metadata")
		val metadata: List<Any?>? = null,

		@field:SerializedName("address_country")
		val addressCountry: String? = null,

		@field:SerializedName("address_state")
		val addressState: String? = null,

		@field:SerializedName("exp_month")
		val expMonth: Int? = null,

		@field:SerializedName("exp_year")
		val expYear: Int? = null,

		@field:SerializedName("address_city")
		val addressCity: String? = null,

		@field:SerializedName("tokenization_method")
		val tokenizationMethod: String? = null,

		@field:SerializedName("cvc_check")
		val cvcCheck: String? = null,

		@field:SerializedName("address_line2")
		val addressLine2: String? = null,

		@field:SerializedName("address_line1")
		val addressLine1: String? = null,

		@field:SerializedName("fingerprint")
		val fingerprint: String? = null,

		@field:SerializedName("name")
		val name: String? = null,

		@field:SerializedName("id")
		val id: String? = null,

		@field:SerializedName("address_line1_check")
		val addressLine1Check: String? = null,

		@field:SerializedName("address_zip")
		val addressZip: String? = null,

		@field:SerializedName("dynamic_last4")
		val dynamicLast4: String? = null,

		@field:SerializedName("brand")
		val brand: String? = null,

		@field:SerializedName("object")
		val jsonObject: String? = null,

		@field:SerializedName("customer")
		val customer: String? = null
	){
		fun iconChannel():String? {
			//TODO Casting bran logo channel credit card
			return if (brand.isNullOrBlank()){
				BuildConfig.BASE_URL_IMAGE_PLACEHOLDER
			} else {
				"${BuildConfig.BASE_URL_IMAGE_BANK}$brand"
			}
		}
	}
}