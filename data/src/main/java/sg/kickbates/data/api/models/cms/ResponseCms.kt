package sg.kickbates.data.api.models.cms


import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class ResponseCms(
	@field:SerializedName("data") val data: List<DataItem>? = null,
	@field:SerializedName("message") val message: String? = null,
	@field:SerializedName("status") val status: Int? = null
) : Parcelable {
	fun title():String? {
		return if (data.isNullOrEmpty()) "" else {
			if (data[0].cmsStatus == 1){
				data[0].cmsTitle
			}else ""
		}
	}
	fun content():String? {
		return if (data.isNullOrEmpty()) "" else {
			if (data[0].cmsStatus == 1){
				data[0].cmsContent
			}else ""
		}
	}
	@Parcelize
	data class DataItem(
		@field:SerializedName("cms_key") val cmsKey: String? = null,
		@field:SerializedName("cms_title") val cmsTitle: String? = null,
		@field:SerializedName("cms_status") val cmsStatus: Int? = null,
		@field:SerializedName("cms_content") val cmsContent: String? = null,
		@field:SerializedName("id") val id: Int? = null
	) : Parcelable
}