package sg.kickbates.data.api.models.payment

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class ResponseCalculatePromo(
	@field:SerializedName("data") val data: Data? = null,
	@field:SerializedName("status") val status: Int? = null
) : Parcelable {
	@Parcelize
	data class Data(
		@field:SerializedName("gst_amount") val gstAmount: Double? = null,
		@field:SerializedName("input_pay_by_kick") val inputPayByKick: Double? = null,
		@field:SerializedName("input_payable") val inputPayable: Double? = null,
		@field:SerializedName("payable_amount") val payableAmount: Double? = null,
		@field:SerializedName("input_promo_amount") val inputPromoAmount: Double? = null,
		@field:SerializedName("input_promo_percentage") val inputPromoPercentage: Double? = null
	) : Parcelable
}