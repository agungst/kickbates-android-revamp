package sg.kickbates.data.api.models


import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize
import sg.kickbates.data.BuildConfig
import sg.kickbates.data.api.models.common.Links
import sg.kickbates.data.api.models.common.Meta

@Parcelize
data class ResponseBanners(

	@field:SerializedName("data") val data: List<DataItem>? = null,
	@field:SerializedName("meta") val meta: Meta? = null,
	@field:SerializedName("links") val links: Links? = null,
	@field:SerializedName("message") val message: String? = null,
	@field:SerializedName("status") val status: Int? = null
) : Parcelable {
	@Parcelize
	data class DataItem(
		@field:SerializedName("updated_at") val updatedAt: String? = null,
		@field:SerializedName("satus") val satus: String? = null,
		@field:SerializedName("image_url") val imageUrl: String? = null,
		@field:SerializedName("ordering") val ordering: String? = null,
		@field:SerializedName("name") val name: String? = null,
		@field:SerializedName("created_at") val createdAt: String? = null,
		@field:SerializedName("id") val id: Int? = null,
		@field:SerializedName("desc") val desc: String? = null
	) : Parcelable {
		fun image():String {
			return if (imageUrl.isNullOrBlank()){
				BuildConfig.BASE_URL_IMAGE_PLACEHOLDER
			} else {
				"${BuildConfig.BASE_URL_IMAGE_BANNER}$imageUrl"
			}
		}
	}
}