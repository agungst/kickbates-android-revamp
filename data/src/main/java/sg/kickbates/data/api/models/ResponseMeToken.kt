package sg.kickbates.data.api.models


import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize
import sg.kickbates.data.helper.CommonTools

@Parcelize
data class ResponseMeToken(

	@field:SerializedName("user") val data: UserMe? = null,
    @field:SerializedName("message") val message: String? = null,
	@field:SerializedName("status") val status: Int? = null
) : Parcelable {
    @Parcelize
    data class UserMe(
        @field:SerializedName("id") val id: Int? = null,
        @field:SerializedName("name") val name: String? = null,
        @field:SerializedName("username") val username: String? = null,
        @field:SerializedName("email") val email: String? = null,
        @field:SerializedName("phone") val phone: String? = null,
        @field:SerializedName("email_verified") val emailVerified: Boolean? = null,
        @field:SerializedName("party_token") val partyToken: String? = null,
        @field:SerializedName("account_token") val accountToken: String? = null,
        @field:SerializedName("email_verified_at") val emailVerifiedAt: String? = null,
        @field:SerializedName("ffp_no") val ffpNo: String? = null,
        @field:SerializedName("party_id") val partyId: String? = null,
        @field:SerializedName("ffp_name") val ffpName: String? = null,
        @field:SerializedName("stripe_id") val stripeId: String? = null,
        @field:SerializedName("account_token_fb") val accountTokenFb: String? = null,
        @field:SerializedName("fcm_token") val fcmToken: String? = null,
        @field:SerializedName("is_subscribe_premium") val isSubscribePremium: Int? = null,
        @field:SerializedName("status") val status: Int? = null,
        @field:SerializedName("customer_country_code") val customerCountryCode: String? = null
    ) : Parcelable {
        fun subscribePremium():Boolean? { return isSubscribePremium == 1}
    }



}