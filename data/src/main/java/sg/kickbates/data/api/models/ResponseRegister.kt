package sg.kickbates.data.api.models

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class ResponseRegister(

	@field:SerializedName("data")
	val data: Data? = null,

	@field:SerializedName("message")
	val message: String? = null,

	@field:SerializedName("status")
	val status: Int? = null
) : Parcelable {
	@Parcelize
	data class Data(

		@field:SerializedName("phone")
		val phone: String? = null,

		@field:SerializedName("name")
		val name: String? = null,

		@field:SerializedName("account_token")
		val accountToken: String? = null,

		@field:SerializedName("id")
		val id: Int? = null,

		@field:SerializedName("email")
		val email: String? = null,

		@field:SerializedName("username")
		val username: String? = null
	) : Parcelable
}