package sg.kickbates.data.api.models.booking


import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class ResponseBooking(

	@field:SerializedName("data")
	val data: Data? = null,

	@field:SerializedName("message")
	val message: String? = null,

	@field:SerializedName("status")
	val status: Int? = null
) : Parcelable {
	@Parcelize
	data class Data(

		@field:SerializedName("date")
		val date: String? = null,

		@field:SerializedName("no_of_pax")
		val noOfPax: String? = null,

		@field:SerializedName("users_id")
		val usersId: Int? = null,

		@field:SerializedName("cellphone")
		val cellphone: String? = null,

		@field:SerializedName("id")
		val id: Int? = null,

		@field:SerializedName("time")
		val time: String? = null,

		@field:SerializedName("kick_merchant_id")
		val kickMerchantId: Int? = null,

		@field:SerializedName("status")
		val status: Int? = null
	) : Parcelable
}