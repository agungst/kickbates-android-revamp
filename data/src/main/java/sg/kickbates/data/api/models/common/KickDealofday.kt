package sg.kickbates.data.api.models.common

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

/**
 * Created by simx on 23,November,2019
 */
@Parcelize
class KickDealofday(

    @field:SerializedName("id") val id: Int? = null,
    @field:SerializedName("deal_name") val dealName: String? = null
) : Parcelable