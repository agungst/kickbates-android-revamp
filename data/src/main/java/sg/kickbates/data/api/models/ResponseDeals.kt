package sg.kickbates.data.api.models


import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize
import sg.kickbates.data.BuildConfig
import sg.kickbates.data.api.models.common.Links
import sg.kickbates.data.api.models.common.Meta
import java.text.SimpleDateFormat
import java.util.*

@Parcelize
data class ResponseDeals(

    @field:SerializedName("data") val data: List<DataItem>? = null,

    @field:SerializedName("meta") val meta: Meta? = null,

    @field:SerializedName("links") val links: Links? = null,

    @field:SerializedName("message") val message: String? = null,

    @field:SerializedName("status") val status: Int? = null
) : Parcelable {
    @Parcelize
    data class DataItem(

        @field:SerializedName("deal_created_date") val dealCreatedDate: String? = null,
        @field:SerializedName("is_banner") val isBanner: Int? = null,
        @field:SerializedName("deal_updated_by") val dealUpdatedBy: Int? = null,
        @field:SerializedName("deal_deleted_date") val dealDeletedDate: String? = null,
        @field:SerializedName("deal_cashback") val dealCashback: String? = null,
        @field:SerializedName("deal_created_by") val dealCreatedBy: Int? = null,
        @field:SerializedName("deal_end_to") val dealEndTo: String? = null,
        @field:SerializedName("deal_deleted_by") val dealDeletedBy: Int? = null,
        @field:SerializedName("deal_discount_amount") val dealDiscountAmount: String? = null,
        @field:SerializedName("deal_description") val dealDescription: String? = null,
        @field:SerializedName("deal_category_id") val dealCategoryId: Int? = null,
        @field:SerializedName("id") val id: Int? = null,
        @field:SerializedName("deal_merchant_id") val dealMerchantId: Int? = null,
        @field:SerializedName("deal_status") val dealStatus: Int? = null,
        @field:SerializedName("deal_discount_text") val dealDiscountText: String? = null,
        @field:SerializedName("deal_updated_date") val dealUpdatedDate: String? = null,
        @field:SerializedName("deal_is_delete") val dealIsDelete: Int? = null,
        @field:SerializedName("deal_type") val dealType: Int? = null,
        @field:SerializedName("deal_price") val dealPrice: String? = null,
        @field:SerializedName("deal_image") val dealImage: String? = null,
        @field:SerializedName("deal_show_order") val dealShowOrder: Int? = null,
        @field:SerializedName("deal_up_price") val dealUpPrice: String? = null,
        @field:SerializedName("deal_company_id") val dealCompanyId: Int? = null,
        @field:SerializedName("deal_banner_image") val dealBannerImage: String? = null,
        @field:SerializedName("deal_name") val dealName: String? = null,
        @field:SerializedName("deal_start_from") val dealStartFrom: String? = null,
        @field:SerializedName("deal_kick") val dealKick: String? = null
    ) : Parcelable {
        fun image():String {
            return  if (dealImage.isNullOrBlank()){
                BuildConfig.BASE_URL_IMAGE_PLACEHOLDER
            }else {
                "${BuildConfig.BASE_URL_IMAGE_DEAL}${this.dealImage}"
            }
        }
        fun imageBanner():String {
            return  if (dealBannerImage.isNullOrBlank()){
                BuildConfig.BASE_URL_IMAGE_PLACEHOLDER
            }else {
                "${BuildConfig.BASE_URL_IMAGE_DEAL}${this.dealBannerImage}"
            }
        }

        fun till():String? {
            val sdf = SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.US)
            val sdfConverter = SimpleDateFormat("dd MMM", Locale.US)
            return if (dealEndTo.isNullOrEmpty())"Valid till -- ----" else "Valid till ${sdfConverter.format(sdf.parse(dealEndTo)!!)}"
        }

        fun noBanner() :Boolean {
            return  isBanner == 0
        }

        fun disRemark():String{
            return  if (noBanner()) "$dealDiscountAmount % OFF!" else ""
        }

        fun showRemark():Boolean { return !dealDiscountText.isNullOrEmpty()}
        fun remark():String {
            return if (dealDiscountText.isNullOrEmpty())"-" else dealDiscountText
        }
    }
}