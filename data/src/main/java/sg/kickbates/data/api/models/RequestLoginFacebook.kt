package sg.kickbates.data.api.models

import com.google.gson.annotations.SerializedName

/**
 * Created by simx on 21,September,2019
 */
data class RequestLoginFacebook (
    @field:SerializedName("account_token")
    val account_token:String? =null
)

