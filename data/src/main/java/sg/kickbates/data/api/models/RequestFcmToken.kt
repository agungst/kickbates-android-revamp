package sg.kickbates.data.api.models

import com.google.gson.annotations.SerializedName

/**
 * Created by simx on 21,September,2019
 */
data class RequestFcmToken (
    @field:SerializedName("fcm_token")
    var fcmToken:String? =null
)
