package sg.kickbates.data.api.models.card


import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize
import sg.kickbates.data.api.models.common.Links
import sg.kickbates.data.api.models.common.Meta
import sg.kickbates.data.api.models.payment.DetailInvoice
import sg.kickbates.data.helper.CommonTools

@Parcelize
data class ResponseTransactionsHistory(
	@field:SerializedName("data") val data: List<DetailInvoice>? = null,
	@field:SerializedName("meta") val meta: Meta? = null,
	@field:SerializedName("links") val links: Links? = null,
	@field:SerializedName("message") val message: String? = null,
	@field:SerializedName("status") val status: Int? = null
) : Parcelable