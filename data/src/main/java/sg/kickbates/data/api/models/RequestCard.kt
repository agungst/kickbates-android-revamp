package sg.kickbates.data.api.models

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

/**
 * Created by simx on 14,November,2019
 */
@Parcelize
data class RequestCard(
    var issuer:String? = null,
    var url:String? = null,
    var code_auth:String? = null
) : Parcelable