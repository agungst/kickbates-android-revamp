package sg.kickbates.data.api.models.voucher


import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize
import sg.kickbates.data.api.models.common.Links
import sg.kickbates.data.api.models.common.Meta

@Parcelize
data class ResponseVouchersDetail(
	@field:SerializedName("data") val data: ResponseVouchers.DataItem? = null,
	@field:SerializedName("message") val message: String? = null,
	@field:SerializedName("status") val status: Int? = null
) : Parcelable