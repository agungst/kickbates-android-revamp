package sg.kickbates.data.api.models.payment

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

/**
 * Created by simx on 23,November,2019
 */
@Parcelize
data class RequestInvoiceDeal (
    var name:String? = null,
    var image:String? = null,
    @field:SerializedName("deal_id") var dealId:Int? =null,
    @field:SerializedName("payable") var payable:Double? =null,
    @field:SerializedName("pay_by_kick") var payByKick:Double? =null,
    @field:SerializedName("promocode_id") var promoCodeId:Int? =null
) : Parcelable