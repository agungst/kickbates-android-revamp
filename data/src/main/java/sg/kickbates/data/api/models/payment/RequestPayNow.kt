package sg.kickbates.data.api.models.payment


import com.google.gson.annotations.SerializedName

data class RequestPayNow(
	@field:SerializedName("ref") var ref: String? = null,
	@field:SerializedName("account_id") var accountId: String? = null,
	@field:SerializedName("kick_invoice_id") var kickInvoiceId: Int? = null,
	@field:SerializedName("issuer") var issuer: String? = null,
	@field:SerializedName("token") var token: String? = null
)