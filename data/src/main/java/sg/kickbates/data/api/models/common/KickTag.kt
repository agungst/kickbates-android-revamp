package sg.kickbates.data.api.models.common


import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class KickTag(

	@field:SerializedName("id")
	val id: Int? = null,

	@field:SerializedName("kick_merchant_category_tag_id")
	val kickMerchantCategoryTagId: Int? = null,

	@field:SerializedName("kick_merchant_id")
	val kickMerchantId: Int? = null,

	@field:SerializedName("kick_merchant_category_tag")
	val kickCategoryTag: KickCategoryTag? = null
) : Parcelable {
	@Parcelize
	data class KickCategoryTag(

		@field:SerializedName("code")
		val code: String? = null,

		@field:SerializedName("name")
		val name: String? = null,

		@field:SerializedName("id")
		val id: Int? = null
	) : Parcelable
}