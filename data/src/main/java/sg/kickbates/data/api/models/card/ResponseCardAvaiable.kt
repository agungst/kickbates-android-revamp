package sg.kickbates.data.api.models.card


import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize
import sg.kickbates.data.BuildConfig
import sg.kickbates.data.api.models.common.Links
import sg.kickbates.data.api.models.common.Meta

@Parcelize
data class ResponseCardAvaiable(
	@field:SerializedName("data") val data: List<DataItem>? = null,
	@field:SerializedName("meta") val meta: Meta? = null,
	@field:SerializedName("links") val links: Links? = null,
	@field:SerializedName("message") val message: String? = null,
	@field:SerializedName("status") val status: Int? = null
) : Parcelable {
	@Parcelize
	data class DataItem(
		@field:SerializedName("bank_code") val bankCode: String? = null,
		@field:SerializedName("bank_image") val bankImage: String? = null,
		@field:SerializedName("bank_status") val bankStatus: Int? = null,
		@field:SerializedName("bank_slug") val bankSlug: String? = null,
		@field:SerializedName("bank_card_color") val bankCardColor: String? = null,
		@field:SerializedName("bank_name") val bankName: String? = null,
		@field:SerializedName("id") val id: Int? = null,
		@field:SerializedName("bank_card_available") val bankCardAvailable: Int? = null
	) : Parcelable {
		fun bankIcon():String {
			return if (bankImage != null){
				"${BuildConfig.BASE_URL_IMAGE_BANK}$bankImage"
			}else {
				BuildConfig.BASE_URL_IMAGE_PLACEHOLDER
			}
		}
	}
}