package sg.kickbates.data.api.models.card.token

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Party(

	@field:SerializedName("retailParty")
	val retailParty: RetailParty? = null
) : Parcelable {
	@Parcelize
	data class RetailParty(

		@field:SerializedName("contactDetl")
		val contactDetl: ContactDetl? = null,

		@field:SerializedName("retailDemographic")
		val retailDemographic: RetailDemographic? = null,

		@field:SerializedName("employmentDetl")
		val employmentDetl: EmploymentDetl? = null,

		@field:SerializedName("partyId")
		val partyId: String? = null
	) : Parcelable {
		@Parcelize
		data class EmploymentDetl(

			@field:SerializedName("occupation")
			val occupation: String? = null,

			@field:SerializedName("jobTitle")
			val jobTitle: String? = null,

			@field:SerializedName("employerName")
			val employerName: String? = null,

			@field:SerializedName("occupationGroup")
			val occupationGroup: String? = null
		) : Parcelable

		@Parcelize
		data class ContactDetl(

			@field:SerializedName("phoneDetl")
			val phoneDetl: List<PhoneDetlItem>? = null,

			@field:SerializedName("addressDetl")
			val addressDetl: List<AddressDetlItem?>? = null,

			@field:SerializedName("emailDetl")
			val emailDetl: List<EmailDetlItem>? = null
		) : Parcelable {
			@Parcelize
			data class EmailDetlItem(

				@field:SerializedName("isPreferredEmail")
				val isPreferredEmail: Boolean? = null,

				@field:SerializedName("emailAddressDetl")
				val emailAddressDetl: EmailAddressDetl? = null
			) : Parcelable {
				@Parcelize
				data class EmailAddressDetl(

					@field:SerializedName("emailAddressType")
					val emailAddressType: String? = null,

					@field:SerializedName("emailAddress")
					val emailAddress: String? = null
				) : Parcelable
			}
			@Parcelize
			data class PhoneDetlItem(

				@field:SerializedName("phone")
				val phone: Phone? = null,

				@field:SerializedName("lastUpdateDate")
				val lastUpdateDate: String? = null,

				@field:SerializedName("phoneId")
				val phoneId: String? = null,

				@field:SerializedName("phoneUsage")
				val phoneUsage: String? = null,

				@field:SerializedName("isPreferredPhone")
				val isPreferredPhone: Boolean? = null
			) : Parcelable {
				@Parcelize
				data class Phone(

					@field:SerializedName("phoneType")
					val phoneType: String? = null,

					@field:SerializedName("phoneCtryCode")
					val phoneCtryCode: String? = null,

					@field:SerializedName("phoneNumber")
					val phoneNumber: String? = null
				) : Parcelable
			}
			@Parcelize
			data class AddressDetlItem(

				@field:SerializedName("isPreferredAddress")
				val isPreferredAddress: Boolean? = null,

				@field:SerializedName("addressType")
				val addressType: String? = null,

				@field:SerializedName("lastUpdateDate")
				val lastUpdateDate: String? = null,

				@field:SerializedName("partyAddress")
				val partyAddress: PartyAddress? = null,

				@field:SerializedName("addressId")
				val addressId: String? = null
			) : Parcelable {
				@Parcelize
				data class PartyAddress(

					@field:SerializedName("localAddress")
					val localAddress: LocalAddress? = null
				) : Parcelable {
					@Parcelize
					data class LocalAddress(

						@field:SerializedName("country")
						val country: String? = null,

						@field:SerializedName("unit")
						val unit: String? = null,

						@field:SerializedName("level")
						val level: String? = null,

						@field:SerializedName("postalCode")
						val postalCode: String? = null,

						@field:SerializedName("addressLine1")
						val addressLine1: String? = null,

						@field:SerializedName("block")
						val block: String? = null
					) : Parcelable
				}
			}
		}
	}
}