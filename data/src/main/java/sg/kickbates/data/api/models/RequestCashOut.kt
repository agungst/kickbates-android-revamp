package sg.kickbates.data.api.models

import com.google.gson.annotations.SerializedName

/**
 * Created by simx on 21,September,2019
 */
data class RequestCashOut (
    @field:SerializedName("request_account_name") var requestAccountName:String? =null,
    @field:SerializedName("request_account_number") var requestAccountNumber:String? =null,
    @field:SerializedName("request_cash") var requestCash:Double? =null,
    @field:SerializedName("request_bank_id") var requestBankId:Int? =null,
    @field:SerializedName("users_id") var usersId:Int? =null
)
