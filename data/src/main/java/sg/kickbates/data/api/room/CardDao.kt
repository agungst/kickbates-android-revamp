package sg.kickbates.data.api.room

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query

/**
 * Created by simx on 21,April,2020
 */
@Dao
interface CardDao {
    @Query("SELECT * FROM card_model")
    fun all():List<CardModel>

    @Query("SELECT * FROM card_model WHERE card_number LIKE :cardNumber")
    fun cardByCardNumber(cardNumber:String) :List<CardModel>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(cardModel: CardModel?)
}