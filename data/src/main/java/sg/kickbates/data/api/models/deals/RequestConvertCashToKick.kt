package sg.kickbates.data.api.models.deals


import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class RequestConvertCashToKick(

	@field:SerializedName("earn_cashback")
	var earnCashback: Int? = null,

	@field:SerializedName("earn_remark")
	var earnRemark: String? = null,

	@field:SerializedName("earn_kick")
	var earnKick: Int? = null,

	@field:SerializedName("kick_invoice_id")
	var kickInvoiceId: Int? = null,

	@field:SerializedName("earn_status")
	var earnStatus: Int? = null
) : Parcelable