package sg.kickbates.data.api.models.card


import com.google.gson.annotations.SerializedName


data class ResponseAuthCard(
	@field:SerializedName("data") val data: String? = null,
	@field:SerializedName("status") val status: Int? = null
)