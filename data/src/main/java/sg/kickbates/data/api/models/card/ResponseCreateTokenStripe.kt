package sg.kickbates.data.api.models.card


import com.google.gson.annotations.SerializedName
data class ResponseCreateTokenStripe(

	@field:SerializedName("livemode")
	val livemode: Boolean? = null,

	@field:SerializedName("created")
	val created: Int? = null,

	@field:SerializedName("client_ip")
	val clientIp: String? = null,

	@field:SerializedName("id")
	val id: String? = null,

	@field:SerializedName("used")
	val used: Boolean? = null,

	@field:SerializedName("type")
	val type: String? = null,

	@field:SerializedName("card")
	val card: Card? = null,

	@field:SerializedName("object")
	val jsonObject: String? = null
) {
	data class Card(

		@field:SerializedName("address_zip_check")
		val addressZipCheck: String? = null,

		@field:SerializedName("country")
		val country: String? = null,

		@field:SerializedName("last4")
		val last4: String? = null,

		@field:SerializedName("funding")
		val funding: String? = null,

		@field:SerializedName("metadata")
		val metadata: Metadata? = null,

		@field:SerializedName("address_country")
		val addressCountry: String? = null,

		@field:SerializedName("address_state")
		val addressState: String? = null,

		@field:SerializedName("exp_month")
		val expMonth: Int? = null,

		@field:SerializedName("exp_year")
		val expYear: Int? = null,

		@field:SerializedName("address_city")
		val addressCity: String? = null,

		@field:SerializedName("tokenization_method")
		val tokenizationMethod: String? = null,

		@field:SerializedName("cvc_check")
		val cvcCheck: String? = null,

		@field:SerializedName("address_line2")
		val addressLine2: String? = null,

		@field:SerializedName("address_line1")
		val addressLine1: String? = null,

		@field:SerializedName("name")
		val name: String? = null,

		@field:SerializedName("id")
		val id: String? = null,

		@field:SerializedName("address_line1_check")
		val addressLine1Check: String? = null,

		@field:SerializedName("address_zip")
		val addressZip: String? = null,

		@field:SerializedName("dynamic_last4")
		val dynamicLast4: String? = null,

		@field:SerializedName("brand")
		val brand: String? = null,

		@field:SerializedName("object")
		val jsonObject: String? = null
	) {
		data class Metadata(
			val any: Any? = null
		)
	}
}