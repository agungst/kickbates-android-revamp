package sg.kickbates.data.api.models.cash_back


import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize
import sg.kickbates.data.api.models.common.CreatedAt
import sg.kickbates.data.api.models.common.Links
import sg.kickbates.data.api.models.common.Meta
import sg.kickbates.data.api.models.payment.DetailInvoice
import sg.kickbates.data.helper.CommonTools

@Parcelize
data class ResponseCashBackHistory(
	@field:SerializedName("data") val data: List<DataItem>? = null,
	@field:SerializedName("meta") val meta: Meta? = null,
	@field:SerializedName("links") val links: Links? = null,
	@field:SerializedName("message") val message: String? = null,
	@field:SerializedName("status") val status: Int? = null
) : Parcelable {
	@Parcelize
	data class DataItem(
		@field:SerializedName("id") val id: Int? = null,
		@field:SerializedName("earn_cashback") val earnCashBack: Double? = null,
		@field:SerializedName("earn_remark") val earnRemark: String? = null,
		@field:SerializedName("earn_kick") val earnKick: Double? = null,
		@field:SerializedName("users_id") val usersId: Int? = null,
		@field:SerializedName("created_at") val createdAt: CreatedAt? = null,
		@field:SerializedName("kick_invoice_id") val kickInvoiceId: Int? = null,
		@field:SerializedName("kick_invoice") val kickInvoice: DetailInvoice? = null,
		@field:SerializedName("earn_status") val earnStatus: Int? = null
	) : Parcelable {
		fun status():String? {
			return when(earnStatus) {
				1 -> "Earning"
				2 -> "Deduction"
				3 -> "Conversion"
				4 -> "Processed Cashout"
				5 -> "Adjustment"
				else -> "Undefine"
			}
		}
		fun storeName():String {
			return  kickInvoice?.merchant?.merchantName ?: "-"
		}
		fun kickUsed():String {
			return  kickInvoice?.invoiceKickpointPaid?.let { CommonTools.formatDollarCurrency(it) } ?: "-"
		}
		fun kickEarn():String {
			return  earnKick?.let { CommonTools.formatDollarCurrency(it) } ?: "-"
		}
		fun cashEarn():String? {
			return  earnCashBack?.let { CommonTools.formatDollarCurrency(it) } ?: "-"
		}
		fun spendAmount():String? {
			return  kickInvoice?.invoiceSmPaid?.let { CommonTools.formatDollarCurrency(it) } ?: "-"
		}

		fun storeNameVisible(): Boolean {
			return  kickInvoice?.merchant?.merchantName != null
		}
		fun kickUsedVisible(): Boolean {
			return  kickInvoice?.invoiceKickpointPaid != null
		}
		fun kickEarnVisible(): Boolean {
			return  earnKick != null && earnStatus != 4
		}
		fun cashEarnVisible(): Boolean {
			return  earnCashBack != null
		}

		fun kickEarnTitle(): String {
			return  if (earnStatus == 3 || earnStatus == 4) "Kick$" else "Kick$ Earned"
		}
		fun cashEarnTitle(): String {
			return  if (earnStatus == 3 || earnStatus == 4) "Cash$" else "Cash$ Earned"
		}
		fun spendAmountVisible(): Boolean {
			return  kickInvoice?.invoiceSmPaid != null
		}

	}
}