package sg.kickbates.data.api.models

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

/**
 * Created by simx on 28,November,2019
 */
@Parcelize
data class RequestUpdateProfile (
    @field:SerializedName("name")
    var name:String? = null,
    @field:SerializedName("email")
    var email:String? = null,
    @field:SerializedName("phone")
    var phone:String? = null,
    @field:SerializedName("ffp_name")
    var ffpName:String? = null,
    @field:SerializedName("ffp_no")
    var ffpNo:String? = null
) : Parcelable