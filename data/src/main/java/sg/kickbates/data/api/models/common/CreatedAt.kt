package sg.kickbates.data.api.models.common

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize
import java.text.SimpleDateFormat
import java.util.*

@Parcelize
data class CreatedAt(
	@field:SerializedName("date") val date: String? = null,
	@field:SerializedName("timezone") val timezone: String? = null,
	@field:SerializedName("timezone_type") val timezoneType: Int? = null
) : Parcelable {
	fun dateConverter():String? {
		val sdf = SimpleDateFormat("yyyy-MM-dd HH:mm:ss.000000", Locale.US)
		val sdfConverter = SimpleDateFormat("dd MMM yyyy", Locale.US)
		return if (date.isNullOrEmpty()) "----/--/--" else {
			sdfConverter.format(sdf.parse(date)!!)
		}
	}
}