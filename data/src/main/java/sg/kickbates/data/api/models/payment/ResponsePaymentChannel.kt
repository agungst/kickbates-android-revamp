package sg.kickbates.data.api.models.payment


import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize
import sg.kickbates.data.BuildConfig
import sg.kickbates.data.api.models.common.Links
import sg.kickbates.data.api.models.common.Meta

@Parcelize
data class ResponsePaymentChannel(

	@field:SerializedName("data")
	val data: List<DataItem>? = null,

	@field:SerializedName("meta")
	val meta: Meta? = null,

	@field:SerializedName("links")
	val links: Links? = null,

	@field:SerializedName("message")
	val message: String? = null,

	@field:SerializedName("status")
	val status: Int? = null
) : Parcelable {
	@Parcelize
	data class DataItem(

		@field:SerializedName("image")
		val image: String? = null,

		@field:SerializedName("code")
		val code: String? = null,

		@field:SerializedName("name")
		val name: String? = null,

		@field:SerializedName("active")
		val active: Int? = null,

		@field:SerializedName("id")
		val id: Int? = null
	) : Parcelable {
		fun iconChannel():String? {
			return if (image.isNullOrBlank()){
				BuildConfig.BASE_URL_IMAGE_PLACEHOLDER
			} else {
				"${BuildConfig.BASE_URL_IMAGE_BANK}$image"
			}
		}
	}
}