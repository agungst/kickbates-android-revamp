package sg.kickbates.data.api.models.notification

import com.google.gson.annotations.SerializedName
import sg.kickbates.data.helper.CommonTools

data class ResponseNotificationHistory(
	@field:SerializedName("data") val data: List<DataNotification>? = null,
	@field:SerializedName("message") val message: String? = null,
	@field:SerializedName("status") val status: Int? = null
){
	data class DataNotification(
		@field:SerializedName("notification_type") val notificationType: String? = null,
		@field:SerializedName("notify_title") val notifyTitle: String? = null,
		@field:SerializedName("notify_company_id") val notifyCompanyId: Int? = null,
		@field:SerializedName("users_id") val usersId: Int? = null,
		@field:SerializedName("id") val id: Int? = null,
		@field:SerializedName("notify_status") val notifyStatus: String? = null,
		@field:SerializedName("notify_msg") val notifyMsg: String? = null,
		@field:SerializedName("notify_date") val notifyDate: String? = null,
		@field:SerializedName("created_at") val createdAt: String? = null
	){
		fun data():String?{
			return CommonTools.formatDate(createdAt)
		}
	}

}