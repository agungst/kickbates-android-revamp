package sg.kickbates.data.api.models.card


import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class KickCustomerCardStatements(

	@field:SerializedName("amountDue") val amountDue: Double? = null,
	@field:SerializedName("dueDate") val dueDate: String? = null,
	@field:SerializedName("statementDate") val statementDate: String? = null,
	@field:SerializedName("id") val id: Int? = null,
	@field:SerializedName("kick_customer_cards_id") val kickCustomerCardsId: String? = null,
	@field:SerializedName("paidAmount") val paidAmount: Double? = null,
	@field:SerializedName("status") val status: Int? = null
) : Parcelable