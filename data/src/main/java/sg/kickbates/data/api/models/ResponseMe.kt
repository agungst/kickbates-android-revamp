package sg.kickbates.data.api.models


import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize
import sg.kickbates.data.helper.CommonTools

@Parcelize
data class ResponseMe(

	@field:SerializedName("data") val data: Data? = null,
	@field:SerializedName("message") val message: String? = null,
	@field:SerializedName("status") val status: Int? = null
) : Parcelable {
    @Parcelize
    data class Data(

        @field:SerializedName("ffp_no") val ffpNo: String? = null,
        @field:SerializedName("phone") val phone: String? = null,
        @field:SerializedName("name") val name: String? = null,
        @field:SerializedName("account_token") val accountToken: String? = null,
        @field:SerializedName("ffp_name") val ffpName: String? = null,
        @field:SerializedName("kick_premium_customer") val kickPremiumCustomer: KickPremiumCustomer? = null,
        @field:SerializedName("id") val id: Int? = null,
        @field:SerializedName("email") val email: String? = null,
        @field:SerializedName("stripe_id") val stripeId: String? = null,
        @field:SerializedName("fcm_token") val fcmToken: String? = null,
        @field:SerializedName("is_subscribe_premium") val isSubscribePremium: Int? = null,
        @field:SerializedName("is_premium_purchasable") val isPremiumPurchasable: Int? = null,
        @field:SerializedName("username") val username: String? = null
    ) : Parcelable {

        fun isPremiumAccount():Boolean? {
            return if (kickPremiumCustomer != null ) {
                CommonTools.isDateAfter(kickPremiumCustomer.activeTo)!!
            }
            else {
                false
            }

        }
        fun premiumAccount():String? {
            return if (isPremiumAccount()!!){
                "Premium Account (until ${CommonTools.formatDate(kickPremiumCustomer?.activeTo)})"
            }else {
                "Upgrade to Premium"
            }
        }
        fun subscribePremium():Boolean? { return isSubscribePremium == 1}

        fun canSubscribePremium():Boolean {return isPremiumPurchasable == 1}

        fun title():String? {
             return  if (subscribePremium()!!){
                "Congratulations! You are a premium user"
            }else {
                "You are not a premium user yet"
            }
        }
        fun subTitle():String? {
            return  if (subscribePremium()!!){
                validUntil()
            }else {
                "Subscribe to experience the benefit"
            }
        }

        fun validUntil():String {
            return if (kickPremiumCustomer == null )"" else {
                "Valid till ${CommonTools.formatDate(kickPremiumCustomer.activeTo) } - ${activeSubscription()}"
            }
        }
        private fun activeSubscription():String?{
            return if (subscribePremium()!!) "Subscription Active"  else "Will not be renewed"
        }
        @Parcelize
        data class KickPremiumCustomer(
            @field:SerializedName("id") val id: Int? = null,
            @field:SerializedName("active_to") val activeTo: String? = null,
            @field:SerializedName("users_id") val usersId: Int? = null,
            @field:SerializedName("active_from") val activeFrom: String? = null,
            @field:SerializedName("cashback_percentage") val cashBackPercentage: Int? = null,
            @field:SerializedName("kick_invoice_id") val kickInvoiceId: Int? = null
        ) : Parcelable
    }
}