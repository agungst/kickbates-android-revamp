package sg.kickbates.data.api.models


import com.google.gson.annotations.SerializedName

data class ResponseCorverKick(

	@field:SerializedName("data")
	val data: Data? = null,

	@field:SerializedName("message")
	val message: String? = null,

	@field:SerializedName("status")
	val status: Int? = null
){
	data class Data(

		@field:SerializedName("earn_cashback")
		val earnCashback: String? = null,

		@field:SerializedName("earn_remark")
		val earnRemark: String? = null,

		@field:SerializedName("earn_kick")
		val earnKick: String? = null,

		@field:SerializedName("users_id")
		val usersId: Int? = null,

		@field:SerializedName("id")
		val id: Int? = null,

		@field:SerializedName("kick_invoice_id")
		val kickInvoiceId: String? = null,

		@field:SerializedName("earn_status")
		val earnStatus: String? = null
	)
}