package sg.kickbates.data.api.models.payment

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize
import sg.kickbates.data.api.models.card.KickCustomerCardStatements
import sg.kickbates.data.api.models.card.KickCustomerEarning
import sg.kickbates.data.api.models.common.KickDealofday
import sg.kickbates.data.api.models.common.KickMerchant
import sg.kickbates.data.helper.CommonTools

/**
 * Created by simx on 24,November,2019
 */
@Parcelize
data  class DetailInvoice (
    @field:SerializedName("invoice_sm_paid") val invoiceSmPaid: Double? = null,
    @field:SerializedName("invoice_kickpoint_paid") val invoiceKickpointPaid: Double? = null,
    @field:SerializedName("invoice_payble") val invoicePayble: Double? = null,
    @field:SerializedName("gst_percentage") val gstPercentage: String? = null,
    @field:SerializedName("sc_percentage") val scPercentage: String? = null,
    @field:SerializedName("invoice_paid_by") val invoicePaidBy: Int? = null,
    @field:SerializedName("invoice_use_type") val invoiceUseType: Int? = null,
    @field:SerializedName("users_id") val usersId: Int? = null,
    @field:SerializedName("invoice_status") val invoiceStatus: Int? = null,
    @field:SerializedName("invoice_sm_reference_code") val invoiceSmReferenceCode: String? = null,
    @field:SerializedName("invoice_promocode_id") val invoicePromocodeId: String? = null,
    @field:SerializedName("invoice_number") val invoiceNumber: String? = null,
    @field:SerializedName("invoice_promocode_paid") val invoicePromocodePaid: String? = null,
    @field:SerializedName("invoice_paid_date") val invoicePaidDate: String? = null,
    @field:SerializedName("kick_promocode") val kickPromocode: KickPromocode? = null,
    @field:SerializedName("kick_dealofday") val kickDealOfDay: KickDealofday? = null,
    @field:SerializedName("kick_merchant") val merchant: KickMerchant? = null,
    @field:SerializedName("kick_customer_card_statements") val kickCustomerCardStatements: KickCustomerCardStatements? = null,
    @field:SerializedName("kick_customer_earning") val kickCustomerEarning: KickCustomerEarning? = null,
    @field:SerializedName("id") val id: Int? = null
) : Parcelable {

    fun paidBy():String {
        return if (invoicePaidBy != null){
            when(invoicePaidBy){
                1 -> "Paid with Credit Card"
                2 -> "Paid with PayNow (DBS/POSB)"
                3 -> "Paid with Kick$"
                else -> "Paid with Credit Card"
            }
        } else "Paid with Credit Card"
    }

    fun earningStaus():String? {
        return if (kickCustomerEarning != null){
            when (kickCustomerEarning.earnStatus) {
                1 -> "Earning"
                2 -> "Deduction"
                3 -> "Conversion"
                4 -> "Processed Cashout"
                5 -> "Adjustment"
                else -> "Earning"
            }
        } else "Earning"
    }

    fun dealPromoName():String? {
        return if (kickDealOfDay != null) kickDealOfDay.dealName else {
            if (merchant != null) merchant.merchantName else "Payment"
        }
    }
    fun promoCode():String? {
        return if (kickPromocode != null){
            kickPromocode.promocodeCode
        } else {
            "-"
        }
    }

    fun amountPayable():String? {
        return invoiceSmPaid?.let { CommonTools.formatDollarCurrency(it) }?: CommonTools.formatDollarCurrency(0.0)
    }
    fun invoicePayableString():String? {
        return invoicePayble?.let { CommonTools.formatDollarCurrency(it) }?: CommonTools.formatDollarCurrency(0.0)
    }
    fun kickUsed():String? {
        return invoiceKickpointPaid?.let { CommonTools.formatDollarCurrency(it) } ?: CommonTools.formatDollarCurrency(0.0)
    }

    fun earningAmount(): String? {
        return if (kickCustomerEarning != null) { kickCustomerEarning.cashBackAmount()?.let { CommonTools.formatDollarCurrency(it) } }
        else CommonTools.formatDollarCurrency(0.0)
    }
    fun painDate():String? {
        return if (invoicePaidDate != null){
            CommonTools.formatDateTime(invoicePaidDate)
        }else {
            "-"
        }
    }
    @Parcelize
    data class KickPromocode(

        @field:SerializedName("image") val image: String? = null,
        @field:SerializedName("promocode_merchant_id") val promocodeMerchantId: String? = null,
        @field:SerializedName("product_status") val productStatus: Int? = null,
        @field:SerializedName("created_at") val createdAt: String? = null,
        @field:SerializedName("kick_invoice_id") val kickInvoiceId: String? = null,
        @field:SerializedName("promocode_type") val promocodeType: Int? = null,
        @field:SerializedName("promocode_benifit") val promocodeBenifit: String? = null,
        @field:SerializedName("promocode_start_date") val promocodeStartDate: String? = null,
        @field:SerializedName("promocode_end_date") val promocodeEndDate: String? = null,
        @field:SerializedName("promocode_usage_limit") val promocodeUsageLimit: Int? = null,
        @field:SerializedName("updated_at") val updatedAt: String? = null,
        @field:SerializedName("users_id") val usersId: String? = null,
        @field:SerializedName("id") val id: Int? = null,
        @field:SerializedName("promocode_code") val promocodeCode: String? = null,
        @field:SerializedName("promocode_name") val promocodeName: String? = null,
        @field:SerializedName("promocode_price") val promocodePrice: String? = null,
        @field:SerializedName("promocode_desc") val promocodeDesc: String? = null,
        @field:SerializedName("promocode_exclude_merchant")
        val promocodeExcludeMerchant: String? = null
    ) : Parcelable

}