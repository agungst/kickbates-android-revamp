package sg.kickbates.data.api.models.payment

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

/**
 * Created by simx on 23,November,2019
 */
@Parcelize
data class RequestInvoicePremium (
    @field:SerializedName("premium")
    var premium:Int? =null,
    @field:SerializedName("pay_by_kick")
    var payByKick:Double? =null,
    @field:SerializedName("promocode_id")
    var promoCodeId:Int? =null
) : Parcelable