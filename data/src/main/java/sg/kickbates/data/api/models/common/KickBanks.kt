package sg.kickbates.data.api.models.common


import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class KickBanks(
	@field:SerializedName("id") val id: Int? = null,
	@field:SerializedName("bank_code") val bankCode: String? = null,
	@field:SerializedName("bank_image") val bankImage: String? = null,
	@field:SerializedName("bank_uen") val bankUen: String? = null,
	@field:SerializedName("bank_status") val bankStatus: Int? = null,
	@field:SerializedName("bank_slug") val bankSlug: String? = null,
	@field:SerializedName("bank_card_color") val bankCardColor: String? = null,
	@field:SerializedName("bank_name") val bankName: String? = null,
	@field:SerializedName("bank_card_available") val bankCardAvailable: Int? = null
) : Parcelable