package sg.kickbates.data.api.room

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

/**
 * Created by simx on 21,April,2020
 */
@Entity(tableName = "card_model")
class CardModel (
    @PrimaryKey(autoGenerate = true)
    var id:Int? = null,
    @ColumnInfo(name = "card_id")
    var cardId:String? = null,
    @ColumnInfo(name = "card_number")
    var cardNumber:String? = null
)