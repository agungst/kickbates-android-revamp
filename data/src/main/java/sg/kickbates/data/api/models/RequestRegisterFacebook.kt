package sg.kickbates.data.api.models

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize
import java.util.*

/**
 * Created by simx on 21,September,2019
 */
@Parcelize
data class RequestRegisterFacebook(
    @field:SerializedName("name") var name:String? = null,
    @field:SerializedName("email") var email:String? = null,
    @field:SerializedName("phone") var phone:String? = null,
    @field:SerializedName("account_token") var accountTokenFb:String? = null,
    @field:SerializedName("password") var password:String? = null
) : Parcelable