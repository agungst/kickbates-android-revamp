package sg.kickbates.data.api.room

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase

/**
 * Created by simx on 21,April,2020
 */
@Database(entities = [CardModel::class],version = 1, exportSchema = false)
abstract class AppDatabase :RoomDatabase(){
    abstract fun cardDao(): CardDao
    companion object {
        @Volatile
        private var INSTANCE: AppDatabase? = null
        fun getDatabase(context: Context): AppDatabase {
            val tempInstance = INSTANCE
            if (tempInstance != null) return tempInstance
            synchronized(this){
                val instance = Room.databaseBuilder(
                    context.applicationContext,
                    AppDatabase::class.java,
                    "kickbates_db"
                ).build()
                INSTANCE = instance
                return  instance
            }
        }
    }
}