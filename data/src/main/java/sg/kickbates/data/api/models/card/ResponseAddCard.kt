package sg.kickbates.data.api.models.card


import com.google.gson.annotations.SerializedName


data class ResponseAddCard(
	@field:SerializedName("data") val data: List<DataItem>? = null,
	@field:SerializedName("status") val status: Int? = null){
	data class DataItem(
		@field:SerializedName("activation_date") val activationDate: String? = null,
		@field:SerializedName("due_date") val dueDate: String? = null,
		@field:SerializedName("card_exp") val cardExp: String? = null,
		@field:SerializedName("active_date") val activeDate: String? = null,
		@field:SerializedName("created_at") val createdAt: String? = null,
		@field:SerializedName("card_type") val cardType: String? = null,
		@field:SerializedName("is_primary_card") val isPrimaryCard: String? = null,
		@field:SerializedName("card_id") val cardId: String? = null,
		@field:SerializedName("linked_accounts") val linkedAccounts: String? = null,
		@field:SerializedName("card_status") val cardStatus: String? = null,
		@field:SerializedName("card_face") val cardFace: String? = null,
		@field:SerializedName("overseas_atm") val overseasAtm: String? = null,
		@field:SerializedName("issue_date") val issueDate: String? = null,
		@field:SerializedName("card_no") val cardNo: String? = null,
		@field:SerializedName("remaning_limit") val remaningLimit: String? = null,
		@field:SerializedName("updated_at") val updatedAt: String? = null,
		@field:SerializedName("card_brand") val cardBrand: String? = null,
		@field:SerializedName("users_id") val usersId: Int? = null,
		@field:SerializedName("current_balance") val currentBalance: String? = null,
		@field:SerializedName("id") val id: Int? = null,
		@field:SerializedName("kick_banks_id") val kickBanksId: Int? = null,
		@field:SerializedName("card_desc") val cardDesc: String? = null,
		@field:SerializedName("name_on_card") val nameOnCard: String? = null
	)
}