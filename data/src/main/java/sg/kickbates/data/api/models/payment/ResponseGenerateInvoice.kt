package sg.kickbates.data.api.models.payment


import com.google.gson.annotations.SerializedName


data class ResponseGenerateInvoice(
	@field:SerializedName("data") val data: DetailInvoice? = null,
	@field:SerializedName("status") val status: Int? = null,
	@field:SerializedName("message") val message: String? = null
)