package sg.kickbates.data.helper

import android.annotation.SuppressLint
import android.content.Context
import android.content.SharedPreferences

/**
 * Created by simx on 11,November,2019
 */
class Pref(context: Context) {
    var pref: SharedPreferences? = null
    init {
        pref = context.getSharedPreferences(PREF_NAME,Context.MODE_PRIVATE)
    }

    var premiumAccount: Boolean
        set(value) = pref?.edit()?.putBoolean(KEY_USER_PREMIUM,value)?.apply()!!
        get() = pref?.getBoolean(KEY_USER_PREMIUM,false)!!

    var subscribePremiumAccount: Boolean
        set(value) = pref?.edit()?.putBoolean(KEY_SUBSCRIBE_PREMIUM,value)?.apply()!!
        get() = pref?.getBoolean(KEY_SUBSCRIBE_PREMIUM,false)!!

    var userType: Int
        set(value) = pref?.edit()?.putInt(KEY_USER_TYPE,value)?.apply()!!
        get() = pref?.getInt(KEY_USER_TYPE,0)!!

    var token: String
        set(value) = pref?.edit()?.putString(KEY_TOKEN,value)?.apply()!!
        get() = pref?.getString(KEY_TOKEN,"")!!

    var fcmToken: String
        set(value) = pref?.edit()?.putString(KEY_TOKEN_FIREBASE,value)?.apply()!!
        get() = pref?.getString(KEY_TOKEN_FIREBASE,"")!!

    var dataUser: String?
        set(value) = pref?.edit()?.putString(KEY_DATA_USER,value)?.apply()!!
        get() = pref?.getString(KEY_DATA_USER,"")!!

    var email: String?
        set(value) = pref?.edit()?.putString(KEY_EMAIL,value)?.apply()!!
        get() = pref?.getString(KEY_EMAIL,"")!!
    var photo: String?
        set(value) = pref?.edit()?.putString(KEY_PHOTO,value)?.apply()!!
        get() = pref?.getString(KEY_PHOTO,"")!!

    var username: String?
        set(value) = pref?.edit()?.putString(KEY_USERNAME,value)?.apply()!!
        get() = pref?.getString(KEY_USERNAME,"")!!


    var name: String?
        set(value) = pref?.edit()?.putString(KEY_NAME,value)?.apply()!!
        get() = pref?.getString(KEY_NAME,"")!!

    var layout: String?
        set(value) = pref?.edit()?.putString(KEY_LAYOUT,value)?.apply()!!
        get() = pref?.getString(KEY_LAYOUT,"")!!


    var usersId: Int?
        set(value) = value?.let { pref?.edit()?.putInt(KEY_USERS_ID, it)?.apply() }!!
        get() = pref?.getInt(KEY_USERS_ID,0)!!

    var id: Int?
        set(value) = value?.let { pref?.edit()?.putInt(KEY_ID, it)?.apply() }!!
        get() = pref?.getInt(KEY_ID,0)!!

    var lat: String?
        set(value) = value?.let { pref?.edit()?.putString(KEY_DATA_LAT, it)?.apply() }!!
        get() = pref?.getString(KEY_DATA_LAT,"0")!!

    var lng: String?
        set(value) = value?.let { pref?.edit()?.putString(KEY_DATA_LNG, it)?.apply() }!!
        get() = pref?.getString(KEY_DATA_LNG,"0")!!


    @SuppressLint("ApplySharedPref")
    fun clear(){
        pref!!.edit().clear().commit()
    }

    companion object {
        private var PREF_NAME  = Pref::class.java.simpleName
        const val KEY_USER_TYPE = "user_type"
        const val KEY_TOKEN = "token"
        const val KEY_TOKEN_FIREBASE = "token_firebase"
        const val KEY_PHOTO = "photo"
        const val KEY_USERS_ID = "usersId"
        const val KEY_ID = "id"
        const val KEY_USERNAME = "username"
        const val KEY_USER_PREMIUM = "username_premium"
        const val KEY_SUBSCRIBE_PREMIUM = "subscribe_premium"
        const val KEY_NAME = "name"
        const val KEY_EMAIL = "email"
        const val KEY_LAYOUT = "layout"
        const val KEY_DATA_USER = "data_user"
        const val KEY_DATA_LAT = "data_lat"
        const val KEY_DATA_LNG = "data_lng"
    }
}
