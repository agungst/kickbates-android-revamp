package sg.kickbates.data.helper


import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class CardModel(

	@field:SerializedName("number")
	var number: String? = null,

	@field:SerializedName("cvc")
	var cvc: String? = null,

	@field:SerializedName("exp_month")
	var expMonth: Int? = null,

	@field:SerializedName("exp_year")
	var expYear: Int? = null
) : Parcelable