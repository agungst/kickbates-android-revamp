package sg.kickbates.data.helper

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

/**
 * Created by simx on 23,November,2019
 */
@Parcelize
enum class ProductType : Parcelable {
    DEALS, MERCHANT, CARD
}