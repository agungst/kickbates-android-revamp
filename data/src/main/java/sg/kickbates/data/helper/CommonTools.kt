package sg.kickbates.data.helper


import androidx.room.util.StringUtil
import sg.kickbates.data.R
import java.math.RoundingMode
import java.text.DecimalFormat
import java.text.SimpleDateFormat
import java.util.*
import java.util.regex.Pattern

/**
 * Created by simx on 20,November,2019
 * MM-dd-yyyy HH:mm
 */
object CommonTools {

    fun currentDateFrom(day:Int, month:Int, year:Int): String? {
        val cal = Calendar.getInstance()
        cal.set(Calendar.DAY_OF_YEAR, day)
        cal.set(Calendar.MONTH, month)
        cal.set(Calendar.YEAR, year)
        val sdfResults = SimpleDateFormat("dd-MM-yyyy", Locale.US)
        return sdfResults.format(cal.timeInMillis)
    }
    fun currentDateYearFirstFrom(day:Int, month:Int, year:Int): String? {
        val cal = Calendar.getInstance()
        cal.set(Calendar.DAY_OF_YEAR, day)
        cal.set(Calendar.MONTH, month)
        cal.set(Calendar.YEAR, year)
        val sdfResults = SimpleDateFormat("yyyy-MM-dd", Locale.US)
        return sdfResults.format(cal.timeInMillis)
    }
    fun formatDate(value:String?):String{
        return if (value.isNullOrEmpty()) "-"
        else {
            val sdf = SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.US)
            val sdfResults = SimpleDateFormat("MMM d, yyyy", Locale.US)
            val date = sdf.parse(value)
            sdfResults.format(date?.time)
        }
    }
    fun formatDateTime(value:String?):String{
        return if (value.isNullOrEmpty()) "-"
        else {
            val sdf = SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.US)
            val sdfResults = SimpleDateFormat("MMM d, yyyy kk:mm", Locale.US)
            val date = sdf.parse(value)
            sdfResults.format(date?.time)
        }
    }
    fun formatTimeAmPm(value:String?):String{
        return if (value.isNullOrEmpty()) "-"
        else {
            val sdf = SimpleDateFormat("HH:mm", Locale.US)
            val sdfResults = SimpleDateFormat("hh.mm aa", Locale.US)
            val date = sdf.parse(value)
            sdfResults.format(date?.time)
        }

    }

    fun isDateAfter(value:String?): Boolean? {
        return if (value.isNullOrEmpty()) false
        else {
            val cal = Calendar.getInstance()
            cal.timeInMillis = System.currentTimeMillis()
            val sdf = SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.US)
            val date = sdf.parse(value)
            cal.time.before(date)
        }

    }

    fun formatDecimal2Digit(value:Double):Double {
        val df = DecimalFormat("#,##")
        df.roundingMode = RoundingMode.CEILING
        return df.format(value).toDouble()
    }

    fun formatDollarCurrency(value:Double):String? {
        val nf =DecimalFormat.getCurrencyInstance(Locale.US)
        nf.roundingMode = RoundingMode.CEILING
        return nf.format(value)
    }

    fun cardLogo(name:String?):Int? {
        return when(name) {
            "Visa"-> R.drawable.logo_visa
            "visa"-> R.drawable.logo_visa

            "matercard"-> R.drawable.logo_mastercard
            "MasterCard"-> R.drawable.logo_mastercard

            "jcb"-> R.drawable.logo_jcb
            "JCB"-> R.drawable.logo_jcb

            "AE"-> R.drawable.logo_jcb
            "AmericanExpress"-> R.drawable.logo_american_express
            "americanexpress"-> R.drawable.logo_american_express
            "american_express"-> R.drawable.logo_american_express

            else -> R.drawable.logo_generic
        }
    }

    fun isNumeric(value:String?):Boolean {
        val pattern = Pattern.compile("\"-?\\d+(\\.\\d+)?\"")
        return  if (value.isNullOrEmpty()) false else pattern.matcher(value).matches()
    }

    fun validateString(value:String?):Boolean {
        return value?.substring(0).equals(".",true)
    }
}