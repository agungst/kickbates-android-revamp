package sg.kickbates.data.helper

import android.annotation.SuppressLint
import android.content.Context
import android.content.SharedPreferences

/**
 * Created by simx on 21,September,2019
 */
object PrefBuilder {
    /**
     * Preference name
     * for create new preference
     */
    const val PREF_NAME_LOGIN ="login"
    const val PREF_NAME_ACCOUNT = "account"
    const val PREF_NAME_APPS = "apps"

    const val PREF_KEY_LOGIN_METHOD = "login_method"
    const val PREF_KEY_TOKEN = "token"

    private fun build(context: Context, name:String): SharedPreferences? {
        return context.getSharedPreferences(name, Context.MODE_PRIVATE)
    }

    @SuppressLint("CommitPrefEdits")
    fun createPref(context: Context, name:String, key:String, value:String){
        build(name = name, context = context)?.edit()?.putString(key, value)?.apply()
    }
    @SuppressLint("CommitPrefEdits")
    fun createPref(context: Context, name:String, key:String, value:Int){
        build(name = name, context = context)?.edit()?.putInt(key, value)?.apply()
    }
    @SuppressLint("CommitPrefEdits")
    fun createPref(context: Context, name:String, key:String, value:Boolean){
        build(name = name, context = context)?.edit()?.putBoolean(key, value)?.apply()
    }

}