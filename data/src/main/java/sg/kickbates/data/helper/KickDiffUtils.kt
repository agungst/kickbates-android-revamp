package sg.kickbates.data.helper

import androidx.recyclerview.widget.DiffUtil
import sg.kickbates.data.api.models.ResponseDeals
import sg.kickbates.data.api.models.ResponseMerchants
import sg.kickbates.data.api.models.cash_back.ResponseCashBackHistory
import sg.kickbates.data.api.models.cash_out.ResponseCashOutHistory
import sg.kickbates.data.api.models.payment.DetailInvoice
import sg.kickbates.data.api.models.voucher.ResponseVouchers

/**
 * Created by simx on 20,November,2019
 */
class KickDiffUtils {
    companion object {
        val MerchantsDiff = object : DiffUtil.ItemCallback<ResponseMerchants.DataItem>(){

            override fun areItemsTheSame(
                oldItem: ResponseMerchants.DataItem,
                newItem: ResponseMerchants.DataItem
            ): Boolean {
                return oldItem.id == newItem.id
            }

            override fun areContentsTheSame(
                oldItem: ResponseMerchants.DataItem,
                newItem: ResponseMerchants.DataItem
            ): Boolean {
                return oldItem.id == newItem.id
            }
        }
        val DealsDiff = object : DiffUtil.ItemCallback<ResponseDeals.DataItem>(){

            override fun areItemsTheSame(
                oldItem: ResponseDeals.DataItem,
                newItem: ResponseDeals.DataItem
            ): Boolean {
                return oldItem.id == newItem.id
            }
            override fun areContentsTheSame(
                oldItem: ResponseDeals.DataItem,
                newItem: ResponseDeals.DataItem
            ): Boolean {
                return oldItem.id == newItem.id
            }
        }
        val VoucherDiff = object : DiffUtil.ItemCallback<ResponseVouchers.DataItem>(){

            override fun areItemsTheSame(
                oldItem: ResponseVouchers.DataItem,
                newItem: ResponseVouchers.DataItem
            ): Boolean {
                return oldItem.id == newItem.id
            }
            override fun areContentsTheSame(
                oldItem: ResponseVouchers.DataItem,
                newItem: ResponseVouchers.DataItem
            ): Boolean {
                return oldItem.id == newItem.id
            }
        }
        val TransactionHistoryDiff = object : DiffUtil.ItemCallback<DetailInvoice>(){

            override fun areItemsTheSame(oldItem: DetailInvoice, newItem: DetailInvoice): Boolean {
                return oldItem.id == newItem.id
            }
            override fun areContentsTheSame(
                oldItem: DetailInvoice,
                newItem: DetailInvoice
            ): Boolean {
                return oldItem.id == newItem.id
            }

        }

        val CashBackHistoryDiff = object : DiffUtil.ItemCallback<ResponseCashBackHistory.DataItem>(){

            override fun areItemsTheSame(oldItem: ResponseCashBackHistory.DataItem, newItem: ResponseCashBackHistory.DataItem): Boolean {
                return oldItem.id == newItem.id
            }
            override fun areContentsTheSame(
                oldItem: ResponseCashBackHistory.DataItem,
                newItem: ResponseCashBackHistory.DataItem
            ): Boolean {
                return oldItem.id == newItem.id
            }

        }

        val CashOutHistoryDiff = object : DiffUtil.ItemCallback<ResponseCashOutHistory.DataItem>(){

            override fun areItemsTheSame(oldItem: ResponseCashOutHistory.DataItem, newItem: ResponseCashOutHistory.DataItem): Boolean {
                return oldItem.id == newItem.id
            }
            override fun areContentsTheSame(
                oldItem: ResponseCashOutHistory.DataItem,
                newItem: ResponseCashOutHistory.DataItem
            ): Boolean {
                return oldItem.id == newItem.id
            }

        }
    }
}