package sg.kickbates.data.helper

/**
 * Created by simx on 21,September,2019
 */
enum class States {
    LOADING, DONE
}