package sg.kickbates.data.paging

import androidx.lifecycle.MutableLiveData
import androidx.paging.DataSource
import sg.kickbates.data.api.models.ResponseDeals
import sg.kickbates.data.api.models.ResponseMerchants

/**
 * Created by simx on 20,November,2019
 */
class DealsDataSourceFactory: DataSource.Factory<Int, ResponseDeals.DataItem>(){
    var deals : MutableLiveData<DealsDataSource> ? = null
    private var source: DealsDataSource? = null

    init {
        deals = MutableLiveData()
    }

    private var token:String? = null
    private var name:String? = null
    private var code:String? = null
    private var lat:String? = null
    private var lng:String? = null

    override fun create(): DataSource<Int, ResponseDeals.DataItem> {
        source = DealsDataSource(token, lat, lng, name, code)
        deals?.postValue(source)
        return source!!
    }

    fun query(_token:String?, _name:String?, _code:String?, _lat:String?, _lng:String?){
        this.token  = _token
        this.name   = _name
        this.code   = _code
        this.lat    = _lat
        this.lng    = _lng
    }
}