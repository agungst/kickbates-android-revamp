package sg.kickbates.data.paging

import androidx.lifecycle.MutableLiveData
import androidx.paging.PageKeyedDataSource
import kotlinx.coroutines.CoroutineExceptionHandler
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.SupervisorJob
import kotlinx.coroutines.launch
import retrofit2.HttpException
import sg.kickbates.data.BuildConfig
import sg.kickbates.data.api.Api
import sg.kickbates.data.api.models.payment.DetailInvoice

import sg.kickbates.data.helper.States

/**
 * Created by simx on 20,November,2019
 */
class PaymentHistoryDataSource(
    private var token:String?,
    private var userId:Int?

): PageKeyedDataSource<Int, DetailInvoice>() {
    private var job          = SupervisorJob()
    var state:MutableLiveData<States>       = MutableLiveData()
    var errorMsg:MutableLiveData<String>    = MutableLiveData()
    var empty:MutableLiveData<Boolean>      = MutableLiveData()

    override fun loadInitial(
        params: LoadInitialParams<Int>,
        callback: LoadInitialCallback<Int, DetailInvoice>
    ) {
        state.postValue(States.LOADING)
        try {
            CoroutineScope(CoroutineExceptionHandler { _, throwable -> run {
                job = SupervisorJob()
                throwable.printStackTrace()
                updateMsg(throwable.message)
            } }).launch {
                val res = Api.Factory.create(BuildConfig.BASE_URL).paymentHistoryAsync(token,userId, 1).await()
                run {
                    when (res.status) {
                        200 -> {
                            empty.postValue(res.data.isNullOrEmpty())
                            if (res.data.isNullOrEmpty()) callback.onResult(listOf(), null, 2)
                            else callback.onResult(res.data.filter { it.invoicePaidBy == 1 || it.invoicePaidBy == 2 || it.invoicePaidBy == 3 },null, 2)
                        }
                        else -> {
                            updateMsg(res.message)
                        }
                    }
                }
                state.postValue(States.DONE)
            }
        }catch (e: HttpException){
            e.printStackTrace()
            updateMsg(e.message)
        }
    }


    override fun loadAfter(
        params: LoadParams<Int>,
        callback: LoadCallback<Int, DetailInvoice>
    ) {

    }

    override fun loadBefore(
        params: LoadParams<Int>,
        callback: LoadCallback<Int, DetailInvoice>
    ) {
        state.postValue(States.LOADING)
        try {
            CoroutineScope(CoroutineExceptionHandler { _, throwable -> run {
                job = SupervisorJob()
                throwable.printStackTrace()
                updateMsg(throwable.message)
            } }).launch {
                val res = Api.Factory.create(BuildConfig.BASE_URL).paymentHistoryAsync(token,userId, params.key).await()
                run  {
                    when (res.status) {
                        200 -> {
                            empty.postValue(res.data.isNullOrEmpty())
                            if (res.data.isNullOrEmpty()) callback.onResult(listOf(), params.key + 1)
                            else callback.onResult(res.data.filter { it.invoicePaidBy == 1 || it.invoicePaidBy == 2 || it.invoicePaidBy == 3}, params.key + 1)
                        }
                        else -> {
                            updateMsg(res.message)
                        }
                    }
                }
                state.postValue(States.DONE)
            }
        }catch (e: HttpException){
            e.printStackTrace()
            updateMsg(e.message)
        }
    }

    private fun updateMsg(msg:String?){
        empty.postValue(true)
        state.postValue(States.DONE)
        errorMsg.postValue(msg)
    }
}