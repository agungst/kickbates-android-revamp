package sg.kickbates.data.paging

import androidx.lifecycle.MutableLiveData
import androidx.paging.DataSource
import sg.kickbates.data.api.models.cash_out.ResponseCashOutHistory


/**
 * Created by simx on 02,January,2020
 */
class CashOutHistoryDataSourceFactory : DataSource.Factory<Int, ResponseCashOutHistory.DataItem>(){
    var history: MutableLiveData<CashOutHistoryDataSource> = MutableLiveData()
    private var source: CashOutHistoryDataSource? = null
    private var _token:String? = null
    private var _userId:Int? = null

    override fun create(): DataSource<Int, ResponseCashOutHistory.DataItem> {
        source = CashOutHistoryDataSource(_token, _userId)
        history.postValue(source)
        return  source!!
    }

    fun query(token:String?, userid:Int?){
        this._token = token
        this._userId = userid
    }
}