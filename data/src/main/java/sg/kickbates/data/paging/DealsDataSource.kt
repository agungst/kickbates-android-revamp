package sg.kickbates.data.paging

import androidx.lifecycle.MutableLiveData
import androidx.paging.PageKeyedDataSource
import kotlinx.coroutines.CoroutineExceptionHandler
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.SupervisorJob
import kotlinx.coroutines.launch
import retrofit2.HttpException
import sg.kickbates.data.BuildConfig
import sg.kickbates.data.api.Api
import sg.kickbates.data.api.models.ResponseDeals
import sg.kickbates.data.api.models.ResponseMerchants
import sg.kickbates.data.helper.States

/**
 * Created by simx on 20,November,2019
 */
class DealsDataSource(
    private var token:String?,
    private var lat:String?,
    private var lng:String?,
    private var name:String?,
    private var code:String?

): PageKeyedDataSource<Int, ResponseDeals.DataItem>() {
    private var job          = SupervisorJob()
    var state:MutableLiveData<States>       = MutableLiveData()
    var errorMsg:MutableLiveData<String>    = MutableLiveData()
    var empty:MutableLiveData<Boolean>      = MutableLiveData()

    override fun loadInitial(
        params: LoadInitialParams<Int>,
        callback: LoadInitialCallback<Int, ResponseDeals.DataItem>
    ) {
        state.postValue(States.LOADING)
        try {
            CoroutineScope(CoroutineExceptionHandler { _, throwable -> run {
                job = SupervisorJob()
                throwable.printStackTrace()
                updateMsg(throwable.message)
            } }).launch {
                val res = Api.Factory.create(BuildConfig.BASE_URL).dealsAsync(token, 1, lat, lng, name, code).await()
                res.let {
                    when (it.status) {
                         200 -> {
                             if (res.data.isNullOrEmpty()) callback.onResult(listOf(), null, 2)
                             else callback.onResult(res.data,null, 2)
                         }
                         else -> {
                             updateMsg(res.message)
                         }
                   }
                }
                state.postValue(States.DONE)
            }
        }catch (e: HttpException){
            e.printStackTrace()
            updateMsg(e.message)
        }
    }


    override fun loadAfter(
        params: LoadParams<Int>,
        callback: LoadCallback<Int, ResponseDeals.DataItem>
    ) {

    }

    override fun loadBefore(
        params: LoadParams<Int>,
        callback: LoadCallback<Int, ResponseDeals.DataItem>
    ) {
        state.postValue(States.LOADING)
        try {
            CoroutineScope(CoroutineExceptionHandler { _, throwable -> run {
                job = SupervisorJob()
                throwable.printStackTrace()
                updateMsg(throwable.message)
            } }).launch {
                val res = Api.Factory.create(BuildConfig.BASE_URL).dealsAsync(token, params.key, lat, lng, name, code).await()
                res.let {
                    when (it.status) {
                        200 -> {
                            if (res.data.isNullOrEmpty()) callback.onResult(listOf(), params.key + 1)
                            else callback.onResult(res.data, params.key + 1)
                        }
                        else -> {
                            updateMsg(res.message)
                        }
                    }
                }
                state.postValue(States.DONE)
            }
        }catch (e: HttpException){
            e.printStackTrace()
            updateMsg(e.message)
        }
    }

    private fun updateMsg(msg:String?){
        state.postValue(States.DONE)
        errorMsg.postValue(msg)
    }
}