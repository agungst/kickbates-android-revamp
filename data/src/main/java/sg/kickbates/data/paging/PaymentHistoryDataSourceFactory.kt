package sg.kickbates.data.paging

import androidx.lifecycle.MutableLiveData
import androidx.paging.DataSource
import sg.kickbates.data.api.models.payment.DetailInvoice


/**
 * Created by simx on 20,November,2019
 */
class PaymentHistoryDataSourceFactory: DataSource.Factory<Int, DetailInvoice>(){
    var history : MutableLiveData<PaymentHistoryDataSource> ? = null
    private var source: PaymentHistoryDataSource? = null

    init {
        history = MutableLiveData()
    }

    private var token:String? = null
    private var userId:Int? = null

    override fun create(): DataSource<Int, DetailInvoice> {
        source = PaymentHistoryDataSource(token, userId)
        history?.postValue(source)
        return source!!
    }

    fun query(_token:String?, _userId:Int?){
        this.token  = _token
        this.userId = _userId
    }
}