package sg.kickbates.data.paging

import androidx.lifecycle.MutableLiveData
import androidx.paging.DataSource
import sg.kickbates.data.api.models.cash_back.ResponseCashBackHistory
import sg.kickbates.data.api.models.cash_out.ResponseCashOutHistory


/**
 * Created by simx on 02,January,2020
 */
class CashBackHistoryDataSourceFactory : DataSource.Factory<Int, ResponseCashBackHistory.DataItem>(){
    var history: MutableLiveData<CashBackHistoryDataSource> = MutableLiveData()
    private var source: CashBackHistoryDataSource? = null
    private var _token:String? = null
    private var _userId:Int? = null

    override fun create(): DataSource<Int, ResponseCashBackHistory.DataItem> {
        source = CashBackHistoryDataSource(_token, _userId)
        history.postValue(source)
        return  source!!
    }

    fun query(token:String?, userId:Int?){
        this._token = token
        this._userId = userId
    }
}