package sg.kickbates.data.paging

import androidx.lifecycle.MutableLiveData
import androidx.paging.DataSource
import sg.kickbates.data.api.models.ResponseMerchants

/**
 * Created by simx on 20,November,2019
 */
class MerchantsDataSourceFactory: DataSource.Factory<Int, ResponseMerchants.DataItem>(){
    var merchants : MutableLiveData<MerchantsDataSource> ? = null
    private var source: MerchantsDataSource? = null

    init {
        merchants = MutableLiveData()
    }

    private var token:String? = null
    private var name:String? = null
    private var code:String? = null
    private var lat:String? = null
    private var lng:String? = null

    override fun create(): DataSource<Int, ResponseMerchants.DataItem> {
        source = MerchantsDataSource(token, lat, lng, name, code)
        merchants?.postValue(source)
        return source!!
    }

    fun query(_token:String?, _name:String?, _code:String?, _lat:String?, _lng:String?){
        this.token  = _token
        this.name   = _name
        this.code   = _code
        this.lat    = _lat
        this.lng    = _lng
    }
}