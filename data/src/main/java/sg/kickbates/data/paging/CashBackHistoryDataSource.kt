package sg.kickbates.data.paging

import androidx.lifecycle.MutableLiveData
import androidx.paging.PageKeyedDataSource
import kotlinx.coroutines.CoroutineExceptionHandler
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.SupervisorJob
import kotlinx.coroutines.launch
import retrofit2.HttpException
import sg.kickbates.data.BuildConfig
import sg.kickbates.data.api.Api
import sg.kickbates.data.api.models.cash_back.ResponseCashBackHistory

/**
 * Created by simx on 02,January,2020
 */
class CashBackHistoryDataSource(private var token:String?, private var userId:Int?): PageKeyedDataSource<Int, ResponseCashBackHistory.DataItem>() {

    private var job  = SupervisorJob()

    var loading: MutableLiveData<Boolean> = MutableLiveData()
    var empty: MutableLiveData<Boolean> = MutableLiveData()
    var error: MutableLiveData<String> = MutableLiveData()

    override fun loadInitial(params: LoadInitialParams<Int>, callback: LoadInitialCallback<Int, ResponseCashBackHistory.DataItem>) {
        loading.postValue(true)
        try {
            CoroutineScope(CoroutineExceptionHandler { _, throwable -> run {
                job = SupervisorJob()
                throwable.printStackTrace()
                updateMsg(BuildConfig.ERROR_MSG)
            } }).launch {
                val res = Api.Factory.create(BuildConfig.BASE_URL).cashBackHistoryAsync(token, userId, 1).await()
                res.data?.filterNot { it.earnStatus == 2 }.let { it?.let { it1 ->
                    callback.onResult(
                        it1, null, 2)
                } }
                empty.postValue(res.data.isNullOrEmpty())
                loading.postValue(false)
            }
        }catch (e: HttpException){
            e.printStackTrace()
            updateMsg(BuildConfig.ERROR_MSG)
        }
    }

    override fun loadAfter(params: LoadParams<Int>, callback: LoadCallback<Int, ResponseCashBackHistory.DataItem>) {

    }

    override fun loadBefore(params: LoadParams<Int>, callback: LoadCallback<Int, ResponseCashBackHistory.DataItem>) {
        loading.postValue(true)
        try {
            CoroutineScope(CoroutineExceptionHandler { _, throwable -> run {
                job = SupervisorJob()
                throwable.printStackTrace()
                updateMsg(BuildConfig.ERROR_MSG)
            } }).launch {
                val res = Api.Factory.create(BuildConfig.BASE_URL).cashBackHistoryAsync(token,userId, params.key).await()
                res.data?.filterNot { it.earnStatus == 2 }.let { it?.let { it1 ->
                    callback.onResult(
                        it1, params.key.plus(1))
                } }
                empty.postValue(res.data.isNullOrEmpty())
                loading.postValue(false)
            }
        }catch (e: HttpException){
            e.printStackTrace()
            updateMsg(BuildConfig.ERROR_MSG)
        }
    }

    private fun updateMsg(msg:String?){
        loading.postValue(false)
        error.postValue(msg)
        empty.postValue(true)
    }
}