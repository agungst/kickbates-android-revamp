package sg.kickbates.customer.ui.voucher

import androidx.databinding.BaseObservable
import androidx.databinding.Bindable
import androidx.databinding.ObservableField
import sg.kickbates.data.api.models.voucher.ResponseVouchers

/**
 * Created by simx on 11,August,2019
 */
class ItemVouchersVM(data: ResponseVouchers.DataItem?): BaseObservable() {
    @Bindable var image = ObservableField(data?.urlImage())
}