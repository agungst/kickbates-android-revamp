package sg.kickbates.customer.ui.main.cards.details.history


import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView

import sg.kickbates.customer.R
import sg.kickbates.customer.databinding.TransactionsCardHistoryFragmentBinding
import sg.kickbates.customer.ui.main.cards.details.adapter.AdapterUnBilledTransactions
import sg.kickbates.customer.ui.main.pay.PayActivity
import sg.kickbates.data.api.models.card.ResponseCustomerCards
import sg.kickbates.data.helper.Pref


/**
 * A simple [Fragment] subclass.
 *
 */
class TransactionsCardHistoryFragment : Fragment() {
    private lateinit var binding:TransactionsCardHistoryFragmentBinding
    private lateinit var vm:TransactionsCardHistoryVM
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater,R.layout.transactions_card_history_fragment, container,false)
        vm = TransactionsCardHistoryVM(Pref(this.context!!))
        binding.lifecycleOwner = this
        binding.transactionsCardHistoryVm = vm
        val data: ResponseCustomerCards.DataItem? = arguments?.getParcelable(KEY_DATA)
        vm.transactions.postValue(data?.kickCustomerCardStatements?.filter { it.status == 2 })
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        vm.transactions.observe(this, Observer { data -> run {
            vm.empty.set(data.isNullOrEmpty())
            binding.rcvTransactions.apply {
                hasFixedSize()
                itemAnimator  = DefaultItemAnimator()
                layoutManager = LinearLayoutManager(this.context!!, RecyclerView.VERTICAL, false)
                adapter       = AdapterUnBilledTransactions(data, object : AdapterUnBilledTransactions.OnAdapterUnBilledTransactionsListener{
                    override fun onAdapterClicked(data: ResponseCustomerCards.DataItem.DataCardStatements) {

                    }

                    override fun onBottomPayClicked(data: ResponseCustomerCards.DataItem.DataCardStatements) {
                        var intent = Intent(this@TransactionsCardHistoryFragment.context!!, PayActivity::class.java)
                        intent.putExtra(PayActivity.KEY_TYPE, PayActivity.TYPE_CARD)
                        intent.putExtra(PayActivity.KEY_DATA, data)
                        startActivityForResult(intent, PayActivity.RC_PAYMENT)
                    }
                })
            }
        } })

    }
    companion object {
        const val KEY_DATA = "key_data"
        fun instance(data: ResponseCustomerCards.DataItem?) : TransactionsCardHistoryFragment{
            var bundle =  Bundle()
            bundle.putParcelable(KEY_DATA, data)
            var fragment = TransactionsCardHistoryFragment()
            fragment.arguments = bundle
            return fragment
        }
    }
}
