package sg.kickbates.customer.ui.dialog.booking

import android.app.AlertDialog
import android.content.DialogInterface
import android.graphics.Color
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import androidx.databinding.DataBindingUtil
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog
import com.wdullaer.materialdatetimepicker.time.TimePickerDialog
import sg.kickbates.customer.R
import sg.kickbates.customer.databinding.BookingDialogBinding
import sg.kickbates.data.api.models.ResponseMerchants
import sg.kickbates.data.api.models.booking.RequestBooking
import sg.kickbates.data.helper.CommonTools
import sg.kickbates.data.helper.Pref
import java.util.*

/**
 * Created by simx on 10,August,2019
 */
class BookingDialog constructor(private var data: ResponseMerchants.DataItem?, private var listener: BookingDialogListener): BottomSheetDialogFragment() {
    private lateinit var binding:BookingDialogBinding
    private lateinit var vm:BookingDialogVM
    private lateinit var pref: Pref
    private var dateSelected:String? = null
    private var timeSelected:String? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.booking_dialog, container,false)
        pref = Pref(this.context!!)
        vm = BookingDialogVM(pref)
        binding.lifecycleOwner = this
        binding.bookingDialogVm = vm
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.etDate.setOnClickListener { showDatePicker(binding.etDate) }
        binding.etDate.setOnFocusChangeListener { _, hasFocus -> run {
            if (hasFocus)showTimePicker(binding.etDate)
        } }



        binding.etTime.setOnClickListener { showTimePicker(binding.etTime) }
        binding.etTime.setOnFocusChangeListener { _, hasFocus -> run {
            if (hasFocus)showTimePicker(binding.etTime)
        } }

        binding.etNoPax.setOnClickListener { showDialogPax() }
        binding.etNoPax.setOnFocusChangeListener { _, hasFocus -> run {
            if (hasFocus)showDialogPax()
        } }

        binding.btnBook.setOnClickListener {
            if (binding.etPhone.text.isNullOrBlank()){
                binding.etPhone.requestFocus()
                binding.lytPhone.error = "Please provide your phone number"
            }else {
                var requestBooking = RequestBooking()
                val phone = binding.etPhone.text.toString()
                requestBooking.cellphone = phone
                if (!binding.etNoPax.text.isNullOrBlank()){
                    val noPax = binding.etNoPax.text.toString()
                    requestBooking.noOfPax = noPax.toInt()
                }
                requestBooking.date = dateSelected
                requestBooking.time = timeSelected
                requestBooking.usersId = pref.usersId
                requestBooking.kickMerchantId = data?.id
                vm.createBooking(requestBooking)
            }
        }
        vm.booking.observe(this, androidx.lifecycle.Observer { data -> run {
            dialog?.dismiss()
            if (data == null) listener.onFailedDismissListener() else listener.onSuccessDismissListener()
        } })
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        view?.setBackgroundColor(Color.TRANSPARENT)
    }

    private fun showDatePicker(view: EditText){
        val cal = Calendar.getInstance()
        var dialog = DatePickerDialog.newInstance({
            /**
             * @param view        The view associated with this listener.
             * @param year        The year that was set.
             * @param monthOfYear The month that was set (0-11) for compatibility
             * with [java.util.Calendar].
             * @param dayOfMonth  The day of the month that was set.
             */ _, year, monthOfYear, dayOfMonth -> run {
            dateSelected = CommonTools.currentDateYearFirstFrom(dayOfMonth, monthOfYear.plus(1),year)
            view.setText(dateSelected)
        }
        }, cal.get(Calendar.YEAR),cal.get(Calendar.MONTH), cal.get(Calendar.DAY_OF_MONTH))
        dialog.version = DatePickerDialog.Version.VERSION_2
        dialog.accentColor = Color.parseColor("#1f908a")

        //dialog.setCancelColor(Color.parseColor("#787878"))
        dialog.setCancelColor(Color.parseColor("#ffffff"))
        //dialog.setOkColor(Color.parseColor("#1f908a"))
        dialog.setOkColor(Color.parseColor("#ffffff"))
        dialog.show(childFragmentManager, BookingDialog::class.java.simpleName)
    }

    private fun showTimePicker(view: EditText){
        val cal = Calendar.getInstance()
        var dialog = TimePickerDialog.newInstance(
            {
                /**
                 * @param view The view associated with this listener.
                 * @param hourOfDay The hour that was set.
                 * @param minute The minute that was set.
                 * @param second The second that was set
                 */
                    _, hourOfDay, minute, _ -> run {
                timeSelected = "$hourOfDay:$minute"

                view.setText(CommonTools.formatTimeAmPm(timeSelected))

            }},
            cal.get(Calendar.HOUR_OF_DAY), cal.get(Calendar.MINUTE), cal.get(Calendar.SECOND), true)

        dialog?.setTimeInterval(1,15)
        dialog.version = TimePickerDialog.Version.VERSION_2

        dialog.accentColor = Color.parseColor("#1f908a")
        //dialog.setCancelColor(Color.parseColor("#787878"))
        dialog.setCancelColor(Color.parseColor("#ffffff"))
        //dialog.setOkColor(Color.parseColor("#1f908a"))
        dialog.setOkColor(Color.parseColor("#ffffff"))
        dialog.show(childFragmentManager, BookingDialog::class.java.simpleName)
    }

    private fun showDialogPax(){
        var builder = AlertDialog.Builder(this.context!!)
        builder.setTitle("No of Pax")
        var paxs:Array<out String> = arrayOf("1","2","3","4","5","6","7","8","9","10","11","12","13","14","15","16","17","18","19","20")
        builder.setSingleChoiceItems(paxs ,0

        ) { dialog, which ->  run {
            dialog.dismiss()
            binding.etNoPax.setText(paxs[which])
        }}

        var d = builder.create()
        d.show()
    }
    interface BookingDialogListener {
        fun onSuccessDismissListener()
        fun onFailedDismissListener()
    }
}