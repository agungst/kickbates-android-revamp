package sg.kickbates.customer.ui.deals.list

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import androidx.recyclerview.widget.RecyclerView
import sg.kickbates.customer.R
import sg.kickbates.customer.databinding.ItemCreditCardSmallBinding
import sg.kickbates.customer.databinding.ItemDealsSmallBinding
import sg.kickbates.customer.databinding.ItemMerchantsBinding
import sg.kickbates.data.api.models.ResponseDeals

/**
 * Created by simx on 31,July,2019
 */
class AdapterDealsList(private var datas:List<ResponseDeals.DataItem>, private var listener: OnAdapterDealsListListener): RecyclerView.Adapter<AdapterDealsList.Holder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): Holder {
        return Holder(ItemDealsSmallBinding.bind(LayoutInflater.from(parent.context).inflate(R.layout.item_deals_small,parent,false)))
    }

    override fun getItemCount(): Int {
        return datas.size
    }

    override fun onBindViewHolder(holder: Holder, position: Int) {
        holder.bind(datas[position])
        holder.itemView.setOnClickListener { listener.onClicked(datas[position]) }

    }

    interface OnAdapterDealsListListener {
        fun onClicked(data: ResponseDeals.DataItem)
    }
    class Holder(var binding: ItemDealsSmallBinding):RecyclerView.ViewHolder(binding.root) {
        fun bind(data: ResponseDeals.DataItem){
            with(binding){
                itemDealVm = ItemDealsVM(data)
                executePendingBindings()
            }
        }

    }
}