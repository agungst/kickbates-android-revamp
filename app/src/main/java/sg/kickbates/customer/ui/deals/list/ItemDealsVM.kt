package sg.kickbates.customer.ui.deals.list

import androidx.databinding.BaseObservable
import androidx.databinding.Bindable
import androidx.databinding.ObservableField
import sg.kickbates.data.api.models.ResponseDeals

/**
 * Created by simx on 31,July,2019
 */
class ItemDealsVM(data:ResponseDeals.DataItem):BaseObservable() {
    @Bindable var image = ObservableField(data.image())
    @Bindable var name = ObservableField(data.dealName)
}