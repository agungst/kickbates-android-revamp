package sg.kickbates.customer.ui.splash

import androidx.databinding.BaseObservable
import androidx.databinding.Bindable
import androidx.lifecycle.MutableLiveData
import kotlinx.coroutines.CoroutineExceptionHandler
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.SupervisorJob
import kotlinx.coroutines.launch
import retrofit2.HttpException
import sg.kickbates.data.BuildConfig
import sg.kickbates.data.api.Api

import sg.kickbates.data.helper.Pref

/**
 * Created by simx on 30,July,2019
 */
class SplashActivityVM(private var pref:Pref?):BaseObservable() {
    private var job = SupervisorJob()
    @Bindable var isValid = MutableLiveData<Boolean>()
    @Bindable var error = MutableLiveData<String>()

    fun checkIsValidToken() {
        try {
            CoroutineScope(CoroutineExceptionHandler { _, throwable -> run {
                throwable.printStackTrace()
                job = SupervisorJob()
                error.postValue(BuildConfig.ERROR_MSG)
                isValid.postValue(false)
            } }).launch {
                var res = Api.Factory.create(BuildConfig.BASE_URL).meFromIdAsync(pref?.token, pref?.usersId).await()
                if (res.data != null) {
                    pref?.usersId = res.data?.id
                    pref?.premiumAccount = res.data?.isPremiumAccount()!!
                    res.data?.subscribePremium().let {
                        pref?.subscribePremiumAccount = it!!
                    }
                }
                isValid.postValue(true)

            }
        }catch (e:HttpException){
            isValid.postValue(false)
            e.printStackTrace()
            error.postValue(BuildConfig.ERROR_MSG)
        }
    }
}