package sg.kickbates.customer.ui.main.cards.dialog

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.FragmentManager
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.*
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import sg.kickbates.customer.R
import sg.kickbates.customer.databinding.DialogCardsAvailableBinding
import sg.kickbates.customer.helper.SnackBarHelper
import sg.kickbates.customer.ui.auth.AuthWebViewActivity
import sg.kickbates.data.api.models.RequestCard
import sg.kickbates.data.api.models.card.ResponseCardAvaiable
import sg.kickbates.data.helper.Pref

/**
 * Created by simx on 13,November,2019
 */
class DialogCardsAvailable constructor(private var callback: OnDialogCardsAvailableCallback): BottomSheetDialogFragment() {
    private lateinit var binding:DialogCardsAvailableBinding
    private lateinit var vm:DialogCardsAvailableVM
    private var requestCard = RequestCard()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.dialog_cards_available, container,false)
        vm = DialogCardsAvailableVM(Pref(this.context!!))
        binding.lifecycleOwner = this
        binding.dialogCardsAvailableVm = vm
        vm.dataBanks(false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.imgClose.setOnClickListener { dialog?.dismiss() }
        vm.banks.observe(this, Observer { data -> run {
            vm.empty.set(data.isEmpty())
            binding.rcvBanks.apply {
                hasFixedSize()
                itemAnimator = DefaultItemAnimator()
                layoutManager = LinearLayoutManager(this@DialogCardsAvailable.context!!, RecyclerView.VERTICAL, false)
                adapter = AdapterDialogCardsAvailable(data, object : AdapterDialogCardsAvailable.OnAdapterDialogCardsAvailableCallback {
                    override fun onClicked(data: ResponseCardAvaiable.DataItem) {
                        requestCard.issuer = data.bankSlug
                        vm.authCard(data.bankSlug)
                    }
                })
            }
        } })
        vm.dataAuth.observe(this, Observer { url -> run {
            if (url != null){
                var intent = Intent(this@DialogCardsAvailable.context!!, AuthWebViewActivity::class.java)
                requestCard.url = url
                intent.putExtra(AuthWebViewActivity.KEY_DATA, requestCard)
                startActivityForResult(intent,AuthWebViewActivity.RC_WEB_VIEW)
            }
        } })
        vm.error.observe(this, Observer { msg -> run {
            SnackBarHelper.showSnackBarError(msg,binding.rootView)
        } })
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        when (resultCode) {
            Activity.RESULT_OK -> {
                when (requestCode) {
                    AuthWebViewActivity.RC_WEB_VIEW -> {
                        var requestCard :RequestCard? = data?.extras?.getParcelable(AuthWebViewActivity.KEY_DODE)
                        Toast.makeText(this.context!!, "${requestCard?.code_auth}", Toast.LENGTH_LONG).show()
                        callback.onDialogDismiss(requestCard)
                        dialog?.dismiss()
                    }
                    else -> {
                    }
                }
            }
            else -> {
                dialog?.dismiss()
            }
        }
        super.onActivityResult(requestCode, resultCode, data)
    }
    interface OnDialogCardsAvailableCallback {
        fun onDialogDismiss(code:RequestCard?)
    }
    companion object {
        fun show(fm:FragmentManager, callback: OnDialogCardsAvailableCallback){
            var fragment = DialogCardsAvailable(callback)
            fragment.show(fm, DialogCardsAvailable::class.java.simpleName)
        }
    }
}