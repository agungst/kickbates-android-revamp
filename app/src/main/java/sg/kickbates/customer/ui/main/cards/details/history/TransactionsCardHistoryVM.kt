package sg.kickbates.customer.ui.main.cards.details.history

import androidx.databinding.BaseObservable
import androidx.databinding.Bindable
import androidx.databinding.ObservableField
import androidx.lifecycle.MutableLiveData
import sg.kickbates.data.api.models.card.ResponseCustomerCards
import sg.kickbates.data.helper.Pref

/**
 * Created by simx on 11,August,2019
 */
class TransactionsCardHistoryVM(private var pref: Pref):BaseObservable() {

    @Bindable var errorMsg = MutableLiveData<String>()
    @Bindable var loading = ObservableField<Boolean>()
    @Bindable var empty = ObservableField<Boolean>()
    @Bindable var transactions = MutableLiveData<List<ResponseCustomerCards.DataItem.DataCardStatements>>()

}