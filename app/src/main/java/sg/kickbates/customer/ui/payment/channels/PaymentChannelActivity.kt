package sg.kickbates.customer.ui.payment.channels

import android.app.Activity
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView

import sg.kickbates.customer.R
import sg.kickbates.customer.databinding.PaymentChannelActivityBinding
import sg.kickbates.customer.helper.DialogHelper
import sg.kickbates.customer.helper.SnackBarHelper
import sg.kickbates.customer.ui.cash_out.DialogBank
import sg.kickbates.customer.ui.main.cards.add.AddStripeCardActivity
import sg.kickbates.customer.ui.main.pay.PayCardActivity
import sg.kickbates.data.BuildConfig
import sg.kickbates.data.api.models.card.ResponseCardAvaiable
import sg.kickbates.data.api.models.card.ResponseCustomerCards
import sg.kickbates.data.api.models.payment.DetailInvoice
import sg.kickbates.data.api.models.payment.ResponsePaymentChannelCard
import sg.kickbates.data.helper.Pref

class PaymentChannelActivity : AppCompatActivity() {
    private lateinit var binding:PaymentChannelActivityBinding
    private lateinit var vm:PaymentChannelVM
    private var invoiceId:Int? = null
    private var type:Int? = null
    private var dataCard: ResponseCustomerCards.DataItem.DataCardStatements? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this,R.layout.payment_channel_activity)
        vm = PaymentChannelVM(Pref(this))
        binding.lifecycleOwner = this
        binding.paymentChannelVm = vm
        setSupportActionBar(binding.toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        binding.tvTitleToolbar.text = "Payment Channels"
        invoiceId = intent?.extras?.getInt(KEY_DATA)
        type = intent?.extras?.getInt(KEY_TYPE)
        when (type) {
            KEY_TYPE_CARD -> {
                dataCard = intent?.extras?.getParcelable(KEY_DATA_STATEMENT)
                binding.tvAddNewCard.visibility = View.GONE
            }
            else -> vm.getPaymentChannel()
        }

        vm.channels.observe(this, Observer { data -> run {
            vm.empty.set(data.isNullOrEmpty())
            binding.rcvChannels.apply {
                hasFixedSize()
                itemAnimator = DefaultItemAnimator()
                layoutManager = LinearLayoutManager(this@PaymentChannelActivity, RecyclerView.VERTICAL, false)
                adapter = AdapterPaymentChannels(data, object : AdapterPaymentChannels.OnAdapterVouchersListener{
                    override fun onAdapterPaymentChannelsClicked(data: ResponsePaymentChannelCard.DataItem) {
                        vm.chargeCard(invoiceId, data.id)
                    }
                })
            }
        } })

        binding.tvAddNewCard.setOnClickListener {
            val addCardIntent = Intent(this, AddStripeCardActivity::class.java)
            addCardIntent.putExtra(AddStripeCardActivity.KEY_TYPE, type)
            addCardIntent.putExtra(AddStripeCardActivity.KEY_ID_INVOICE, invoiceId)
            startActivityForResult(addCardIntent, AddStripeCardActivity.RC_CODE)
        }
        vm.error.observe(this, Observer { msg -> run {
            SnackBarHelper.showSnackBarError(msg, binding.rootView, object : SnackBarHelper.OnSnackBarHelperListener {
                override fun onError() {

                }
            })
        } })

        binding.lytPayNow.setOnClickListener {
            DialogBank.show(supportFragmentManager, object : DialogBank.OnDialogBankListener {
                override fun onSelectedBank(data: ResponseCardAvaiable.DataItem) {
                    val intent = Intent(this@PaymentChannelActivity, PayCardActivity::class.java)
                    intent.putExtra(PayCardActivity.KEY_BANK_SLUG, data.bankSlug)
                    intent.putExtra(PayCardActivity.KEY_ID_INVOICE, invoiceId)
                    if (type == KEY_TYPE_CARD) intent.putExtra(PayCardActivity.KET_DATA_CARD, dataCard)
                    startActivityForResult(intent, PayCardActivity.RC_PAY_STATEMENT_CARD)

                }
            },false, isStatic = true)
        }
        vm.resultChargeCard.observe(this, Observer { data -> run {
            if (data != null) showDialogPaymentSuccess(data)
        } })

    }

    private fun  showDialogPaymentSuccess(data: DetailInvoice?){
        DialogHelper.showDialogSuccessPayment(this,type, data, object : DialogHelper.OnDialogSuccessListener {
            override fun onOk() {
                backToWithResultsOK(data)
            }
        })
    }
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        when (requestCode) {
            AddStripeCardActivity.RC_CODE -> {
                when (resultCode) {
                    Activity.RESULT_OK -> handleResultIntentAddCardStrip(data)
                    else -> SnackBarHelper.showSnackBarError("Failed Add", binding.rootView)
                }
            }
            PayCardActivity.RC_PAY_STATEMENT_CARD -> {
                when (resultCode) {
                    Activity.RESULT_OK -> backToWithResultsOK(null)
                    else -> SnackBarHelper.showSnackBarError(BuildConfig.ERROR_MSG, binding.rootView)
                }
            }
            else -> {
            }
        }

        super.onActivityResult(requestCode, resultCode, data)
    }

    private fun handleResultIntentAddCardStrip(data: Intent?) {
        val invoice:DetailInvoice? = data?.extras?.getParcelable(AddStripeCardActivity.KEY_DATA)
        backToWithResultsOK(invoice)
    }

    private fun  backToWithResultsOK(data: DetailInvoice?) {
        setResult(Activity.RESULT_OK)
        finish()
    }
    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> {
                setResult(Activity.RESULT_CANCELED)
                finish()
            }
            else -> {
            }
        }
        return super.onOptionsItemSelected(item)
    }
    companion object {
        const val KEY_VIEW_CHANNEL = 1232
        const val KEY_DATA = "channel"
        const val KEY_DATA_STATEMENT = "data_statement"
        const val KEY_TYPE = "type"

        const val KEY_TYPE_DEAL = 1
        const val KEY_TYPE_MERCHANT = 2
        const val KEY_TYPE_CARD = 3


    }
}
