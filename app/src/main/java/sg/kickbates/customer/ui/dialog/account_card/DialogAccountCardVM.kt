package sg.kickbates.customer.ui.dialog.account_card

import androidx.databinding.BaseObservable
import androidx.databinding.Bindable
import androidx.databinding.ObservableField
import androidx.lifecycle.MutableLiveData
import kotlinx.coroutines.CoroutineExceptionHandler
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.SupervisorJob
import kotlinx.coroutines.launch
import retrofit2.HttpException
import sg.kickbates.data.BuildConfig
import sg.kickbates.data.api.Api
import sg.kickbates.data.api.models.payment.RequestPayNow
import sg.kickbates.data.api.models.payment.ResponseChargeCard
import sg.kickbates.data.api.room.AppDatabase
import sg.kickbates.data.api.room.CardModel
import sg.kickbates.data.helper.Pref

/**
 * Created by simx on 30,January,2020
 */
class DialogAccountCardVM(private var pref: Pref, private var appDatabase: AppDatabase): BaseObservable() {


    private var job = SupervisorJob()
    @Bindable var loading = ObservableField<Boolean>()
    @Bindable var error = MutableLiveData<String>()
    @Bindable var results = MutableLiveData<ResponseChargeCard>()
    @Bindable var cards = MutableLiveData<List<CardModel>>()

    fun payNow(requestPayNow: RequestPayNow) {
        loading.set(true)
        try {
            CoroutineScope(CoroutineExceptionHandler { _, throwable -> run {
                job = SupervisorJob()
                throwable.printStackTrace()
                loading.set(false)
                error.postValue("Payment failed")
                results.postValue(null)
            } }).launch {
                val res = Api.Factory.create(BuildConfig.BASE_URL).payNowAsync(pref.token, requestPayNow).await()
                results.postValue(res)
                loading.set(false)
            }
        }catch (e: HttpException){
            e.printStackTrace()
            loading.set(false)
        }
    }

    fun getCards(){
        try {
            CoroutineScope(CoroutineExceptionHandler { _, throwable -> run {
                job = SupervisorJob()
                throwable.printStackTrace()
            } }).launch {
                appDatabase.cardDao().all().let { cards.postValue(it) }
            }
        }catch (e: HttpException){
            e.printStackTrace()
        }
    }
    fun saveCards(cardModel: CardModel?){
        try {
            CoroutineScope(CoroutineExceptionHandler { _, throwable -> run {
                job = SupervisorJob()
                throwable.printStackTrace()
            } }).launch {
                appDatabase.cardDao().insert(cardModel)
            }
        }catch (e: HttpException){
            e.printStackTrace()
        }
    }

}