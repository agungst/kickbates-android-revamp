package sg.kickbates.customer.ui.main.cards.details

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.graphics.Color
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.MenuItem
import androidx.databinding.DataBindingUtil
import coil.Coil
import coil.api.load
import coil.size.Scale
import sg.kickbates.customer.R
import sg.kickbates.customer.databinding.CardListActivityBinding
import sg.kickbates.customer.databinding.CardsDetailsActivityBinding
import sg.kickbates.data.api.models.card.ResponseCustomerCards

class CardsDetailsActivity : AppCompatActivity() {
    private lateinit var binding:CardsDetailsActivityBinding
    private lateinit var vm:CardsDetailsVM

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this,R.layout.cards_details_activity)
        vm = CardsDetailsVM()
        binding.lifecycleOwner = this
        binding.cardDetailsVm = vm
        setSupportActionBar(binding.toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        binding.tvTitleToolbar.text = "Card"
        run {
            var data : ResponseCustomerCards.DataItem? = intent.extras?.getParcelable(KEY_DATA)
            initViewCard(data)
            binding.vpCardsDetails.adapter = AdapterPageDetailCards(supportFragmentManager, data)
            binding.tabs.setupWithViewPager(binding.vpCardsDetails, true)
        }

    }

    @SuppressLint("SetTextI18n")
    private fun initViewCard(data: ResponseCustomerCards.DataItem?) {

        data?.kickBanks?.bankIcon().let {
            binding.imgLogo.load(it) {
                size(80 * 30)
            }
        }
        binding.tvCardNumber.text = "**** **** **** ${data?.cardNo}"
        binding.tvBalance.text = "Outstading: $ ${data?.balance()}"
        binding.lytCard.setCardBackgroundColor(Color.parseColor("#${data?.kickBanks?.bankCardColor}"))

    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> {
                onBackPressed()
            }
            else -> {
            }
        }
        return super.onOptionsItemSelected(item)
    }
    companion object {
        private const val KEY_DATA = "key_data"
        fun start(context: Context, data: ResponseCustomerCards.DataItem){
            val intent = Intent(context, CardsDetailsActivity::class.java)
            intent.putExtra(KEY_DATA, data)
            intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP
            context.startActivity(intent)
        }
    }
}
