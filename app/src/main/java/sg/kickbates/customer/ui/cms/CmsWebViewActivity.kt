package sg.kickbates.customer.ui.cms

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.method.ScrollingMovementMethod
import android.util.Base64
import android.util.Log
import android.view.MenuItem
import android.webkit.WebChromeClient
import android.webkit.WebView
import android.webkit.WebViewClient
import androidx.core.text.HtmlCompat
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import sg.kickbates.customer.R
import sg.kickbates.customer.databinding.CmsWebViewActivityBinding
import sg.kickbates.data.helper.Pref

class CmsWebViewActivity : AppCompatActivity() {
    private lateinit var binding:CmsWebViewActivityBinding
    private lateinit var vm:CmsWebViewVM

    private var key:String? =null

    @SuppressLint("SetJavaScriptEnabled")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this,R.layout.cms_web_view_activity)
        vm = CmsWebViewVM(Pref(this))
        binding.lifecycleOwner = this
        binding.cmsVm = vm
        setSupportActionBar(binding.toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        key = intent?.extras?.getString(KEY_DATA)
        vm.dataCms(key)
        binding.cmsWv.settings.javaScriptEnabled = true
        binding.cmsWv.webViewClient = object : WebViewClient(){
            override fun onPageFinished(view: WebView?, url: String?) {
                super.onPageFinished(view, url)
                vm.loading.set(false)
            }
        }
        binding.cmsWv.webChromeClient = object : WebChromeClient(){
            override fun onProgressChanged(view: WebView?, newProgress: Int) {
                super.onProgressChanged(view, newProgress)
                Log.v("CmsWebViewActivity","onProgressChanged -> $newProgress")
            }
        }
        vm.cms.observe(this, Observer { data -> run {
            if (data != null){
                binding.tvTitleToolbar.text = data.title()
                binding.cmsWv.settings.allowContentAccess = true
                data.content()?.let { binding.cmsWv.loadData(it,"text/html; charset=utf-8","utf-8") }
                binding.tvContent.movementMethod = ScrollingMovementMethod()
                binding.tvContent.text = data.content()?.let { HtmlCompat.fromHtml(it,HtmlCompat.FROM_HTML_OPTION_USE_CSS_COLORS) }
            }
        } })
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> onBackPressed()
            else -> { }
        }
        return super.onOptionsItemSelected(item)
    }
    companion object {
        const val CMS_FAQ = "faqs"
        const val CMS_PRIVACY_GOOGLE_FACEBOOK = "facebookgoogleprivacy"
        const val CMS_PRIVACY_POLICY = "privacypolicies"
        const val CMS_TERMS = "termsofuse"
        const val CMS_CONTACT = "contactus"
        const val CMS_HOW_WE = "howwekick"
        const val KEY_DATA = "key_data"
        fun start(context: Context, key:String?){
            val intent = Intent(context, CmsWebViewActivity::class.java)
            intent.putExtra(KEY_DATA, key)
            intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
            context.startActivity(intent)
        }
    }
}

