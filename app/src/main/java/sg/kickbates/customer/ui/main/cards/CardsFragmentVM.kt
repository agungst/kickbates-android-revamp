package sg.kickbates.customer.ui.main.cards

import androidx.databinding.BaseObservable
import androidx.databinding.Bindable
import androidx.databinding.ObservableField
import androidx.lifecycle.MutableLiveData
import kotlinx.coroutines.*
import sg.kickbates.data.BuildConfig
import sg.kickbates.data.api.Api
import sg.kickbates.data.api.models.card.ResponseCustomerCards
import sg.kickbates.data.helper.Pref

/**
 * Created by simx on 30,July,2019
 */
class CardsFragmentVM(private val pref: Pref): BaseObservable() {

    private var job: Job? = SupervisorJob()
    @Bindable var errorMsg = MutableLiveData<String>()
    @Bindable var loading = ObservableField<Boolean>()
    @Bindable var empty = ObservableField<Boolean>()
    @Bindable var cards = MutableLiveData<List<ResponseCustomerCards.DataItem>>()


    fun getCards(){
            loading.set(true)
            try {
                CoroutineScope(CoroutineExceptionHandler { _, throwable -> run {
                    throwable.printStackTrace()
                    job = SupervisorJob()
                } }).launch {
                    val res = Api.Factory.create(BuildConfig.BASE_URL).customerCardsAsync(pref.token, pref.usersId).await()
                    res.let {
                        when (it.status) {
                             200 -> {if (res.data.isNullOrEmpty()) cards.postValue(listOf()) else cards.postValue(res.data?.sortedByDescending { da -> da.id })}
                             else -> {updateErrorMsg(res.message)}
                       }
                    }
                    loading.set(false)
                }
            }catch (e:Exception){
                updateErrorMsg(e.message)
            }
        }
        private fun updateErrorMsg(msg: String?){
            loading.set(false)
            errorMsg.postValue(BuildConfig.ERROR_MSG)
        }
}