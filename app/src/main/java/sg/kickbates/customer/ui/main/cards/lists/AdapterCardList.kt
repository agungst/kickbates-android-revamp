package sg.kickbates.customer.ui.main.cards.lists

import android.graphics.Color
import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import sg.kickbates.customer.R
import sg.kickbates.customer.databinding.ItemCreditCardSmallBinding
import sg.kickbates.data.api.models.card.ResponseCustomerCards

/**
 * Created by simx on 31,July,2019
 */
class AdapterCardList(private var datas:List<ResponseCustomerCards.DataItem>, private var listener: OnAdapterCardListListener): RecyclerView.Adapter<AdapterCardList.Holder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): Holder {
        return Holder(ItemCreditCardSmallBinding.bind(LayoutInflater.from(parent.context).inflate(R.layout.item_credit_card_small,parent,false)))
    }

    override fun getItemCount(): Int {
        return datas.size
    }

    override fun onBindViewHolder(holder: Holder, position: Int) {
        holder.bind(datas[position])
        holder.binding.rootView.setBackgroundColor(Color.parseColor("#${datas[position].kickBanks?.bankCardColor}"))

        /**
         * Card Paid or not
         */
        val pp = datas[position].haveStatementPaid(datas[position].kickCustomerCardStatements)

        if (datas[position].haveStatementPaid(datas[position].kickCustomerCardStatements)) {
            holder.binding.tvPaid.background = ContextCompat.getDrawable(holder.itemView.context, R.drawable.bg_card_paid)
            holder.binding.tvPaid.setTextColor(Color.parseColor("#1f908a"))
        } else {
            holder.binding.tvPaid.background = ContextCompat.getDrawable(holder.itemView.context, R.drawable.bg_card_un_paid)
            holder.binding.tvPaid.setTextColor(Color.parseColor("#ed0006"))
        }

        holder.itemView.setOnClickListener { listener.onAdapterClicked(datas[position]) }
    }

    interface OnAdapterCardListListener {
        fun onAdapterClicked(data: ResponseCustomerCards.DataItem)
    }

    class Holder(var binding: ItemCreditCardSmallBinding):RecyclerView.ViewHolder(binding.root) {
        fun bind(data: ResponseCustomerCards.DataItem){
            with(binding){
                itemCreditCardVm = ItemCreditCardVM(data)
                executePendingBindings()
            }
        }

    }
}