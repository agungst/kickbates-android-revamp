package sg.kickbates.customer.ui.main.cards.details.unbilled


import android.os.Bundle

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView

import sg.kickbates.customer.R
import sg.kickbates.customer.databinding.UnBilledTransactionsFragmentBinding
import sg.kickbates.customer.ui.main.cards.details.adapter.AdapterBilledTransactions
import sg.kickbates.data.api.models.card.ResponseCustomerCards


/**
 * A simple [Fragment] subclass.
 *
 */
class UnBilledTransactionsFragment : Fragment() {

    private lateinit var binding: UnBilledTransactionsFragmentBinding
    private lateinit var vm:UnBilledTransactionsFragmentVM
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater,R.layout.un_billed_transactions_fragment, container,false)
        vm = UnBilledTransactionsFragmentVM()
        binding.lifecycleOwner = this
        binding.unBilledTransactionsVm = vm
        val data: ResponseCustomerCards.DataItem? = arguments?.getParcelable(KEY_DATA)
        vm.transactions.postValue(data?.kickCustomerCardTransactions)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        vm.transactions.observe(this, Observer { data -> run {
            vm.empty.set(data.isNullOrEmpty())
            binding.rcvUnBilled.apply {
                hasFixedSize()
                itemAnimator = DefaultItemAnimator()
                layoutManager = LinearLayoutManager(this@UnBilledTransactionsFragment.context!!, RecyclerView.VERTICAL, false)
                adapter = AdapterBilledTransactions(data, object : AdapterBilledTransactions.OnAdapterBilledTransactionsListener {
                    override fun onAdapterClicked(data: ResponseCustomerCards.DataItem.DataCardTransactions) {

                    }
                })
            }
        } })

    }

    companion object {
        const val KEY_DATA = "key_data"
        fun instance(data: ResponseCustomerCards.DataItem?) : UnBilledTransactionsFragment{
            var bundle =  Bundle()
            bundle.putParcelable(KEY_DATA, data)
            var fragment = UnBilledTransactionsFragment()
            fragment.arguments = bundle
            return fragment
        }
    }
}
