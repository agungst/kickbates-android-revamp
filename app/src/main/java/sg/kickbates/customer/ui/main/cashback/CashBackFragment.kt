package sg.kickbates.customer.ui.main.cashback


import android.annotation.SuppressLint
import android.app.Activity
import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer

import sg.kickbates.customer.R
import sg.kickbates.customer.databinding.CashbackFragmentBinding
import sg.kickbates.customer.ui.cash_out.CashOutActivity
import sg.kickbates.customer.ui.dialog.convert.DialogConvertCash
import sg.kickbates.customer.ui.history.cash_back.CashBackHistoryActivity
import sg.kickbates.customer.ui.history.cash_out.CashOutHistoryActivity
import sg.kickbates.customer.ui.main.MainActivity
import sg.kickbates.data.helper.CommonTools
import sg.kickbates.data.helper.Pref

/**
 * A simple [Fragment] subclass.
 *
 */
@SuppressLint("UseRequireInsteadOfGet")
class CashBackFragment : Fragment() {

    private lateinit var binding:CashbackFragmentBinding
    private lateinit var vm:CashBackFragmentVM

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater,R.layout.cashback_fragment, container,false)
        vm = CashBackFragmentVM(Pref(this.context!!))
        binding.lifecycleOwner = this
        binding.cashBackVm = vm
        vm.getCashBacks()
        return binding.root
    }

    @SuppressLint("SetTextI18n", "FragmentLiveDataObserve")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        vm.cashbacks.observe(this, Observer { data -> run {
            if (data != null){
                binding.tvKickBalace.text = data.kickBalance?.let { CommonTools.formatDollarCurrency(it) }
                binding.tvKickSpend.text = data.kickSpent?.let { CommonTools.formatDollarCurrency(it) }
                binding.tvAvailable.text = data.cashAvailable?.let { CommonTools.formatDollarCurrency(it) }
                binding.tvPending.text = data.cashProcessing?.let { CommonTools.formatDollarCurrency(it) }
                binding.tvCashPaid.text = data.cashProcessed?.let { CommonTools.formatDollarCurrency(it) }
                binding.btnConvert.setOnClickListener {
                    if (data.cashAvailable!! > 0){
                        val dialog = DialogConvertCash(data.cashAvailable, object : DialogConvertCash.OnDialogConvertCashListener {
                            override fun onDismiss() {
                                vm.getCashBacks()
                            }
                        })
                        dialog.show(childFragmentManager,"convert")
                    }else {
                        (activity as MainActivity).showDialogError("We can't process your request because your cash available is ${CommonTools.formatDollarCurrency(data.cashAvailable!!)}")
                    }

                }
            }
            binding.btnRequestCashOut.setOnClickListener {
                if (data.cashAvailable!! > 0){
                    val intent = Intent(this@CashBackFragment.context!!, CashOutActivity::class.java)
                    intent.putExtra(CashOutActivity.KEY_DATA, data)
                    startActivityForResult(intent, CashOutActivity.RC_CASH_OUT)
                }else {
                    (activity as MainActivity).showDialogError("Insufficient Cash$")
                }
            }
        } })

        binding.tvCashBackHistory.setOnClickListener { CashBackHistoryActivity.start(this.context!!) }
        binding.tvCashOutHistory.setOnClickListener { CashOutHistoryActivity.start(this.context!!) }
        vm.errorMsg.observe(this, Observer { data -> run {
            Toast.makeText(this@CashBackFragment.context, data, Toast.LENGTH_SHORT).show()
        } })
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        when (requestCode) {
            CashOutActivity.RC_CASH_OUT -> {
                when (resultCode) {
                    Activity.RESULT_OK -> {
                        vm.getCashBacks()
                        (activity as MainActivity).showDialogSuccess("Success Request Cashout")
                    }
                    else -> { }
                }
            }
            else -> { }
        }
        super.onActivityResult(requestCode, resultCode, data)
    }

    companion object {
        const val KEY_DATA = "key_Data"
        fun instance() : CashBackFragment{
            var bundle =  Bundle()
            var fragment = CashBackFragment()
            fragment.arguments = bundle
            return fragment
        }
    }
}
