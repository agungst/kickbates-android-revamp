package sg.kickbates.customer.ui.main.pay

import android.util.Log
import androidx.databinding.BaseObservable
import androidx.databinding.Bindable
import androidx.databinding.ObservableField
import androidx.lifecycle.MutableLiveData
import kotlinx.coroutines.CoroutineExceptionHandler
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.SupervisorJob
import kotlinx.coroutines.launch
import retrofit2.HttpException
import sg.kickbates.data.BuildConfig
import sg.kickbates.data.api.Api
import sg.kickbates.data.api.models.RequestCard
import sg.kickbates.data.api.models.card.ResponseAccountCard
import sg.kickbates.data.api.models.card.ResponseAddCard
import sg.kickbates.data.api.models.card.token.ResponseAuthCardToken

import sg.kickbates.data.helper.Pref

/**
 * Created by simx on 28,January,2020
 * SandboxUser1 : P@ssUser1$
2	SandboxUser2	P@ssUser2$
3	SandboxUser3	P@ssUser3$
4	SandboxUser4	P@ssUser4$
5	SandboxUser5	P@ssUser5$
dbs
iw038 : 456789
 */
class PayCardActivityVM(private var pref: Pref):BaseObservable() {
    private var job = SupervisorJob()

    @Bindable var loading = ObservableField<Boolean>()
    @Bindable var error = MutableLiveData<String>()

    @Bindable var urlBank = MutableLiveData<String>()
    @Bindable var urlAuth2f = MutableLiveData<String>()

    @Bindable var note = MutableLiveData<String>()
    @Bindable var cards = MutableLiveData<List<ResponseAddCard.DataItem>>()
    @Bindable var resultToken = MutableLiveData<ResponseAuthCardToken.Data>()
    @Bindable var accountCards = MutableLiveData<List<ResponseAccountCard.DataItem>>()

    fun issuerUrl(issuer:String?){
        loading.set(true)
        try {
            CoroutineScope(CoroutineExceptionHandler { _, throwable -> run {
                job = SupervisorJob()
                throwable.printStackTrace()
                urlBank.postValue(null)
                loading.set(false)
            } }).launch {
                val res = Api.Factory.create(BuildConfig.BASE_URL).authCardAsync(pref.token,issuer).await()
                urlBank.postValue(res.data)
                loading.set(false)
            }
        }catch (e: HttpException){
            e.printStackTrace()
            loading.set(false)
        }
    }
    fun issuerUrl(issuer:String?, code:String?){
        loading.set(true)
        try {
            CoroutineScope(CoroutineExceptionHandler { _, throwable -> run {
                job = SupervisorJob()
                throwable.printStackTrace()
                urlBank.postValue(null)
                loading.set(false)
            } }).launch {
                val res = Api.Factory.create(BuildConfig.BASE_URL).auth2fCardAsync(pref.token,code, issuer).await()
                urlAuth2f.postValue(res.data)
                loading.set(false)
            }
        }catch (e: HttpException){
            e.printStackTrace()
            loading.set(false)
        }
    }

    fun getTokenCard(requestCard: RequestCard?){
        loading.set(true)
        note.postValue("Please be patient.. \nWe are requesting token to bank ")
        try {
            CoroutineScope(CoroutineExceptionHandler { _, throwable -> run {
                job = SupervisorJob()
                throwable.printStackTrace()
                loading.set(false)
                resultToken.postValue(null)
                note.postValue("We are sorry.. \n We are failed add your card \n${BuildConfig.ERROR_MSG} ")
            } }).launch {
                val res = Api.Factory.create(BuildConfig.BASE_URL).tokenCardAsync(pref.token, requestCard?.issuer, requestCard?.code_auth).await()
                resultToken.postValue(res.data)
                loading.set(false)
            }
        }catch (e: HttpException){
            e.printStackTrace()
            loading.set(false)
        }
    }

    fun getAccountCard(issuer: String?, partyId: String?, sessionToken:String?) {
        loading.set(true)
        try {
            CoroutineScope(CoroutineExceptionHandler { _, throwable -> run {
                job = SupervisorJob()
                throwable.printStackTrace()
                loading.set(false)
                accountCards.postValue(listOf())
            } }).launch {
                val res = Api.Factory.create(BuildConfig.BASE_URL).accountCardAsync(
                    pref.token,
                    issuer,
                    partyId, sessionToken
                ).await()
                accountCards.postValue(res.data)
                loading.set(false)
            }
        }catch (e: HttpException){
            e.printStackTrace()
            loading.set(false)
        }
    }
}