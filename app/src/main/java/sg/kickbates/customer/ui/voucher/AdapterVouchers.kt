package sg.kickbates.customer.ui.voucher

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.paging.PagedListAdapter
import androidx.recyclerview.widget.RecyclerView
import sg.kickbates.customer.R
import sg.kickbates.customer.databinding.ItemVoucherBinding
import sg.kickbates.data.api.models.voucher.ResponseVouchers
import sg.kickbates.data.helper.KickDiffUtils

/**
 * Created by simx on 31,July,2019
 */
class AdapterVouchers(private var listener: OnAdapterVouchersListener):PagedListAdapter<ResponseVouchers.DataItem, AdapterVouchers.Holder>(KickDiffUtils.VoucherDiff){

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): Holder {
        return Holder(ItemVoucherBinding.bind(LayoutInflater.from(parent.context).inflate(R.layout.item_voucher,parent,false)))
    }

    override fun onBindViewHolder(holder: Holder, position: Int) {
        holder.bind(getItem(position))
        holder.itemView.setOnClickListener { listener.onAdapterVouchersClicked(getItem(position)) }

    }

    interface OnAdapterVouchersListener {
        fun onAdapterVouchersClicked(data: ResponseVouchers.DataItem?)
    }
    class Holder(var binding: ItemVoucherBinding):RecyclerView.ViewHolder(binding.root) {
        fun bind(data: ResponseVouchers.DataItem?){
            with(binding){
                itemVoucherVm = ItemVouchersVM(data)
                executePendingBindings()
            }
        }

    }
}