package sg.kickbates.customer.ui.main

import androidx.databinding.BaseObservable
import androidx.databinding.Bindable
import androidx.lifecycle.MutableLiveData

/**
 * Created by simx on 30,July,2019
 */
class MainActivityVM: BaseObservable() {
    @Bindable var error = MutableLiveData<String>()
}