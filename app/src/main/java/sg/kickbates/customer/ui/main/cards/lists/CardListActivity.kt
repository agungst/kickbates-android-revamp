package sg.kickbates.customer.ui.main.cards.lists

import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.MenuItem
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import sg.kickbates.customer.R
import sg.kickbates.customer.databinding.CardListActivityBinding
import sg.kickbates.customer.ui.main.cards.add.AddCardsActivity

import sg.kickbates.customer.ui.main.cards.details.CardsDetailsActivity
import sg.kickbates.customer.ui.main.cards.dialog.DialogCardsAvailable
import sg.kickbates.data.api.models.RequestCard
import sg.kickbates.data.api.models.card.ResponseCustomerCards
import sg.kickbates.data.helper.Pref

class CardListActivity : AppCompatActivity() {
    private lateinit var binding:CardListActivityBinding
    private lateinit var vm:CardListVM


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this,R.layout.card_list_activity)
        vm = CardListVM(Pref(this))
        binding.lifecycleOwner = this
        binding.cardListVm = vm
        setSupportActionBar(binding.toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        binding.tvTitleToolbar.text = "Cards"
        vm.dataCards()
        vm.cards.observe(this, Observer { data -> run {
            vm.empty.set(data.isNullOrEmpty())
            if (data.isNotEmpty())initCards(data)
        } })
        binding.fabAddCard.setOnClickListener {
            DialogCardsAvailable.show(supportFragmentManager, object : DialogCardsAvailable.OnDialogCardsAvailableCallback {
                override fun onDialogDismiss(code: RequestCard?) {
                    var intent = Intent(this@CardListActivity, AddCardsActivity::class.java)
                    intent.putExtra(AddCardsActivity.KEY_DATA, code)
                    startActivityForResult(intent, AddCardsActivity.RC_ADD)
                }
            })
        }
    }

    private fun initCards(data: List<ResponseCustomerCards.DataItem>) {
        binding.rcvCards.apply {
            hasFixedSize()
            itemAnimator = DefaultItemAnimator()
            layoutManager = GridLayoutManager(this@CardListActivity, 2)
            adapter = AdapterCardList(data, object : AdapterCardList.OnAdapterCardListListener {
                override fun onAdapterClicked(data: ResponseCustomerCards.DataItem) {
                    CardsDetailsActivity.start(this@CardListActivity, data)
                }
            })
        }
        binding.rcvCards.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
                if (newState == RecyclerView.SCROLL_STATE_IDLE) binding.fabAddCard.show()
                super.onScrollStateChanged(recyclerView, newState)

            }

            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                if (dy > 0 ||dy<0 && binding.fabAddCard.isShown)
                {
                    binding.fabAddCard.hide()
                }
                super.onScrolled(recyclerView, dx, dy)
            }
        })
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> {
                onBackPressed()
            }
            else -> {
            }
        }
        return super.onOptionsItemSelected(item)
    }
    companion object {
        fun start(context: Context){
            val intent = Intent(context, CardListActivity::class.java)
            intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP
            context.startActivity(intent)
        }
    }
}
