package sg.kickbates.customer.ui.payment.channels

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import sg.kickbates.customer.R
import sg.kickbates.customer.databinding.ItemPaymentChannelsBinding
import sg.kickbates.data.api.models.payment.ResponsePaymentChannel
import sg.kickbates.data.api.models.payment.ResponsePaymentChannelCard

/**
 * Created by simx on 31,July,2019
 */
class AdapterPaymentChannels(private var items:List<ResponsePaymentChannelCard.DataItem>, private var listener: OnAdapterVouchersListener): RecyclerView.Adapter<AdapterPaymentChannels.Holder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): Holder {
        return Holder(ItemPaymentChannelsBinding.bind(LayoutInflater.from(parent.context).inflate(R.layout.item_payment_channels,parent,false)))
    }

    override fun getItemCount(): Int {
        return items.size
    }

    override fun onBindViewHolder(holder: Holder, position: Int) {
        holder.bind(items[position])
        holder.itemView.setOnClickListener { listener.onAdapterPaymentChannelsClicked(items[position]) }

    }

    interface OnAdapterVouchersListener {
        fun onAdapterPaymentChannelsClicked(data: ResponsePaymentChannelCard.DataItem)
    }
    class Holder(var binding: ItemPaymentChannelsBinding):RecyclerView.ViewHolder(binding.root) {
        fun bind(data: ResponsePaymentChannelCard.DataItem){
            with(binding){
                itemPaymentChannelVm = ItemPaymentChannelsVM(data)
                executePendingBindings()
            }
        }

    }
}