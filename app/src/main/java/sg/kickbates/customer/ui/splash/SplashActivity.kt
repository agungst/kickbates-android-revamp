package sg.kickbates.customer.ui.splash

import android.annotation.SuppressLint
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.util.Log
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import com.facebook.login.LoginManager
import sg.kickbates.customer.BuildConfig
import sg.kickbates.customer.R
import sg.kickbates.customer.databinding.SplashActivityBinding
import sg.kickbates.customer.helper.AuthHelper
import sg.kickbates.customer.ui.auth.AuthActivity
import sg.kickbates.customer.ui.main.MainActivity
import sg.kickbates.data.helper.Pref

class SplashActivity : AppCompatActivity() {
    private lateinit var binding:SplashActivityBinding
    private lateinit var vm:SplashActivityVM
    private lateinit var pref: Pref
    @SuppressLint("SetTextI18n")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this,R.layout.splash_activity)
        pref = Pref(this)
        vm = SplashActivityVM(pref)
        binding.lifecycleOwner = this
        binding.slashVM = vm
        vm.checkIsValidToken()
        vm.isValid.observe(this, { valid -> run {
            if (valid) gotoMain() else logout()
        } })
        binding.tvVersion.text = "Version ${BuildConfig.VERSION_NAME}.${BuildConfig.VERSION_CODE}"
    }
    private fun logout(){
        AuthHelper.getGoogleClient(this)?.signOut()?.addOnCompleteListener { task -> run {
            if (task.result == null){
                LoginManager.getInstance().logOut()
                gotoLogin()
            } else {
                if(task.isSuccessful) gotoLogin()
            }
        } }?.addOnFailureListener { exception -> run {
            exception.printStackTrace()
            gotoLogin()

        } }
    }
    private fun gotoLogin(){
        pref.clear()
        Handler(Looper.getMainLooper()).postDelayed( {
            AuthActivity.start(this)
            finish()
        },2000)
    }

    private fun gotoMain(){
        Handler(Looper.getMainLooper()).postDelayed( {
            MainActivity.start(this)
            finish()
        },2000)
    }
}
