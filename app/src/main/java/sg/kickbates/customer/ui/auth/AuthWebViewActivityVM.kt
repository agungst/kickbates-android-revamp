package sg.kickbates.customer.ui.auth

import androidx.databinding.BaseObservable
import androidx.databinding.Bindable
import androidx.databinding.ObservableField

/**
 * Created by simx on 09,November,2019
 */
class AuthWebViewActivityVM: BaseObservable() {
    /**
     * buat ngambil card flow nya agak beda dikit tiap issuer, paling gampang ocbc
    ocbc:
    1. auth di webview, dapet code
    2. tinggal hit /cards method get, issuer: ocbc ; session_token: code

    citi:
    1. auth di webview, dapet code
    2. hit kick_customer_cards/token issuer: citi ; code: code . nanti dapet token
    3. baru hit /cards method get, issuer: citi ; session_token: token

    dbs rada2 nih:
    1. auth di webview dapet code
    2. hit kick_customer_cards/token issuer: dbs ; code: code . nanti dapet token dan party_id
    3. hit kick_customer_cards/party issuer: dbs ; party_id: party id ; accessToken: token , nanti ada party_token
    3. baru hit /cards method get, issuer: dbs ; token: token ; party_token: party_token
     */

    @Bindable var loading = ObservableField<Boolean>()

}