package sg.kickbates.customer.ui.merchants.list

import android.app.Activity
import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.KeyEvent
import android.view.MenuItem
import android.view.View
import android.view.inputmethod.InputMethodManager
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import kotlinx.coroutines.launch
import sg.kickbates.customer.R
import sg.kickbates.customer.databinding.MerchantsActivityBinding
import sg.kickbates.customer.ui.merchants.details.MerchantsDetailsActivity
import sg.kickbates.data.api.models.ResponseMerchants
import sg.kickbates.data.helper.Pref
import sg.kickbates.data.helper.States

class MerchantsActivity : AppCompatActivity() {

    private lateinit var binding:MerchantsActivityBinding
    private lateinit var vm:MerchantsVM
    private lateinit var pref: Pref

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this,R.layout.merchants_activity)
        pref = Pref(this)
        vm = MerchantsVM(pref)
        binding.lifecycleOwner = this
        binding.merchantsVm = vm
        lifecycleScope.launch { vm.getMerchants() }
        setSupportActionBar(binding.toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        binding.tvTitleToolbar.text = "Store"
        binding.refresh.setOnRefreshListener {
            lifecycleScope.launch { vm.getMerchantsByName(binding.etKeywordSerch.text.toString()) }
        }
        vm.merchants.observe(this, { data -> run {
            if (binding.refresh.isRefreshing) binding.refresh.isRefreshing = false
            vm.empty.set(data.isNullOrEmpty())
            val adapterPagingMerchantsList = AdapterPagingMerchantsListFull(object : AdapterPagingMerchantsListFull.OnAdapterMerchantsListListener{
                override fun onAdapterMerchantsClicked(data: ResponseMerchants.DataItem?) {
                    MerchantsDetailsActivity.start(this@MerchantsActivity, data)
                }
            })
            adapterPagingMerchantsList.submitList(data)

            binding.rcvMerchants.apply {
                hasFixedSize()
                itemAnimator = DefaultItemAnimator()
                layoutManager = LinearLayoutManager(this@MerchantsActivity, RecyclerView.VERTICAL, false)
                adapter = adapterPagingMerchantsList
            }

        } })
        binding.etKeywordSerch.setOnKeyListener { _, keyCode, event ->
            if (event.action == KeyEvent.ACTION_DOWN){
                if (keyCode == KeyEvent.KEYCODE_ENTER){
                    vm.getMerchantsByName(binding.etKeywordSerch.text.toString())
                    hideKeyboard()
                    return@setOnKeyListener true
                }
            }
            return@setOnKeyListener false
        }
        binding.etKeywordSerch.addTextChangedListener(watcher)

        binding.lytBtnFilter.setOnClickListener {
            hideKeyboard()
            if (binding.etKeywordSerch.text.toString().isNotEmpty()){
                vm.getMerchantsByName(binding.etKeywordSerch.text.toString())
            }
        }

        binding.etKeywordSerch.addTextChangedListener(watcher)

        vm.states?.observe(this, Observer { state -> run {
            when (state) {
                States.LOADING -> vm.loading.set(true)
                States.DONE -> vm.loading.set(false)
                else -> {}
            }
        } })
    }

    private fun hideKeyboard(){
        val imm: InputMethodManager? = getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
        var v = currentFocus
        if (v == null) v = View(this)

        imm?.hideSoftInputFromWindow(v.windowToken,0)
    }

    private val watcher = object :TextWatcher {

        override fun afterTextChanged(s: Editable?) {
            if (s.isNullOrBlank()){
                vm.getMerchants()
                hideKeyboard()
            }
        }

        override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}

        override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {}
    }
    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> {
                onBackPressed()
            }
            else -> {
            }
        }
        return super.onOptionsItemSelected(item)
    }
    companion object {
        fun start(context: Context){
            val intent = Intent(context, MerchantsActivity::class.java)
            intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP
            context.startActivity(intent)
        }
    }
}
