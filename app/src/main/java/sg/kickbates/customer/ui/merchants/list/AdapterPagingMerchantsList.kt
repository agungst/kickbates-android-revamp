package sg.kickbates.customer.ui.merchants.list

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.paging.PagedListAdapter
import androidx.recyclerview.widget.RecyclerView
import sg.kickbates.customer.R
import sg.kickbates.customer.databinding.ItemCreditCardSmallBinding
import sg.kickbates.customer.databinding.ItemMerchantsBinding
import sg.kickbates.data.api.models.ResponseMerchants
import sg.kickbates.data.helper.KickDiffUtils

/**
 * Created by simx on 31,July,2019
 */
class AdapterPagingMerchantsList(private var listener: OnAdapterMerchantsListListener): PagedListAdapter<ResponseMerchants.DataItem, AdapterPagingMerchantsList.Holder>(KickDiffUtils.MerchantsDiff) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): Holder {
        return Holder(ItemMerchantsBinding.bind(LayoutInflater.from(parent.context).inflate(R.layout.item_merchants,parent,false)))
    }


    override fun onBindViewHolder(holder: Holder, position: Int) {
        holder.bind(getItem(position))
        holder.itemView.setOnClickListener { listener.onAdapterMerchantsClicked(getItem(position)) }
    }

    interface OnAdapterMerchantsListListener {
        fun onAdapterMerchantsClicked(data: ResponseMerchants.DataItem?)
    }
    class Holder(var binding: ItemMerchantsBinding):RecyclerView.ViewHolder(binding.root) {
        fun bind(data: ResponseMerchants.DataItem?){
            with(binding){
                itemMerchantVm = ItemMerchantsVM(data)
                executePendingBindings()
            }
        }

    }
}