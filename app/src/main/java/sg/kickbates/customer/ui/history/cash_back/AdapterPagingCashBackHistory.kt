package sg.kickbates.customer.ui.history.cash_back

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.paging.PagedListAdapter
import androidx.recyclerview.widget.RecyclerView
import sg.kickbates.customer.R
import sg.kickbates.customer.databinding.ItemCashBackHistoryBinding
import sg.kickbates.data.api.models.cash_back.ResponseCashBackHistory
import sg.kickbates.data.helper.KickDiffUtils

/**
 * Created by simx on 31,July,2019
 */
class AdapterPagingCashBackHistory: PagedListAdapter<ResponseCashBackHistory.DataItem, AdapterPagingCashBackHistory.Holder>(
    KickDiffUtils.CashBackHistoryDiff)  {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): Holder {
        return Holder(ItemCashBackHistoryBinding.bind(LayoutInflater.from(parent.context).inflate(R.layout.item_cash_back_history,parent,false)))
    }

    override fun onBindViewHolder(holder: Holder, position: Int) {
        holder.bind(getItem(position))
    }

    class Holder(var binding: ItemCashBackHistoryBinding):RecyclerView.ViewHolder(binding.root) {
        fun bind(data: ResponseCashBackHistory.DataItem?){
            with(binding){
                itemCashBackVm = ItemCashBackHistoryVM(data)
                executePendingBindings()
            }
        }

    }
}