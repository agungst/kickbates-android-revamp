package sg.kickbates.customer.ui.cash_out

import android.app.Activity
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.MenuItem
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import sg.kickbates.customer.R
import sg.kickbates.customer.databinding.CashOutActivityBinding
import sg.kickbates.data.api.models.RequestCashOut
import sg.kickbates.data.api.models.ResponseCashbackSummary
import sg.kickbates.data.api.models.card.ResponseCardAvaiable
import sg.kickbates.data.helper.Pref

class CashOutActivity : AppCompatActivity() {

    private lateinit var binding:CashOutActivityBinding
    private lateinit var vm:CashOutVM

    private var summary:ResponseCashbackSummary.Data? = null
    private var requestCashOut = RequestCashOut()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this,R.layout.cash_out_activity)
        vm = CashOutVM(Pref(this))
        binding.lifecycleOwner = this
        binding.cashOutVm = vm
        setSupportActionBar(binding.toolbar)
        binding.tvTitleToolbar.text = "Request Cashout"
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        summary = intent?.extras?.getParcelable(KEY_DATA)

        binding.etBank.setOnClickListener { showDialogBank() }
        binding.etBank.setOnFocusChangeListener { _, hasFocus -> if (hasFocus) showDialogBank() }

        binding.btnSubmit.setOnClickListener {
            if (handleInputAmount() && handleInputBankName() && handleInputBankAccountName() && handleInputBankAccountNumber()){
                vm.processCashOut(requestCashOut)
            }
        }
        vm.cashOut.observe(this, Observer { data -> run {
            if (data != null){
                setResult(Activity.RESULT_OK)
                finish()
            }
        } })
    }

    private fun showDialogBank() {
        DialogBank.show(supportFragmentManager, object : DialogBank.OnDialogBankListener {
            override fun onSelectedBank(data: ResponseCardAvaiable.DataItem) {
                binding.etBank.setText(data.bankName)
                requestCashOut.requestBankId = data.id
            }
        },true, isStatic = false)
    }

    private fun handleInputAmount() : Boolean {
        val amount = binding.etCashAmount.text.toString()
        return if (amount.isEmpty()){
            binding.lytCashAmount.isErrorEnabled = true
            binding.lytCashAmount.error = "Amount required"
            false
        } else {
            when {
                amount.toDouble() >= 10 -> {
                    binding.lytCashAmount.error = null
                    binding.lytCashAmount.isErrorEnabled = false
                    requestCashOut.requestCash = amount.toDouble()
                    true
                }
                else -> {
                    binding.lytCashAmount.isErrorEnabled = true
                    binding.lytCashAmount.error ="Minimal cashout is $10.00"
                    false
                }
            }
        }
    }

    private fun handleInputBankName() : Boolean {
        val amount = binding.etBank.text.toString()
        return if (amount.isEmpty()){
            binding.lytBank.isErrorEnabled = true
            binding.lytBank.error = "Please choose a bank"
            false
        } else {
            binding.lytBank.isErrorEnabled = false
            binding.lytBank.error = null
            true
        }
    }

    private fun handleInputBankAccountName() : Boolean {
        val amount = binding.etBankAccountName.text.toString()
        return if (amount.isEmpty()){
            binding.lytBankAccountName.isErrorEnabled = true
            binding.lytBankAccountName.error = "Please fill the account name field"
            false
        } else {
            binding.lytBankAccountName.isErrorEnabled = false
            binding.lytBankAccountName.error = null
            requestCashOut.requestAccountName = amount
            true
        }
    }

    private fun handleInputBankAccountNumber() : Boolean {
        val amount = binding.etBankNumber.text.toString()
        return if (amount.isEmpty()){
            binding.lytBankNumber.isErrorEnabled = true
            binding.lytBankNumber.error = "Please fill the account number field"
            false
        } else {
            binding.lytBankNumber.isErrorEnabled = false
            binding.lytBankNumber.error = null
            requestCashOut.requestAccountNumber = amount
            true
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> onBackPressed()
            else -> {
            }
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onBackPressed() {
        super.onBackPressed()
        setResult(Activity.RESULT_CANCELED)
        finish()
    }

    companion object {
        const val KEY_DATA = "key_data"
        const val RC_CASH_OUT = 11
    }
}
