package sg.kickbates.customer.ui.merchants.list

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import sg.kickbates.customer.R
import sg.kickbates.customer.databinding.ItemCreditCardSmallBinding
import sg.kickbates.customer.databinding.ItemMerchantsBinding
import sg.kickbates.customer.databinding.ItemMerchantsFullBinding
import sg.kickbates.data.api.models.ResponseMerchants

/**
 * Created by simx on 31,July,2019
 */
class AdapterMerchantsListFull(private var datas:List<ResponseMerchants.DataItem>, private var listener: OnAdapterMerchantsListFullListener): RecyclerView.Adapter<AdapterMerchantsListFull.Holder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): Holder {
        return Holder(ItemMerchantsFullBinding.bind(LayoutInflater.from(parent.context).inflate(R.layout.item_merchants_full,parent,false)))
    }

    override fun getItemCount(): Int {
        return datas.size
    }

    interface OnAdapterMerchantsListFullListener {
        fun onAdapterClicked(data: ResponseMerchants.DataItem)
    }
    override fun onBindViewHolder(holder: Holder, position: Int) {
        holder.bind(datas[position])
        holder.itemView.setOnClickListener { listener.onAdapterClicked(datas[position]) }
        holder.binding.btnView.setOnClickListener { listener.onAdapterClicked(datas[position]) }
    }

    class Holder(var binding: ItemMerchantsFullBinding):RecyclerView.ViewHolder(binding.root) {
        fun bind(data: ResponseMerchants.DataItem){
            with(binding){
                itemMerchantVm = ItemMerchantsVM(data)
                executePendingBindings()
            }
        }

    }
}