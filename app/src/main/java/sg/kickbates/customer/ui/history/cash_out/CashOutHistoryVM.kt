package sg.kickbates.customer.ui.history.cash_out

import androidx.databinding.BaseObservable
import androidx.databinding.Bindable
import androidx.databinding.ObservableField
import androidx.lifecycle.LiveData
import androidx.lifecycle.Transformations
import androidx.paging.LivePagedListBuilder
import androidx.paging.PagedList
import sg.kickbates.data.api.models.cash_out.ResponseCashOutHistory
import sg.kickbates.data.helper.Pref
import sg.kickbates.data.paging.CashOutHistoryDataSource
import sg.kickbates.data.paging.CashOutHistoryDataSourceFactory

/**
 * Created by simx on 02,January,2020
 */
class CashOutHistoryVM(private var pref: Pref): BaseObservable() {
    private val config = PagedList.Config.Builder()
        .setEnablePlaceholders(true)
        .build()

    private var sourceFactory: CashOutHistoryDataSourceFactory = CashOutHistoryDataSourceFactory()
    var history: LiveData<PagedList<ResponseCashOutHistory.DataItem>>
    var errorState: LiveData<String>? = null
    var loadingState: LiveData<Boolean>? = null
    var emptyState: LiveData<Boolean>? = null

    @Bindable var loading = ObservableField<Boolean>()
    @Bindable var empty = ObservableField<Boolean>()

    init {
        history = LivePagedListBuilder(sourceFactory, config).build()
        errorState = Transformations.switchMap<CashOutHistoryDataSource,String>(sourceFactory.history, CashOutHistoryDataSource::error)
        loadingState = Transformations.switchMap<CashOutHistoryDataSource,Boolean>(sourceFactory.history, CashOutHistoryDataSource::loading)
        emptyState = Transformations.switchMap<CashOutHistoryDataSource,Boolean>(sourceFactory.history, CashOutHistoryDataSource::empty)
    }

    fun dataHistory(){
        sourceFactory.query(pref.token, pref.usersId)
        sourceFactory.history.value?.invalidate()
    }

}