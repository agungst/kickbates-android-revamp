package sg.kickbates.customer.ui.dialog.account_card

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.FragmentManager
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView

import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import sg.kickbates.customer.R
import sg.kickbates.customer.databinding.DialogCardAccountBinding
import sg.kickbates.customer.helper.DialogHelper
import sg.kickbates.data.api.models.card.ResponseAccountCard
import sg.kickbates.data.api.models.card.ResponseCustomerCards
import sg.kickbates.data.api.models.payment.RequestPayNow
import sg.kickbates.data.api.models.payment.ResponseChargeCard
import sg.kickbates.data.api.room.AppDatabase
import sg.kickbates.data.api.room.CardModel
import sg.kickbates.data.helper.Pref

/**
 * Created by simx on 30,January,2020
 */
class DialogAccountCard constructor(var items:List<ResponseAccountCard.DataItem>,var invoiceId:Int?, var sessionToken:String?,var dataCard: ResponseCustomerCards.DataItem.DataCardStatements?, var callback: OnDialogAccountCardCallback): BottomSheetDialogFragment() {
    private lateinit var binding:DialogCardAccountBinding
    private lateinit var vm:DialogAccountCardVM

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.dialog_card_account, container,false)
        vm = DialogAccountCardVM(Pref(this.context!!), AppDatabase.getDatabase(this.context!!))
        binding.lifecycleOwner = this
        binding.dialogAccountCardVm = vm
        vm.getCards()
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.rcvAccountBank.apply {
            hasFixedSize()
            itemAnimator = DefaultItemAnimator()
            layoutManager = LinearLayoutManager(this@DialogAccountCard.context!!, RecyclerView.VERTICAL, false)
            adapter = AdapterAccountsBank(items, object : AdapterAccountsBank.OnAdapterAccountsBankCalback{
                override fun onItemClicked(data: ResponseAccountCard.DataItem) {
                    val requestPayNow = RequestPayNow()
                    requestPayNow.kickInvoiceId = invoiceId
                    requestPayNow.issuer ="dbs"
                    requestPayNow.accountId = data.id
                    requestPayNow.token = sessionToken
                    vm.cards.observe(this@DialogAccountCard, Observer { dataCards -> kotlin.run {
                        DialogHelper.dialogCardNumber(this@DialogAccountCard.context!!, dataCards as ArrayList<CardModel>, object :DialogHelper.OnDialogHelperCardNumber{
                            override fun onSuccessCardInput(cardNumber:String?) {
                                val cardModel = CardModel()
                                cardModel.cardId = dataCard?.kickCustomerCardsId
                                cardModel.cardNumber = cardNumber
                                vm.saveCards(cardModel)
                                vm.payNow(requestPayNow)
                            }
                        })
                    } })

                }
            })
        }
        vm.results.observe(this, Observer { response ->
            run {
                if (response != null){
                    dialog?.dismiss()
                    callback.onSuccess(response)
                }
                else {
                    dialog?.dismiss()
                    callback.onFailed()
                }

            }
        })
        binding.imgClose.setOnClickListener {
            dialog?.dismiss()
            callback.onFailed()
        }
    }

    interface OnDialogAccountCardCallback {
        fun onSuccess(data:ResponseChargeCard)
        fun onFailed()
    }

    companion object {
        fun show(items:List<ResponseAccountCard.DataItem>, invoiceId:Int?, sessionToken:String?, dataCard: ResponseCustomerCards.DataItem.DataCardStatements?, fm: FragmentManager, callback: OnDialogAccountCardCallback){
            val dialog = DialogAccountCard(items, invoiceId, sessionToken,dataCard, callback)
            dialog.show(fm, DialogAccountCard::class.java.simpleName)
        }
    }

}