package sg.kickbates.customer.ui.main.cashback

import androidx.databinding.BaseObservable
import androidx.databinding.Bindable
import androidx.databinding.ObservableField
import androidx.lifecycle.MutableLiveData
import kotlinx.coroutines.*
import retrofit2.HttpException
import sg.kickbates.data.BuildConfig
import sg.kickbates.data.api.Api
import sg.kickbates.data.api.models.ResponseCashbackSummary
import sg.kickbates.data.helper.Pref

/**
 * Created by simx on 30,July,2019
 */
class CashBackFragmentVM(private var pref: Pref): BaseObservable() {

    private var job: Job? = SupervisorJob()
    @Bindable var errorMsg = MutableLiveData<String>()
    @Bindable var loading = ObservableField<Boolean>()
    @Bindable var cashbacks = MutableLiveData<ResponseCashbackSummary.Data>()

    fun getCashBacks(){
        loading.set(true)
        try {
            CoroutineScope(CoroutineExceptionHandler { _, throwable -> run {
                job = SupervisorJob()
                throwable.printStackTrace()
                updateErrorMsg(throwable.message)
                cashbacks.postValue(null)
            } }).launch {
                val res = Api.Factory.create(BuildConfig.BASE_URL).cashBackSummaryAsync(pref.token).await()
                cashbacks.postValue(res.data)
                loading.set(false)
            }
        }catch (e: HttpException){
            e.printStackTrace()
            updateErrorMsg(e.message)

        }
    }
    private fun updateErrorMsg(msg: String?){
        loading.set(false)
        errorMsg.postValue(msg)
    }
}