package sg.kickbates.customer.ui.merchants.list

import androidx.databinding.BaseObservable
import androidx.databinding.Bindable
import androidx.databinding.ObservableField
import sg.kickbates.data.api.models.ResponseMerchants

/**
 * Created by simx on 31,July,2019
 */
class ItemMerchantsVM(data:ResponseMerchants.DataItem?):BaseObservable() {
    @Bindable var image = ObservableField(data?.image())
    @Bindable var name = ObservableField(data?.merchantName)
    @Bindable var cat = ObservableField(data?.cat())
}