package sg.kickbates.customer.ui.merchants.details

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import sg.kickbates.customer.R

import sg.kickbates.customer.databinding.ItemDealsSmallSmallBinding
import sg.kickbates.customer.ui.deals.list.ItemDealsVM
import sg.kickbates.data.api.models.ResponseDeals

/**
 * Created by simx on 31,July,2019
 */
class AdapterDealsListSmall(private var datas:List<ResponseDeals.DataItem>, private var listener: OnAdapterDealListSmallListener): RecyclerView.Adapter<AdapterDealsListSmall.Holder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): Holder {
        return Holder(ItemDealsSmallSmallBinding.bind(LayoutInflater.from(parent.context).inflate(R.layout.item_deals_small_small,parent,false)))
    }

    override fun getItemCount(): Int {
        return datas.size
    }

    override fun onBindViewHolder(holder: Holder, position: Int) {
        holder.bind(datas[position])
        holder.itemView.setOnClickListener { listener.onClicked(datas[position]) }
    }
    interface OnAdapterDealListSmallListener {
        fun onClicked(data: ResponseDeals.DataItem)
    }

    class Holder(var binding: ItemDealsSmallSmallBinding):RecyclerView.ViewHolder(binding.root) {
        fun bind(data: ResponseDeals.DataItem){
            with(binding){
                itemDealVm = ItemDealsVM(data)
                executePendingBindings()
            }
        }

    }
}