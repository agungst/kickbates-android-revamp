package sg.kickbates.customer.ui.premium

import android.app.Activity
import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.MenuItem
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import sg.kickbates.customer.R
import sg.kickbates.customer.databinding.PremiumPageActivityBinding
import sg.kickbates.customer.helper.DialogHelper
import sg.kickbates.customer.ui.payment.channels.PaymentChannelActivity
import sg.kickbates.data.api.models.ResponseMe
import sg.kickbates.data.helper.Pref

class PremiumPageActivity : AppCompatActivity() {
    private lateinit var binding:PremiumPageActivityBinding
    private lateinit var vm:PremiumPageVM
    private var dataUser:ResponseMe.Data? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this,R.layout.premium_page_activity)
        vm = PremiumPageVM(Pref(this))
        binding.lifecycleOwner = this
        binding.premiumVm = vm
        vm.dataPremiumPage()
        setSupportActionBar(binding.toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        binding.tvTitleToolbar.text = "Premium"
        binding.refresh.setOnRefreshListener {
            if (binding.refresh.isRefreshing) vm.dataPremiumPage()
        }

        dataUser = intent?.extras?.getParcelable(KEY_DATA)
        vm.userData.postValue(dataUser)
        vm.isPremium.set(dataUser?.isPremiumAccount())
        vm.premium.observe(this, Observer { data -> run {
            if (binding.refresh.isRefreshing) binding.refresh.isRefreshing = false
            if (data != null){
                binding.tvDesc.text = data.dec()
                binding.tvTitlePrice.text = data.titlePrice()
                binding.tvPrice.text = data.price()
                binding.tvTitleCash.text = data.titleCash()
                binding.tvCash.text = data.cash()
            }
        } })

        vm.userData.observe(this, Observer { data -> run {
            if (data != null){
                dataUser = data
                binding.tvTitle.text = data.title()
                binding.tvSubTitle.text= data.subTitle()
                dataUser?.isPremiumAccount().let {
                    if (it!!) binding.btnSubscribe.text = "Unsubscribe" else binding.btnSubscribe.text = "Subscribe"
                }
            }
        } })
        vm.error.observe(this, Observer { msg -> run {
            Toast.makeText(this@PremiumPageActivity, msg, Toast.LENGTH_SHORT).show()
        } })
        vm.invoice.observe(this, Observer { data -> run {
            var intent = Intent(this, PaymentChannelActivity::class.java)
            intent.putExtra(PaymentChannelActivity.KEY_DATA, data.id)
            startActivityForResult(intent, PaymentChannelActivity.KEY_VIEW_CHANNEL)
        } })

        binding.btnSubscribe.setOnClickListener {
            Log.v("PremiumPageActivity","onCreate -> ${dataUser?.subscribePremium()}")
            if (dataUser?.subscribePremium() != null){
                if (dataUser?.subscribePremium()!!){
                    DialogHelper.dialogOKCancel("Are you sure you want to unsubscribe?", this, object : DialogHelper.OnDialogSuccessListener {
                        override fun onOk() {
                            vm.unSubscribe(dataUser?.id)
                        }})
                }else {
                    vm.upgradeToPremium()
                }
            }
        }

    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> onBackPressed()
            else -> {
            }
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        when (requestCode) {
            PaymentChannelActivity.KEY_VIEW_CHANNEL -> {
                when (resultCode) {
                    Activity.RESULT_OK -> vm.dataPremiumPage()
                    else -> { }
                }
            }
            else -> { }
        }
        super.onActivityResult(requestCode, resultCode, data)
    }
    companion object {
        const val KEY_DATA = "key_data"
        fun start(context: Context, data:ResponseMe.Data?){
            val intent = Intent(context, PremiumPageActivity::class.java)
            intent.putExtra(KEY_DATA, data)
            intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
            context.startActivity(intent)
        }
    }
}
