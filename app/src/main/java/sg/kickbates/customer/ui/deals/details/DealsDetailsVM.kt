package sg.kickbates.customer.ui.deals.details

import androidx.databinding.BaseObservable
import androidx.databinding.Bindable
import androidx.databinding.ObservableField


/**
 * Created by simx on 07,August,2019
 */
class DealsDetailsVM : BaseObservable() {
    @Bindable var noBanner = ObservableField<Boolean>()

}

