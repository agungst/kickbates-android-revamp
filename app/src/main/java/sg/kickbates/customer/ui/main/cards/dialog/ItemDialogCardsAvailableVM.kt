package sg.kickbates.customer.ui.main.cards.dialog

import androidx.databinding.BaseObservable
import androidx.databinding.Bindable
import androidx.databinding.ObservableField
import sg.kickbates.data.api.models.card.ResponseCardAvaiable

/**
 * Created by simx on 13,November,2019
 */
class ItemDialogCardsAvailableVM(data: ResponseCardAvaiable.DataItem):BaseObservable() {
    @Bindable var name = ObservableField(data.bankName)
    @Bindable var img = ObservableField(data.bankIcon())
}