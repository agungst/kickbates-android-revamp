package sg.kickbates.customer.ui.payment.history

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.paging.PagedListAdapter
import androidx.recyclerview.widget.RecyclerView
import sg.kickbates.customer.R
import sg.kickbates.customer.databinding.ItemHistoryTransactionsBinding
import sg.kickbates.customer.ui.main.cards.details.adapter.ItemTransactionsVM

import sg.kickbates.data.api.models.payment.DetailInvoice
import sg.kickbates.data.helper.KickDiffUtils

/**
 * Created by simx on 24,November,2019
 */

class AdapterPagingTransactionHistory(private var listener:OnAdapterPagingTransactionHistoryListener):PagedListAdapter<DetailInvoice, AdapterPagingTransactionHistory.Holder>(KickDiffUtils.TransactionHistoryDiff){
    class Holder(var binding: ItemHistoryTransactionsBinding):RecyclerView.ViewHolder(binding.root) {
        fun bind(data: DetailInvoice?){
            with(binding){
                itemTransactionVm = ItemTransactionsVM(data)
                executePendingBindings()
            }

        }
    }
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): Holder {
        return Holder(
            ItemHistoryTransactionsBinding.bind(
                LayoutInflater.from(parent.context).inflate(R.layout.item_history_transactions, parent, false)
            )
        )
    }

    override fun onBindViewHolder(holder: Holder, position: Int) {
        holder.bind(getItem(position))
        holder.itemView.setOnClickListener { listener.onClicker(getItem(position)) }
    }
    interface OnAdapterPagingTransactionHistoryListener {
        fun onClicker(data:DetailInvoice?)
    }
}