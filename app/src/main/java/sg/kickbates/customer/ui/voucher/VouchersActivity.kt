package sg.kickbates.customer.ui.voucher

import android.app.Activity
import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.KeyEvent
import android.view.MenuItem
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import sg.kickbates.customer.R
import sg.kickbates.customer.databinding.VouchersActivityBinding
import sg.kickbates.customer.helper.SnackBarHelper
import sg.kickbates.data.api.models.voucher.ResponseVouchers
import sg.kickbates.data.helper.Pref
import sg.kickbates.data.helper.States

class VouchersActivity : AppCompatActivity() {
    private lateinit var binding:VouchersActivityBinding
    private lateinit var vm:VouchersActivityVM
    private var dataVoucer: ResponseVouchers.DataItem? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this,R.layout.vouchers_activity)
        vm = VouchersActivityVM(Pref(this))
        binding.lifecycleOwner = this
        binding.vouchersVm = vm
        setSupportActionBar(binding.toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        binding.tvTitleToolbar.text = "Use Promo Code / My Vouchers"
        vm.getVoucher()
        vm.states?.observe(this, Observer { state -> run {
            when (state) {
                States.LOADING -> vm.loading.set(true)
                States.DONE -> vm.loading.set(false)
                else -> {}
            }
        } })
        vm.vouchers.observe(this, Observer { data -> run {
            val adapterVouchers = AdapterVouchers(object : AdapterVouchers.OnAdapterVouchersListener {
                override fun onAdapterVouchersClicked(data: ResponseVouchers.DataItem?) {
                    if (data == null){
                        vm.msg.postValue("Promo Code Can't be used")
                    }else {
                        dataVoucer = data
                        setResultOkeWithData(data)
                    }
                }

            })
            adapterVouchers.submitList(data)
            binding.rcvMyVoucher.apply {
                hasFixedSize()
                itemAnimator = DefaultItemAnimator()
                layoutManager = LinearLayoutManager(this@VouchersActivity, RecyclerView.VERTICAL, false)
                adapter = adapterVouchers
            }
        } })

        binding.btnUse.setOnClickListener {
            if (binding.etCode.text.toString().isNullOrBlank()){
                binding.etCode.requestFocus()
            }else {
                vm.searchVoucher(binding.etCode.text.toString())
            }
        }

        binding.etCode.setOnKeyListener { _, keyCode, event ->
            if (event.action == KeyEvent.ACTION_DOWN && keyCode == KeyEvent.KEYCODE_ENTER){
                vm.searchVoucher(binding.etCode.text.toString())
                true
            }else {
                false
            }

        }

        vm.detailVoucher.observe(this, Observer { code -> run {
            if (code != null)setResultOkeWithData(code) else vm.msg.postValue("Promo Code not found")
        } })

        vm.msg.observe(this, Observer { value -> run {
            SnackBarHelper.showSnackBarError(value, binding.rootView, object : SnackBarHelper.OnSnackBarHelperListener {
                override fun onError() {
                    binding.etCode.setText("")
                }
            })
        } })
    }

    private fun setResultOkeWithData(voucerDetail: ResponseVouchers.DataItem?) {
        val intent = Intent()
        intent.putExtra(KEY_DATA, voucerDetail)
        setResult(Activity.RESULT_OK, intent)
        finish()
    }

    private fun setResultCancel(){
        setResult(Activity.RESULT_CANCELED)
        finish()
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> {
                setResultCancel()
            }
            else -> {
            }
        }
        return super.onOptionsItemSelected(item)
    }

    companion object {
        const val RC_VIEW_PROMO = 1231
        const val KEY_DATA = "code"
        fun start(context: Context): Intent {
            return Intent(context, VouchersActivity::class.java)
        }
    }
}
