package sg.kickbates.customer.ui.main.pay

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Intent
import android.graphics.Bitmap
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.MenuItem
import android.view.View
import android.webkit.WebChromeClient
import android.webkit.WebView
import android.webkit.WebViewClient
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import okhttp3.HttpUrl.Companion.toHttpUrlOrNull
import retrofit2.http.Url
import sg.kickbates.customer.R
import sg.kickbates.customer.databinding.PayCardActivityBinding
import sg.kickbates.customer.helper.DialogHelper
import sg.kickbates.customer.ui.auth.AuthWebViewActivity
import sg.kickbates.customer.ui.dialog.account_card.DialogAccountCard
import sg.kickbates.customer.ui.payment.channels.PaymentChannelActivity
import sg.kickbates.data.api.models.RequestCard
import sg.kickbates.data.api.models.card.ResponseCustomerCards
import sg.kickbates.data.api.models.payment.RequestPayNow
import sg.kickbates.data.api.models.payment.ResponseChargeCard
import sg.kickbates.data.helper.Pref
import java.net.URI

/**
 *Auth, dapet code
 *Auth 2f, pke code td
 *Req token,pke code dr auth 2f
 */
class PayCardActivity : AppCompatActivity() {

    private lateinit var binding:PayCardActivityBinding
    private lateinit var vm:PayCardActivityVM
    private var slug:String? = null
    private var invoiceId:Int? = null
    private var sessionToken:String? = null
    private var dataCard: ResponseCustomerCards.DataItem.DataCardStatements? = null

    @SuppressLint("SetJavaScriptEnabled")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this,R.layout.pay_card_activity)
        vm = PayCardActivityVM(Pref(this))
        binding.lifecycleOwner = this
        setSupportActionBar(binding.toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        binding.tvTitleToolbar.text = "Pay"
        slug = intent?.extras?.getString(KEY_BANK_SLUG)
        invoiceId = intent?.extras?.getInt(KEY_ID_INVOICE)
        dataCard = intent?.extras?.getParcelable(KET_DATA_CARD)
        val setting = binding.vwPaymentCard.settings
        setting.javaScriptEnabled = true

        vm.issuerUrl(slug)
        vm.urlBank.observe(this, Observer { url ->
            run {
                if (url != null)initWebView(url)
                else onBackPressed()
            }
        })
        vm.urlAuth2f.observe(this, Observer { url ->
            run {
                if (url != null)initWebView(url)
                else onBackPressed()
            }
        })

        vm.resultToken.observe(this, Observer { data ->
            run {
                if (data != null){
                    sessionToken = data.token?.accessToken
                    val partyId = data.party?.retailParty?.partyId
                    vm.getAccountCard("dbs", partyId, sessionToken)
                }
            } })
        vm.accountCards.observe(this, Observer { data ->
            run {
                binding.vwPaymentCard.visibility = View.GONE
                DialogAccountCard.show(data,invoiceId,sessionToken, dataCard, supportFragmentManager, object : DialogAccountCard.OnDialogAccountCardCallback {
                    override fun onSuccess(data: ResponseChargeCard) {
                        DialogHelper.showDialogSuccessPayment(this@PayCardActivity, PaymentChannelActivity.KEY_TYPE_CARD, data.data, object : DialogHelper.OnDialogSuccessListener{
                            override fun onOk() {
                                setResult(Activity.RESULT_OK)
                                finish()
                            }
                        })
                    }
                    override fun onFailed() {
                        DialogHelper.showDialogError(this@PayCardActivity, "Payment Failed",
                            View.OnClickListener {
                                setResult(Activity.RESULT_CANCELED)
                                finish()
                            })
                    }
                })
            }
        })
    }

    private fun initWebView(url: String?) {
        val httpUrl = url?.toHttpUrlOrNull()
        if (!httpUrl?.host.equals("git.wmzhubo.com", true)){
            binding.vwPaymentCard.webViewClient = webClient
            binding.vwPaymentCard.webChromeClient = chromeClient
            url?.let { binding.vwPaymentCard.loadUrl(it) }
        } else handleUrl(url)

    }

    private val webClient = object : WebViewClient(){
        override fun onPageFinished(view: WebView?, url: String?) {
            super.onPageFinished(view, url)
            vm.loading.set(false)
        }

        override fun onPageStarted(view: WebView?, url: String?, favicon: Bitmap?) {
            vm.loading.set(true)
            handleUrl(url)
            val httpUrl = url?.toHttpUrlOrNull()
            if (httpUrl?.host.equals("git.wmzhubo.com", true)) binding.vwPaymentCard.visibility = View.GONE
            else binding.vwPaymentCard.visibility = View.VISIBLE
            super.onPageStarted(view, url, favicon)


        }
    }

    private fun handleUrl(url:String?){
        if (url != null){
            val httpUrl = url.toHttpUrlOrNull()
            val host = httpUrl?.host
            if (host.equals("git.wmzhubo.com", true)) {
                val code: String?
                val state: String?
                when (slug) {
                    "ocbc" -> code = httpUrl?.queryParameter("access_token")
                    "dbs" -> {
                        state = httpUrl?.queryParameter("state")
                        if (state == "2fsuccess"){
                            code = httpUrl?.queryParameter("code")
                            val requestCard = RequestCard()
                            requestCard.code_auth = code
                            requestCard.issuer = "dbs"
                            vm.getTokenCard(requestCard)
                        }else {
                            code = httpUrl?.queryParameter("code")
                            vm.issuerUrl(slug, code)
                        }
                    }
                    "citi" -> code = httpUrl?.queryParameter("code")
                    else -> { }
                }
            }
        }
    }

    private val chromeClient = object : WebChromeClient(){
        override fun onProgressChanged(view: WebView?, newProgress: Int) {
            super.onProgressChanged(view, newProgress)
            vm.loading.set(newProgress > 100)
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> onBackPressed()
            else -> {
            }
        }
        return super.onOptionsItemSelected(item)
    }
    companion object {
        const val RC_PAY_STATEMENT_CARD = 1233
        const val KEY_ID_INVOICE = "key_invoice_id"
        const val KEY_BANK_SLUG = "key_slug"
        const val KET_DATA_CARD = "key_data_card"
    }
}
