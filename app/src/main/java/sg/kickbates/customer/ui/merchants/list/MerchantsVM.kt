package sg.kickbates.customer.ui.merchants.list

import androidx.databinding.BaseObservable
import androidx.databinding.Bindable
import androidx.databinding.ObservableField
import androidx.lifecycle.LiveData
import androidx.lifecycle.Transformations
import androidx.paging.LivePagedListBuilder
import androidx.paging.PagedList
import sg.kickbates.data.api.models.ResponseMerchants
import sg.kickbates.data.helper.Pref
import sg.kickbates.data.helper.States
import sg.kickbates.data.paging.MerchantsDataSource
import sg.kickbates.data.paging.MerchantsDataSourceFactory


/**
 * Created by simx on 30,July,2019
 */
class MerchantsVM(private var pref: Pref) : BaseObservable() {

    private val config = PagedList.Config.Builder()
        .setPageSize(8)
        .setInitialLoadSizeHint(8 * 3)
        .setEnablePlaceholders(true)
        .build()


    private var merchantsSourceFactory: MerchantsDataSourceFactory = MerchantsDataSourceFactory()
    var merchants: LiveData<PagedList<ResponseMerchants.DataItem>>
    var  states : LiveData<States>? = null
    var  msg : LiveData<String>? = null

    @Bindable var loading = ObservableField<Boolean>()
    @Bindable var empty = ObservableField<Boolean>()

    init {
        merchants = LivePagedListBuilder(merchantsSourceFactory, config).build()
        states =  Transformations.switchMap<MerchantsDataSource, States>(merchantsSourceFactory.merchants!!, MerchantsDataSource::state)
        msg  = Transformations.switchMap<MerchantsDataSource, String>(merchantsSourceFactory.merchants!!, MerchantsDataSource::errorMsg)

    }

    fun getMerchants(){

        merchantsSourceFactory.query(pref.token,null,null,pref.lat, pref.lng)
        merchantsSourceFactory.merchants?.value?.invalidate()
    }
    fun getMerchantsByName(name:String?){
        merchantsSourceFactory.query(pref.token,name,null,pref.lat, pref.lng)
        merchantsSourceFactory.merchants?.value?.invalidate()
    }
    fun getMerchantsByCode(code:String?){
        merchantsSourceFactory.query(pref.token,null,code,pref.lat, pref.lng)
        merchantsSourceFactory.merchants?.value?.invalidate()
    }
}