package sg.kickbates.customer.ui.main.home


import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout

import sg.kickbates.customer.R
import sg.kickbates.customer.databinding.HomeFragmentBinding
import sg.kickbates.customer.helper.slider.AdapterBannerDeals
import sg.kickbates.customer.helper.slider.AdapterImageSlider
import sg.kickbates.customer.ui.deals.DealsActivity
import sg.kickbates.customer.ui.deals.details.DealsDetailsActivity
import sg.kickbates.customer.ui.deals.list.AdapterDealsList
import sg.kickbates.customer.ui.main.MainActivity
import sg.kickbates.customer.ui.main.cards.CardsFragment
import sg.kickbates.customer.ui.main.cards.add.AddCardsActivity
import sg.kickbates.customer.ui.main.cards.details.CardsDetailsActivity
import sg.kickbates.customer.ui.main.cards.dialog.DialogCardsAvailable
import sg.kickbates.customer.ui.main.cards.lists.AdapterCardList
import sg.kickbates.customer.ui.merchants.details.MerchantsDetailsActivity
import sg.kickbates.customer.ui.merchants.list.AdapterMerchantsList
import sg.kickbates.customer.ui.merchants.list.MerchantsActivity
import sg.kickbates.data.api.models.RequestCard
import sg.kickbates.data.api.models.ResponseBanners
import sg.kickbates.data.api.models.ResponseDeals
import sg.kickbates.data.api.models.ResponseMerchants
import sg.kickbates.data.api.models.card.ResponseCustomerCards
import sg.kickbates.data.helper.Pref

/**
 * A simple [Fragment] subclass.
 *
 */

class HomeFragment : Fragment() {
    private lateinit var binding:HomeFragmentBinding
    private lateinit var vm:HomeFragmentVM

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater,R.layout.home_fragment, container,false)
        vm = HomeFragmentVM(Pref(this.requireContext()))
        binding.lifecycleOwner = viewLifecycleOwner
        binding.homeVm = vm
        binding.refresh.setOnRefreshListener {
            if (binding.refresh.isRefreshing){
                vm.getBannerDeals()
            }
        }
        vm.getBannerDeals()
        return binding.root
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        vm.promos.observe(viewLifecycleOwner, { data -> run {
            vm.emptyPromo.set(data.isNullOrEmpty())
                if (data != null)initPromo(data)
            }
        })
        vm.cards.observe(viewLifecycleOwner, { data -> run {
            vm.emptyCards.set(data.isNullOrEmpty())
            if (data != null)initCards(data) }
        })
        vm.merchants.observe(viewLifecycleOwner, { data -> run {
            vm.emptyMerchant.set(data.isNullOrEmpty())
            if (data != null)initMerchants(data)
        }
        })

        vm.deals.observe(viewLifecycleOwner, { data -> run {
            vm.emptyDeals.set(data.isNullOrEmpty())
            binding.refresh.isRefreshing = false
            if (data != null)initDeals(data) }
        })

        binding.tvMore.setOnClickListener {
            (activity as MainActivity).loadFragment(CardsFragment.instance(),resources.getString(R.string.title_cards),false)
            (activity as MainActivity).carNavigationClik()
        }
        binding.tvMoreMerchants.setOnClickListener {
            MerchantsActivity.start(this.requireContext())

        }
        binding.tvMoreDeals.setOnClickListener {
            DealsActivity.start(this.requireContext())
        }
        vm.dealsBanner.observe(viewLifecycleOwner, { data -> run {
            binding.vpBanner.adapter = AdapterBannerDeals(data)
            binding.tabIndicatorBanner.setupWithViewPager(binding.vpBanner, true)
        } })

        binding.btnAddCard.setOnClickListener {
            DialogCardsAvailable.show(childFragmentManager, object : DialogCardsAvailable.OnDialogCardsAvailableCallback {
                override fun onDialogDismiss(code: RequestCard?) {
                    val intent = Intent(this@HomeFragment.requireContext(), AddCardsActivity::class.java)
                    intent.putExtra(AddCardsActivity.KEY_DATA, code)
                    startActivityForResult(intent, AddCardsActivity.RC_ADD)
                }
            })
        }
    }



    private fun initPromo(data: List<ResponseBanners.DataItem>) {
        binding.vpBanner.adapter = AdapterImageSlider(data)
        binding.tabIndicatorBanner.setupWithViewPager(binding.vpBanner, true)

    }

    private fun initCards(data: List<ResponseCustomerCards.DataItem>){
        binding.rcvCard.apply {
            hasFixedSize()
            itemAnimator = DefaultItemAnimator()
            isNestedScrollingEnabled = false
            layoutManager = GridLayoutManager(this@HomeFragment.requireContext(),2)
            adapter = AdapterCardList(data, object : AdapterCardList.OnAdapterCardListListener {
                override fun onAdapterClicked(data: ResponseCustomerCards.DataItem) {
                    CardsDetailsActivity.start(this@HomeFragment.requireContext(),data)
                }
            })
        }
    }
    private fun initMerchants(data:List<ResponseMerchants.DataItem>){
        binding.rcvMerchants.apply {
            hasFixedSize()
            itemAnimator = DefaultItemAnimator()
            layoutManager = LinearLayoutManager(this@HomeFragment.requireContext(),RecyclerView.HORIZONTAL,false)
            isNestedScrollingEnabled = false
            adapter = AdapterMerchantsList(data, object : AdapterMerchantsList.OnAdapterMerchantsListListener{
                override fun onAdapterMerchantsClicked(data: ResponseMerchants.DataItem) {
                    MerchantsDetailsActivity.start(this@HomeFragment.requireContext(), data)
                }

            })
        }
    }
    private fun initDeals(data:List<ResponseDeals.DataItem>){
        binding.rcvDeals.apply {
            hasFixedSize()
            itemAnimator = DefaultItemAnimator()
            isNestedScrollingEnabled = false
            layoutManager = LinearLayoutManager(this@HomeFragment.requireContext(),RecyclerView.HORIZONTAL,false)
            adapter = AdapterDealsList(data, object : AdapterDealsList.OnAdapterDealsListListener{
                override fun onClicked(data: ResponseDeals.DataItem) {
                    DealsDetailsActivity.start(this@HomeFragment.requireContext(),data)
                }
            })
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        when (requestCode) {
            AddCardsActivity.RC_ADD -> vm.dataCards()
            else -> {
            }
        }
        super.onActivityResult(requestCode, resultCode, data)
    }


    companion object {
        const val KEY_DATA = "key_data"
        fun instance() : HomeFragment{
            val bundle =  Bundle()
            val fragment = HomeFragment()
            fragment.arguments = bundle
            return fragment
        }
    }

}
