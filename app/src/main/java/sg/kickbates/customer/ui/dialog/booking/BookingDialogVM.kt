package sg.kickbates.customer.ui.dialog.booking

import androidx.databinding.BaseObservable
import androidx.databinding.Bindable
import androidx.databinding.ObservableField
import androidx.lifecycle.MutableLiveData
import kotlinx.coroutines.CoroutineExceptionHandler
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.SupervisorJob
import kotlinx.coroutines.launch
import retrofit2.HttpException
import sg.kickbates.data.BuildConfig
import sg.kickbates.data.api.Api
import sg.kickbates.data.api.models.booking.RequestBooking
import sg.kickbates.data.api.models.booking.ResponseBooking
import sg.kickbates.data.helper.Pref

/**
 * Created by simx on 10,August,2019
 */
class BookingDialogVM(private var pref: Pref): BaseObservable() {
    private var job = SupervisorJob()
    @Bindable var loading = ObservableField<Boolean>()
    @Bindable var error = MutableLiveData<String>()
    @Bindable var booking = MutableLiveData<ResponseBooking.Data>()
    fun createBooking(requestBooking: RequestBooking) {
        loading.set(true)
        try {
            CoroutineScope(CoroutineExceptionHandler { _, throwable -> run {
                job = SupervisorJob()
                throwable.printStackTrace()
                updateMsg(throwable.message)
            } }).launch {
                val res = Api.Factory.create(BuildConfig.BASE_URL).bookMerchantsAsync(pref.token, requestBooking).await()
                res.let {
                    when (it.status) {
                         200 -> {if (res.data == null) booking.postValue(null) else booking.postValue(res.data)}
                         else -> {updateMsg(res.message)}
                   }
                }
                loading.set(false)
            }
        }catch (e: HttpException){
            e.printStackTrace()
            updateMsg(e.message)
        }
    }

    private fun updateMsg(msg:String?){
        loading.set(false)
        error.postValue(msg)
    }
}