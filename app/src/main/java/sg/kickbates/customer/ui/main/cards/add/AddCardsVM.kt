package sg.kickbates.customer.ui.main.cards.add

import androidx.databinding.BaseObservable
import androidx.databinding.Bindable
import androidx.databinding.ObservableField
import androidx.lifecycle.MutableLiveData
import kotlinx.coroutines.CoroutineExceptionHandler
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.SupervisorJob
import kotlinx.coroutines.launch
import retrofit2.HttpException
import sg.kickbates.data.BuildConfig
import sg.kickbates.data.api.Api
import sg.kickbates.data.api.models.RequestCard
import sg.kickbates.data.api.models.card.ResponseAddCard
import sg.kickbates.data.helper.Pref

/**
 * Created by simx on 30,July,2019
 */
class AddCardsVM(private var pref: Pref): BaseObservable() {
    private var job  = SupervisorJob()
    @Bindable var loading = ObservableField<Boolean>()
    @Bindable var note = MutableLiveData<String>()
    @Bindable var cards = MutableLiveData<List<ResponseAddCard.DataItem>>()


    /**
     * buat ngambil card flow nya agak beda dikit tiap issuer, paling gampang ocbc
    ocbc:
    1. auth di webview, dapet code
    2. tinggal hit /cards method get, issuer: ocbc ; session_token: code
    user/pass = random 6 digit / bebas

    citi:
    1. auth di webview, dapet code
    2. hit kick_customer_cards/token issuer: citi ; code: code . nanti dapet token
    3. baru hit /cards method get, issuer: citi ; session_token: token

    user / pass  = SandboxUser2	P@ssUser2$

    dbs rada2 nih:
    1. auth di webview dapet code
    2. hit kick_customer_cards/token issuer: dbs ; code: code . nanti dapet token dan party_id
    3. hit kick_customer_cards/party issuer: dbs ; party_id: party id ; accessToken: token , nanti ada party_token
    baru hit /cards method get, issuer: dbs ; token: token ; party_token: party_token

    user / pass iw038 : 456789
     */

    fun prepareAddNewCar(requestCard: RequestCard?){
        when (requestCard?.issuer) {
            "ocbc" -> addCard(requestCard?.issuer, requestCard.code_auth)
            "dbs" -> {getTokenCard(requestCard)}
            "citi" -> {getTokenCardCiti(requestCard)}
            else -> {}
        }
    }

    private fun getTokenCard(requestCard: RequestCard?){
        loading.set(true)
        note.postValue("Please be patient.. \nWe are requesting token to bank ")
        try {
            CoroutineScope(CoroutineExceptionHandler { _, throwable -> run {
                job = SupervisorJob()
                throwable.printStackTrace()
                loading.set(false)
                note.postValue("We are sorry.. \n We are failed add your card \n${BuildConfig.ERROR_MSG} ")
            } }).launch {
                val res = Api.Factory.create(BuildConfig.BASE_URL).tokenCardAsync(pref.token, requestCard?.issuer, requestCard?.code_auth).await()
                res.let {
                    when (it.status) {
                        200 -> getPartyCard(requestCard?.issuer, res.data?.token?.partyId, res.data?.token?.accessToken)
                        else -> note.postValue("Please try again.. \nWe are failed requesting  token to bank ")
                    }
                }
                loading.set(false)
            }
        }catch (e: HttpException){
            e.printStackTrace()
            loading.set(false)
        }
    }
    private fun getTokenCardCiti(requestCard: RequestCard?){
        loading.set(true)
        note.postValue("Please be patient.. \nWe are requesting token to bank ")
        try {
            CoroutineScope(CoroutineExceptionHandler { _, throwable -> run {
                job = SupervisorJob()
                throwable.printStackTrace()
                loading.set(false)
                note.postValue("We are sorry.. \n We are failed add your card \n${BuildConfig.ERROR_MSG} ")
            } }).launch {
                val res = Api.Factory.create(BuildConfig.BASE_URL).tokenCardCitiAsync(pref.token, requestCard?.issuer, requestCard?.code_auth).await()
                res.let {
                    when (it.status) {
                        200 -> addCard(requestCard?.issuer, res.data)
                        else -> {
                            note.postValue("Please try again.. \nWe are failed requesting  token to bank ")
                        }
                    }
                }
                loading.set(false)
            }
        }catch (e: HttpException){
            e.printStackTrace()
            loading.set(false)
        }
    }
    private fun getPartyCard(issuer:String?, partyId:String?, accessToken:String?){
        loading.set(true)
        note.postValue("Please be patient.. \nWe are requesting party token to bank ")

        try {
            CoroutineScope(CoroutineExceptionHandler { _, throwable -> run {
                job = SupervisorJob()
                throwable.printStackTrace()
                loading.set(false)
                note.postValue("We are sorry.. \n We are failed add your card \n${BuildConfig.ERROR_MSG} ")
            } }).launch {
                val res = Api.Factory.create(BuildConfig.BASE_URL).partyCardAsync(
                    pref.token, issuer, partyId, accessToken
                ).await()
                res.let {
                    when (it.status) {
                         200 -> {
                             addCard(issuer, res.data?.party?.partyId, accessToken)
                         }
                         else -> {
                             note.postValue("Please try again.. \nWe are failed requesting party token to bank ")
                         }
                   }
                }
                loading.set(false)
            }
        }catch (e: HttpException){
            e.printStackTrace()
            loading.set(false)
        }
    }
    private fun addCard(issuer:String?, partyId:String?, accessToken:String?){
        loading.set(true)
        note.postValue("Please be patient.. \nWe are requesting Card to bank ")
        try {
            CoroutineScope(CoroutineExceptionHandler { _, throwable -> run {
                job = SupervisorJob()
                throwable.printStackTrace()
                loading.set(false)
                note.postValue("We are sorry.. \n We are failed add your card \n${BuildConfig.ERROR_MSG} ")
            } }).launch {
                val res = Api.Factory.create(BuildConfig.BASE_URL).addCardAsync(
                    pref.token, issuer, partyId, accessToken
                ).await()
                res.let {
                    when (it.status) {
                         200 -> {
                             if (res.data?.isEmpty()!!)cards.postValue(listOf()) else cards.postValue(res.data)
                             note.postValue("Thanks for be patient.. \nWe are success add your card ")
                         }
                         else -> {
                             note.postValue("We are sorry.. \nWe are failed add your card ")
                         }
                   }
                }
                loading.set(false)
            }
        }catch (e: HttpException){
            e.printStackTrace()
            loading.set(false)
        }
    }
    private fun addCard(issuer:String?, seasonToken:String?){
        loading.set(true)
        note.postValue("Please be patient.. \nWe are requesting Card to bank ")
        try {
            CoroutineScope(CoroutineExceptionHandler { _, throwable -> run {
                job = SupervisorJob()
                throwable.printStackTrace()
                loading.set(false)
                note.postValue("We are sorry.. \n We are failed add your card \n${BuildConfig.ERROR_MSG} ")
            } }).launch {
                val res = Api.Factory.create(BuildConfig.BASE_URL).addCardAsync(pref.token, issuer, seasonToken).await()
                res.let {
                    when (it.status) {
                        200 -> {
                            if (res.data?.isEmpty()!!)cards.postValue(listOf()) else cards.postValue(res.data)
                            note.postValue("Thanks for be patient.. \nWe are success add your card ")
                        }
                        else -> {
                            note.postValue("We are sorry.. \nWe are failed add your card ")
                        }
                    }
                }
                loading.set(false)
            }
        }catch (e: HttpException){
            e.printStackTrace()
            loading.set(false)
        }
    }

}