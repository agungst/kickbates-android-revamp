package sg.kickbates.customer.ui.deals.list

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.paging.PagedListAdapter
import androidx.recyclerview.widget.RecyclerView
import sg.kickbates.customer.R
import sg.kickbates.customer.databinding.ItemDealsFullBinding
import sg.kickbates.customer.ui.merchants.list.AdapterPagingMerchantsListFull
import sg.kickbates.data.api.models.ResponseDeals
import sg.kickbates.data.api.models.ResponseMerchants
import sg.kickbates.data.helper.KickDiffUtils

/**
 * Created by simx on 31,July,2019
 */
class AdapterPagingDealsListFull(private var listener: OnAdapterDealsFullListener): PagedListAdapter<ResponseDeals.DataItem, AdapterPagingDealsListFull.Holder>(
    KickDiffUtils.DealsDiff)  {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): Holder {
        return Holder(ItemDealsFullBinding.bind(LayoutInflater.from(parent.context).inflate(R.layout.item_deals_full,parent,false)))
    }

    override fun onBindViewHolder(holder: Holder, position: Int) {
        holder.bind(getItem(position))
        holder.itemView.setOnClickListener { listener.onClicked(getItem(position)) }
        holder.binding.btnView.setOnClickListener { listener.onClicked(getItem(position)) }

    }

    interface OnAdapterDealsFullListener {
        fun onClicked(data: ResponseDeals.DataItem?)
    }
    class Holder(var binding: ItemDealsFullBinding):RecyclerView.ViewHolder(binding.root) {
        fun bind(data: ResponseDeals.DataItem?){
            with(binding){
                itemDealFullVm = ItemDealsFullVM(data)
                executePendingBindings()
            }
        }

    }
}