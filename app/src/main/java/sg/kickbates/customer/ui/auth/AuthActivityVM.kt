package sg.kickbates.customer.ui.auth


import androidx.databinding.BaseObservable
import androidx.databinding.Bindable
import androidx.lifecycle.MutableLiveData
import kotlinx.coroutines.*
import retrofit2.HttpException
import sg.kickbates.data.BuildConfig
import sg.kickbates.data.api.Api
import sg.kickbates.data.api.models.RequestLoginFacebook
import sg.kickbates.data.api.models.RequestLoginGoogle
import sg.kickbates.data.api.models.RequestRegisterFacebook
import sg.kickbates.data.api.models.RequestRegisterGoogle
import sg.kickbates.data.helper.LoginCase
import sg.kickbates.data.helper.Pref
import sg.kickbates.data.helper.States
import java.util.*


/**
 * Created by simx on 10,August,2019
 */
class AuthActivityVM(private var pref: Pref):BaseObservable() {
    private var job: Job? = SupervisorJob()


    @Bindable var errorMsg = MutableLiveData<String>()
    @Bindable var token = MutableLiveData<String>()
    @Bindable var state = MutableLiveData<States>()

    fun login(id: String?, email: String?, name: String?, type: LoginCase) {
        state.postValue(States.LOADING)
        try {
            CoroutineScope(CoroutineExceptionHandler { _, throwable -> run {
                state.postValue(States.DONE)
                job = SupervisorJob()
                throwable.printStackTrace()
                register(id,email, name, type)
            }
            }).launch {
                when (type) {
                    LoginCase.FACEBOOK -> {
                        val requestLoginFacebook = RequestLoginFacebook(id)
                        val res = Api.Factory.create(BuildConfig.BASE_URL).loginFbAsync(requestLoginFacebook).await()
                        if (res.status == 200){
                            state.postValue(States.DONE)
                            pref.token = res.token()!!
                            getMe(res.token()!!)
                        }else register(id,email, name, type)
                    }
                    else -> {
                        val requestLoginGoogle = RequestLoginGoogle(id)
                        val res = Api.Factory.create(BuildConfig.BASE_URL).loginGoogleAsync(requestLoginGoogle).await()
                        if (res.status == 200){
                            state.postValue(States.DONE)
                            pref.token = res.token()!!
                            getMe(res.token()!!)
                        } else register(id,email, name, type)
                    }
                }
            }


        }catch (e: HttpException){
            e.printStackTrace()
        }
    }

    private fun register(id: String?, email: String?, name: String?, type: LoginCase) {
        try {
            CoroutineScope(CoroutineExceptionHandler { _, throwable -> run {
                state.postValue(States.DONE)
                job = SupervisorJob()
                throwable.printStackTrace()
            } }).launch {
                when (type) {
                    LoginCase.FACEBOOK -> {
                        val requestRegisterFacebook = RequestRegisterFacebook(name,email,null,id, UUID.randomUUID().toString())
                        val res = Api.Factory.create(BuildConfig.BASE_URL).registerFacebookAsync(requestRegisterFacebook).await()
                        state.postValue(States.DONE)
                        if (res.status == 200){
                            login(id, email, name, type)
                        } else errorMsg.postValue(BuildConfig.ERROR_MSG)
                    }
                    else -> {
                        val requestRegisterGoogle = RequestRegisterGoogle(name, email,null, id, UUID.randomUUID().toString())
                        val res = Api.Factory.create(BuildConfig.BASE_URL).registerGoogleAsync(requestRegisterGoogle).await()
                        state.postValue(States.DONE)
                        if (res.status == 200){
                            login(id, email, name, type)
                        } else errorMsg.postValue(BuildConfig.ERROR_MSG)
                    }
                }
            }

        }catch (e:HttpException){
            e.printStackTrace()
        }
    }

    private fun getMe(access_token:String?){
        try {
            CoroutineScope(CoroutineExceptionHandler { _, throwable -> run {
                job = SupervisorJob()
                throwable.printStackTrace()
            } }).launch {
                val res = Api.Factory.create(BuildConfig.BASE_URL).meAsync(access_token).await()
                pref.usersId = res.data?.id
                pref.email = res.data?.email
                pref.username = res.data?.username
                pref.token = access_token!!
                res.data?.fcmToken.let { pref.fcmToken = it.toString() }
                token.postValue(access_token)
                res.data?.subscribePremium().let { if (it != null) pref.subscribePremiumAccount = it }
            }
        }catch (e: HttpException){
            e.printStackTrace()

        }
    }
}