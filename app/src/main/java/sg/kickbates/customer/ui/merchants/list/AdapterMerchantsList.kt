package sg.kickbates.customer.ui.merchants.list

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import sg.kickbates.customer.R
import sg.kickbates.customer.databinding.ItemMerchantsBinding
import sg.kickbates.data.api.models.ResponseMerchants

/**
 * Created by simx on 31,July,2019
 */
class AdapterMerchantsList(private var datas:List<ResponseMerchants.DataItem>, private var listener: OnAdapterMerchantsListListener): RecyclerView.Adapter<AdapterMerchantsList.Holder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): Holder {
        return Holder(ItemMerchantsBinding.bind(LayoutInflater.from(parent.context).inflate(R.layout.item_merchants,parent,false)))
    }


    override fun getItemCount(): Int {
        return datas.size
    }

    override fun onBindViewHolder(holder: Holder, position: Int) {
        holder.bind(datas[position])
        holder.itemView.setOnClickListener { listener.onAdapterMerchantsClicked(datas[position]) }
    }

    interface OnAdapterMerchantsListListener {
        fun onAdapterMerchantsClicked(data: ResponseMerchants.DataItem)
    }
    class Holder(var binding: ItemMerchantsBinding):RecyclerView.ViewHolder(binding.root) {
        fun bind(data: ResponseMerchants.DataItem){
            with(binding){
                itemMerchantVm = ItemMerchantsVM(data)
                executePendingBindings()
            }
        }

    }
}