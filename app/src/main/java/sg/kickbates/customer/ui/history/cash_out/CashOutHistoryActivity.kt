package sg.kickbates.customer.ui.history.cash_out

import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.MenuItem
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import sg.kickbates.customer.R
import sg.kickbates.customer.databinding.CashOutHistoryActivityBinding
import sg.kickbates.data.helper.Pref

class CashOutHistoryActivity : AppCompatActivity() {
    private lateinit var binding:CashOutHistoryActivityBinding
    private lateinit var vm:CashOutHistoryVM

    private lateinit var adapterPagingCashOutHistory: AdapterPagingCashOutHistory

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this,R.layout.cash_out_history_activity)
        vm = CashOutHistoryVM(Pref(this))
        binding.lifecycleOwner = this
        binding.cashOutVm = vm
        setSupportActionBar(binding.toolbar)
        binding.tvTitleToolbar.text = "Cashout History"
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        adapterPagingCashOutHistory = AdapterPagingCashOutHistory()
        vm.dataHistory()
        vm.emptyState?.observe(this, Observer { data -> vm.empty.set(data) })
        vm.loadingState?.observe(this, Observer { data -> vm.loading.set(data) })
        vm.history.observe(this, Observer { data -> run {
            if (binding.refresh.isRefreshing) binding.refresh.isRefreshing = false
            adapterPagingCashOutHistory.submitList(data)
            binding.rcvCashOutHistory.apply {
                hasFixedSize()
                itemAnimator = DefaultItemAnimator()
                layoutManager = LinearLayoutManager(this@CashOutHistoryActivity)
                adapter = adapterPagingCashOutHistory
            }
        } })

        binding.refresh.setOnRefreshListener { if (binding.refresh.isRefreshing) vm.dataHistory() }

    }
    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> onBackPressed()
            else -> { }
        }
        return super.onOptionsItemSelected(item)
    }

    companion object {
        fun start(context: Context){
            val intent = Intent(context, CashOutHistoryActivity::class.java)
            intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
            context.startActivity(intent)
        }
    }
}
