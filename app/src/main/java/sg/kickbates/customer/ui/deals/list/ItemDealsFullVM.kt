package sg.kickbates.customer.ui.deals.list

import androidx.databinding.BaseObservable
import androidx.databinding.Bindable
import androidx.databinding.ObservableField
import sg.kickbates.data.api.models.ResponseDeals

/**
 * Created by simx on 07,August,2019
 */
class ItemDealsFullVM(data:ResponseDeals.DataItem?):BaseObservable() {
    @Bindable var image = ObservableField(data?.image())
    @Bindable var name = ObservableField(data?.dealName)
}