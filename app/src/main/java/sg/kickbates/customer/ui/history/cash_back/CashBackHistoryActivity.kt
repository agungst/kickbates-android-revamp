package sg.kickbates.customer.ui.history.cash_back

import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.MenuItem
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import sg.kickbates.customer.R
import sg.kickbates.customer.databinding.CashBackHistoryActivityBinding
import sg.kickbates.data.helper.Pref

class CashBackHistoryActivity : AppCompatActivity() {
    private lateinit var binding:CashBackHistoryActivityBinding
    private lateinit var vm:CashBackHistoryVM
    private lateinit var adapterPagingCashBackHistory: AdapterPagingCashBackHistory
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this,R.layout.cash_back_history_activity)
        vm = CashBackHistoryVM(Pref(this))
        binding.lifecycleOwner = this
        binding.cashBackHistoryVM = vm
        setSupportActionBar(binding.toolbar)
        binding.tvTitleToolbar.text = "Earning History"
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        adapterPagingCashBackHistory = AdapterPagingCashBackHistory()
        vm.dataHistory()
        vm.emptyState?.observe(this, Observer { data -> vm.empty.set(data) })
        vm.loadingState?.observe(this, Observer { data -> vm.loading.set(data) })
        vm.history.observe(this, Observer { data -> run {
            if (binding.refresh.isRefreshing) binding.refresh.isRefreshing = false
            adapterPagingCashBackHistory.submitList(data)
            binding.rcvCashBackHistory.apply {
                hasFixedSize()
                itemAnimator = DefaultItemAnimator()
                layoutManager = LinearLayoutManager(this@CashBackHistoryActivity)
                adapter = adapterPagingCashBackHistory
            }
        } })

        binding.refresh.setOnRefreshListener { if (binding.refresh.isRefreshing) vm.dataHistory() }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> onBackPressed()
            else -> { }
        }
        return super.onOptionsItemSelected(item)
    }
    companion object {
        fun start(context: Context){
            val intent = Intent(context, CashBackHistoryActivity::class.java)
            intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
            context.startActivity(intent)
        }
    }
}
