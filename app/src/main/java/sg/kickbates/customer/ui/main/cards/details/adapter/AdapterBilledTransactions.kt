package sg.kickbates.customer.ui.main.cards.details.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import sg.kickbates.customer.R
import sg.kickbates.customer.databinding.ItemBilledTransactionsBinding
import sg.kickbates.data.api.models.card.ResponseCustomerCards


/**
 * Created by simx on 31,July,2019
 */
class AdapterBilledTransactions(private var items:List<ResponseCustomerCards.DataItem.DataCardTransactions>, private var listener: OnAdapterBilledTransactionsListener): RecyclerView.Adapter<AdapterBilledTransactions.Holder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): Holder {
        return Holder(ItemBilledTransactionsBinding.bind(LayoutInflater.from(parent.context).inflate(R.layout.item_billed_transactions,parent,false)))
    }

    override fun getItemCount(): Int {
        return items.size
    }

    override fun onBindViewHolder(holder: Holder, position: Int) {
        holder.bind(items[position])
        holder.itemView.setOnClickListener { listener.onAdapterClicked(items[position]) }
    }

    interface OnAdapterBilledTransactionsListener {
        fun onAdapterClicked(data: ResponseCustomerCards.DataItem.DataCardTransactions)
    }

    class Holder(var binding: ItemBilledTransactionsBinding):RecyclerView.ViewHolder(binding.root) {
        fun bind(data: ResponseCustomerCards.DataItem.DataCardTransactions){
            with(binding){
                itemBilledTransactionVm = ItemBilledTransactionsVM(data)
                executePendingBindings()
            }
        }

    }
}