package sg.kickbates.customer.ui.banner

import androidx.databinding.BaseObservable
import androidx.databinding.Bindable
import androidx.databinding.ObservableField

/**
 * Created by simx on 20,November,2019
 */
class DetailBannerVM: BaseObservable() {
    @Bindable
    var noBanner = ObservableField<Boolean>()
}