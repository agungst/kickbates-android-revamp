package sg.kickbates.customer.ui.notification

import androidx.databinding.BaseObservable
import androidx.databinding.Bindable
import androidx.databinding.ObservableField
import sg.kickbates.data.api.models.notification.ResponseNotificationHistory

/**
 * Created by simx on 14,April,2020
 */
class ItemNotificationHistoryVM(data:ResponseNotificationHistory.DataNotification):BaseObservable() {
    @Bindable var title = ObservableField<String>(data.notifyMsg)
    @Bindable var date = ObservableField<String>(data.data())
}