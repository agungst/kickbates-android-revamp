package sg.kickbates.customer.ui.main.cards.dialog

import androidx.databinding.BaseObservable
import androidx.databinding.Bindable
import androidx.databinding.ObservableField
import androidx.lifecycle.MutableLiveData
import kotlinx.coroutines.CoroutineExceptionHandler
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.SupervisorJob
import kotlinx.coroutines.launch
import retrofit2.HttpException
import sg.kickbates.data.BuildConfig
import sg.kickbates.data.api.Api
import sg.kickbates.data.api.models.card.ResponseCardAvaiable
import sg.kickbates.data.helper.Pref

/**
 * Created by simx on 13,November,2019
 */
class DialogCardsAvailableVM(private var pref: Pref): BaseObservable() {
    private var job = SupervisorJob()

    @Bindable var loading = ObservableField<Boolean>()
    @Bindable var empty = ObservableField<Boolean>()

    @Bindable var error = MutableLiveData<String>()
    @Bindable var banks = MutableLiveData<List<ResponseCardAvaiable.DataItem>>()
    @Bindable var dataAuth = MutableLiveData<String>()

    fun dataBanks(all: Boolean) {
        loading.set(true)
        try {
            CoroutineScope(CoroutineExceptionHandler { _, throwable -> run {
                job = SupervisorJob()
                throwable.printStackTrace()
                updateMsg(throwable.message)
            } }).launch {
                val res = if (!all) Api.Factory.create(BuildConfig.BASE_URL).cardsAvailableAsync(pref.token).await()
                else Api.Factory.create(BuildConfig.BASE_URL).allBankAvailableAsync(pref.token).await()

                res.let {
                    when (it.status) {
                        200 -> {if (res.data.isNullOrEmpty())banks.postValue(listOf())else banks.postValue(res.data)}
                        else -> {updateMsg(res.message)}
                    }
                }
                loading.set(false)
            }
        }catch (e: HttpException){
            e.printStackTrace()
            updateMsg(e.message)

        }
    }




    fun authCard(bankSlug: String?) {
        loading.set(true)
        try {
            CoroutineScope(CoroutineExceptionHandler { _, throwable -> run {
                job = SupervisorJob()
                throwable.printStackTrace()
                updateMsg(throwable.message)
            } }).launch {
                val res = Api.Factory.create(BuildConfig.BASE_URL).authCardAsync(pref.token, bankSlug).await()
                res.let {
                    when (it.status) {
                        200 -> {if (res.data.isNullOrEmpty())dataAuth.postValue(null)else dataAuth.postValue(res.data)}
                        else -> {updateMsg("")}
                    }
                }
                loading.set(false)
            }
        }catch (e: HttpException){
            e.printStackTrace()
            updateMsg(e.message)

        }
    }

    private fun updateMsg(msg:String?){
        error.postValue(BuildConfig.ERROR_MSG)
        loading.set(false)
    }

    fun dataBankStatic() {
        var dbsPosb = ResponseCardAvaiable.DataItem("dbs","",0,"dbs","","DBS/POSB",1, 1)
        banks.postValue(listOf(dbsPosb))
    }
}