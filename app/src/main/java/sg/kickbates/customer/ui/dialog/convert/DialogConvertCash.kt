package sg.kickbates.customer.ui.dialog.convert

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import sg.kickbates.customer.R
import sg.kickbates.customer.databinding.DialogConvertCashBinding
import sg.kickbates.customer.helper.DialogHelper
import sg.kickbates.data.helper.CommonTools
import sg.kickbates.data.helper.Pref

/**
 * Created by simx on 11,August,2019
 */
class DialogConvertCash constructor(private var availableCash:Double?, private var listener: OnDialogConvertCashListener): BottomSheetDialogFragment() {
    private lateinit var binding:DialogConvertCashBinding
    private lateinit var vm:DialogConvertCashVM
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater,R.layout.dialog_convert_cash, container,false)
        vm = DialogConvertCashVM(Pref(this.context!!))
        binding.lifecycleOwner = this
        binding.convertCashVm = vm
        return binding.root

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.etCash.setText(availableCash?.let { CommonTools.formatDollarCurrency(it) })
        binding.btnConvert.setOnClickListener {

            if (binding.etAmountTransfer.text.isNullOrBlank()){
                binding.etAmountTransfer.requestFocus()
            }else {
                var amountTransfer = binding.etAmountTransfer.text.toString().toDouble()
                if (amountTransfer > availableCash!!){

                    binding.lytEtAmountTransfer.error = "Invalid transfer amount"
                    binding.etAmountTransfer.requestFocus()
                }else {
                    binding.lytEtAmountTransfer.error = null
                    DialogHelper.dialogOKCancel("Confirm To Transfer ${CommonTools.formatDollarCurrency(amountTransfer)} Hard Cash to Kick$", this@DialogConvertCash.context!!, object : DialogHelper.OnDialogSuccessListener {
                        override fun onOk() {
                            vm.convertCash(amountTransfer)
                        }
                    })
                }
            }
        }
        vm.cash.observe(this, Observer { data -> run {
            if (data != null){
                dismiss()
                listener.onDismiss()
            }
        } })
        vm.error.observe(this, Observer { msg -> run {
            Toast.makeText(this@DialogConvertCash.context!!, msg, Toast.LENGTH_LONG).show()
        } })
    }
    interface OnDialogConvertCashListener {
        fun onDismiss()
    }
}