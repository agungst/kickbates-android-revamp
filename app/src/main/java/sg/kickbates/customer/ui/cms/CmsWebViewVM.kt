package sg.kickbates.customer.ui.cms

import androidx.databinding.BaseObservable
import androidx.databinding.Bindable
import androidx.databinding.ObservableField
import androidx.lifecycle.MutableLiveData
import kotlinx.coroutines.CoroutineExceptionHandler
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.SupervisorJob
import kotlinx.coroutines.launch
import retrofit2.HttpException
import sg.kickbates.data.BuildConfig
import sg.kickbates.data.api.Api
import sg.kickbates.data.api.models.cms.ResponseCms
import sg.kickbates.data.helper.Pref

/**
 * Created by simx on 02,January,2020
 */
class CmsWebViewVM (private var pref: Pref):BaseObservable() {
    private var job = SupervisorJob()

    @Bindable var loading = ObservableField<Boolean>()
    @Bindable var error = MutableLiveData<String>()
    @Bindable var cms = MutableLiveData<ResponseCms>()

    fun dataCms(key:String? ) {
        loading.set(true)
        try {
            CoroutineScope(CoroutineExceptionHandler { _, throwable -> run {
                job = SupervisorJob()
                throwable.printStackTrace()
                updateMsg(BuildConfig.ERROR_MSG)
            } }).launch {
                val res = Api.Factory.create(BuildConfig.BASE_URL).cmsAsync(pref.token, key).await()
                cms.postValue(res)
                loading.set(false)
            }
        }catch (e: HttpException){
            e.printStackTrace()
            updateMsg(BuildConfig.ERROR_MSG)
        }
    }

    private fun updateMsg(msg:String?){
        loading.set(false)
        error.postValue(msg)
        cms.postValue(null)
    }

}