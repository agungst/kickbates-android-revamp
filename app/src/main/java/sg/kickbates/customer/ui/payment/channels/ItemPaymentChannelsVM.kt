package sg.kickbates.customer.ui.payment.channels

import androidx.databinding.BaseObservable
import androidx.databinding.Bindable
import androidx.databinding.ObservableField
import sg.kickbates.data.api.models.payment.ResponsePaymentChannel
import sg.kickbates.data.api.models.payment.ResponsePaymentChannelCard
import sg.kickbates.data.helper.CommonTools

/**
 * Created by simx on 11,August,2019
 */
class ItemPaymentChannelsVM(data: ResponsePaymentChannelCard.DataItem): BaseObservable() {
    //TODO Image payment channel
    @Bindable var name = ObservableField("**** **** **** ${data.last4}")
    @Bindable var icon = ObservableField(CommonTools.cardLogo(data.brand))

}