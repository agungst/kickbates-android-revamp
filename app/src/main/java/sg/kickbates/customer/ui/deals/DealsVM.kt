package sg.kickbates.customer.ui.deals

import androidx.databinding.BaseObservable
import androidx.databinding.Bindable
import androidx.databinding.ObservableField
import androidx.lifecycle.LiveData
import androidx.lifecycle.Transformations
import androidx.paging.LivePagedListBuilder
import androidx.paging.PagedList
import sg.kickbates.data.api.models.ResponseDeals
import sg.kickbates.data.helper.Pref
import sg.kickbates.data.helper.States
import sg.kickbates.data.paging.DealsDataSource
import sg.kickbates.data.paging.DealsDataSourceFactory

/**
 * Created by simx on 30,July,2019
 */
class DealsVM(private var pref: Pref): BaseObservable() {
    private val config = PagedList.Config.Builder()
        .setPageSize(8)
        .setInitialLoadSizeHint(8 * 3)
        .setEnablePlaceholders(true)
        .build()


    private var dealsSourceFactory: DealsDataSourceFactory = DealsDataSourceFactory()
    var  deals  : LiveData<PagedList<ResponseDeals.DataItem>>
    var  states : LiveData<States>? = null
    var  msg    : LiveData<String>? = null

    @Bindable var loading = ObservableField<Boolean>()

    init {
        deals  = LivePagedListBuilder(dealsSourceFactory, config).build()
        states =  Transformations.switchMap<DealsDataSource, States>(dealsSourceFactory.deals!!, DealsDataSource::state)
        msg    = Transformations.switchMap<DealsDataSource, String>(dealsSourceFactory.deals!!, DealsDataSource::errorMsg)
    }
    fun getDeals() {
        dealsSourceFactory.query(pref.token, null, null, pref.lat, pref.lng)
        dealsSourceFactory.deals?.value?.invalidate()
    }
    fun getDealsByName(name:String?) {
        dealsSourceFactory.query(pref.token, name, null, pref.lat, pref.lng)
        dealsSourceFactory.deals?.value?.invalidate()
    }
    fun getDealsByCode(code:String?) {
        dealsSourceFactory.query(pref.token, null, code, pref.lat, pref.lng)
        dealsSourceFactory.deals?.value?.invalidate()
    }

}