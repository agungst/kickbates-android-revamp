package sg.kickbates.customer.ui.auth

import android.app.Activity
import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AlertDialog
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import com.facebook.*
import com.facebook.login.LoginBehavior
import com.facebook.login.LoginManager
import com.facebook.login.LoginResult
import com.google.android.gms.auth.api.signin.*
import com.google.android.gms.common.api.ApiException
import com.google.android.gms.tasks.Task
import sg.kickbates.customer.R
import sg.kickbates.customer.databinding.AuthActivityBinding
import sg.kickbates.customer.helper.DialogHelper
import sg.kickbates.customer.helper.SnackBarHelper
import sg.kickbates.customer.ui.main.MainActivity
import sg.kickbates.data.helper.LoginCase
import sg.kickbates.data.helper.Pref


private const val RC_LOGIN_GOOGLE = 1001

class AuthActivity : AppCompatActivity() {
    private lateinit var binding:AuthActivityBinding
    private lateinit var vm:AuthActivityVM
    private lateinit var callbackManager: CallbackManager
    private lateinit var dialogProses: AlertDialog
    private lateinit var pref: Pref
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        FacebookSdk.isInitialized()
        binding = DataBindingUtil.setContentView(this,R.layout.auth_activity)
        pref = Pref(this)
        vm = AuthActivityVM(pref)
        binding.lifecycleOwner = this
        binding.authVm = vm
        dialogProses = DialogHelper.showDialogProcess(this, "Loading...")
        binding.lytGoogle.setOnClickListener {
            startActivityForResult(gsi()?.signInIntent, RC_LOGIN_GOOGLE)
        }
        callbackManager = CallbackManager.Factory.create()
        binding.lytFacebook.setOnClickListener {
            LoginManager.getInstance().setLoginBehavior(LoginBehavior.WEB_VIEW_ONLY).logInWithReadPermissions(this, listOf("email","public_profile"))
        }
        LoginManager.getInstance().registerCallback(callbackManager, object : FacebookCallback<LoginResult>{
            override fun onSuccess(result: LoginResult?) {
                val graph  = GraphRequest.newMeRequest(result?.accessToken) { `object`, _ -> run {
                    try {
                        vm.login(`object`.getString("id"), `object`.getString("email"), `object`.getString("name"), LoginCase.FACEBOOK)
                    } catch (e:Exception){
                        e.printStackTrace()
                    }

                } }
                val param = Bundle()
                param.putString("fields", "id,name,email,link")
                graph.parameters = param
                graph.executeAsync()
            }

            override fun onCancel() {
                vm.errorMsg.postValue("Login with facebook is canceled")
            }

            override fun onError(error: FacebookException?) {
                vm.errorMsg.postValue(error?.message)
                error?.printStackTrace()
            }
        })

        vm.token.observe(this, Observer { t -> run { if (!t.isNullOrEmpty())gotoMain() } })
        vm.state.observe(this, Observer {_ -> run {} })
        vm.errorMsg.observe(this, Observer { msg -> run { SnackBarHelper.showSnackBarError(msg, binding.rootView) } })

    }

    private fun gsi(): GoogleSignInClient? {
        var gso = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
            .requestIdToken(resources.getString(R.string.default_web_client_id))
            .requestEmail().build()

        return GoogleSignIn.getClient(this, gso)
    }

    private fun checkLastLoginGoogle(): GoogleSignInAccount? {
        return GoogleSignIn.getLastSignedInAccount(this)

    }

    override fun onStart() {
        super.onStart()
    }



    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        callbackManager.onActivityResult(requestCode, resultCode, data)
        when (resultCode) {
            Activity.RESULT_OK -> {
                when (requestCode) {
                    RC_LOGIN_GOOGLE -> {
                        handleSingingRequestGoogle(GoogleSignIn.getSignedInAccountFromIntent(data))
                    }
                    else -> {
                    }
                }
            }
            Activity.RESULT_CANCELED -> {
                when (resultCode) {
                    RC_LOGIN_GOOGLE -> Log.e("AuthActivity","onActivityResult -> CANCEL")
                    else -> {
                    }
                }
            }
            else -> {
                when (resultCode) {
                    RC_LOGIN_GOOGLE -> Log.e("AuthActivity","onActivityResult -> CANCEL")
                    else -> {
                    }
                }
            }
        }
        super.onActivityResult(requestCode, resultCode, data)

    }

    private fun handleSingingRequestGoogle(task: Task<GoogleSignInAccount>?) {

        try {
            var acc = task?.getResult(ApiException::class.java)
            Log.v("AuthActivity","handleSingingRequestGoogle -> ${acc?.displayName}")
            handleGoogleLogin(acc)
        }catch (e: ApiException){
            vm.errorMsg.postValue(e.message)
            handleGoogleLogin(null)
        }
    }
    private fun handleGoogleLogin(gsi: GoogleSignInAccount?) {
        vm.login(gsi?.id, gsi?.email, gsi?.displayName, LoginCase.GOOGLE)

    }

    private fun gotoMain(){
        MainActivity.start(this)
        finish()
    }

    companion object {
        fun start(context: Context){
            val intent = Intent(context, AuthActivity::class.java)
            intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP
            context.startActivity(intent)
        }
    }
}
