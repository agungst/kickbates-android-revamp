package sg.kickbates.customer.ui.main.cards.details.adapter

import androidx.databinding.BaseObservable
import androidx.databinding.Bindable
import androidx.databinding.ObservableField
import sg.kickbates.data.api.models.card.ResponseCustomerCards

/**
 * Created by simx on 06,August,2019
 */
class ItemBilledTransactionsVM(data: ResponseCustomerCards.DataItem.DataCardTransactions) :BaseObservable() {
    @Bindable var transactionDate = ObservableField(data.transactionDate)
    @Bindable var outliteName = ObservableField(data.transactionDescription)
    @Bindable var amount = ObservableField("$ ${data.transactionAmount}")
}