package sg.kickbates.customer.ui.merchants.details

import androidx.databinding.BaseObservable
import androidx.databinding.Bindable
import androidx.databinding.ObservableField
import androidx.lifecycle.MutableLiveData
import kotlinx.coroutines.*
import retrofit2.HttpException
import sg.kickbates.data.BuildConfig
import sg.kickbates.data.api.Api
import sg.kickbates.data.api.models.ResponseDeals
import sg.kickbates.data.helper.Pref

/**
 * Created by simx on 30,July,2019
 */
class MerchantsDetailsVM(private var pref: Pref): BaseObservable() {
    private var job: Job? = SupervisorJob()

    @Bindable var error = MutableLiveData<String>()

    @Bindable var loading = ObservableField<Boolean>()
    @Bindable var notBooking = ObservableField<Boolean>()
    @Bindable var new = ObservableField<Boolean>()
    @Bindable var feature = ObservableField<Boolean>()
    @Bindable var empty = ObservableField<Boolean>()
    @Bindable var deals = MutableLiveData<List<ResponseDeals.DataItem>>()


    fun getDataDeals(merchantId:Int?) {
        loading.set(true)
        try {
            CoroutineScope(CoroutineExceptionHandler { _, throwable -> run {
                job = SupervisorJob() }
                throwable.printStackTrace()
                updateMsg(throwable.message)
            }).launch {
                var res = Api.Factory.create(BuildConfig.BASE_URL).dealsByMerchantIdAsync(pref.token, merchantId).await()
                deals.postValue(res.data)
                loading.set(false)
            }
        }catch (e:HttpException){
            e.printStackTrace()
            updateMsg(e.message)
        }
    }

    private fun updateMsg(msg:String?){
        error.postValue(BuildConfig.ERROR_MSG)
        loading.set(false)
    }
}