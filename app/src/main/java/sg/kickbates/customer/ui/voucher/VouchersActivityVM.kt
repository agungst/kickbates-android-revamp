package sg.kickbates.customer.ui.voucher

import androidx.databinding.BaseObservable
import androidx.databinding.Bindable
import androidx.databinding.ObservableField
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import androidx.paging.LivePagedListBuilder
import androidx.paging.PagedList
import kotlinx.coroutines.CoroutineExceptionHandler
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.SupervisorJob
import kotlinx.coroutines.launch
import retrofit2.HttpException
import sg.kickbates.data.BuildConfig
import sg.kickbates.data.api.Api
import sg.kickbates.data.api.models.voucher.ResponseVouchers
import sg.kickbates.data.api.models.voucher.ResponseVouchersDetail
import sg.kickbates.data.helper.Pref
import sg.kickbates.data.helper.States
import sg.kickbates.data.paging.VoucherDataSource
import sg.kickbates.data.paging.VoucherDataSourceFactory

/**
 * Created by simx on 11,August,2019
 */
class VouchersActivityVM (private var pref: Pref) : BaseObservable(){
    private var job = SupervisorJob()
    private val config = PagedList.Config.Builder()
        .setPageSize(8)
        .setInitialLoadSizeHint(8 * 3)
        .setEnablePlaceholders(true)
        .build()


    private var voucherSourceFactory: VoucherDataSourceFactory = VoucherDataSourceFactory()
    var vouchers: LiveData<PagedList<ResponseVouchers.DataItem>>
    var  states : LiveData<States>? = null

    @Bindable var loading = ObservableField<Boolean>()
    @Bindable var msg = MutableLiveData<String>()
    @Bindable var detailVoucher = MutableLiveData<ResponseVouchers.DataItem>()

    init {
        vouchers = LivePagedListBuilder(voucherSourceFactory, config).build()
        states =  Transformations.switchMap<VoucherDataSource, States>(voucherSourceFactory.vouchers!!, VoucherDataSource::state)
    }

    fun getVoucher(){
        voucherSourceFactory.query(pref.token, pref.usersId)
        //voucherSourceFactory.query(pref.token,3)
        voucherSourceFactory.vouchers?.value?.invalidate()
    }

    fun searchVoucher(code:String?){
        loading.set(true)
        try {
            CoroutineScope(CoroutineExceptionHandler { _, throwable -> run {
                job = SupervisorJob()
                throwable.printStackTrace()
                updateMsg(throwable.message)
            } }).launch {
                Api.Factory.create(BuildConfig.BASE_URL).vouchersDetailsAsync(pref.token, code).await().let {
                    if (it.data == null) detailVoucher.postValue(null)
                    else detailVoucher.postValue(it.data)
                }
                loading.set(false)
            }
        }catch (e: HttpException){
            e.printStackTrace()
            updateMsg(e.message)
        }

    }

    private fun  updateMsg(value:String?){
        msg.postValue(BuildConfig.ERROR_MSG)
        loading.set(false)
    }
}