package sg.kickbates.customer.ui.merchants.details

import android.app.Activity
import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.MenuItem
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import coil.api.load
import sg.kickbates.customer.R
import sg.kickbates.customer.databinding.MerchantsDetailsActivityBinding
import sg.kickbates.customer.helper.SnackBarHelper
import sg.kickbates.customer.ui.deals.details.DealsDetailsActivity
import sg.kickbates.customer.ui.deals.list.AdapterDealsList
import sg.kickbates.customer.ui.dialog.booking.BookingDialog
import sg.kickbates.customer.ui.main.pay.PayActivity
import sg.kickbates.data.api.models.ResponseDeals
import sg.kickbates.data.api.models.ResponseMerchants
import sg.kickbates.data.helper.Pref
import sg.kickbates.data.helper.PrefBuilder

class MerchantsDetailsActivity : AppCompatActivity() {
    private lateinit var binding:MerchantsDetailsActivityBinding
    private lateinit var vm:MerchantsDetailsVM
    private var dataMerchants: ResponseMerchants.DataItem? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this,R.layout.merchants_details_activity)
        vm = MerchantsDetailsVM(Pref(this))
        binding.lifecycleOwner = this
        binding.merchentsDetailVm = vm
        setSupportActionBar(binding.toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        binding.tvTitleToolbar.text = "Details"
        if (!intent?.extras!!.isEmpty)intent?.extras?.getParcelable<ResponseMerchants.DataItem>(
            KEY_DATA).let {data -> run {
            dataMerchants = data
            vm.getDataDeals(dataMerchants?.id)
            vm.new.set(dataMerchants?.merchantIsNew == 1)
            vm.feature.set(dataMerchants?.merchantIsFeatured == 1)
            vm.notBooking.set(dataMerchants?.notBooking())
            binding.tvTitleToolbar.text = data?.sortTitle()
            binding.tvTitle.text = data?.merchantName
            binding.tvCat.text = data?.merchantContactPerson
            binding.tvDesc.text = data?.desc()
            binding.tvAddress.text = data?.merchantAddress
            binding.tvPhone.text = data?.phone()
            binding.tvHours.text = data?.merchantOperatingHour
            binding.imageDetail.load(data?.image()){
                error(R.drawable.placeholder)
                placeholder(R.drawable.placeholder)
            }
        }
        }

        vm.deals.observe(this, Observer { data -> run {initDeals(data)} })
    }

    private fun initDeals(data: List<ResponseDeals.DataItem>) {
        vm.empty.set(data.isNullOrEmpty())
        binding.rcvDeals.apply {
            hasFixedSize()
            itemAnimator = DefaultItemAnimator()
            layoutManager = LinearLayoutManager(this@MerchantsDetailsActivity,RecyclerView.HORIZONTAL, false)
            adapter = AdapterDealsList(data, object : AdapterDealsList.OnAdapterDealsListListener{
                override fun onClicked(data: ResponseDeals.DataItem) {
                    DealsDetailsActivity.start(this@MerchantsDetailsActivity, data)
                }
            })
        }
        binding.lytBookNow.setOnClickListener {
            val dialog = BookingDialog(dataMerchants, object : BookingDialog.BookingDialogListener{
                override fun onSuccessDismissListener() {
                    SnackBarHelper.showSnackBarSuccess("Success", binding.rootView)
                }

                override fun onFailedDismissListener() {
                    SnackBarHelper.showSnackBarError("Failed", binding.rootView)
                }

            })
            dialog.show(supportFragmentManager,"booking")
        }
        binding.lytMakePayment.setOnClickListener {
            val intent = Intent(this, PayActivity::class.java)
            intent.putExtra(PayActivity.KEY_TYPE, PayActivity.TYPE_MERCHANT)
            intent.putExtra(PayActivity.KEY_DATA, dataMerchants)
            startActivityForResult(intent, PayActivity.RC_PAYMENT)
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        when (requestCode) {
            PayActivity.RC_PAYMENT -> {
                when (resultCode) {
                    Activity.RESULT_OK -> onBackPressed()
                    else -> {
                    }
                }
            }
            else -> {
            }
        }
        super.onActivityResult(requestCode, resultCode, data)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> {
                onBackPressed()
            }
            else -> {
            }
        }
        return super.onOptionsItemSelected(item)
    }
    companion object {
        const val KEY_DATA = "key_data"
        fun start(context: Context, data: ResponseMerchants.DataItem?){
            val intent = Intent(context, MerchantsDetailsActivity::class.java)
            intent.putExtra(KEY_DATA, data)
            intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP
            context.startActivity(intent)
        }
    }
}
