package sg.kickbates.customer.ui.main.cards.add

import androidx.databinding.BaseObservable
import androidx.databinding.Bindable
import androidx.databinding.ObservableField
import androidx.lifecycle.MutableLiveData
import kotlinx.coroutines.CoroutineExceptionHandler
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.SupervisorJob
import kotlinx.coroutines.launch
import retrofit2.HttpException
import sg.kickbates.data.BuildConfig
import sg.kickbates.data.api.Api
import sg.kickbates.data.api.models.card.ResponseCreateTokenStripe
import sg.kickbates.data.api.models.card.ResponsePaymentMethodAddCard
import sg.kickbates.data.api.models.payment.DetailInvoice
import sg.kickbates.data.helper.Pref

/**
 * Created by simx on 22,November,2019
 */
class AddStripeCardActivityVM(private var pref: Pref) :BaseObservable(){
    private var job = SupervisorJob()

    @Bindable var loading = ObservableField<Boolean>()
    @Bindable var empty = ObservableField<Boolean>()
    @Bindable var error = MutableLiveData<String>()
    @Bindable var dataCard = MutableLiveData<ResponseCreateTokenStripe>()

    @Bindable var paymentMethodCard = MutableLiveData<ResponsePaymentMethodAddCard.Data>()

    @Bindable var result = MutableLiveData<DetailInvoice>()




    fun getCardToken(number: String, expMonth: Int, expYear: Int, cvc: String) {
        loading.set(true)
        try {
            CoroutineScope(CoroutineExceptionHandler { _, throwable -> run {
                job = SupervisorJob()
                throwable.printStackTrace()
                updateMsg(throwable.message)
            } }).launch {
                val res = Api.Factory.create(BuildConfig.STRIPE_BASE_URL).stripeTokenAsync("Bearer ${BuildConfig.STRIPE_KEY}", number,expMonth,expYear,cvc).await()
                addCardToPaymentMethod(res.id)
            }
        }catch (e: HttpException){
            e.printStackTrace()
            updateMsg(e.message)
        }
    }
    private fun addCardToPaymentMethod(token: String?) {
        loading.set(true)
        try {
            CoroutineScope(CoroutineExceptionHandler { _, throwable -> run {
                job = SupervisorJob()
                throwable.printStackTrace()
                updateMsg(throwable.message)
            } }).launch {
                val res = Api.Factory.create(BuildConfig.BASE_URL).addPaymentMethodAsync(pref.token, token).await()
                res.let {
                    when (it.status) {
                         200 -> {if (res.data == null) paymentMethodCard.postValue(null) else paymentMethodCard.postValue(res.data)}
                         else -> {updateMsg("Failed to add card")}
                   }
                }
                loading.set(false)
            }
        }catch (e: HttpException){
            e.printStackTrace()
            updateMsg(e.message)
        }
    }


    fun chargeCard(invoiceId:Int?, cardId:String?){
        loading.set(true)
        try {
            CoroutineScope(CoroutineExceptionHandler { _, throwable -> run {
                job = SupervisorJob()
                throwable.printStackTrace()
                updateMsg(throwable.localizedMessage)
            } }).launch {
                val res = Api.Factory.create(BuildConfig.BASE_URL).chargeCardAsync(pref.token, cardId, invoiceId).await()
                res.let {
                    when (it.status) {
                        200 -> { if (res.data == null) result.postValue(null) else result.postValue(res.data)}
                        else -> {updateMsg(BuildConfig.ERROR_MSG)}
                    }
                }

                loading.set(false)
            }
        }catch (e: HttpException){
            e.printStackTrace()
            updateMsg(e.localizedMessage)
        }

    }

    private fun updateMsg(msg:String?){
        loading.set(false)
        error.postValue(BuildConfig.ERROR_MSG)
    }
}