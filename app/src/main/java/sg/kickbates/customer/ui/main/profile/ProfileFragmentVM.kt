package sg.kickbates.customer.ui.main.profile

import androidx.databinding.BaseObservable
import androidx.databinding.Bindable
import androidx.databinding.ObservableField
import androidx.lifecycle.MutableLiveData

import kotlinx.coroutines.*
import retrofit2.HttpException
import sg.kickbates.data.BuildConfig
import sg.kickbates.data.api.Api
import sg.kickbates.data.api.models.ResponseCashbackSummary
import sg.kickbates.data.api.models.ResponseMe
import sg.kickbates.data.api.models.payment.DetailInvoice
import sg.kickbates.data.api.models.payment.RequestInvoicePremium
import sg.kickbates.data.api.models.payment.ResponseGenerateInvoice
import sg.kickbates.data.helper.Pref

import sg.kickbates.data.helper.States

/**
 * Created by simx on 30,July,2019
 */
class ProfileFragmentVM(private var pref: Pref?): BaseObservable() {
    private var job: Job? = SupervisorJob()

    @Bindable var errorMsg = MutableLiveData<String>()
    @Bindable var state = MutableLiveData<States>()
    @Bindable var user = MutableLiveData<ResponseMe.Data>()
    @Bindable var loading = ObservableField<Boolean>()
    @Bindable var canPurchase = ObservableField<Boolean>()
    @Bindable var error = MutableLiveData<String>()

    @Bindable var availableKick = MutableLiveData<ResponseCashbackSummary.Data>()
    @Bindable var invoice = MutableLiveData<DetailInvoice>()

    fun getMe(){
        loading.set(true)
        CoroutineScope(CoroutineExceptionHandler { _, throwable -> run {
            throwable.printStackTrace()
            job = SupervisorJob() }
            updateMsg(throwable.message)
        }).launch {
            var res = Api.Factory.create(BuildConfig.BASE_URL).meFromIdAsync(pref?.token, pref?.usersId).await()
            user.postValue(res.data)
            pref?.usersId = res.data?.id
            loading.set(false)
        }
    }

    fun getSummaryKick(){
        loading.set(true)
        try {
            CoroutineScope(CoroutineExceptionHandler { _, throwable -> run {
                job = SupervisorJob()
                throwable.printStackTrace()
                updateMsg(throwable.message)
            } }).launch {
                val res = Api.Factory.create(BuildConfig.BASE_URL).cashBackSummaryAsync(pref?.token).await()
                res.let {
                    when (it.status) {
                        200 -> {if (res.data == null) availableKick.postValue(null) else availableKick.postValue(res.data)}
                        else -> {updateMsg(BuildConfig.ERROR_MSG)}
                    }
                }
                loading.set(false)
            }
        }catch (e: HttpException){
            e.printStackTrace()
            updateMsg(e.message)
        }
    }

    fun upgradeToPremium() {
        loading.set(true)
        var requestInvoicePremium = RequestInvoicePremium()
        requestInvoicePremium.payByKick = 20.0
        requestInvoicePremium.premium = 1
        requestInvoicePremium.promoCodeId = null
        CoroutineScope(CoroutineExceptionHandler { _, throwable -> run {
            throwable.printStackTrace()
            job = SupervisorJob() }
            updateMsg(BuildConfig.ERROR_MSG)
        }).launch {
            var res = Api.Factory.create(BuildConfig.BASE_URL).generateInvoiceAsync(pref?.token, requestInvoicePremium).await()
            res.let {
                when (it.status) {
                     200 -> {if (res.data == null) invoice.postValue(null) else invoice.postValue(res.data)}
                     else -> {updateMsg(BuildConfig.ERROR_MSG)}
               }
            }
            loading.set(false)
        }
    }
    private fun updateMsg(msg:String?){
        error.postValue(BuildConfig.ERROR_MSG)
        loading.set(false)
    }
}