package sg.kickbates.customer.ui.main.cards.details.billed


import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView

import sg.kickbates.customer.R
import sg.kickbates.customer.databinding.BilledFragmentBinding
import sg.kickbates.customer.ui.main.cards.details.adapter.AdapterUnBilledTransactions
import sg.kickbates.customer.ui.main.pay.PayActivity
import sg.kickbates.data.api.models.card.ResponseCustomerCards


/**
 * A simple [Fragment] subclass.
 *
 */
class BilledFragment : Fragment() {
    private lateinit var binding:BilledFragmentBinding
    private lateinit var vm:BilledFragmentVM

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater,R.layout.billed_fragment, container,false)
        vm = BilledFragmentVM()
        binding.lifecycleOwner = this
        binding.billedVm = vm
        arguments?.let { b ->
            var data: ResponseCustomerCards.DataItem? = b.getParcelable(KEY_DATA)
            vm.transactions.postValue(data?.kickCustomerCardStatements?.filter { it.status == 1 })
        }
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        vm.transactions.observe(this, Observer { data -> run {
            vm.empty.set(data.isNullOrEmpty())
            binding.rcvBilled.apply {
                hasFixedSize()
                itemAnimator = DefaultItemAnimator()
                layoutManager = LinearLayoutManager(this@BilledFragment.context!!, RecyclerView.VERTICAL, false)
                adapter = AdapterUnBilledTransactions(data, object : AdapterUnBilledTransactions.OnAdapterUnBilledTransactionsListener{
                    override fun onAdapterClicked(data: ResponseCustomerCards.DataItem.DataCardStatements) {

                    }

                    override fun onBottomPayClicked(data: ResponseCustomerCards.DataItem.DataCardStatements) {
                        var intent = Intent(this@BilledFragment.context!!, PayActivity::class.java)
                        intent.putExtra(PayActivity.KEY_TYPE, PayActivity.TYPE_CARD)
                        intent.putExtra(PayActivity.KEY_DATA, data)
                        startActivityForResult(intent, PayActivity.RC_PAYMENT)
                    }
                })
            }
        } })

    }

    companion object {
        const val KEY_DATA = "key_data"
        fun instance(data: ResponseCustomerCards.DataItem?) : BilledFragment{
            var bundle =  Bundle()
            bundle.putParcelable(KEY_DATA, data)
            var fragment = BilledFragment()
            fragment.arguments = bundle
            return fragment
        }
    }
}
