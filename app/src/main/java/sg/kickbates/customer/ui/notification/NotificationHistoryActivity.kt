package sg.kickbates.customer.ui.notification

import android.content.Intent
import android.content.Context
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.MenuItem
import sg.kickbates.customer.R
import sg.kickbates.customer.databinding.NotificationHistoryActivityBinding
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import sg.kickbates.data.helper.Pref

class NotificationHistoryActivity : AppCompatActivity() {
    private lateinit var binding:NotificationHistoryActivityBinding
    private lateinit var vm:NotificationHistoryVM

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this,R.layout.notification_history_activity)
        vm = NotificationHistoryVM(Pref(this))
        binding.lifecycleOwner = this
        binding.notificationHistoryVm = vm
        vm.getDataNotification()
        setSupportActionBar(binding.toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        vm.notifications.observe(this, Observer { data ->
            run {
                binding.rcvNotificationHistory.apply {
                    hasFixedSize()
                    itemAnimator = DefaultItemAnimator()
                    layoutManager = LinearLayoutManager(this@NotificationHistoryActivity, RecyclerView.VERTICAL, false)
                    adapter = AdapterNotificationHistory(data)
                }
            }
        })
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> onBackPressed()
            else -> {
            }
        }
        return super.onOptionsItemSelected(item)
    }
    companion object {
        fun start(context: Context){
            val intent = Intent(context, NotificationHistoryActivity::class.java)
            intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
            context.startActivity(intent)
        }
    }
}
