package sg.kickbates.customer.ui.main.cards.details.adapter

import android.content.res.ColorStateList
import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import sg.kickbates.customer.R

import sg.kickbates.customer.databinding.ItemUnBilledTransactionsBinding
import sg.kickbates.data.api.models.card.ResponseCustomerCards

/**
 * Created by simx on 31,July,2019
 */
class AdapterUnBilledTransactions(private var items:List<ResponseCustomerCards.DataItem.DataCardStatements>, private var listener: OnAdapterUnBilledTransactionsListener): RecyclerView.Adapter<AdapterUnBilledTransactions.Holder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): Holder {
        return Holder(ItemUnBilledTransactionsBinding.bind(LayoutInflater.from(parent.context).inflate(R.layout.item_un_billed_transactions,parent,false)))
    }

    override fun getItemCount(): Int {
        return items.size
    }

    override fun onBindViewHolder(holder: Holder, position: Int) {
        holder.bind(items[position])
        if (items[position].status == 1){
            holder.binding.btnPay.text = "PAY"
            holder.binding.btnPay.setTextColor(Color.parseColor("#ffffff"))
            holder.binding.btnPay.backgroundTintList = ColorStateList.valueOf(Color.parseColor("#1f908a"))
            holder.binding.btnPay.isClickable = true
            holder.binding.btnPay.setOnClickListener {
                listener.onBottomPayClicked(data = items[position])
            }

        }else {
            holder.binding.btnPay.text = "PAID"
            holder.binding.btnPay.setTextColor(Color.parseColor("#000000"))
            holder.binding.btnPay.backgroundTintList = ColorStateList.valueOf(Color.parseColor("#f6f6f8"))
            holder.binding.btnPay.isClickable = false
        }
    }

    interface OnAdapterUnBilledTransactionsListener {
        fun onAdapterClicked(data: ResponseCustomerCards.DataItem.DataCardStatements)
        fun onBottomPayClicked(data: ResponseCustomerCards.DataItem.DataCardStatements)
    }

    class Holder(var binding: ItemUnBilledTransactionsBinding):RecyclerView.ViewHolder(binding.root) {
        fun bind(data:ResponseCustomerCards.DataItem.DataCardStatements){
            with(binding){
                itemUnBilledTransactionsVm = ItemUnBilledTransactionsVM(data)
                executePendingBindings()
            }
        }

    }
}