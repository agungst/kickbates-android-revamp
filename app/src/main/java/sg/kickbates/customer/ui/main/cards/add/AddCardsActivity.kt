package sg.kickbates.customer.ui.main.cards.add

import android.app.Activity
import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.MenuItem
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import sg.kickbates.customer.R
import sg.kickbates.customer.databinding.AddCardsActivityBinding
import sg.kickbates.data.api.models.RequestCard
import sg.kickbates.data.helper.Pref

class AddCardsActivity : AppCompatActivity() {
    private lateinit var binding:AddCardsActivityBinding
    private lateinit var vm:AddCardsVM

    private var requestCard:RequestCard? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this,R.layout.add_cards_activity)
        vm = AddCardsVM(Pref(this))
        binding.lifecycleOwner = this
        binding.addCardVm = vm
        setSupportActionBar(binding.toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        binding.tvTitleToolbar.text = "Add New Card"
        binding.btnAddNewCard.setOnClickListener {
            setResult(Activity.RESULT_OK)
            finish()
        }

        intent.extras.let {
            requestCard = it?.getParcelable(KEY_DATA)
            vm.prepareAddNewCar(requestCard)
        }
        binding.btnTryAgain.setOnClickListener {
            if (requestCard != null)vm.prepareAddNewCar(requestCard)
        }
        vm.note.observe(this, Observer { msg -> run {
            binding.tvNote.text = msg
        } })
        vm.cards.observe(this, Observer { data -> run {
            if (data.isNotEmpty()) {
                setResult(Activity.RESULT_OK)
                finish()
            }
        } })
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> {
                setResult(Activity.RESULT_CANCELED)
                finish()
            }
            else -> {
            }
        }
        return super.onOptionsItemSelected(item)
    }
    companion object {
        const val KEY_DATA = "key_data"
        const val RC_ADD = 11
        fun start(context: Context): Intent {
            val intent = Intent(context, AddCardsActivity::class.java)
            intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP
            return intent
        }
    }
}
