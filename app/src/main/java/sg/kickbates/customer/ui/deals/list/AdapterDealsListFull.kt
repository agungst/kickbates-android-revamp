package sg.kickbates.customer.ui.deals.list

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import sg.kickbates.customer.R
import sg.kickbates.customer.databinding.ItemDealsFullBinding
import sg.kickbates.data.api.models.ResponseDeals

/**
 * Created by simx on 31,July,2019
 */
class AdapterDealsListFull(private var datas:List<ResponseDeals.DataItem>, private var listener: OnAdapterDealsFullListener): RecyclerView.Adapter<AdapterDealsListFull.Holder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): Holder {
        return Holder(ItemDealsFullBinding.bind(LayoutInflater.from(parent.context).inflate(R.layout.item_deals_full,parent,false)))
    }


    override fun getItemCount(): Int {
        return datas.size
    }

    override fun onBindViewHolder(holder: Holder, position: Int) {
        holder.bind(datas[position])
        holder.itemView.setOnClickListener { listener.onClicked(datas[position]) }
        holder.binding.btnView.setOnClickListener { listener.onClicked(datas[position]) }

    }

    interface OnAdapterDealsFullListener {
        fun onClicked(data: ResponseDeals.DataItem)
    }
    class Holder(var binding: ItemDealsFullBinding):RecyclerView.ViewHolder(binding.root) {
        fun bind(data: ResponseDeals.DataItem){
            with(binding){
                itemDealFullVm = ItemDealsFullVM(data)
                executePendingBindings()
            }
        }

    }
}