package sg.kickbates.customer.ui.main.cards.lists

import androidx.databinding.BaseObservable
import androidx.databinding.Bindable
import androidx.databinding.ObservableField
import androidx.lifecycle.MutableLiveData
import kotlinx.coroutines.CoroutineExceptionHandler
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.SupervisorJob
import kotlinx.coroutines.launch
import retrofit2.HttpException
import sg.kickbates.data.BuildConfig
import sg.kickbates.data.api.Api
import sg.kickbates.data.api.models.card.ResponseCustomerCards
import sg.kickbates.data.helper.Pref

/**
 * Created by simx on 30,July,2019
 */
class CardListVM(private var pref: Pref): BaseObservable() {
    private var job = SupervisorJob()
    @Bindable var loading = ObservableField<Boolean>()
    @Bindable var empty = ObservableField<Boolean>()
    @Bindable var error = MutableLiveData<String>()
    @Bindable var cards = MutableLiveData<List<ResponseCustomerCards.DataItem>>()

    private var errorCard = "Failed to get your Cards"
    fun dataCards(){
        loading.set(true)
        try {
            CoroutineScope(CoroutineExceptionHandler { _, throwable -> run {
                job = SupervisorJob()
                throwable.printStackTrace()
                updateMsg(throwable.message)
            } }).launch {
                val res = Api.Factory.create(BuildConfig.BASE_URL).customerCardsAsync(pref.token, pref.usersId).await()
                res.let {
                    when (it.status) {
                         200 -> {if (res.data?.isEmpty()!!)cards.postValue(listOf())else cards.postValue(res.data)}
                         else -> {updateMsg(errorCard)}
                   }
                }
                loading.set(false)
            }
        }catch (e: HttpException){
            e.printStackTrace()
            updateMsg(e.message)
        }
    }

    private fun updateMsg(msg:String?){
        error.postValue(BuildConfig.ERROR_MSG)
        loading.set(false)
    }
}