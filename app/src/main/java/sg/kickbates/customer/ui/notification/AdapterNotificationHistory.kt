package sg.kickbates.customer.ui.notification

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import sg.kickbates.customer.R
import sg.kickbates.customer.databinding.ItemCreditCardSmallBinding
import sg.kickbates.customer.databinding.ItemMerchantsBinding
import sg.kickbates.customer.databinding.ItemNotificationHistoryBinding
import sg.kickbates.data.api.models.ResponseMerchants
import sg.kickbates.data.api.models.notification.ResponseNotificationHistory

/**
 * Created by simx on 31,July,2019
 */
class AdapterNotificationHistory(private var datas:List<ResponseNotificationHistory.DataNotification>): RecyclerView.Adapter<AdapterNotificationHistory.Holder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): Holder {
        return Holder(ItemNotificationHistoryBinding.bind(LayoutInflater.from(parent.context).inflate(R.layout.item_notification_history,parent,false)))
    }


    override fun getItemCount(): Int {
        return datas.size
    }

    override fun onBindViewHolder(holder: Holder, position: Int) {
        holder.bind(datas[position])
    }
    class Holder(var binding: ItemNotificationHistoryBinding):RecyclerView.ViewHolder(binding.root) {
        fun bind(data: ResponseNotificationHistory.DataNotification){
            with(binding){
                itemNotificationHistoryVm = ItemNotificationHistoryVM(data)
                executePendingBindings()
            }
        }

    }
}