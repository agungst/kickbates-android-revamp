package sg.kickbates.customer.ui.dialog.account_card

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.Filter
import android.widget.TextView
import androidx.annotation.Nullable
import android.annotation.SuppressLint
import sg.kickbates.customer.R
import sg.kickbates.data.api.room.CardModel

/**
 * Created by simx on 21,April,2020
 */
class AdapterCardModel(@Nullable  context: Context, var resId:Int, var datas:ArrayList<CardModel>):ArrayAdapter<CardModel>(context, resId, datas) {
    private var items:ArrayList<CardModel> = arrayListOf()
    private var tempItems:ArrayList<CardModel> = arrayListOf()
    private var suggestions:ArrayList<CardModel> = arrayListOf()
    init {
        this.items = datas
        this.tempItems = ArrayList(items)
    }


    @SuppressLint("ViewHolder")
    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        val layout = LayoutInflater.from(context).inflate(resId, parent, false)
        layout.findViewById<TextView>(R.id.tv_card_number).text = getItem(position)?.cardNumber
        return layout
    }

    override fun getDropDownView(position: Int, convertView: View?, parent: ViewGroup): View {
        val layout = LayoutInflater.from(context).inflate(resId, parent, false)
        layout.findViewById<TextView>(R.id.tv_card_number).text = getItem(position)?.cardNumber
        return layout
    }

    override fun getItem(position: Int): CardModel? {
        return datas[position]
    }

    override fun getItemId(position: Int): Long {
        return datas[position].id?.toLong()!!
    }

    override fun getFilter(): Filter {

        return object :Filter(){
            val filterResult = FilterResults()
            override fun convertResultToString(resultValue: Any?): CharSequence {
                val cardModel = resultValue as CardModel
                return cardModel.cardNumber.toString()
            }
            override fun performFiltering(constraint: CharSequence?): FilterResults {
                return if (constraint != null){
                    suggestions.clear()
                    for (i in tempItems){
                        if (i.cardNumber.toString().toLowerCase().startsWith(constraint.toString().toLowerCase())) suggestions.add(i)
                    }
                    filterResult.values = suggestions
                    filterResult.count = suggestions.size
                    filterResult
                }else FilterResults()
            }

            override fun publishResults(constraint: CharSequence?, results: FilterResults?) {
                if (results?.values != null){
                    val temRes = results.values as ArrayList<CardModel>
                    if (filterResult.count >0){
                        clear()
                        for (a in temRes) add(a)
                        notifyDataSetChanged()
                    }else {
                        clear()
                        notifyDataSetChanged()
                    }
                }
            }

        }
    }

}