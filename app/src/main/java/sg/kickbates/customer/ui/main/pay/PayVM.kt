package sg.kickbates.customer.ui.main.pay

import androidx.databinding.BaseObservable
import androidx.databinding.Bindable
import androidx.databinding.ObservableField
import androidx.lifecycle.MutableLiveData
import kotlinx.coroutines.CoroutineExceptionHandler
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.SupervisorJob
import kotlinx.coroutines.launch
import retrofit2.HttpException
import sg.kickbates.data.BuildConfig
import sg.kickbates.data.api.Api
import sg.kickbates.data.api.models.ResponseCashbackSummary
import sg.kickbates.data.api.models.payment.*
import sg.kickbates.data.helper.Pref


/**
 * Created by simx on 30,July,2019
 */
class PayVM(private var pref: Pref): BaseObservable() {
    private var job  = SupervisorJob()

    @Bindable var swChecked = ObservableField<Boolean>()
    @Bindable var loading   = ObservableField<Boolean>()
    @Bindable var error     = MutableLiveData<String>()
    @Bindable var availableKick = MutableLiveData<ResponseCashbackSummary.Data>()
    @Bindable var invoiceDeal = MutableLiveData<DetailInvoice>()
    @Bindable var invoiceMerchant = MutableLiveData<DetailInvoice>()
    @Bindable var fullPayment = MutableLiveData<DetailInvoice>()

    @Bindable var calculatePromo = MutableLiveData<ResponseCalculatePromo.Data>()

    fun getSummaryKick(){
        loading.set(true)
        try {
            CoroutineScope(CoroutineExceptionHandler { _, throwable -> run {
                job = SupervisorJob()
                throwable.printStackTrace()
                updateMsg(throwable.message)
            } }).launch {
                val res = Api.Factory.create(BuildConfig.BASE_URL).cashBackSummaryAsync(pref.token).await()
                res.let {
                    when (it.status) {
                         200 -> {if (res.data == null) availableKick.postValue(null) else availableKick.postValue(res.data)}
                         else -> {updateMsg(BuildConfig.ERROR_MSG)}
                   }
                }
                loading.set(false)
            }
        }catch (e: HttpException){
            e.printStackTrace()
            updateMsg(e.message)
        }
    }


    private fun updateMsg(msg:String?){
        loading.set(false)
        error.postValue(msg)
    }

    fun generateInvoiceMerchant(requestInvoiceMerchant: RequestInvoiceMerchant, fullPayment:Boolean?) {
        loading.set(true)
        try {
            CoroutineScope(CoroutineExceptionHandler { _, throwable -> run {
                job = SupervisorJob()
                throwable.printStackTrace()
                updateMsg(throwable.message)
            } }).launch {
                Api.Factory.create(BuildConfig.BASE_URL).generateInvoiceAsync(pref.token, requestInvoiceMerchant).await().let {
                    if (it.status == 200 && it.data != null) {
                        if (fullPayment!!) chargeCard(it.data?.id) else {
                            invoiceMerchant.postValue(it.data)
                            loading.set(false)
                        }
                    }else updateMsg(it.message)

                }

            }
        }catch (e: HttpException){
            e.printStackTrace()
            updateMsg(e.message)
        }
    }

    private fun chargeCard(invoiceId:Int?){
        try {
            CoroutineScope(CoroutineExceptionHandler { _, throwable -> run {
                job = SupervisorJob()
                throwable.printStackTrace()
                updateMsg(throwable.message)
            } }).launch {
                Api.Factory.create(BuildConfig.BASE_URL).chargeCardAsync(pref.token, null, invoiceId).await().let {
                    if (it.status == 200 && it.data != null) fullPayment.postValue(it.data) else fullPayment.postValue(null)
                    loading.set(false)
                }

            }
        }catch (e: HttpException){
            e.printStackTrace()
            updateMsg(e.message)
        }
    }

    fun generateInvoiceDeal(requestInvoiceDeal: RequestInvoiceDeal,  fullPayment:Boolean?) {
        loading.set(true)
        try {
            CoroutineScope(CoroutineExceptionHandler { _, throwable -> run {
                job = SupervisorJob()
                throwable.printStackTrace()
            } }).launch {
                Api.Factory.create(BuildConfig.BASE_URL).generateInvoiceAsync(pref.token, requestInvoiceDeal).await().let {
                    loading.set(false)
                    if (it.status == 200){
                        if (it.data != null){
                            if (fullPayment!!) chargeCard(it.data?.id) else invoiceDeal.postValue(it.data)
                        }else invoiceDeal.postValue(null)
                    }else {
                        updateMsg(it.message)
                    }
                }
            }
        }catch (e: HttpException){
            e.printStackTrace()
        }
    }

    fun generateInvoiceCard(requestInvoiceCard: RequestInvoiceCard) {
        loading.set(true)
        try {
            CoroutineScope(CoroutineExceptionHandler { _, throwable -> run {
                job = SupervisorJob()
                throwable.printStackTrace()
                updateMsg(throwable.message)
            } }).launch {
                val res = Api.Factory.create(BuildConfig.BASE_URL).generateInvoiceAsync(pref.token, requestInvoiceCard).await()
                res.let {
                    when (it.status) {
                        200 -> {if (res.data == null) invoiceDeal.postValue(null) else invoiceDeal.postValue(res.data)}
                        else -> {error.postValue("Failed Generate Invoice")}
                    }
                }
                loading.set(false)
            }
        }catch (e: HttpException){
            e.printStackTrace()
            updateMsg(e.message)
        }
    }

    fun calculate(payable:Double?,payByKick:Double?,promoAmount:Double?,promoPercentage:Double?, merchantId:Int?){
        val requestCalculate = RequestCalculate(promoAmount, payByKick, payable, promoPercentage,merchantId)
        loading.set(true)
        try {
            CoroutineScope(CoroutineExceptionHandler { _, throwable -> run {
                job = SupervisorJob()
                throwable.printStackTrace()
                updateMsg(throwable.message)
            } }).launch {
                Api.Factory.create(BuildConfig.BASE_URL).calculateAsync(pref.token, requestCalculate).await().let {
                    if (it.status == 200) calculatePromo.postValue(it.data)
                    else updateMsg(BuildConfig.ERROR_MSG)
                }
                loading.set(false)
            }
        }catch (e: HttpException){
            e.printStackTrace()
            updateMsg(e.message)
        }
    }


}