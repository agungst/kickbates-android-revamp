package sg.kickbates.customer.ui.dialog.account_card

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import sg.kickbates.customer.R
import sg.kickbates.customer.databinding.ItemAccountBankBinding
import sg.kickbates.data.api.models.card.ResponseAccountCard

/**
 * Created by simx on 31,January,2020
 */
class AdapterAccountsBank(private var items:List<ResponseAccountCard.DataItem>, private var callback: OnAdapterAccountsBankCalback):RecyclerView.Adapter<AdapterAccountsBank.Holder>() {
    class Holder(var binding: ItemAccountBankBinding):RecyclerView.ViewHolder(binding.root) {
        fun bind(data:ResponseAccountCard.DataItem){
            with(binding){
                itemAccountBankVm = ItemAccountBankVM(data)
                executePendingBindings()
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): Holder {
        return  Holder(
            ItemAccountBankBinding.bind(
                LayoutInflater.from(parent.context).inflate(R.layout.item_account_bank, parent, false)
            )
        )
    }


    override fun getItemCount(): Int {
        return items.size
    }

    interface OnAdapterAccountsBankCalback {
        fun onItemClicked(data: ResponseAccountCard.DataItem)
    }

    override fun onBindViewHolder(holder: Holder, position: Int) {
        holder.bind(items[position])
        holder.itemView.setOnClickListener { callback.onItemClicked(items[position]) }
    }
}