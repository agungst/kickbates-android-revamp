package sg.kickbates.customer.ui.main.cards.details.adapter

import androidx.databinding.BaseObservable
import androidx.databinding.Bindable
import androidx.databinding.ObservableField
import sg.kickbates.data.api.models.card.ResponseCustomerCards

/**
 * Created by simx on 11,August,2019
 */
class ItemUnBilledTransactionsVM(data: ResponseCustomerCards.DataItem.DataCardStatements): BaseObservable() {
    @Bindable var dueDate = ObservableField(data.dueDate)
    @Bindable var statementDate = ObservableField(data.statementDate)
    @Bindable var amountDue = ObservableField("$ ${data.amountDue}")
    @Bindable var paidAmount = ObservableField("$ ${data.paidAmount}")
}