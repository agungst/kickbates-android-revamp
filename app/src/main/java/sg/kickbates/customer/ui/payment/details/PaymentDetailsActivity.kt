package sg.kickbates.customer.ui.payment.details

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import sg.kickbates.customer.R

class PaymentDetailsActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.payment_details_activity)
    }
}
