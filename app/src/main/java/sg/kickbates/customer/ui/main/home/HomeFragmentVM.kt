package sg.kickbates.customer.ui.main.home


import androidx.databinding.BaseObservable
import androidx.databinding.Bindable
import androidx.databinding.ObservableField
import androidx.lifecycle.MutableLiveData
import kotlinx.coroutines.*
import retrofit2.HttpException
import sg.kickbates.data.BuildConfig
import sg.kickbates.data.api.Api
import sg.kickbates.data.api.models.ResponseBanners
import sg.kickbates.data.api.models.ResponseDeals
import sg.kickbates.data.api.models.ResponseMerchants
import sg.kickbates.data.api.models.card.ResponseCustomerCards
import sg.kickbates.data.helper.Pref
import sg.kickbates.data.helper.States


/**
 * Created by simx on 30,July,2019
 */
class HomeFragmentVM(private var pref: Pref): BaseObservable() {
    private var job = SupervisorJob()
    @Bindable var errorMsg = MutableLiveData<String>()
    @Bindable var error = MutableLiveData<String>()

    /**
     * Loading
     */
    @Bindable var loadingPromo = ObservableField<Boolean>()
    @Bindable var loadingCards = ObservableField<Boolean>()
    @Bindable var loadingDeals = ObservableField<Boolean>()
    @Bindable var loadingMerchant = ObservableField<Boolean>()

    /**
     * Empty
     */

    @Bindable var emptyPromo = ObservableField<Boolean>()
    @Bindable var emptyCards = ObservableField<Boolean>()
    @Bindable var emptyDeals = ObservableField<Boolean>()
    @Bindable var emptyMerchant = ObservableField<Boolean>()

    @Bindable var promos = MutableLiveData<List<ResponseBanners.DataItem>>()
    @Bindable var cards = MutableLiveData<List<ResponseCustomerCards.DataItem>>()
    @Bindable var merchants = MutableLiveData<List<ResponseMerchants.DataItem>>()
    @Bindable var deals = MutableLiveData<List<ResponseDeals.DataItem>>()
    @Bindable var dealsBanner = MutableLiveData<List<ResponseDeals.DataItem>>()


    private var errorPromo = "Data promo not found"
    fun getPromos(){
        loadingPromo.set(true)
        try {
            CoroutineScope(CoroutineExceptionHandler { _, throwable -> run {
                job = SupervisorJob()
                throwable.printStackTrace()
                loadingPromo.set(false)
                updateErrorMsg(errorPromo)
            } }).launch {
                val res = Api.Factory.create(BuildConfig.BASE_URL).bannersAsync(pref.token).await()
                res.let {
                    when (it.status) {
                         200 -> {if (res.data.isNullOrEmpty())promos.postValue(listOf()) else promos.postValue(res.data)}
                         else -> {updateErrorMsg(res.message)}
                   }
                }

            }
            loadingPromo.set(false)
        }catch (e:Exception){
            updateErrorMsg(e.message)
            loadingPromo.set(false)
        }
        dataCards()
    }

    fun getBannerDeals(){
        loadingPromo.set(true)
        try {
            CoroutineScope(CoroutineExceptionHandler { _, throwable -> run {
                job = SupervisorJob()
                throwable.printStackTrace()
                loadingPromo.set(false)
                updateErrorMsg(errorPromo)
            } }).launch {
                val res = Api.Factory.create(BuildConfig.BASE_URL).dealsBannerAsync(pref.token).await()
                res.let {
                    when (it.status) {
                        200 -> {if (res.data.isNullOrEmpty())dealsBanner.postValue(listOf()) else dealsBanner.postValue(res.data)}
                        else -> {updateErrorMsg(res.message)}
                    }
                }

            }
            loadingPromo.set(false)
        }catch (e:Exception){
            updateErrorMsg(e.message)
            loadingPromo.set(false)
        }
        dataCards()
    }

    private val errorMerchants = "Data merchants not found"
    private fun getDataMerchants() {
        loadingMerchant.set(true)
        try {
            CoroutineScope(CoroutineExceptionHandler { _, throwable -> run {
                throwable.printStackTrace()
                job = SupervisorJob()
                updateErrorMsg(errorMerchants)
            } }).launch {
                val res = Api.Factory.create(BuildConfig.BASE_URL).merchantsAsync(pref.token).await()
                res.let {
                    when (it.status) {
                         200 -> {if (res.data.isNullOrEmpty()) merchants.postValue(listOf()) else merchants.postValue(res.data)}
                         else -> {updateErrorMsg(errorMerchants)}
                   }
                }
                loadingMerchant.set(false)
            }
        }catch (e:HttpException){
            e.printStackTrace()
            loadingMerchant.set(false)
            updateErrorMsg(errorMerchants)
        }

    }

    private fun updateErrorMsg(msg: String?){
        errorMsg.postValue(BuildConfig.ERROR_MSG)
    }

    private val errorDeals = "Data deals not found"
    private fun getDeals(){
        loadingDeals.set(true)
        try {
            CoroutineScope(CoroutineExceptionHandler { _, throwable -> run {
                loadingDeals.set(false)
                throwable.printStackTrace()
                job = SupervisorJob()
                updateErrorMsg(errorDeals)
            }
            }).launch {
                val res = Api.Factory.create(BuildConfig.BASE_URL).dealsAsync(pref.token).await()
                res.let {
                    when (it.status) {
                         200 -> {if (res.data.isNullOrEmpty())deals.postValue(listOf()) else deals.postValue(res.data)}
                         else -> {updateErrorMsg(errorDeals)}
                   }
                }
                loadingDeals.set(false)

            }
        }catch (e:HttpException){
            e.printStackTrace()
            loadingDeals.set(false)
            updateErrorMsg(errorDeals)
        }
        getDataMerchants()

    }
    private var errorCard = "Failed to get your Cards"
    fun dataCards(){
        loadingCards.set(true)
        try {
            CoroutineScope(CoroutineExceptionHandler { _, throwable -> run {
                job = SupervisorJob()
                throwable.printStackTrace()
                error.postValue(errorCard)
                loadingCards.set(false)
            } }).launch {
                val res = Api.Factory.create(BuildConfig.BASE_URL).customerCardsAsync(pref.token, pref.usersId).await()
                res.let {
                    when (it.status) {
                        200 -> {if (res.data.isNullOrEmpty())cards.postValue(listOf())else cards.postValue(res.data)}
                        else -> {error.postValue(errorCard)}
                    }
                }
                loadingCards.set(false)

            }
        }catch (e: HttpException){
            e.printStackTrace()
            loadingDeals.set(false)
            updateErrorMsg(errorCard)
        }
        getDeals()
    }

}