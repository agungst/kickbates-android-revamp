package sg.kickbates.customer.ui.main.cards


import android.app.Activity
import android.content.Intent
import android.graphics.Color
import android.graphics.PorterDuff
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager

import androidx.recyclerview.widget.RecyclerView

import sg.kickbates.customer.R
import sg.kickbates.customer.databinding.CardsFragmentBinding
import sg.kickbates.customer.helper.DialogHelper
import sg.kickbates.customer.ui.main.MainActivity
import sg.kickbates.customer.ui.main.cards.add.AddCardsActivity
import sg.kickbates.customer.ui.main.cards.details.CardsDetailsActivity
import sg.kickbates.customer.ui.main.cards.dialog.DialogCardsAvailable
import sg.kickbates.customer.ui.main.cards.lists.AdapterCardList
import sg.kickbates.customer.ui.main.cards.lists.AdapterCardListFull
import sg.kickbates.customer.ui.main.pay.PayActivity
import sg.kickbates.data.api.models.RequestCard
import sg.kickbates.data.api.models.card.ResponseCustomerCards
import sg.kickbates.data.api.models.payment.RequestInvoiceCard
import sg.kickbates.data.helper.Pref


/**
 * A simple [Fragment] subclass.
 *
 */
class CardsFragment : Fragment() {
    private lateinit var binding:CardsFragmentBinding
    private lateinit var vm:CardsFragmentVM
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater,R.layout.cards_fragment, container,false)
        vm = CardsFragmentVM(Pref(this.requireContext()))
        binding.lifecycleOwner = viewLifecycleOwner
        binding.cardVm = vm
        vm.getCards()

        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.fabAddCard.setColorFilter(Color.WHITE, PorterDuff.Mode.MULTIPLY)
        vm.cards.observe(viewLifecycleOwner, Observer {
            data -> run {
            vm.empty.set(data.isNullOrEmpty())
            if (data != null){
                iniListCards(data)
            }
        }
        })
        binding.fabAddCard.setOnClickListener {
            DialogHelper.showDialogSuccess(this.requireContext(), "Coming Soon")
            /*DialogCardsAvailable.show(childFragmentManager, object : DialogCardsAvailable.OnDialogCardsAvailableCallback {
                override fun onDialogDismiss(code: RequestCard?) {
                    val intent = Intent(this@CardsFragment.requireContext(), AddCardsActivity::class.java)
                    intent.putExtra(AddCardsActivity.KEY_DATA, code)
                    startActivityForResult(intent, AddCardsActivity.RC_ADD)
                }
            })*/
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        when (requestCode) {
            AddCardsActivity.RC_ADD -> {
                when (resultCode) {
                    Activity.RESULT_OK -> vm.getCards()
                    else -> (activity as MainActivity).showDialogError("Failed Add Card")
                }
            }
            else -> {
            }
        }
        super.onActivityResult(requestCode, resultCode, data)
    }
    private fun iniListCards(data: List<ResponseCustomerCards.DataItem>) {
        vm.empty.set(data.isNullOrEmpty())
        binding.rcvCardFull.apply {
            hasFixedSize()
            itemAnimator = DefaultItemAnimator()
            layoutManager = GridLayoutManager(this@CardsFragment.requireContext(), 2)
            adapter = AdapterCardList(data, object : AdapterCardList.OnAdapterCardListListener {
                override fun onAdapterClicked(data: ResponseCustomerCards.DataItem) {
                    CardsDetailsActivity.start(this@CardsFragment.requireContext(), data)
                }

                /*override fun onBottomPayClicked(data: ResponseCustomerCards.DataItem.DataCardStatements?) {
                    val intent = Intent(this@CardsFragment.context!!, PayActivity::class.java)
                    intent.putExtra(PayActivity.KEY_TYPE, PayActivity.TYPE_CARD)
                    intent.putExtra(PayActivity.KEY_DATA, data)
                    startActivityForResult(intent, PayActivity.RC_PAYMENT)
                }*/
            })
        }
        binding.rcvCardFull.addOnScrollListener(object : RecyclerView.OnScrollListener (){
            /**
             * Callback method to be invoked when the RecyclerView has been scrolled. This will be
             * called after the scroll has completed.
             *
             *
             * This callback will also be called if visible item range changes after a layout
             * calculation. In that case, dx and dy will be 0.
             *
             * @param recyclerView The RecyclerView which scrolled.
             * @param dx The amount of horizontal scroll.
             * @param dy The amount of vertical scroll.
             */
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                if (dy > 0 ||dy<0 && binding.fabAddCard.isShown)
                {
                    binding.fabAddCard.hide()
                }
                super.onScrolled(recyclerView, dx, dy)

            }

            /**
             * Callback method to be invoked when RecyclerView's scroll state changes.
             *
             * @param recyclerView The RecyclerView whose scroll state has changed.
             * @param newState     The updated scroll state. One of [.SCROLL_STATE_IDLE],
             * [.SCROLL_STATE_DRAGGING] or [.SCROLL_STATE_SETTLING].
             */
            override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
                if (newState == RecyclerView.SCROLL_STATE_IDLE) binding.fabAddCard.show()
                super.onScrollStateChanged(recyclerView, newState)

            }
        })
    }

    companion object {
        const val KEY_DATA = "key_Data"
        fun instance() : CardsFragment{
            val bundle =  Bundle()
            val fragment = CardsFragment()
            fragment.arguments = bundle
            return fragment
        }
    }

}
