package sg.kickbates.customer.ui.qr

import android.Manifest
import android.app.Activity
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import com.budiyev.android.codescanner.AutoFocusMode
import com.budiyev.android.codescanner.CodeScanner
import com.budiyev.android.codescanner.ScanMode
import com.google.zxing.Result
import sg.kickbates.customer.R
import sg.kickbates.customer.databinding.QrPayActivityBinding
import sg.kickbates.customer.helper.SnackBarHelper
import sg.kickbates.customer.ui.main.pay.PayActivity
import sg.kickbates.data.BuildConfig
import sg.kickbates.data.helper.Pref
import java.lang.Exception

class QrPayActivity : AppCompatActivity() {
    private lateinit var binding:QrPayActivityBinding
    private lateinit var vm:QrPayVM
    private lateinit var codeScanner:CodeScanner

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this,R.layout.qr_pay_activity)
        vm = QrPayVM(Pref(this))
        binding.lifecycleOwner = this
        binding.qrPayVm = vm
        checkRequestPermissionCamera()
        binding.tvCancel.setOnClickListener {
            onBackPressed()
        }
    }

    private fun iniScanner() {
        codeScanner = CodeScanner(this, binding.scannerView)
        codeScanner.camera = CodeScanner.CAMERA_BACK
        codeScanner.formats = CodeScanner.ALL_FORMATS
        codeScanner.autoFocusMode = AutoFocusMode.SAFE
        codeScanner.scanMode = ScanMode.SINGLE
        codeScanner.isAutoFocusEnabled = true
        codeScanner.isFlashEnabled = false

        codeScanner.startPreview()

        binding.scannerView.setOnClickListener { codeScanner.startPreview() }

        codeScanner.setDecodeCallback { result: Result -> run {
            Log.v("QrPayActivity","iniScanner -> ${result.text}")
            if (result.text.isNullOrBlank()){
                showError(BuildConfig.ERROR_MSG)
            } else {
                val url = result.text
                val uri = Uri.parse(url)
                val host = uri.host
                val code = uri.lastPathSegment
                vm.prepareQuery(host, code)
            }

        } }

        codeScanner.setErrorCallback { error: Exception -> showError(error.message) }

        vm.deal.observe(this, Observer { data -> run {
            if (data != null){
                val intent = Intent(this, PayActivity::class.java)
                intent.putExtra(PayActivity.KEY_TYPE, PayActivity.TYPE_DEAL)
                intent.putExtra(PayActivity.KEY_DATA, data)
                startActivityForResult(intent, PayActivity.RC_PAYMENT)
            }else vm.error.postValue(BuildConfig.ERROR_MSG)
        } })

        vm.merchent.observe(this, Observer { data -> run {
            if (data != null){
                val intent = Intent(this, PayActivity::class.java)
                intent.putExtra(PayActivity.KEY_TYPE, PayActivity.TYPE_MERCHANT)
                intent.putExtra(PayActivity.KEY_DATA, data)
                startActivityForResult(intent, PayActivity.RC_PAYMENT)
            }else vm.error.postValue(BuildConfig.ERROR_MSG)
        } })

        vm.error.observe(this, Observer { msg -> run { if (msg != null)  showError(msg) } })
    }

    override fun onBackPressed() {
        setResult(Activity.RESULT_CANCELED)
        finish()
        super.onBackPressed()
    }

    private fun hasPermissionCamera():Boolean{
        return  ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED
    }

    private fun requestPermissionCamera(){
        ActivityCompat.requestPermissions(this, arrayOf(Manifest.permission.CAMERA), RC_CAMERA)
    }
    private fun checkRequestPermissionCamera() {
        if (hasPermissionCamera()){
            iniScanner()
        }else {
            if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.CAMERA)){
                SnackBarHelper.showSnackBarError("This feature need Camera permission, otherwise this feature will not working", binding.rootView, object : SnackBarHelper.OnSnackBarHelperListener {
                    override fun onError() {
                        requestPermissionCamera()
                    }
                })
            }else {
                requestPermissionCamera()
            }
        }
    }

    private fun showError(msg:String?){
        SnackBarHelper.showSnackBarError(msg!!, binding.rootView, object : SnackBarHelper.OnSnackBarHelperListener {
            override fun onError() {
                codeScanner.startPreview()
            }
        })
    }
    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        var granted = false
        for (i in permissions.indices){
            granted = grantResults[i] == PackageManager.PERMISSION_GRANTED
        }
        if (granted) iniScanner() else checkRequestPermissionCamera()

        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        when (requestCode) {
            PayActivity.RC_PAYMENT -> {
                when (resultCode) {
                    Activity.RESULT_OK -> {
                        setResult(Activity.RESULT_OK)
                        finish()
                    }
                    else -> onBackPressed()
                }
            }
            else -> {
            }
        }
        super.onActivityResult(requestCode, resultCode, data)
    }

    companion object {
        private const val RC_CAMERA = 11
        const val RC_PAY_WITH_QR = 12
    }

}
