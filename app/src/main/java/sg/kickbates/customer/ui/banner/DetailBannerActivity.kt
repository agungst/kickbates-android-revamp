package sg.kickbates.customer.ui.banner

import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import androidx.databinding.DataBindingUtil
import coil.api.load
import sg.kickbates.customer.R
import sg.kickbates.customer.databinding.DetailBannerActivityBinding
import sg.kickbates.customer.ui.main.pay.PayActivity
import sg.kickbates.data.api.models.ResponseBanners
import sg.kickbates.data.api.models.ResponseDeals

class DetailBannerActivity : AppCompatActivity() {
    private lateinit var binding:DetailBannerActivityBinding
    private lateinit var vm:DetailBannerVM

    private var banner:ResponseBanners.DataItem ? = null
    private var bannersDeal: ResponseDeals.DataItem ? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this,R.layout.detail_banner_activity)
        vm = DetailBannerVM()
        binding.lifecycleOwner = this
        binding.detailBannerVm = vm
        setSupportActionBar(binding.toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        banner = intent?.extras?.getParcelable(KEY_DATA)
        bannersDeal = intent?.extras?.getParcelable(KEY_DATA_DEALS)

        if (banner != null){
            banner?.name.let { n -> run {
                binding.tvTitleToolbar.text = n
                binding.tvTitle.text = n
            }
            }
            binding.tvValidTill.visibility = View.GONE
            binding.btnBuy.visibility = View.GONE


            binding.tvDesc.text = banner?.desc
            banner?.image().let { url -> binding.img.load(url) }
        }
        if (bannersDeal != null){

            bannersDeal?.dealName.let { n ->
                run {
                binding.tvTitleToolbar.text = n
                binding.tvTitle.text = n }
            }
            vm.noBanner.set(bannersDeal?.showRemark())
            binding.tvDesc.text = bannersDeal?.dealDescription
            bannersDeal?.imageBanner().let { url -> binding.img.load(url) }
            bannersDeal?.till().let { binding.tvValidTill.text = it }
            bannersDeal?.remark().let { binding.tvDis.text = it }
            binding.btnBuy.setOnClickListener {
                val intent = Intent(this, PayActivity::class.java)
                intent.putExtra(PayActivity.KEY_TYPE, PayActivity.TYPE_DEAL)
                intent.putExtra(PayActivity.KEY_DATA, bannersDeal)
                startActivityForResult(intent, PayActivity.RC_PAYMENT)
            }
        }

    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> onBackPressed()
            else -> {
            }
        }
        return super.onOptionsItemSelected(item)
    }
    companion object {
        private const val KEY_DATA = "key_data"
        private const val KEY_DATA_DEALS = "key_data_deal"

        fun start(context: Context, data: ResponseBanners.DataItem?){
            val intent = Intent(context, DetailBannerActivity::class.java)
            intent.putExtra(KEY_DATA, data)
            intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
            context.startActivity(intent)
        }

        fun startFromBannerDeal(context: Context, data: ResponseDeals.DataItem?){
            val intent = Intent(context, DetailBannerActivity::class.java)
            intent.putExtra(KEY_DATA_DEALS, data)
            intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
            context.startActivity(intent)
        }
    }
}
