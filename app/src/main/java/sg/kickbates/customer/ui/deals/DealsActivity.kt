package sg.kickbates.customer.ui.deals

import android.app.Activity
import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.KeyEvent
import android.view.MenuItem
import android.view.View
import android.view.inputmethod.InputMethodManager
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import kotlinx.coroutines.launch
import sg.kickbates.customer.R
import sg.kickbates.customer.databinding.DealsActivityBinding
import sg.kickbates.customer.ui.deals.details.DealsDetailsActivity
import sg.kickbates.customer.ui.deals.list.AdapterDealsListFull
import sg.kickbates.customer.ui.deals.list.AdapterPagingDealsListFull
import sg.kickbates.data.api.models.ResponseDeals
import sg.kickbates.data.helper.Pref
import sg.kickbates.data.helper.PrefBuilder
import sg.kickbates.data.helper.States

class DealsActivity : AppCompatActivity() {
    private lateinit var binding:DealsActivityBinding
    private lateinit var vm:DealsVM
    private lateinit var pref: Pref
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this,R.layout.deals_activity)
        pref = Pref(this)
        vm = DealsVM(pref)
        binding.lifecycleOwner = this
        binding.dealsVm = vm
        setSupportActionBar(binding.toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        binding.tvTitleToolbar.text = "Deals"
        lifecycleScope.launch { vm.getDeals() }
        binding.refresh.setOnRefreshListener { vm.getDealsByName(binding.etKeywordSerch.text.toString()) }
        vm.deals.observe(this, { data -> run {
            if (binding.refresh.isRefreshing) binding.refresh.isRefreshing = false
            val adapterPagingDealsListFull = AdapterPagingDealsListFull(object : AdapterPagingDealsListFull.OnAdapterDealsFullListener {
                override fun onClicked(data: ResponseDeals.DataItem?) {
                    DealsDetailsActivity.start(this@DealsActivity, data)
                }
            })
            adapterPagingDealsListFull.submitList(data)
            binding.rcvDeals.apply {
                hasFixedSize()
                itemAnimator = DefaultItemAnimator()
                layoutManager = LinearLayoutManager(this@DealsActivity, RecyclerView.VERTICAL,false)
                adapter = adapterPagingDealsListFull
            }
        }
        })
        binding.etKeywordSerch.setOnKeyListener { _, keyCode, event ->
            if (event.action == KeyEvent.ACTION_DOWN){
                if (keyCode == KeyEvent.KEYCODE_ENTER){
                    vm.getDealsByName(binding.etKeywordSerch.text.toString())
                    hideKeyboard()
                    return@setOnKeyListener true
                }
            }
            return@setOnKeyListener false
        }
        binding.etKeywordSerch.addTextChangedListener(watcher)
        binding.lytBtnFilter.setOnClickListener {
            if (!binding.etKeywordSerch.text.toString().isBlank()) vm.getDealsByName(binding.etKeywordSerch.text.toString())
            hideKeyboard()
        }
        vm.states?.observe(this, Observer { state -> run {
            when (state) {
                States.LOADING -> vm.loading.set(true)
                States.DONE -> vm.loading.set(false)
                else -> {}
            }
        } })
    }


    private val watcher = object : TextWatcher {

        override fun afterTextChanged(s: Editable?) {
            if (s.isNullOrBlank()){
                vm.getDeals()
                hideKeyboard()
            }else {
                if (s.length > 3){
                    vm.getDealsByName(s.toString())
                }
            }
        }

        override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}

        override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {}
    }

    private fun hideKeyboard(){
        val imm: InputMethodManager? = getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
        var v = currentFocus
        if (v == null) v = View(this)
        imm?.hideSoftInputFromWindow(v.windowToken,0)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> {
                onBackPressed()
            }
            else -> {
            }
        }
        return super.onOptionsItemSelected(item)
    }
    companion object {
        fun start(context: Context){
            val intent = Intent(context, DealsActivity::class.java)
            intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP
            context.startActivity(intent)
        }
    }
}
