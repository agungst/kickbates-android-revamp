package sg.kickbates.customer.ui.main

import android.Manifest
import android.annotation.SuppressLint
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.IntentSender
import android.content.pm.PackageManager
import android.graphics.Color
import android.graphics.PorterDuff
import android.location.Location
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Toast
import com.google.android.material.bottomnavigation.BottomNavigationView
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.work.OneTimeWorkRequest
import androidx.work.WorkManager
import com.google.android.gms.common.ConnectionResult
import com.google.android.gms.common.api.ApiException
import com.google.android.gms.common.api.GoogleApiClient
import com.google.android.gms.common.api.ResolvableApiException
import com.google.android.gms.location.*
import com.google.android.gms.tasks.Task
import com.google.firebase.iid.FirebaseInstanceId
import com.google.firebase.iid.InstanceIdResult
import kotlinx.android.synthetic.main.main_activity.*
import sg.kickbates.customer.R
import sg.kickbates.customer.databinding.MainActivityBinding
import sg.kickbates.customer.helper.AppsHelper
import sg.kickbates.customer.helper.DialogHelper
import sg.kickbates.customer.ui.main.cards.CardsFragment
import sg.kickbates.customer.ui.main.cashback.CashBackFragment
import sg.kickbates.customer.ui.main.home.HomeFragment
import sg.kickbates.customer.ui.main.profile.ProfileFragment
import sg.kickbates.customer.ui.qr.QrPayActivity
import sg.kickbates.customer.worker.UpdateFcmTokenWorker
import sg.kickbates.data.helper.Pref

class MainActivity : AppCompatActivity() {

    private lateinit var binding:MainActivityBinding
    private lateinit var vm:MainActivityVM
    private lateinit var mGoogleApi: GoogleApiClient
    private lateinit var fusedLocationClient: FusedLocationProviderClient
    private lateinit var pref: Pref

    private val connectionCallback = object : GoogleApiClient.ConnectionCallbacks {
        @SuppressLint("MissingPermission")
        override fun onConnected(data: Bundle?) {
            val lsr = LocationSettingsRequest.Builder().addLocationRequest(locationRequest)
            lsr.setAlwaysShow(true)
            val settingClient = LocationServices.getSettingsClient(this@MainActivity).checkLocationSettings(lsr.build())
            settingClient.addOnCompleteListener { task: Task<LocationSettingsResponse> ->
                try {
                    var response = task.getResult(ApiException::class.java)
                    fusedLocationClient.lastLocation.addOnCompleteListener {task: Task<Location> ->
                        run {
                            if (task.isSuccessful && task.result != null) {
                                pref.lat = task.result?.latitude.toString()
                                pref.lng = task.result?.longitude.toString()
                            }else {
                                pref.lat = "0"
                                pref.lng = "0"
                            }
                        }
                    }
                }catch (e: ApiException){
                    when (e.statusCode) {
                        LocationSettingsStatusCodes.RESOLUTION_REQUIRED -> {
                            try {
                                (e as ResolvableApiException).startResolutionForResult(this@MainActivity, RC_CHECK_SETTINGS)
                            }catch (e: IntentSender.SendIntentException){
                                e.printStackTrace()
                            }catch (e: ClassCastException){
                                e.printStackTrace()
                            }
                        }
                        else -> {
                        }
                    }
                }

            }
            settingClient.addOnFailureListener {e -> run {
                vm.error.postValue(e.message)
            }
            }
        }

        override fun onConnectionSuspended(status: Int) {

        }
    }
    private val failedCallback = GoogleApiClient.OnConnectionFailedListener { connectionResult: ConnectionResult -> run {
        Log.v("ss"," -> ${connectionResult.errorMessage}")
        vm.error.postValue(connectionResult.errorMessage)
    }
    }
    private lateinit var locationRequest: LocationRequest


    private val onNavigationItemSelectedListener = BottomNavigationView.OnNavigationItemSelectedListener { item ->
        when (item.itemId) {
            R.id.nav_home -> {
                loadFragment(HomeFragment.instance(),resources.getString(R.string.title_home),true)
                return@OnNavigationItemSelectedListener true
            }
            R.id.nav_cards -> {
                loadFragment(CardsFragment.instance(),resources.getString(R.string.title_cards),false)
                return@OnNavigationItemSelectedListener true
            }
            R.id.nav_pay -> {
                val intent = Intent(this, QrPayActivity::class.java)
                startActivityForResult(intent, QrPayActivity.RC_PAY_WITH_QR)
                return@OnNavigationItemSelectedListener true
            }
            R.id.nav_cash -> {
                loadFragment(CashBackFragment.instance(),resources.getString(R.string.title_cash_back),false)
                return@OnNavigationItemSelectedListener true
            }
            R.id.nav_profile -> {
                loadFragment(ProfileFragment.instance(),resources.getString(R.string.title_profile),false)
                return@OnNavigationItemSelectedListener true
            }
        }
        false
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this,R.layout.main_activity)
        pref = Pref(this)
        vm = MainActivityVM()
        binding.lifecycleOwner = this
        binding.mainVm = vm
        setSupportActionBar(binding.toolbar)

        mGoogleApi = GoogleApiClient.Builder(this)
            .addApi(LocationServices.API)
            .addConnectionCallbacks(connectionCallback)
            .addOnConnectionFailedListener(failedCallback)
            .build()
        locationRequest = LocationRequest.create()
        locationRequest.priority = LocationRequest.PRIORITY_BALANCED_POWER_ACCURACY
        fusedLocationClient = LocationServices.getFusedLocationProviderClient(this)

        if (hasPermissionLocation()) getCurrentLocation() else requestPermissionLocation()

        binding.navView.setOnNavigationItemSelectedListener(onNavigationItemSelectedListener)
        loadFragment(HomeFragment.instance(),resources.getString(R.string.title_home),true)
        binding.btnFabPay.setColorFilter(Color.WHITE, PorterDuff.Mode.MULTIPLY)
        binding.btnFabPay.setOnClickListener {
            val intent = Intent(this, QrPayActivity::class.java)
            startActivityForResult(intent, QrPayActivity.RC_PAY_WITH_QR)
        }
        runWorkerUpdateFcmToken()
    }


    private fun runWorkerUpdateFcmToken(){
        WorkManager.getInstance(this).beginWith(OneTimeWorkRequest.Builder(UpdateFcmTokenWorker::class.java).build()).enqueue()
    }
    fun loadFragment(fragment: Fragment,title:String,isHome:Boolean){
        AppsHelper.changeFragment(supportFragmentManager,fragment,R.id.main_container)
        if (isHome){
            binding.tvTitleToolbar.visibility  = View.GONE
            binding.imgLogo.visibility = View.VISIBLE
            binding.appBar.setBackgroundColor(Color.parseColor("#ffffff"))
        }else {
            binding.appBar.setBackgroundColor(Color.parseColor("#1f908a"))
            binding.tvTitleToolbar.visibility  = View.VISIBLE
            binding.imgLogo.visibility = View.GONE
            binding.tvTitleToolbar.text = title
        }

    }

    fun carNavigationClik(){
        var navMenu = binding.navView.menu.findItem(R.id.nav_cards)
        navMenu.isChecked = true
    }
    private fun hasPermissionLocation():Boolean {
        return  ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED
    }

    private fun requestPermissionLocation(){
        if (!hasPermissionLocation()){
            ActivityCompat.requestPermissions(this,
                listOf(Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION).toTypedArray(), RC_LOCATION)
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        var granted = false
        for (a in permissions.indices){
            granted = grantResults[a] == PackageManager.PERMISSION_GRANTED
        }
        if (granted)getCurrentLocation() else requestPermissionLocation()

        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
    }


    private fun getCurrentLocation() {
        mGoogleApi.connect()
    }

    fun showDialogError(msg: String){
        DialogHelper.showDialogError(this, msg)
    }
    fun showDialogSuccess(msg: String){
        DialogHelper.showDialogSuccess(this, msg)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        when (requestCode) {
            QrPayActivity.RC_PAY_WITH_QR -> {
                when (resultCode) {
                    Activity.RESULT_OK -> showDialogSuccess("Payment Successfully")
                    else -> {}
                }
            }
            else -> {
            }
        }
        super.onActivityResult(requestCode, resultCode, data)
    }

    companion object {
        private const val RC_LOCATION = 111
        private const val RC_CHECK_SETTINGS = 122

        fun start(context: Context){
            val intent = Intent(context, MainActivity::class.java)
            intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP
            context.startActivity(intent)
        }
    }
}
