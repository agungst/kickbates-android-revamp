package sg.kickbates.customer.ui.otp

import android.app.Activity
import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.TextUtils
import sg.kickbates.customer.R
import androidx.databinding.DataBindingUtil
import com.google.firebase.FirebaseException
import com.google.firebase.auth.PhoneAuthCredential
import com.google.firebase.auth.PhoneAuthProvider
import sg.kickbates.customer.databinding.OtpActivityBinding
import sg.kickbates.customer.helper.AuthHelper
import sg.kickbates.customer.helper.SnackBarHelper
import android.util.Log
import com.google.firebase.auth.AuthCredential

class OtpActivity : AppCompatActivity() {
    private lateinit var binding:OtpActivityBinding
    private lateinit var vm:OtpActivityVM
    private var phone:String? = null
    private var storedVerificationId:String? = null
    private var tokenSaved: PhoneAuthProvider.ForceResendingToken? = null

    private var callback = object : PhoneAuthProvider.OnVerificationStateChangedCallbacks() {
        override fun onVerificationCompleted(phoneAuthCredential: PhoneAuthCredential) {
            Log.v("OtpActivity","onVerificationCompleted -> ${phoneAuthCredential.smsCode}")
        }

        override fun onVerificationFailed(p0: FirebaseException) {
            vm.error.postValue(p0.message)
            vm.loading.set(false)
        }
        override fun onCodeSent(code: String, token: PhoneAuthProvider.ForceResendingToken) {
            super.onCodeSent(code, token)
            storedVerificationId = code
            tokenSaved = token
            binding.tvPhoneNumber.text = parsingPhone(phone)
            vm.loading.set(false)
            vm.sending.set(false)
        }
    }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this,R.layout.otp_activity)
        vm = OtpActivityVM()
        binding.lifecycleOwner = this
        binding.otpVm = vm
        vm.loading.set(true)
        vm.sending.set(true)
        intent?.extras.takeIf { it?.containsKey(KEY_PHONE)!! }.let {
            phone = it?.getString(KEY_PHONE, "0")
            sendOtp(phone)
        }

        binding.tvResend.setOnClickListener {
            if (tokenSaved != null)phone?.let { it1 ->
                AuthHelper.authPhoneReSend(it1, tokenSaved, this, callback)
                vm.sending.set(true)
            }
        }
        vm.error.observe(this, { SnackBarHelper.showSnackBarError(it, binding.btnVerify, object : SnackBarHelper.OnSnackBarHelperListener {
            override fun onError() {
                onBackPressed()
            }
        })})

        binding.btnVerify.setOnClickListener {
            val codeOtp = binding.etOtp.text.toString()
            if (TextUtils.isEmpty(codeOtp)){
                binding.lytEtOtp.isErrorEnabled = true
                binding.lytEtOtp.error = "Required"
                return@setOnClickListener
            }
            binding.lytEtOtp.isErrorEnabled = false
            binding.lytEtOtp.error = null
            if (storedVerificationId != null){
                val credential = PhoneAuthProvider.getCredential(storedVerificationId!!, codeOtp)
                verifyOtp(credential)
            } else vm.error.postValue("Verification Id is null")
        }

        binding.imgBack.setOnClickListener { onBackPressed() }
    }

    private fun sendOtp(phone: String?){
        phone.let { AuthHelper.authPhone(it!!, this, callback) }
    }

    private fun verifyOtp(credential: AuthCredential?) {
        credential?.let { authCredential ->
            AuthHelper.firebaseAuth().signInWithCredential(authCredential).addOnFailureListener {
                it.printStackTrace()
                vm.error.postValue(it.message)
                Log.v("AuthHelper","singInWithPhone -> ${it.message}")
            }.addOnCompleteListener { task ->
                if (task.isSuccessful){
                    setResult(Activity.RESULT_OK)
                    finish()
                }
            }
        }
    }

    override fun onBackPressed() {
        setResult(Activity.RESULT_CANCELED)
        finish()
        super.onBackPressed()
    }

    private fun parsingPhone(phone: String?): String {
        return if (!phone.isNullOrEmpty()){
            val start = phone.substring(0, 3)
            val end = phone.substring(phone.length.minus(3),phone.length )
            "$start ***** $end"
        }else {
            "-";
        }
    }
    companion object {
        const val RC_OTP = 123
        private const val KEY_PHONE = "key_phone"
        @JvmStatic fun instance(context: Context, phone:String): Intent {
            return Intent(context, OtpActivity::class.java).apply {
                putExtra(KEY_PHONE, phone)
            }
        }
    }
}