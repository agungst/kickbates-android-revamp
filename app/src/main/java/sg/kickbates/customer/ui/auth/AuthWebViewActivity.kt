package sg.kickbates.customer.ui.auth

import android.annotation.SuppressLint
import android.app.Activity
import android.app.Instrumentation
import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.net.Uri
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Message
import android.util.Log
import android.view.MenuItem
import android.view.View
import android.webkit.WebChromeClient
import android.webkit.WebResourceRequest
import android.webkit.WebView
import android.webkit.WebViewClient
import androidx.databinding.DataBindingUtil
import okhttp3.HttpUrl
import okhttp3.HttpUrl.Companion.toHttpUrlOrNull
import retrofit2.http.Url
import sg.kickbates.customer.R
import sg.kickbates.customer.databinding.AuthWebViewActivityBinding
import sg.kickbates.data.api.models.RequestCard
import java.net.URL
import java.net.URLEncoder

class AuthWebViewActivity : AppCompatActivity() {
    private lateinit var binding:AuthWebViewActivityBinding
    private lateinit var vm:AuthWebViewActivityVM
    private var data:RequestCard? = null
    @SuppressLint("SetJavaScriptEnabled")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this,R.layout.auth_web_view_activity)
        vm = AuthWebViewActivityVM()
        binding.lifecycleOwner = this
        binding.webAuthVm = vm
        setSupportActionBar(binding.toolbar)
        binding.tvTitleToolbar.text = "Add New Card"
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        var setting = binding.webAuth.settings
        setting.javaScriptEnabled  = true
        binding.webAuth.webChromeClient = chromeClientListener
        binding.webAuth.webViewClient = webClientListener
        intent?.extras?.let {
            if (!it.isEmpty){
                data = it.getParcelable(KEY_DATA)
                data?.url?.isNotEmpty().let {
                    data?.url?.let { it1 -> binding.webAuth.loadUrl(it1) }
                }
            }
        }
    }
    private val webClientListener = object : WebViewClient(){
        override fun onPageFinished(view: WebView?, url: String?) {
            vm.loading.set(false)
            super.onPageFinished(view, url)

        }
        override fun onPageStarted(view: WebView?, url: String?, favicon: Bitmap?) {
            vm.loading.set(true)
            super.onPageStarted(view, url, favicon)
            if (url != null){
                var httpUrl = url.toHttpUrlOrNull()
                var host = httpUrl?.host

                if (host.equals("git.wmzhubo.com", true)) {

                  var code:String? = null
                    when (data?.issuer) {
                        "ocbc" -> code = httpUrl?.queryParameter("access_token")
                        "dbs" -> code = httpUrl?.queryParameter("code")
                        "citi" -> code = httpUrl?.queryParameter("code")
                        else -> { }
                    }

                    data?.code_auth = code
                    if (code.isNullOrEmpty()){
                        setResult(Activity.RESULT_CANCELED)
                        finish()
                    }else {
                        var intent = Intent()
                        intent.putExtra(KEY_DODE,data)
                        setResult(Activity.RESULT_OK, intent)
                        finish()

                    }
                }
            }
        }
    }


    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> onBackPressed()
            else -> {
            }
        }
        return super.onOptionsItemSelected(item)
    }
    override fun onBackPressed() {
        setResult(Activity.RESULT_CANCELED)
        finish()
        super.onBackPressed()
    }
    private val chromeClientListener = object : WebChromeClient() {

        override fun onProgressChanged(view: WebView?, newProgress: Int) {
            super.onProgressChanged(view, newProgress)
            vm.loading.set(newProgress > 100)
        }
    }
    companion object {
        const val KEY_DATA = "key_data"
        const val KEY_DODE = "key_data"
        const val RC_WEB_VIEW = 1010
    }
}
