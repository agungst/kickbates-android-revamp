package sg.kickbates.customer.ui.payment.channels

import androidx.databinding.BaseObservable
import androidx.databinding.Bindable
import androidx.databinding.ObservableField
import androidx.lifecycle.MutableLiveData
import kotlinx.coroutines.CoroutineExceptionHandler
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.SupervisorJob
import kotlinx.coroutines.launch
import retrofit2.HttpException
import sg.kickbates.data.BuildConfig
import sg.kickbates.data.api.Api
import sg.kickbates.data.api.models.payment.*
import sg.kickbates.data.helper.Pref

/**
 * Created by simx on 30,July,2019
 */
class PaymentChannelVM(private var pref: Pref): BaseObservable() {
    private var job = SupervisorJob()

    @Bindable var loading = ObservableField<Boolean>()
    @Bindable var empty = ObservableField<Boolean>()
    @Bindable var error = MutableLiveData<String>()


    @Bindable var channels = MutableLiveData<List<ResponsePaymentChannelCard.DataItem>>()

    @Bindable var resultChargeCard = MutableLiveData<DetailInvoice>()

    fun getPaymentChannel(){
        loading.set(true)
        try {
            CoroutineScope(CoroutineExceptionHandler { _, throwable -> run {
                job = SupervisorJob()
                throwable.printStackTrace()
                updateMsg(throwable.message)
            } }).launch {
                val res = Api.Factory.create(BuildConfig.BASE_URL).paymentChannelCardAsync(pref.token).await()
                res.let {
                    when (it.status) {
                         200 -> {if (res.data.isNullOrEmpty()) channels.postValue(listOf()) else channels.postValue(res.data)}
                         else -> {updateMsg("Failed to get payment channel")}
                   }
                }
                loading.set(false)
            }
        }catch (e: HttpException){
            e.printStackTrace()
            updateMsg(e.message)
        }
    }

    fun chargeCard(invoiceId:Int?, cardId:String?){
        loading.set(true)
        try {
            CoroutineScope(CoroutineExceptionHandler { _, throwable -> run {
                job = SupervisorJob()
                throwable.printStackTrace()
                updateMsg(throwable.localizedMessage)
            } }).launch {
                val res = Api.Factory.create(BuildConfig.BASE_URL).chargeCardAsync(pref.token, cardId, invoiceId).await()
                res.let {
                    when (it.status) {
                        200 -> { if (res.data == null) resultChargeCard.postValue(null) else resultChargeCard.postValue(res.data)}
                        else -> {}
                    }
                }

                loading.set(false)
            }
        }catch (e: HttpException){
            e.printStackTrace()
            updateMsg(e.localizedMessage)
        }

    }

    private fun updateMsg(msg:String?){
        loading.set(false)
        error.postValue(BuildConfig.ERROR_MSG)
    }
}