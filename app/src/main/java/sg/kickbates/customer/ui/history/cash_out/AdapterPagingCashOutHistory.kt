package sg.kickbates.customer.ui.history.cash_out

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.paging.PagedListAdapter
import androidx.recyclerview.widget.RecyclerView
import sg.kickbates.customer.R
import sg.kickbates.customer.databinding.ItemCashOutHistoryBinding
import sg.kickbates.data.api.models.cash_back.ResponseCashBackHistory
import sg.kickbates.data.api.models.cash_out.ResponseCashOutHistory
import sg.kickbates.data.helper.KickDiffUtils

/**
 * Created by simx on 31,July,2019
 */
class AdapterPagingCashOutHistory: PagedListAdapter<ResponseCashOutHistory.DataItem, AdapterPagingCashOutHistory.Holder>(
    KickDiffUtils.CashOutHistoryDiff)  {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): Holder {
        return Holder(ItemCashOutHistoryBinding.bind(LayoutInflater.from(parent.context).inflate(R.layout.item_cash_out_history,parent,false)))
    }

    override fun onBindViewHolder(holder: Holder, position: Int) {
        holder.bind(getItem(position))
    }

    class Holder(var binding: ItemCashOutHistoryBinding):RecyclerView.ViewHolder(binding.root) {
        fun bind(data: ResponseCashOutHistory.DataItem?){
            with(binding){
                itemCashOutVm = ItemCashOutHistoryVM(data)
                executePendingBindings()
            }
        }

    }
}