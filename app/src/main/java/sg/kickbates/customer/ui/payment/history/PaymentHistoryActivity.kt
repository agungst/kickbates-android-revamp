package sg.kickbates.customer.ui.payment.history

import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.MenuItem
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import sg.kickbates.customer.R
import sg.kickbates.customer.databinding.PaymentHistoryActivityBinding
import sg.kickbates.customer.helper.DialogHelper
import sg.kickbates.data.api.models.payment.DetailInvoice
import sg.kickbates.data.helper.Pref
import sg.kickbates.data.helper.States

class PaymentHistoryActivity : AppCompatActivity() {
    private lateinit var binding:PaymentHistoryActivityBinding
    private lateinit var vm:PaymentHistoryVM
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this,R.layout.payment_history_activity)
        vm = PaymentHistoryVM(Pref(this))
        binding.lifecycleOwner = this
        binding.paymentHistoryVm = vm
        setSupportActionBar(binding.toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        binding.tvTitleToolbar.text = "History"
        vm.dataTransactions()
        // TODO
        vm.transactions.observe(this, Observer { data -> run {
            var adapterPaymentHistory = AdapterPagingTransactionHistory(object : AdapterPagingTransactionHistory.OnAdapterPagingTransactionHistoryListener {
                override fun onClicker(data: DetailInvoice?) {
                    /*DialogHelper.showDialogSuccessPayment(this@PaymentHistoryActivity,null, data, object : DialogHelper.OnDialogSuccessListener{
                        override fun onOk() {

                        }
                    })*/
                }
            })
            adapterPaymentHistory.submitList(data)
            binding.rcvTransactionsHistory.apply {
                hasFixedSize()
                itemAnimator = DefaultItemAnimator()
                layoutManager  = LinearLayoutManager(this@PaymentHistoryActivity, RecyclerView.VERTICAL, false)
                adapter = adapterPaymentHistory
            }
        } })

        vm.states?.observe(this, Observer { state -> run {
            when (state) {
                States.LOADING -> vm.loading.set(true)
                States.DONE -> vm.loading.set(false)
                else -> vm.loading.set(false)
            }
        } })


    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> onBackPressed()
            else -> {
            }
        }
        return super.onOptionsItemSelected(item)
    }
    companion object {
        fun start(context: Context){
            val intent = Intent(context, PaymentHistoryActivity::class.java)
            intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP
            context.startActivity(intent)
        }
    }
}
