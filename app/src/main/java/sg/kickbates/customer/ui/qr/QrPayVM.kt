package sg.kickbates.customer.ui.qr

import android.util.Log
import androidx.databinding.BaseObservable
import androidx.databinding.Bindable
import androidx.databinding.ObservableField
import androidx.lifecycle.MutableLiveData
import kotlinx.coroutines.CoroutineExceptionHandler
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.SupervisorJob
import kotlinx.coroutines.launch
import retrofit2.HttpException
import sg.kickbates.data.BuildConfig
import sg.kickbates.data.api.Api
import sg.kickbates.data.api.models.ResponseDeals
import sg.kickbates.data.api.models.ResponseMerchants
import sg.kickbates.data.helper.Pref

/**
 * Created by simx on 30,July,2019
 */
class QrPayVM(private var pref: Pref):BaseObservable() {
    private var job = SupervisorJob()

    @Bindable var loading = ObservableField<Boolean>()
    @Bindable var error = MutableLiveData<String>()

    @Bindable var merchent = MutableLiveData<ResponseMerchants.DataItem>()
    @Bindable var deal = MutableLiveData<ResponseDeals.DataItem>()

    fun prepareQuery(host: String?, code: String?) {
        var merchantHost = "merchant"
        var dealHost = "deal"
        when (host) {
            merchantHost -> searchMerchant(code)
            dealHost -> searchDeal(code)
            else -> updateMsg(BuildConfig.ERROR_MSG)
        }
    }

    private fun searchDeal(code: String?) {
        loading.set(true)
        try {
            CoroutineScope(CoroutineExceptionHandler { _, throwable -> run {
                job = SupervisorJob()
                throwable.printStackTrace()
            } }).launch {
                val res = Api.Factory.create(BuildConfig.BASE_URL).dealsByCodeAsync(pref.token, code).await()
                res.let {
                    when (it.status) {
                        200 -> {if (res.data.isNullOrEmpty()) deal.postValue(null) else deal.postValue(
                            res.data?.get(0))}
                        else -> {updateMsg(BuildConfig.ERROR_MSG)}
                    }
                }
                loading.set(false)

            }
        }catch (e: HttpException){
            e.printStackTrace()

        }
    }

    private fun searchMerchant(code: String?) {
        loading.set(true)
        try {
            CoroutineScope(CoroutineExceptionHandler { _, throwable -> run {
                job = SupervisorJob()
                throwable.printStackTrace()
            } }).launch {
                val res = Api.Factory.create(BuildConfig.BASE_URL).merchantsByCodeAsync(pref.token, code).await()
                res.let {
                    when (it.status) {
                         200 -> {if (res.data.isNullOrEmpty()) merchent.postValue(null) else merchent.postValue(
                             res.data?.get(0))}
                         else -> {updateMsg(BuildConfig.ERROR_MSG)}
                   }
                }
                loading.set(false)
            }
        }catch (e: HttpException){
            e.printStackTrace()

        }
    }



    private fun updateMsg(msg:String?){
        error.postValue(msg)
        loading.set(false)
    }
}