package sg.kickbates.customer.ui.main.profile

import androidx.databinding.BaseObservable
import androidx.databinding.Bindable
import androidx.databinding.ObservableField
import androidx.lifecycle.MutableLiveData

import kotlinx.coroutines.CoroutineExceptionHandler
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.SupervisorJob
import kotlinx.coroutines.launch
import retrofit2.HttpException
import sg.kickbates.data.BuildConfig
import sg.kickbates.data.api.Api
import sg.kickbates.data.api.models.RequestUpdateProfile
import sg.kickbates.data.api.models.ResponseMe
import sg.kickbates.data.helper.Pref

/**
 * Created by simx on 28,November,2019
 */
class UpdateProfileVM(private var pref: Pref): BaseObservable(){

    private var job = SupervisorJob()
    @Bindable var loading = ObservableField<Boolean>()
    @Bindable var error = MutableLiveData<String>()
    @Bindable var sussessUpdate = MutableLiveData<Boolean>()
    @Bindable var user = MutableLiveData<ResponseMe.Data>()


    fun getProfileUser() {
        loading.set(true)
        try {
            CoroutineScope(CoroutineExceptionHandler { _, throwable -> run {
                job = SupervisorJob()
                throwable.printStackTrace()
                updateMsg(throwable.message)
            } }).launch {
                val res = Api.Factory.create(BuildConfig.BASE_URL).meFromIdAsync(pref.token, pref.usersId).await()
                res.let {
                    when (it.status) {
                         200 -> {if (res.data == null) user.postValue(null) else user.postValue(res.data)}
                         else -> {updateMsg(BuildConfig.ERROR_MSG)}
                   }
                }
                loading.set(false)
            }
        }catch (e: HttpException){
            e.printStackTrace()
            updateMsg(e.message)

        }
    }

    private fun updateMsg(msg:String?){
        loading.set(false)
        error.postValue(msg)

    }

    fun updateUser(requestUpdateProfile: RequestUpdateProfile?) {
        try {
            CoroutineScope(CoroutineExceptionHandler { _, throwable -> run {
                job = SupervisorJob()
                throwable.printStackTrace()
                updateMsg(throwable.message)
            } }).launch {
                val res = Api.Factory.create(BuildConfig.BASE_URL).updateProfileAsync(pref.token, pref.usersId, requestUpdateProfile).await()
                sussessUpdate.postValue(res.status == 200)
                loading.set(false)
            }
        }catch (e: HttpException){
            e.printStackTrace()
            updateMsg(e.message)

        }
    }
}