package sg.kickbates.customer.ui.premium

import androidx.databinding.BaseObservable
import androidx.databinding.Bindable
import androidx.databinding.ObservableField
import androidx.lifecycle.MutableLiveData
import kotlinx.coroutines.CoroutineExceptionHandler
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.SupervisorJob
import kotlinx.coroutines.launch
import retrofit2.HttpException
import sg.kickbates.data.BuildConfig
import sg.kickbates.data.api.Api
import sg.kickbates.data.api.models.ResponseMe
import sg.kickbates.data.api.models.payment.DetailInvoice
import sg.kickbates.data.api.models.payment.RequestInvoicePremium
import sg.kickbates.data.api.models.premium.ResponsePremiumPage
import sg.kickbates.data.helper.Pref

/**
 * Created by simx on 24,December,2019
 */
class PremiumPageVM (private var pref: Pref):BaseObservable(){
    private var job = SupervisorJob()
    @Bindable var loading = ObservableField<Boolean>()
    @Bindable var isPremium = ObservableField<Boolean>()
    @Bindable var error = MutableLiveData<String>()
    @Bindable var premium = MutableLiveData<ResponsePremiumPage>()
    @Bindable var userData = MutableLiveData<ResponseMe.Data>()

    @Bindable var invoice = MutableLiveData<DetailInvoice>()

    fun dataPremiumPage(){
        loading.set(true)
        try {
            CoroutineScope(CoroutineExceptionHandler { _, throwable -> run {
                job = SupervisorJob()
                throwable.printStackTrace()
                updateError(throwable.message)
                premium.postValue(null)
            } }).launch {
                val res = Api.Factory.create(BuildConfig.BASE_URL).premiumInfoAsync(pref.token).await()
                premium.postValue(res)
                loading.set(false)
            }
        }catch (e: HttpException){
            e.printStackTrace()
            updateError(e.message)
        }
    }

    private fun updateError(msg:String?){
        loading.set(false)
        error.postValue(BuildConfig.ERROR_MSG)
    }
    fun upgradeToPremium() {
        loading.set(true)
        val requestInvoicePremium = RequestInvoicePremium()
        requestInvoicePremium.payByKick = 20.0
        requestInvoicePremium.premium = 1
        requestInvoicePremium.promoCodeId = null
        CoroutineScope(CoroutineExceptionHandler { _, throwable -> run {
            throwable.printStackTrace()
            job = SupervisorJob() }
            updateError(BuildConfig.ERROR_MSG)
        }).launch {
            val res = Api.Factory.create(BuildConfig.BASE_URL).generateInvoiceAsync(pref.token, requestInvoicePremium).await()
            invoice.postValue(res.data)
            loading.set(false)
        }
    }

    fun unSubscribe(id: Int?) {
        loading.set(true)
        try {
            CoroutineScope(CoroutineExceptionHandler { _, throwable -> run {
                job = SupervisorJob()
                throwable.printStackTrace()
                updateError(BuildConfig.ERROR_MSG)
                userData.postValue(null)
            } }).launch {
                val res = Api.Factory.create(BuildConfig.BASE_URL).updateSubscribeAsync(pref.token, id, false).await()
                userData.postValue(res.data)
            }
        }catch (e: HttpException){
            e.printStackTrace()
            updateError(BuildConfig.ERROR_MSG)
        }
    }
}