package sg.kickbates.customer.ui.payment.history

import androidx.databinding.BaseObservable
import androidx.databinding.Bindable
import androidx.databinding.ObservableField
import androidx.lifecycle.LiveData
import androidx.lifecycle.Transformations
import androidx.paging.LivePagedListBuilder
import androidx.paging.PagedList
import sg.kickbates.data.api.models.payment.DetailInvoice
import sg.kickbates.data.helper.Pref
import sg.kickbates.data.helper.States
import sg.kickbates.data.paging.PaymentHistoryDataSource
import sg.kickbates.data.paging.PaymentHistoryDataSourceFactory

/**
 * Created by simx on 11,August,2019
 */
class PaymentHistoryVM(private  var pref: Pref): BaseObservable() {

    @Bindable var loading = ObservableField<Boolean>()
    private val config = PagedList.Config.Builder()
        .setPageSize(8)
        .setInitialLoadSizeHint(8 * 3)
        .setEnablePlaceholders(true)
        .build()


    private var transactionsSourceFactory: PaymentHistoryDataSourceFactory = PaymentHistoryDataSourceFactory()
    var transactions: LiveData<PagedList<DetailInvoice>>
    var  states : LiveData<States>? = null
    var  msg : LiveData<String>? = null
    var  empty : LiveData<Boolean>? = null
    init {
        transactions = LivePagedListBuilder(transactionsSourceFactory, config).build()
        states =  Transformations.switchMap<PaymentHistoryDataSource, States>(transactionsSourceFactory.history!!, PaymentHistoryDataSource::state)
        msg  = Transformations.switchMap<PaymentHistoryDataSource, String>(transactionsSourceFactory.history!!, PaymentHistoryDataSource::errorMsg)
        empty  = Transformations.switchMap<PaymentHistoryDataSource, Boolean>(transactionsSourceFactory.history!!, PaymentHistoryDataSource::empty)
    }

    fun dataTransactions(){
        transactionsSourceFactory.query(pref.token, pref.usersId)
        transactionsSourceFactory.history?.value?.invalidate()
    }

}