package sg.kickbates.customer.ui.cash_out

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.FragmentManager
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import sg.kickbates.customer.R
import sg.kickbates.customer.databinding.DialogCardsAvailableBinding
import sg.kickbates.customer.ui.main.cards.dialog.AdapterDialogCardsAvailable
import sg.kickbates.customer.ui.main.cards.dialog.DialogCardsAvailableVM
import sg.kickbates.data.api.models.card.ResponseCardAvaiable
import sg.kickbates.data.helper.Pref

/**
 * Created by simx on 02,January,2020
 */
class DialogBank constructor(var callback: OnDialogBankListener,var  isAll:Boolean,var isStatic:Boolean): DialogFragment() {
    private lateinit var binding:DialogCardsAvailableBinding
    private lateinit var vm:DialogCardsAvailableVM

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.dialog_cards_available, container,false)
        vm = DialogCardsAvailableVM(Pref(this.context!!))
        binding.lifecycleOwner = this
        binding.dialogCardsAvailableVm = vm
        if (isStatic)vm.dataBankStatic() else vm.dataBanks(isAll)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.imgClose.setOnClickListener { dialog?.dismiss() }
        vm.banks.observe(this, Observer { data -> run {
            binding.rcvBanks.apply {
                hasFixedSize()
                itemAnimator = DefaultItemAnimator()
                layoutManager = LinearLayoutManager(this@DialogBank.context!!)
                adapter = AdapterDialogCardsAvailable(data, object : AdapterDialogCardsAvailable.OnAdapterDialogCardsAvailableCallback {
                    override fun onClicked(data: ResponseCardAvaiable.DataItem) {
                        dialog?.dismiss()
                        callback.onSelectedBank(data)
                    }
                })
            }
        } })
    }

    interface OnDialogBankListener {
        fun onSelectedBank(data: ResponseCardAvaiable.DataItem)
    }

    companion object {
        fun show(fm: FragmentManager, callback: OnDialogBankListener, isAll: Boolean, isStatic: Boolean){
            var fragment = DialogBank(callback,isAll, isStatic)
            fragment.show(fm, DialogBank::class.java.simpleName)
        }
    }
}