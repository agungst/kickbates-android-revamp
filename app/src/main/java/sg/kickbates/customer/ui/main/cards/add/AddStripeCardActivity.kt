package sg.kickbates.customer.ui.main.cards.add

import android.app.Activity
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.MenuItem
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer

import sg.kickbates.customer.R
import sg.kickbates.customer.databinding.AddStripeCardActivityBinding
import sg.kickbates.customer.helper.DialogHelper
import sg.kickbates.customer.helper.SnackBarHelper
import sg.kickbates.data.api.models.payment.DetailInvoice
import sg.kickbates.data.helper.Pref

class AddStripeCardActivity : AppCompatActivity() {

    private lateinit var binding:AddStripeCardActivityBinding
    private lateinit var vm:AddStripeCardActivityVM
    private lateinit var pref: Pref
    private var invoiceId:Int? = null
    private var type:Int? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this,R.layout.add_stripe_card_activity)
        pref = Pref(this)
        vm = AddStripeCardActivityVM(pref)
        binding.lifecycleOwner = this
        binding.addCardStripeVm = vm
        setSupportActionBar(binding.toolbar)
        binding.tvTitleToolbar.text = "Add New Credit Card"
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        invoiceId = intent.extras?.getInt(KEY_ID_INVOICE)
        type = intent.extras?.getInt(KEY_TYPE)

        binding.btnAdd.setOnClickListener {
            val card = binding.ltyCard.paymentMethodCard?.toParamMap()
            val number = card?.get("number") as String
            val expMonth = card["exp_month"] as Int
            val expYear = card["exp_year"] as Int
            val cvc = card["cvc"] as String
            vm.getCardToken(number, expMonth, expYear, cvc)
        }
        vm.paymentMethodCard.observe(this, Observer { data -> run {
            if (data != null) vm.chargeCard(invoiceId, data.id)
        } })
        vm.error.observe(this, Observer { msg -> run {
            SnackBarHelper.showSnackBarError(msg, binding.appBar, object : SnackBarHelper.OnSnackBarHelperListener {
                override fun onError() {

                }
            })
        } })

        vm.result.observe(this, Observer { data -> run {
            
            if (data != null){
                handleResultOkWithData(data)
            } else {
                setResult(Activity.RESULT_CANCELED)
                finish()
            }
        } })

    }

    private fun handleResultOkWithData(data: DetailInvoice?){
        DialogHelper.showDialogSuccessPayment(this, type,  data, object : DialogHelper.OnDialogSuccessListener {
            override fun onOk() {
                setResult(Activity.RESULT_OK)
                finish()
            }
        })
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> onBackPressed()
            else -> {
            }
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onBackPressed() {
        super.onBackPressed()
        setResult(Activity.RESULT_CANCELED)
        finish()
    }
    companion object {
        const val RC_CODE = 101
        const val KEY_DATA = "payment_method"
        const val KEY_ID_INVOICE = "invoice_id"

        const val KEY_TYPE = "type"
    }
}
