package sg.kickbates.customer.ui.otp

import androidx.databinding.BaseObservable
import androidx.databinding.Bindable
import androidx.databinding.ObservableField
import androidx.lifecycle.MutableLiveData

class OtpActivityVM():BaseObservable() {
    @Bindable var loading = ObservableField<Boolean>()
    @Bindable var sending = ObservableField<Boolean>()
    @Bindable var error = MutableLiveData<String>()
}