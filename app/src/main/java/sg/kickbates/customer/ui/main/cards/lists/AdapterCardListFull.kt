package sg.kickbates.customer.ui.main.cards.lists

import android.content.res.ColorStateList
import android.graphics.Color
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import sg.kickbates.customer.R
import sg.kickbates.customer.databinding.ItemCreditCardBinding
import sg.kickbates.data.api.models.card.ResponseCustomerCards

/**
 * Created by simx on 31,July,2019
 */
class AdapterCardListFull(private var datas:List<ResponseCustomerCards.DataItem>, private var listener: OnAdapterCardListFullListener): RecyclerView.Adapter<AdapterCardListFull.Holder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): Holder {
        return Holder(ItemCreditCardBinding.bind(LayoutInflater.from(parent.context).inflate(R.layout.item_credit_card,parent,false)))
    }

    override fun getItemCount(): Int {
        return datas.size
    }


    interface OnAdapterCardListFullListener {
        fun onAdapterClicked(data: ResponseCustomerCards.DataItem)
        fun onBottomPayClicked(data: ResponseCustomerCards.DataItem.DataCardStatements?)
    }

    override fun onBindViewHolder(holder: Holder, position: Int) {
        holder.bind(datas[position])
        holder.binding.rootView.setBackgroundColor(Color.parseColor("#${datas[position].kickBanks?.bankCardColor}"))
        /**
         * Button Paid
         */

        if (datas[position].summaryIsPaid()){
            holder.binding.btnPay.text = "PAY"
            holder.binding.btnPay.setTextColor(Color.parseColor("#ffffff"))
            holder.binding.btnPay.backgroundTintList = ColorStateList.valueOf(Color.parseColor("#1f908a"))
            holder.binding.btnPay.isClickable = true
            holder.binding.btnPay.setOnClickListener {
                listener.onBottomPayClicked(data = datas[position].kickCustomerCardStatements?.get(0))
            }

        }else {
            holder.binding.btnPay.text = "PAID"
            holder.binding.btnPay.setTextColor(Color.parseColor("#000000"))
            holder.binding.btnPay.backgroundTintList = ColorStateList.valueOf(Color.parseColor("#f6f6f8"))
            holder.binding.btnPay.isClickable = false
        }

        holder.itemView.setOnClickListener { listener.onAdapterClicked(datas[position]) }

    }

    class Holder(var binding: ItemCreditCardBinding):RecyclerView.ViewHolder(binding.root) {
        fun bind(data: ResponseCustomerCards.DataItem){
            with(binding){
                itemCreditCardVm = ItemCreditCardVM(data)
                executePendingBindings()
            }
        }

    }
}