package sg.kickbates.customer.ui.history.cash_out

import androidx.databinding.BaseObservable
import androidx.databinding.Bindable
import androidx.databinding.ObservableField
import sg.kickbates.data.api.models.cash_out.ResponseCashOutHistory

/**
 * Created by simx on 02,January,2020
 */
class ItemCashOutHistoryVM(data: ResponseCashOutHistory.DataItem?):BaseObservable() {
    @Bindable var status = ObservableField(data?.status())
    @Bindable var amount = ObservableField(data?.amount())
    @Bindable var bank = ObservableField(data?.kickBanks?.bankName)
    @Bindable var accountName = ObservableField(data?.requestAccountName)
    @Bindable var accountNumber = ObservableField(data?.requestAccountNumber)
    @Bindable var date = ObservableField(data?.createdAt?.dateConverter())
}