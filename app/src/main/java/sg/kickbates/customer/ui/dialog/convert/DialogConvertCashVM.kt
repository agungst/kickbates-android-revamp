package sg.kickbates.customer.ui.dialog.convert

import androidx.databinding.BaseObservable
import androidx.databinding.Bindable
import androidx.databinding.ObservableField
import androidx.lifecycle.MutableLiveData
import kotlinx.coroutines.CoroutineExceptionHandler
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.SupervisorJob
import kotlinx.coroutines.launch
import retrofit2.HttpException
import sg.kickbates.data.BuildConfig
import sg.kickbates.data.api.Api
import sg.kickbates.data.api.models.ResponseCorverKick
import sg.kickbates.data.helper.Pref

/**
 * Created by simx on 11,August,2019
 */
class DialogConvertCashVM(private var pref: Pref): BaseObservable() {
    private var job = SupervisorJob()
    @Bindable var loading = ObservableField<Boolean>()
    @Bindable var error = MutableLiveData<String>()
    @Bindable var cash = MutableLiveData<ResponseCorverKick.Data>()

    fun convertCash(amountTransfer: Double?) {
        loading.set(true)
        try {
            CoroutineScope(CoroutineExceptionHandler { _, throwable -> run {
                job = SupervisorJob()
                throwable.printStackTrace()
                updateMsg(BuildConfig.ERROR_MSG)
            } }).launch {
                val res = Api.Factory.create(BuildConfig.BASE_URL).convertCashAsync(pref.token, pref.usersId, amountTransfer, -amountTransfer!!,3).await()
                res.let {
                    when (it.status) {
                        200 -> {if (res.data == null) cash.postValue(null)  else cash.postValue(res.data)}
                        else -> {updateMsg(BuildConfig.ERROR_MSG)}
                    }
                }

            }
        }catch (e: HttpException){
            e.printStackTrace()
            updateMsg(BuildConfig.ERROR_MSG)
        }
    }
    private fun updateMsg(msg:String?){
        loading.set(false)
        error.postValue(msg)
    }

}