package sg.kickbates.customer.ui.main.pay

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log

import android.view.MenuItem
import android.view.View
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import com.google.android.material.textfield.TextInputEditText
import sg.kickbates.customer.R
import sg.kickbates.customer.databinding.PayActivityBinding
import sg.kickbates.customer.helper.DialogHelper
import sg.kickbates.customer.helper.SnackBarHelper
import sg.kickbates.customer.ui.payment.channels.PaymentChannelActivity
import sg.kickbates.customer.ui.voucher.VouchersActivity
import sg.kickbates.data.api.models.ResponseDeals
import sg.kickbates.data.api.models.ResponseMerchants
import sg.kickbates.data.api.models.card.ResponseCustomerCards
import sg.kickbates.data.api.models.payment.*
import sg.kickbates.data.api.models.voucher.ResponseVouchers
import sg.kickbates.data.helper.CommonTools
import sg.kickbates.data.helper.Pref
import sg.kickbates.data.helper.ProductType
import java.lang.NumberFormatException

class PayActivity : AppCompatActivity() {
    private lateinit var binding:PayActivityBinding
    private lateinit var vm:PayVM
    private var promoCode:String? = null
    private var promoPercentage:Double = 0.0

    private var productId:Int? = null
    private var productName:String? = null
    private var productImage:String? = null
    private var productType:ProductType? = null
    private var voucer:ResponseVouchers.DataItem? = null
    private var dataCard: ResponseCustomerCards.DataItem.DataCardStatements? = null
    private var type:Int? = null
    private var validInput = true
    private var amount:Double? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this,R.layout.pay_activity)
        vm = PayVM(Pref(this))
        binding.lifecycleOwner = this
        binding.payVm = vm
        setSupportActionBar(binding.toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        binding.tvTitleToolbar.text = "Pay"

        if (intent.hasExtra(KEY_TYPE)){
            type = intent.extras?.getInt(KEY_TYPE)

            when (type) {
                TYPE_DEAL -> {
                    vm.swChecked.set(true)
                    productType = ProductType.DEALS
                    val dataDeal:ResponseDeals.DataItem? = intent.extras?.getParcelable(KEY_DATA)
                    productId = dataDeal?.id
                    productImage = dataDeal?.image()
                    productName = dataDeal?.dealName
                    binding.etPayment.setText("${dataDeal?.dealPrice}")
                    binding.lytCoupon.visibility = View.GONE
                }
                TYPE_MERCHANT -> {
                    vm.swChecked.set(true)
                    productType = ProductType.MERCHANT
                    val dataMerchants:ResponseMerchants.DataItem? = intent.extras?.getParcelable(KEY_DATA)
                    productId = dataMerchants?.id
                    productImage = dataMerchants?.image()
                    productName = dataMerchants?.merchantName
                    binding.etPayment.isEnabled = true
                }
                TYPE_CARD -> {
                    binding.swUseKick.isChecked = false
                    binding.swUseKick.isClickable = false
                    vm.swChecked.set(false)
                    binding.lytCoupon.visibility  = View.GONE
                    productType = ProductType.CARD
                    dataCard = intent.extras?.getParcelable(KEY_DATA)
                    binding.etPayment.setText(dataCard?.amount())
                    binding.etPayment.isEnabled = false
                    binding.tvAmountPayable.text = dataCard?.amountString()
                    productId = dataCard?.id
                    productImage = ""
                    productName = ""
                }
                else -> { }
            }
        }

        /**
         * Handle Switch
         */
        vm.getSummaryKick()

        handleOnChecked(true)

        binding.swUseKick.setOnCheckedChangeListener { _, isChecked -> run {
            vm.swChecked.set(isChecked)
            if (isChecked) binding.lytKickUsed.error = null
            if (isChecked)vm.getSummaryKick()
            handleOnChecked(isChecked)
        } }

        /**
         * Data Summary Kick
         */
        vm.availableKick.observe(this, Observer { data -> run {
            if (data != null){
                if (vm.availableKick.value?.kickBalance!! > 0){
                    val kickBalance:Double? = data.kickBalance
                    val balance:String? = kickBalance?.let { CommonTools.formatDollarCurrency(it) }
                    binding.tvKickAvailable.text = balance
                    binding.etKickUsed.isEnabled = true
                    handleOnChecked(binding.swUseKick.isChecked)
                }else {
                    val kickBalance:Double? = data.kickBalance
                    val balance:String? = kickBalance?.let { CommonTools.formatDollarCurrency(it) }
                    binding.tvKickAvailable.text = balance
                    binding.etKickUsed.isEnabled = false
                }
            }
        } })

        calculateKickUsed()

        binding.tvTitlePromoCode.setOnClickListener {
            startActivityForResult(Intent(this, VouchersActivity::class.java),VouchersActivity.RC_VIEW_PROMO)
        }
        binding.tvSelectChannel.setOnClickListener {
            val intent = Intent(this, PaymentChannelActivity::class.java)
            startActivityForResult(intent,PaymentChannelActivity.KEY_VIEW_CHANNEL)
        }
        binding.btnPay.setOnClickListener {
            if (validInput){
                if (binding.etPayment.text.toString().isNotEmpty()){
                    val requestInvoice = InvoiceHelper.generateInvoice(productType, productId, price(), availableKick(), voucer?.id, productImage, productImage)
                    when (productType) {
                        ProductType.MERCHANT -> vm.generateInvoiceMerchant(requestInvoice as RequestInvoiceMerchant, amount!! <= 0.0)
                        ProductType.DEALS -> vm.generateInvoiceDeal(requestInvoice as RequestInvoiceDeal, amount!! <= 0.0)
                        ProductType.CARD -> vm.generateInvoiceCard(requestInvoice as RequestInvoiceCard)
                        else -> {}
                    }
                } else {
                    SnackBarHelper.showSnackBarError("Invalid pay amount", binding.rootView)

                }
            } else {
                SnackBarHelper.showSnackBarError("Invalid pay amount", binding.rootView)
            }


        }

        vm.invoiceDeal.observe(this, Observer { data -> run {
            when (type) {
                TYPE_CARD -> if (data != null)showPaymentChannel(data.id, dataCard)
                else -> if (data != null)showPaymentChannel(data.id, null)
            }
        } })

        vm.invoiceMerchant.observe(this, { data ->
            if (data != null){
                showPaymentChannel(data.id, null)
            }
        })

        vm.error.observe(this, Observer { msg -> run {
            SnackBarHelper.showSnackBarError(msg, binding.rootView)
        } })
        vm.fullPayment.observe(this, Observer { invoice -> run {
            DialogHelper.showDialogSuccessPayment(this@PayActivity, type, invoice, object : DialogHelper.OnDialogSuccessListener {
                override fun onOk() {
                    setResult(Activity.RESULT_OK)
                    finish()
                }
            })
        } })

        binding.tvRemovePromoCode.setOnClickListener {
            promoPercentage = 0.0
            voucer = null
            voucer?.id = null
            binding.lytPromoCode.visibility = View.GONE
            calculateKickUsed()
        }


        binding.etPayment.addTextChangedListener(object : TextWatcher{
            override fun afterTextChanged(s: Editable?) {
                if (s.toString().isNotEmpty()){
                    calculateKickUsed()
                } else {
                    calculateKickUsed()
                }

            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}
            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {}
        })
    }

    private fun showPaymentChannel(id:Int?,dataCard:ResponseCustomerCards.DataItem.DataCardStatements?){
        val intent = Intent(this, PaymentChannelActivity::class.java)
        intent.putExtra(PaymentChannelActivity.KEY_DATA, id)
        when (productType) {
            ProductType.DEALS -> { intent.putExtra(PaymentChannelActivity.KEY_TYPE, PaymentChannelActivity.KEY_TYPE_DEAL) }
            ProductType.MERCHANT -> { intent.putExtra(PaymentChannelActivity.KEY_TYPE, PaymentChannelActivity.KEY_TYPE_MERCHANT) }
            ProductType.CARD -> {
                intent.putExtra(PaymentChannelActivity.KEY_TYPE, PaymentChannelActivity.KEY_TYPE_CARD)
                intent.putExtra(PaymentChannelActivity.KEY_DATA_STATEMENT, dataCard)
            }
        }
        startActivityForResult(intent,PaymentChannelActivity.KEY_VIEW_CHANNEL)
    }

    private fun handleOnChecked(checked: Boolean) {
        if (checked){
            /**
             * Handle EditText Available Kick
             */
            binding.etKickUsed.addTextChangedListener(object : TextWatcher{
                override fun afterTextChanged(s: Editable?) {

                    if (vm.swChecked.get()!! && !s.isNullOrBlank()){
                        val kickCanUsed:Double? = if (vm.availableKick.value?.kickBalance!! > 0){
                            promoValue(promoPercentage)?.let { vm.availableKick.value?.kickBalance?.minus(it)
                            }
                        }else {
                            0.00
                        }
                        val inputPrice = if (castToDouble(binding.etKickUsed)) { if (binding.etKickUsed.text.isNullOrEmpty() ){0.0}else {binding.etKickUsed.text.toString().toDouble()} } else 0.0
                        if (inputPrice > kickCanUsed!!){
                            binding.lytKickUsed.error = "Your kick$ balance is insufficient"
                            validInput = false
                            calculateKickUsed()
                        }else{
                            validInput = true
                            binding.lytKickUsed.error = null
                            calculateKickUsed()
                        }

                        if (inputPrice > price()){
                            binding.lytKickUsed.error = "Please enter a correct value"
                            validInput = false
                            calculateKickUsed()
                        } else{
                            validInput = true
                            binding.lytKickUsed.error = null
                            calculateKickUsed()
                        }

                    }else {
                        validInput = true
                        calculateKickUsed()
                    }
                }

                override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}

                override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {

                }
            })
        }else {
            binding.etKickUsed.setText("")
            calculateKickUsed()
        }

    }

    private fun availableKick():Double{
        return if (castToDouble(binding.etKickUsed)) {
            if (binding.etKickUsed.text.isNullOrEmpty() ){0.0}else {binding.etKickUsed.text.toString().toDouble()}
        }
        else 0.0

    }
    private fun price():Double{
        return if (castToDouble(binding.etPayment)) {
            if (binding.etPayment.text.isNullOrEmpty() ){0.0}else {binding.etPayment.text.toString().toDouble()}
        }
        else 0.0

    }
    /**
     * Handle Payable Amount change
     */
    @SuppressLint("SetTextI18n")
    private fun calculateKickUsed() {
        val p = price()
        val kickUser = availableKick()
        val percentagePromo = promoPercentage / 100
        val priceWithVoucher = price() * percentagePromo
        val kickWithPriceWithVoucher = kickUser + priceWithVoucher
        val amountMustPay = if (kickWithPriceWithVoucher > p){ 0.0 } else { p - kickWithPriceWithVoucher }
        //val amountMustPay = p - (kickUser + priceWithVoucher)
        val stringPayable = amountMustPay.let { CommonTools.formatDollarCurrency(it) }
        if (type == TYPE_MERCHANT){
            if (voucer != null) vm.calculate(p, kickUser, voucer?.promoAmount(), voucer?.promoPercentage(), productId)
            else vm.calculate(p, kickUser, 0.0, 0.0, productId)
            vm.calculatePromo.observe(this, Observer { data ->
                amount = data?.payableAmount
                binding.tvAmountPayable.text = data?.payableAmount?.let { CommonTools.formatDollarCurrency(it) }
                binding.btnPay.text = "Pay ${data?.payableAmount?.let { CommonTools.formatDollarCurrency(it) }}"
            })
        } else {
            if (stringPayable == null){
                amount = amountMustPay
                binding.tvAmountPayable.text = CommonTools.formatDollarCurrency(0.0)
                binding.btnPay.text = "Pay  ${CommonTools.formatDollarCurrency(0.0)}"
            }else {
                amount = amountMustPay
                binding.tvAmountPayable.text = stringPayable
                binding.btnPay.text = "Pay  $stringPayable"
            }
        }

    }

    private fun promoValue(percent: Double?): Double? {
        return  percent?.div(100)

    }

    @SuppressLint("SetTextI18n")
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        when (requestCode) {
            PaymentChannelActivity.KEY_VIEW_CHANNEL -> {
                when (resultCode) {
                    Activity.RESULT_OK -> resultOk()
                    else -> { }
                }
            }
            VouchersActivity.RC_VIEW_PROMO -> {
                when (resultCode) {
                    Activity.RESULT_OK -> {
                        if (amount == 0.0){
                            SnackBarHelper.showSnackBarError("Voucher can't be use because amount payable already $amount", binding.rootView, binding.btnPay, object : SnackBarHelper.OnSnackBarHelperListener {
                                override fun onError() {

                                }
                            })
                        }else {
                            val dataVoucher: ResponseVouchers.DataItem? = data?.extras?.getParcelable(VouchersActivity.KEY_DATA)
                            if (dataVoucher != null){
                                if (dataVoucher.all()){
                                    /**
                                     * For all merchant
                                     */
                                    voucer = dataVoucher
                                    Toast.makeText(this, "Promo Code ${voucer?.promocodeCode} has applied", Toast.LENGTH_SHORT).show()
                                    promoCode = voucer?.promocodeCode
                                    if (voucer?.promoCodeBenefit != null){
                                        promoPercentage = voucer?.promoCodeBenefit!!
                                    }
                                    binding.lytPromoCode.visibility = View.VISIBLE
                                    binding.tvPromoCode.text = voucer?.promoName()
                                    calculateKickUsed()
                                }else {
                                    /**
                                     * For current merchant
                                     */
                                    if (dataVoucher.promocodeMerchantId == productId){
                                        voucer = dataVoucher
                                        Toast.makeText(this, "Promo Code ${voucer?.promocodeCode} has applied", Toast.LENGTH_SHORT).show()
                                        promoCode = voucer?.promocodeCode
                                        if (voucer?.promoCodeBenefit != null){
                                            promoPercentage = voucer?.promoCodeBenefit!!
                                        }

                                        binding.lytPromoCode.visibility = View.VISIBLE
                                        binding.tvPromoCode.text = voucer?.promoName()
                                        calculateKickUsed()
                                    } else {
                                        Toast.makeText(this, "Promo Code can't be use", Toast.LENGTH_SHORT).show()
                                    }
                                }

                            } else Toast.makeText(this, "Promo Code can't be use", Toast.LENGTH_SHORT).show()
                        }

                    }
                    else -> { }
                }
            }
            else -> {
            }
        }

        super.onActivityResult(requestCode, resultCode, data)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> {
                resultCancel()
            }
            else -> {
            }
        }
        return super.onOptionsItemSelected(item)
    }
    fun resultOk(){
        setResult(Activity.RESULT_OK)
        finish()
    }
    fun resultCancel(){
        setResult(Activity.RESULT_CANCELED)
        finish()
    }

    fun castToDouble(input:TextInputEditText): Boolean {
        var valid:Boolean = false
        return if (!input.text.isNullOrEmpty()){
            try {
                input.text.toString().toDouble()
                input.error = null
                valid  = true
            } catch (e:NumberFormatException){
                input.error = e.message
                valid  = false
            }
            valid
        }else {
            valid
        }
    }

    companion object {
        const val RC_PAYMENT = 101
        const val KEY_TYPE = "type"
        const val KEY_DATA = "key_data"
        const val TYPE_DEAL = 1
        const val TYPE_MERCHANT = 2
        const val TYPE_CARD = 3


    }
}
