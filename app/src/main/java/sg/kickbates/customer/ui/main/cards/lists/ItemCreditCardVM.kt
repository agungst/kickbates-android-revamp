package sg.kickbates.customer.ui.main.cards.lists

import androidx.databinding.BaseObservable
import androidx.databinding.Bindable
import androidx.databinding.ObservableField
import sg.kickbates.data.api.models.card.ResponseCustomerCards

/**
 * Created by simx on 31,July,2019
 */
class ItemCreditCardVM(data: ResponseCustomerCards.DataItem):BaseObservable() {
    @Bindable var number = ObservableField("**** **** **** ${data.cardNo}")
    @Bindable var balance = ObservableField("Outstanding: $ ${data.balance()}")
    @Bindable var statementDate = ObservableField(data.statementDate())
    @Bindable var dueDate = ObservableField(data.dueDate())
    @Bindable var dueAmount = ObservableField(data.dueAmount())
    @Bindable var paid = ObservableField(data.paidStatusName())
    @Bindable var paidVisible = ObservableField(data.paidStatusNameVisible())
    @Bindable var image = ObservableField(data.kickBanks?.bankIcon())
}