package sg.kickbates.customer.ui.history.cash_back

import androidx.databinding.BaseObservable
import androidx.databinding.Bindable
import androidx.databinding.ObservableField
import androidx.lifecycle.LiveData
import androidx.lifecycle.Transformations
import androidx.paging.LivePagedListBuilder
import androidx.paging.PagedList
import sg.kickbates.data.api.models.cash_back.ResponseCashBackHistory

import sg.kickbates.data.helper.Pref
import sg.kickbates.data.paging.CashBackHistoryDataSource
import sg.kickbates.data.paging.CashBackHistoryDataSourceFactory


/**
 * Created by simx on 02,January,2020
 */
class CashBackHistoryVM(private var pref: Pref):BaseObservable() {
    private val config = PagedList.Config.Builder()
        .setEnablePlaceholders(true)
        .build()

    private var sourceFactory: CashBackHistoryDataSourceFactory = CashBackHistoryDataSourceFactory()
    var history: LiveData<PagedList<ResponseCashBackHistory.DataItem>>
    var errorState: LiveData<String>? = null
    var loadingState: LiveData<Boolean>? = null
    var emptyState: LiveData<Boolean>? = null

    @Bindable var loading = ObservableField<Boolean>()
    @Bindable var empty = ObservableField<Boolean>()
    init {
        history = LivePagedListBuilder(sourceFactory, config).build()
        errorState = Transformations.switchMap<CashBackHistoryDataSource,String>(sourceFactory.history, CashBackHistoryDataSource::error)
        loadingState = Transformations.switchMap<CashBackHistoryDataSource,Boolean>(sourceFactory.history, CashBackHistoryDataSource::loading)
        emptyState = Transformations.switchMap<CashBackHistoryDataSource,Boolean>(sourceFactory.history, CashBackHistoryDataSource::empty)
    }

    fun dataHistory(){
        sourceFactory.query(pref.token, pref.usersId)
        sourceFactory.history.value?.invalidate()
    }
}