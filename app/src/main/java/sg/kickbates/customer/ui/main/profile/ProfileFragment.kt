package sg.kickbates.customer.ui.main.profile


import android.annotation.SuppressLint
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import com.facebook.login.LoginManager
import sg.kickbates.customer.BuildConfig

import sg.kickbates.customer.R
import sg.kickbates.customer.databinding.ProfileFragmentBinding
import sg.kickbates.customer.helper.AuthHelper
import sg.kickbates.customer.helper.SnackBarHelper
import sg.kickbates.customer.ui.auth.AuthActivity
import sg.kickbates.customer.ui.cms.CmsWebViewActivity
import sg.kickbates.customer.ui.main.MainActivity
import sg.kickbates.customer.ui.notification.NotificationHistoryActivity
import sg.kickbates.customer.ui.payment.channels.PaymentChannelActivity
import sg.kickbates.customer.ui.payment.history.PaymentHistoryActivity
import sg.kickbates.customer.ui.premium.PremiumPageActivity
import sg.kickbates.data.helper.Pref



/**
 * A simple [Fragment] subclass.
 *
 */
class ProfileFragment : Fragment() {
    private lateinit var binding:ProfileFragmentBinding
    private lateinit var vm:ProfileFragmentVM
    private lateinit  var pref:Pref
    private var isPremiumAccount:Boolean? = null

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil.inflate(inflater,R.layout.profile_fragment, container,false)
        pref = Pref(this.context!!)
        vm = ProfileFragmentVM(pref)
        binding.lifecycleOwner = this
        binding.profilVm = vm
        vm.getMe()
        return binding.root
    }

    @SuppressLint("CommitPrefEdits")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.tvVersion.text = BuildConfig.VERSION_NAME
        vm.user.observe(this, Observer { data -> run {
            binding.tvName.text = data.name
            binding.tvEmail.text = data.email
            vm.canPurchase.set(data.canSubscribePremium())
            if (data.isPremiumAccount()!!){
                binding.tvUpgrade.text = data.premiumAccount()
                pref.premiumAccount = data.isPremiumAccount()!!
                isPremiumAccount = data.isPremiumAccount()
            }else {
                binding.tvUpgrade.text = data.premiumAccount()
                pref.premiumAccount = data.isPremiumAccount()!!
                isPremiumAccount = data.isPremiumAccount()
            }
        } })
        binding.lytHistory.setOnClickListener {
            PaymentHistoryActivity.start(this.context!!)
        }
        binding.btnLogout.setOnClickListener {
            var googleClient = AuthHelper.getGoogleClient(this.activity!!)
            googleClient?.signOut()?.addOnCompleteListener { task -> run {
                if (task.result == null){
                    LoginManager.getInstance().logOut()
                    googleClient.revokeAccess()
                    pref.clear()
                    gotoLogin()
                } else {
                    if(task.isSuccessful) gotoLogin()
                }
            } }?.addOnFailureListener { exception -> run {


            } }
        }
        binding.tvEdit.setOnClickListener {
            var intent = Intent(this.context!!, UpdateProfileActivity::class.java)
            startActivityForResult(intent, UpdateProfileActivity.RC_UPDATE)
        }


        vm.invoice.observe(this, Observer { data -> run {
            var intent = Intent(this@ProfileFragment.context, PaymentChannelActivity::class.java)
            intent.putExtra(PaymentChannelActivity.KEY_DATA, data.id)
            startActivityForResult(intent, PaymentChannelActivity.KEY_VIEW_CHANNEL)
        } })
        binding.lytUpgrade.setOnClickListener {
            PremiumPageActivity.start(this.context!!, vm.user.value)
        }
        binding.lytNotification.setOnClickListener {
            NotificationHistoryActivity.start(this.context!!)
        }
        binding.lytFbGooglePrivacyPolicy.setOnClickListener {
            CmsWebViewActivity.start(this.context!!, CmsWebViewActivity.CMS_PRIVACY_GOOGLE_FACEBOOK)
        }
        binding.lytFaq.setOnClickListener {
            CmsWebViewActivity.start(this.context!!, CmsWebViewActivity.CMS_FAQ)
        }
        binding.lytTerm.setOnClickListener {
            CmsWebViewActivity.start(this.context!!, CmsWebViewActivity.CMS_TERMS)
        }
        binding.lytPrivacyPolicy.setOnClickListener {
            CmsWebViewActivity.start(this.context!!, CmsWebViewActivity.CMS_PRIVACY_POLICY)
        }
        binding.lytContactUs.setOnClickListener {
            CmsWebViewActivity.start(this.context!!, CmsWebViewActivity.CMS_CONTACT)
        }
        binding.lytHowWeKick.setOnClickListener {
            CmsWebViewActivity.start(this.context!!, CmsWebViewActivity.CMS_HOW_WE)
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        when (requestCode) {
            UpdateProfileActivity.RC_UPDATE -> {
                when (resultCode) {
                    Activity.RESULT_OK -> vm.getMe()
                    else -> {
                    }
                }
            }
            PaymentChannelActivity.KEY_VIEW_CHANNEL -> {
                when (resultCode) {
                    Activity.RESULT_OK -> vm.getMe()
                    else -> {
                    }
                }
            }
            else -> {
            }
        }
        super.onActivityResult(requestCode, resultCode, data)
    }

    private fun gotoLogin(){
        AuthActivity.start(this.context!!)
        (activity as MainActivity).finish()
    }

    companion object {
        const val KEY_DATA = "key_data"
        fun instance() : ProfileFragment{
            val bundle =  Bundle()
            val fragment = ProfileFragment()
            fragment.arguments = bundle
            return fragment
        }
    }

}
