package sg.kickbates.customer.ui.dialog.account_card

import androidx.databinding.BaseObservable
import androidx.databinding.Bindable
import androidx.databinding.ObservableField
import sg.kickbates.data.api.models.card.ResponseAccountCard

/**
 * Created by simx on 31,January,2020
 */
class ItemAccountBankVM(data: ResponseAccountCard.DataItem):BaseObservable() {
    @Bindable var name = ObservableField("${data.accountNo} - ${data.accountName}")
}