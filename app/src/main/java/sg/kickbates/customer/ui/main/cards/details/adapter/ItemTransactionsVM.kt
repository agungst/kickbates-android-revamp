package sg.kickbates.customer.ui.main.cards.details.adapter

import androidx.databinding.BaseObservable
import androidx.databinding.Bindable
import androidx.databinding.ObservableField
import sg.kickbates.data.api.models.payment.DetailInvoice

/**
 * Created by simx on 11,August,2019
 */
class ItemTransactionsVM(data : DetailInvoice?): BaseObservable() {
    @Bindable var payBy = ObservableField(data?.paidBy())
    @Bindable var invoiceNumber = ObservableField("${data?.painDate()} - Invoice no ${data?.invoiceNumber}")
    @Bindable var amount = ObservableField(data?.amountPayable())
    @Bindable var amountPayable = ObservableField(data?.invoicePayableString())
    @Bindable var amountEarning = ObservableField(data?.earningAmount())
    @Bindable var promoCode = ObservableField(data?.promoCode())
    @Bindable var kickUse = ObservableField(data?.kickUsed())
    @Bindable var promoName = ObservableField(data?.dealPromoName())
    @Bindable var earningStatusNAme = ObservableField(data?.earningStaus())
}