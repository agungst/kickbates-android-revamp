package sg.kickbates.customer.ui.main.cards.details

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import sg.kickbates.customer.ui.main.cards.details.billed.BilledFragment
import sg.kickbates.customer.ui.main.cards.details.history.TransactionsCardHistoryFragment
import sg.kickbates.customer.ui.main.cards.details.unbilled.UnBilledTransactionsFragment
import sg.kickbates.data.api.models.card.ResponseCustomerCards

/**
 * Created by simx on 06,August,2019
 */
class AdapterPageDetailCards(fm: FragmentManager, private var  data: ResponseCustomerCards.DataItem?): FragmentPagerAdapter(fm, BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT) {
    /**
     * Return the Fragment associated with a specified position.
     */
    override fun getItem(position: Int): Fragment {
       return when (position) {
            0 -> BilledFragment.instance(data)
            1 -> UnBilledTransactionsFragment.instance(data)
            2 -> TransactionsCardHistoryFragment.instance(data)
            else -> BilledFragment.instance(data)
        }
    }

    /**
     * Return the number of views available.
     */
    override fun getCount(): Int {
        return 3
    }

    override fun getPageTitle(position: Int): CharSequence? {
        return when (position) {
          0 -> "Billed"
          1 -> "Unbilled"
          2 -> "Paid History"
          else -> ""
      }

    }
}