package sg.kickbates.customer.ui.deals.details

import android.app.Activity
import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.MenuItem
import androidx.databinding.DataBindingUtil
import coil.api.load
import sg.kickbates.customer.R
import sg.kickbates.customer.databinding.DealsDetailsActivityBinding
import sg.kickbates.customer.ui.main.pay.PayActivity
import sg.kickbates.data.api.models.ResponseDeals
import sg.kickbates.data.helper.ProductType

class DealsDetailsActivity : AppCompatActivity() {
    private lateinit var binding:DealsDetailsActivityBinding
    private lateinit var vm:DealsDetailsVM
    private var dataDeals: ResponseDeals.DataItem? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this,R.layout.deals_details_activity)
        vm = DealsDetailsVM()
        binding.lifecycleOwner = this
        binding.dealsDetailVm = vm
        setSupportActionBar(binding.toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        binding.tvTitleToolbar.text = "Details"
        if (!intent.extras?.isEmpty!!)intent?.extras?.getParcelable<ResponseDeals.DataItem>(KEY_DATA).let {data -> run {
            dataDeals = data
            binding.tvName.text = data?.dealName
            binding.tvTitleToolbar.text = data?.dealName
            binding.tvDesc.text = data?.dealDescription
            vm.noBanner.set(dataDeals?.showRemark())
            dataDeals?.till().let { binding.tvValidTill.text = it }
            dataDeals?.remark().let { binding.tvDis.text = it }
            binding.imageDeal.load(data?.image()){
                placeholder(R.drawable.placeholder)
                error(R.drawable.placeholder)
            }
        }
        }
        binding.btnBuy.setOnClickListener {
            val intent = Intent(this, PayActivity::class.java)
            intent.putExtra(PayActivity.KEY_TYPE, PayActivity.TYPE_DEAL)
            intent.putExtra(PayActivity.KEY_DATA, dataDeals)
            startActivityForResult(intent, PayActivity.RC_PAYMENT)

        }

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        when (requestCode) {
            PayActivity.RC_PAYMENT -> {
                when (resultCode) {
                    Activity.RESULT_OK -> onBackPressed()
                    else -> {
                    }
                }
            }
            else -> {
            }
        }
        super.onActivityResult(requestCode, resultCode, data)
    }
    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> {
                onBackPressed()
            }
            else -> {
            }
        }
        return super.onOptionsItemSelected(item)
    }
    companion object {
        const val KEY_DATA = "key_data"
        fun start(context: Context, data: ResponseDeals.DataItem?){
            val intent = Intent(context, DealsDetailsActivity::class.java)
            intent.putExtra(KEY_DATA, data)
            intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP
            context.startActivity(intent)
        }
    }
}
