package sg.kickbates.customer.ui.history.cash_back

import androidx.databinding.BaseObservable
import androidx.databinding.Bindable
import androidx.databinding.ObservableField
import sg.kickbates.data.api.models.cash_back.ResponseCashBackHistory

/**
 * Created by simx on 02,January,2020
 */
class ItemCashBackHistoryVM(data: ResponseCashBackHistory.DataItem?) : BaseObservable(){
    @Bindable var status = ObservableField(data?.status())
    @Bindable var store = ObservableField(data?.storeName())
    @Bindable var spendAmount = ObservableField(data?.spendAmount())
    @Bindable var kickUsed = ObservableField(data?.kickUsed())
    @Bindable var kickEarn = ObservableField(data?.kickEarn())
    @Bindable var cashEarn = ObservableField(data?.cashEarn())
    @Bindable var date = ObservableField(data?.createdAt?.dateConverter())

    @Bindable var storeVisible = ObservableField(data?.storeNameVisible())
    @Bindable var spendAmountVisible = ObservableField(data?.spendAmountVisible())
    @Bindable var kickUsedVisible = ObservableField(data?.kickUsedVisible())
    @Bindable var kickEarnVisible = ObservableField(data?.kickEarnVisible())
    @Bindable var cashEarnVisible = ObservableField(data?.cashEarnVisible())

    @Bindable var kickEarnTitle = ObservableField(data?.kickEarnTitle())
    @Bindable var cashEarnTitle = ObservableField(data?.cashEarnTitle())

}