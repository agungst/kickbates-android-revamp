package sg.kickbates.customer.ui.cash_out

import androidx.databinding.BaseObservable
import androidx.databinding.Bindable
import androidx.databinding.ObservableField
import androidx.lifecycle.MutableLiveData
import kotlinx.coroutines.CoroutineExceptionHandler
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.SupervisorJob
import kotlinx.coroutines.launch
import retrofit2.HttpException
import sg.kickbates.data.BuildConfig
import sg.kickbates.data.api.Api
import sg.kickbates.data.api.models.RequestCashOut
import sg.kickbates.data.api.models.cash_out.ResponseCashOut
import sg.kickbates.data.helper.Pref

/**
 * Created by simx on 02,January,2020
 */
class CashOutVM(private var pref: Pref): BaseObservable() {

    private var job = SupervisorJob()
    @Bindable var loading = ObservableField<Boolean> ()
    @Bindable var error = MutableLiveData<String>()
    @Bindable var cashOut = MutableLiveData<ResponseCashOut.Data>()

    fun processCashOut(requestCashOut: RequestCashOut?) {
        requestCashOut?.usersId = pref.usersId
        loading.set(true)
        try {
            CoroutineScope(CoroutineExceptionHandler { _, throwable -> run {
                job = SupervisorJob()
                throwable.printStackTrace()
                updateMsg(throwable.message)
                cashOut.postValue(null)
            } }).launch {
                val res  = Api.Factory.create(BuildConfig.BASE_URL).cashOutStoreAsync(pref.token, requestCashOut).await()
                cashOut.postValue(res.data)
                loading.set(false)
            }
        }catch (e: HttpException){
            e.printStackTrace()
            updateMsg(e.message)
        }
    }

    private fun updateMsg(msg:String?){
        loading.set(false)
        error.postValue(BuildConfig.ERROR_MSG)
    }
}