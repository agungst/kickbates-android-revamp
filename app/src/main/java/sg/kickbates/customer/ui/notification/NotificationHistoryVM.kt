package sg.kickbates.customer.ui.notification


import androidx.databinding.BaseObservable
import androidx.databinding.Bindable
import androidx.databinding.ObservableField
import androidx.lifecycle.MutableLiveData
import sg.kickbates.data.api.models.notification.ResponseNotificationHistory
import sg.kickbates.data.helper.Pref
import kotlinx.coroutines.CoroutineExceptionHandler
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.SupervisorJob
import kotlinx.coroutines.launch
import retrofit2.HttpException
import sg.kickbates.data.BuildConfig
import sg.kickbates.data.api.Api

/**
 * Created by simx on 14,April,2020
 */
class NotificationHistoryVM(private var pref:Pref): BaseObservable() {
    private var job = SupervisorJob()
    @Bindable var loading = ObservableField<Boolean>()
    @Bindable var empty = ObservableField<Boolean>()
    @Bindable var error = ObservableField<String>()
    @Bindable var notifications = MutableLiveData<List<ResponseNotificationHistory.DataNotification>>()

    fun getDataNotification(){
        loading.set(true)
        try {
            CoroutineScope(CoroutineExceptionHandler { _, throwable -> run {
                job = SupervisorJob()
                throwable.printStackTrace()
                loading.set(false)
                error.set(throwable.message)
            } }).launch {
                val res  = Api.Factory.create(BuildConfig.BASE_URL).notificationsAsync(pref.token, pref.usersId).await()
                loading.set(false)
                empty.set(res.data?.isEmpty())
                notifications.postValue(res.data)

            }
        }catch (e: HttpException){
            e.printStackTrace()
            loading.set(false)
            error.set(e.message)
        }
    }
}