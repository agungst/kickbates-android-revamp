package sg.kickbates.customer.ui.main.cards.details.unbilled

import androidx.databinding.BaseObservable
import androidx.databinding.Bindable
import androidx.databinding.ObservableField
import androidx.lifecycle.MutableLiveData
import sg.kickbates.data.api.models.card.ResponseCustomerCards

/**
 * Created by simx on 30,July,2019
 */
class UnBilledTransactionsFragmentVM: BaseObservable() {
    @Bindable var transactions = MutableLiveData<List<ResponseCustomerCards.DataItem.DataCardTransactions>>()
    @Bindable var empty = ObservableField<Boolean>()
}