package sg.kickbates.customer.ui.main.profile

import android.app.Activity
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.MenuItem
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import sg.kickbates.customer.R
import sg.kickbates.customer.databinding.UpdateProfileActivityBinding
import sg.kickbates.customer.ui.otp.OtpActivity
import sg.kickbates.data.api.models.RequestUpdateProfile
import sg.kickbates.data.helper.Pref

class UpdateProfileActivity : AppCompatActivity() {
    private lateinit var binding:UpdateProfileActivityBinding
    private lateinit var vm:UpdateProfileVM
    private var oldPhone:String? = null
    private var requestUpdateProfile: RequestUpdateProfile? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this,R.layout.update_profile_activity)
        vm = UpdateProfileVM(Pref(this))
        binding.lifecycleOwner = this
        binding.updateProfileVm = vm
        setSupportActionBar(binding.toolbar)
        requestUpdateProfile = RequestUpdateProfile()
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        binding.tvTitleToolbar.text = "Profile Info"
        vm.getProfileUser()
        vm.user.observe(this, { data -> run {
            if (data != null){
                oldPhone = data.phone
                binding.etEmail.setText(data.email)
                binding.etName.setText(data.name)
                binding.etPhone.setText(data.phone)
                binding.etMembershipName.setText(data.ffpName)
                binding.etMembershipNumber.setText(data.ffpNo)
                requestUpdateProfile?.email = data.email
            }
        } })

        binding.btnUpdate.setOnClickListener {
            val valid = oldPhone != binding.etPhone.text.toString()
            if (valid){
                startActivityForResult(OtpActivity.instance(this, binding.etPhone.text.toString()), OtpActivity.RC_OTP)
            }else update()
        }
        binding.btnUpdate.setOnLongClickListener {
            update()
            true }
        vm.sussessUpdate.observe(this, { isSuccess -> run {
            if (isSuccess){
                setResult(Activity.RESULT_OK)
                finish()
            }else onBackPressed()
        } })
    }

    private fun update(){
        requestUpdateProfile?.name = binding.etName.text.toString()
        requestUpdateProfile?.phone = binding.etPhone.text.toString()
        requestUpdateProfile?.ffpName = binding.etMembershipName.text.toString()
        requestUpdateProfile?.ffpNo = binding.etMembershipNumber.text.toString()
        vm.updateUser(requestUpdateProfile)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode == OtpActivity.RC_OTP && resultCode == Activity.RESULT_OK) {
            update()
        }else vm.error.postValue("Failed to verify phone")
        super.onActivityResult(requestCode, resultCode, data)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> onBackPressed()
            else -> {
            }
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onBackPressed() {
        setResult(Activity.RESULT_CANCELED)
        finish()
        super.onBackPressed()
    }
    companion object {
        const val RC_UPDATE = 11
        const val KEY_DATA = "key_data"
    }
}
