package sg.kickbates.customer

import android.app.Application
import android.content.Context
import androidx.multidex.MultiDex

import com.facebook.FacebookSdk
import com.facebook.LoggingBehavior
import com.google.firebase.crashlytics.FirebaseCrashlytics
import com.stripe.android.PaymentConfiguration
import sg.kickbates.customer.helper.AuthHelper


/**
 * Created by simx on 30,July,2019
 */
class Apps: Application() {
    override fun attachBaseContext(base: Context?) {
        super.attachBaseContext(base)
        MultiDex.install(this)
    }
    override fun onCreate() {
        super.onCreate()
        FacebookSdk.isInitialized()
        PaymentConfiguration.init(this, sg.kickbates.data.BuildConfig.STRIPE_KEY)
        AuthHelper.firebaseAuth().setLanguageCode("id")
        FirebaseCrashlytics.getInstance().setCrashlyticsCollectionEnabled(true)
        if (BuildConfig.DEBUG){
            FacebookSdk.setIsDebugEnabled(true)
            FacebookSdk.addLoggingBehavior(LoggingBehavior.INCLUDE_ACCESS_TOKENS)
        }
    }
}