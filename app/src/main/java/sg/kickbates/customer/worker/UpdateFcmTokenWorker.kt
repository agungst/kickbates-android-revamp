package sg.kickbates.customer.worker

import android.content.Context
import android.util.Log
import androidx.work.CoroutineWorker
import androidx.work.WorkerParameters
import com.google.firebase.iid.FirebaseInstanceId
import kotlinx.coroutines.*
import retrofit2.HttpException
import sg.kickbates.data.BuildConfig
import sg.kickbates.data.api.Api
import sg.kickbates.data.api.models.RequestFcmToken
import sg.kickbates.data.helper.Pref

/**
 * Created by simx on 02,January,2020
 */
class UpdateFcmTokenWorker(var context: Context, param:WorkerParameters): CoroutineWorker(context, param) {
    /**
     * A suspending method to do your work.  This function runs on the coroutine context specified
     * by [coroutineContext].
     * <p>
     * A CoroutineWorker is given a maximum of ten minutes to finish its execution and return a
     * [ListenableWorker.Result].  After this time has expired, the worker will be signalled to
     * stop.
     *
     * @return The [ListenableWorker.Result] of the result of the background work; note that
     * dependent work will not execute if you return [ListenableWorker.Result.failure]
     */

    private var job = SupervisorJob()
    override suspend fun doWork(): Result {
        val pref = Pref(context)
        FirebaseInstanceId.getInstance().instanceId.addOnCompleteListener { task ->
            run {
                if (task.isSuccessful){
                    val fcmToken = task.result?.token
                    Log.v("UpdateFcmTokenWorker","doWork -> $fcmToken")
                    val requestFcmToken = RequestFcmToken()
                    requestFcmToken.fcmToken = fcmToken

                    try {
                        CoroutineScope(CoroutineExceptionHandler { _, throwable -> run {
                            job = SupervisorJob()
                            throwable.printStackTrace()
                        } }).launch {
                            val res = Api.Factory.create(BuildConfig.BASE_URL).updateFcmTokenAsync(pref.token, pref.usersId, requestFcmToken).await()
                            Log.v("UpdateFcmTokenWorker","doWork -> $res")
                        }
                    }catch (e: HttpException){
                        e.printStackTrace()

                    }
                }
            }
        }
        return Result.success()
    }
}