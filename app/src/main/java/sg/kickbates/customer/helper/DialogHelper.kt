package sg.kickbates.customer.helper


import android.annotation.SuppressLint
import android.content.Context
import android.content.DialogInterface
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.view.LayoutInflater
import android.view.View
import android.view.Window
import android.view.WindowManager
import android.widget.AutoCompleteTextView
import android.widget.LinearLayout

import android.widget.TextView
import androidx.appcompat.app.AlertDialog
import com.google.android.material.textfield.TextInputEditText
import com.google.android.material.textfield.TextInputLayout
import sg.kickbates.customer.R
import sg.kickbates.customer.ui.dialog.account_card.AdapterCardModel
import sg.kickbates.customer.ui.payment.channels.PaymentChannelActivity
import sg.kickbates.data.api.models.payment.DetailInvoice
import sg.kickbates.data.api.room.CardModel

/**
 * Created by simx on 03,August,2019
 */
object DialogHelper {
    interface OnDialogHelperCardNumber {
        fun onSuccessCardInput(cardNumber:String?)
    }
    @SuppressLint("InflateParams")
    fun dialogCardNumber(context: Context, dataCards:ArrayList<CardModel>,  listener: OnDialogHelperCardNumber){
        val builder = AlertDialog.Builder(context)
        val view = LayoutInflater.from(context).inflate(R.layout.dialog_enter_credit_card_number,null,false)
        builder.setView(view)
        val dialog = builder.create()
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        view.findViewById<AutoCompleteTextView>(R.id.auto_card).setAdapter(AdapterCardModel(context, R.layout.item_card_model, dataCards))
        view.findViewById<TextView>(R.id.tv_ok).setOnClickListener {
            if (view.findViewById<AutoCompleteTextView>(R.id.auto_card).text.isNullOrEmpty()){
                view.findViewById<AutoCompleteTextView>(R.id.auto_card).error = "Required"
                view.findViewById<AutoCompleteTextView>(R.id.auto_card).requestFocus()
            }else{
                view.findViewById<AutoCompleteTextView>(R.id.auto_card).error = null
                listener.onSuccessCardInput(view.findViewById<AutoCompleteTextView>(R.id.auto_card).text.toString())
                dialog.dismiss()
            }
        }
        dialog.show()
    }
    fun showDialogError(contect: Context, msg:String, listener: View.OnClickListener){
        val builder = AlertDialog.Builder(contect)
        val view = LayoutInflater.from(contect).inflate(R.layout.custom_dialog_error,null,false)
        builder.setView(view)
        builder.setOnDismissListener { DialogInterface.OnDismissListener { dialogInterface -> dialogInterface.dismiss() } }
        val dialog = builder.create()
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        view.findViewById<TextView>(R.id.tv_msg).text = msg
        view.findViewById<TextView>(R.id.tv_OK).setOnClickListener (listener)
        if (!dialog.isShowing)dialog.show()
    }

    fun showDialogError(contect: Context, msg:String){
        val builder = AlertDialog.Builder(contect)
        val view = LayoutInflater.from(contect).inflate(R.layout.custom_dialog_error,null,false)
        builder.setView(view)
        builder.setOnDismissListener { DialogInterface.OnDismissListener { dialogInterface -> dialogInterface.dismiss() } }
        val dialog = builder.create()
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        view.findViewById<TextView>(R.id.tv_msg).text = msg
        view.findViewById<TextView>(R.id.tv_OK).setOnClickListener {
            dialog.dismiss()
        }
        if (!dialog.isShowing)dialog.show()
    }

    fun showDialogSuccess(contect: Context, msg:String, listener: View.OnClickListener){
        val builder = AlertDialog.Builder(contect)
        val view = LayoutInflater.from(contect).inflate(R.layout.custom_dialog_success,null,false)
        builder.setView(view)
        builder.setOnDismissListener { DialogInterface.OnDismissListener { dialogInterface -> dialogInterface.dismiss() } }
        val dialog = builder.create()
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        view.findViewById<TextView>(R.id.tv_msg).text = msg
        view.findViewById<TextView>(R.id.tv_OK).setOnClickListener (listener)
        if (!dialog.isShowing)dialog.show()
    }

    fun showDialogSuccess(contect: Context, msg:String){
        val builder = AlertDialog.Builder(contect)
        val view = LayoutInflater.from(contect).inflate(R.layout.custom_dialog_success,null,false)
        builder.setView(view)
        builder.setOnDismissListener { DialogInterface.OnDismissListener { dialogInterface -> dialogInterface.dismiss() } }
        val dialog = builder.create()
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        view.findViewById<TextView>(R.id.tv_msg).text = msg
        view.findViewById<TextView>(R.id.tv_OK).setOnClickListener { dialog.dismiss() }
        if (!dialog.isShowing)dialog.show()
    }

    fun showDialogProcess(contect: Context, msg:String): AlertDialog {
        val builder = AlertDialog.Builder(contect)
        val view = LayoutInflater.from(contect).inflate(R.layout.custom_dialog_prosess,null,false)
        builder.setView(view)
        val dialog = builder.create()
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        view.findViewById<TextView>(R.id.tv_msg).text = msg
        return dialog
    }
    fun showDialogSuccess(context: Context, msg:String, amount:String, receiptNo:String, painBy:String, kickUse:String, promoCode:String, date:String, listener: View.OnClickListener){
        val builder = AlertDialog.Builder(context)
        val view = LayoutInflater.from(context).inflate(R.layout.dialog_payment_success,null,false)
        builder.setView(view)
        view.findViewById<TextView>(R.id.tv_msg).text = "Your Payment at $msg"
        view.findViewById<TextView>(R.id.tv_amount).text = amount

        view.findViewById<TextView>(R.id.tv_receipt_no).text = receiptNo
        view.findViewById<TextView>(R.id.tv_pain_by).text = painBy
        view.findViewById<TextView>(R.id.tv_pay_amount).text = amount
        view.findViewById<TextView>(R.id.tv_kick_used).text = kickUse
        view.findViewById<TextView>(R.id.tv_promo_code).text = promoCode
        view.findViewById<TextView>(R.id.tv_date).text = date

        val dialog = builder.create()
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        view.findViewById<LinearLayout>(R.id.lyt_ok).setOnClickListener(listener)
        val lp = WindowManager.LayoutParams()
        lp.copyFrom(dialog.window?.attributes)
        lp.width = 150
        dialog.window?.attributes = lp
        dialog.show()
    }
    @SuppressLint("SetTextI18n")
    fun showDialogSuccessPayment(context: Context, type:Int?, data: DetailInvoice?, listener: OnDialogSuccessListener){
        val builder = AlertDialog.Builder(context)
        val view = LayoutInflater.from(context).inflate(R.layout.dialog_payment_success,null,false)
        builder.setView(view)
        val merchantName = view.findViewById<TextView>(R.id.tv_merchant_name)
        when (type) {
            PaymentChannelActivity.KEY_TYPE_MERCHANT -> {
                view.findViewById<TextView>(R.id.tv_msg).text = "Cashback Earned"
                merchantName.text = data?.merchant?.name()
                view.findViewById<TextView>(R.id.tv_amount).text = data?.earningAmount()
                view.findViewById<TextView>(R.id.tv_pay_amount).text = data?.amountPayable()
            }
            else -> {
                merchantName.visibility = View.GONE
                view.findViewById<TextView>(R.id.tv_amount).text = data?.invoicePayableString()
                view.findViewById<TextView>(R.id.tv_pay_amount).text = data?.amountPayable()
            }
        }


        view.findViewById<TextView>(R.id.tv_cash_back_earned).text = data?.earningAmount()
        view.findViewById<TextView>(R.id.tv_receipt_no).text = "${data?.invoiceNumber}"
        view.findViewById<TextView>(R.id.tv_total_amount).text = data?.invoicePayableString()
        view.findViewById<TextView>(R.id.tv_pain_by).text = "${data?.paidBy()}"
        view.findViewById<TextView>(R.id.tv_kick_used).text = data?.kickUsed()
        view.findViewById<TextView>(R.id.tv_promo_code).text = "${data?.promoCode()}"
        view.findViewById<TextView>(R.id.tv_date).text = "${data?.painDate()}"

        val dialog = builder.create()
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        view.findViewById<LinearLayout>(R.id.lyt_ok).setOnClickListener {
            dialog.dismiss()
            listener.onOk()
        }
        val lp = WindowManager.LayoutParams()
        lp.copyFrom(dialog.window?.attributes)
        lp.width = 150
        dialog.window?.attributes = lp
        dialog.show()
    }

    fun dialogOKCancel(msg: String, context: Context, listener: OnDialogSuccessListener){
        val builder = AlertDialog.Builder(context)
        val view = LayoutInflater.from(context).inflate(R.layout.dialog_ok_cancel,null,false)
        builder.setView(view)

        val dialog = builder.create()
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))

        view.findViewById<TextView>(R.id.tv_msg).text  = msg
        view.findViewById<TextView>(R.id.tv_cancel).setOnClickListener {
            dialog.dismiss()
        }
        view.findViewById<TextView>(R.id.tv_ok).setOnClickListener {
            dialog.dismiss()
            listener.onOk()
        }
        val lp = WindowManager.LayoutParams()
        lp.copyFrom(dialog.window?.attributes)
        lp.width = 150
        dialog.window?.attributes = lp
        dialog.show()
    }
    interface OnDialogSuccessListener {
        fun onOk()
    }
}