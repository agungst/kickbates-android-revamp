package sg.kickbates.customer.helper

import android.annotation.TargetApi
import android.app.DatePickerDialog
import android.content.Context
import android.os.Build
import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentTransaction
import com.google.android.material.snackbar.Snackbar
import com.google.android.material.textfield.TextInputEditText
import java.util.*

/**
 * Created by simx on 30,July,2019
 */
object AppsHelper {
    fun changeFragment(fm: FragmentManager, fragment: Fragment, tag: String, layout: Int) {
        var  ft : FragmentTransaction = fm.beginTransaction()
        ft.replace(layout, fragment)
        ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE)
        ft.isAddToBackStackAllowed
        ft.addToBackStack(tag)
        ft.commit()
    }
    fun changeFragment(fm: FragmentManager, fragment: Fragment, tag: String, layout: Int, bundle: Bundle) {
        fragment.arguments  = bundle
        var  ft : FragmentTransaction = fm.beginTransaction()
        ft.replace(layout, fragment)
        ft.isAddToBackStackAllowed
        ft.addToBackStack(tag)
        ft.commit()
    }
    fun changeFragment(fm: FragmentManager, fragment: Fragment, layout: Int) {
        var  ft : FragmentTransaction = fm.beginTransaction()
        ft.replace(layout, fragment)
        ft.commitAllowingStateLoss()
    }
    fun changeFragment(fm: FragmentManager, fragment: Fragment, layout: Int, bundle: Bundle) {
        fragment.arguments = bundle
        var  ft : FragmentTransaction = fm.beginTransaction()
        ft.replace(layout, fragment)
        ft.commitAllowingStateLoss()
    }

    fun isValidEmail(email:String):Boolean{
        var pattern: Regex = "[a-zA-Z0-9._-]+@[a-z]+.+[a-z]+".toRegex()
        return email.matches(pattern)
    }
    fun isValidPhone(phone:String):Boolean{
        return phone.length > 6
    }
    fun isValidPass(pass: String):Boolean{
        return pass.length >= 6
    }

    fun input(view: List<TextInputEditText>): Boolean {
        var isValid = false
        for (i in 0 until view.size){
            if (view[i].text.isNullOrEmpty()){
                view[i].requestFocus()
                view[i].error = "Required"
                isValid =  false
            } else {
                isValid =  true
            }
        }
        return isValid
    }

    fun showSnackbar(msg:String?, view: View){
        var snackbar = Snackbar.make(view,msg.toString(), Snackbar.LENGTH_LONG)
        snackbar.setAction("OK") {
            snackbar.dismiss()
        }
        snackbar.show()
    }
    fun showSnackbar(msg:String?, view: View, listener: View.OnClickListener){
        var snackbar = Snackbar.make(view,msg.toString(), Snackbar.LENGTH_LONG)
        snackbar.setAction("OK") {
            snackbar.dismiss()
            listener.onClick(view)
        }
        snackbar.show()
    }


    @TargetApi(Build.VERSION_CODES.N)
    fun showDatePicker(context: Context, listener: DatePickerDialog.OnDateSetListener){
        val year = Calendar.getInstance().get(Calendar.YEAR)
        val month = Calendar.getInstance().get(Calendar.MONDAY)
        val day = Calendar.getInstance().get(Calendar.DAY_OF_MONTH)
        val dateDialog = DatePickerDialog(context, listener,year,month,day)
        dateDialog.show()
    }

}