package sg.kickbates.customer.helper


import android.view.View

import com.google.android.material.snackbar.Snackbar
import sg.kickbates.customer.R

/**
 * Created by simx on 22,September,2019
 */
object SnackBarHelper {

    fun showSnackBarError(msg:String, view:View){
        val snack = Snackbar.make(view,msg,Snackbar.LENGTH_SHORT)
        snack.view.setBackgroundResource(R.drawable.bg_rounded_fill_tomato)
        snack.show()
    }
    fun showSnackBarError(msg:String, view:View, anchor: View){
        val snack = Snackbar.make(view,msg,Snackbar.LENGTH_SHORT)
        snack.anchorView = anchor
        snack.view.setBackgroundResource(R.drawable.bg_rounded_fill_tomato)
        snack.show()
    }
    fun showSnackBarSuccess(msg:String, view:View){
        val snack = Snackbar.make(view,msg,Snackbar.LENGTH_SHORT)
        snack.view.setBackgroundResource(R.drawable.bg_rounded_fill_green)
        snack.show()
    }
    fun showSnackBarSuccess(msg:String, view:View, anchor:View){
        val snack = Snackbar.make(view,msg,Snackbar.LENGTH_SHORT)
        snack.anchorView = anchor
        snack.view.setBackgroundResource(R.drawable.bg_rounded_fill_green)
        snack.show()
    }
    fun showSnackBarSuccess(msg:String, view:View, callback: OnSnackSuccessBarHelperListener){
        val snack = Snackbar.make(view,msg,Snackbar.LENGTH_INDEFINITE)
        snack.view.setBackgroundResource(R.drawable.bg_rounded_fill_green)
        snack.setAction("OK"){
            snack.dismiss()
            callback.onSuccess()
        }
        snack.show()
    }
    fun showSnackBarError(msg:String, view:View, listener: OnSnackBarHelperListener){
        val snack = Snackbar.make(view,msg,Snackbar.LENGTH_INDEFINITE)
        snack.setAction("OK") {
            snack.dismiss()
            listener.onError()
        }
        snack.view.setBackgroundResource(R.drawable.bg_rounded_fill_tomato)
        snack.show()
    }
    fun showSnackBarError(msg:String, view:View, anchor: View, listener: OnSnackBarHelperListener){
        val snack = Snackbar.make(view,msg,Snackbar.LENGTH_INDEFINITE)
        snack.anchorView = anchor
        snack.setAction("OK") {
            snack.dismiss()
            listener.onError()
        }
        snack.view.setBackgroundResource(R.drawable.bg_rounded_fill_tomato)
        snack.show()
    }
    interface OnSnackBarHelperListener {
        fun onError()
    }
    interface OnSnackSuccessBarHelperListener {
        fun onSuccess()
    }
}