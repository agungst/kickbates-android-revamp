package sg.kickbates.customer.helper.slider

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.viewpager.widget.PagerAdapter
import sg.kickbates.customer.R
import sg.kickbates.customer.databinding.ItemImageSliderBinding
import sg.kickbates.customer.ui.banner.DetailBannerActivity
import sg.kickbates.data.api.models.ResponseBanners
import sg.kickbates.data.api.models.ResponseDeals

/**
 * Created by simx on 31,July,2019
 */
class AdapterBannerDeals(private var sliders:List<ResponseDeals.DataItem>): PagerAdapter() {

    override fun isViewFromObject(view: View, `object`: Any): Boolean {
     return view === `object`
    }
    override fun getCount(): Int {
        return sliders.size
    }

    override fun instantiateItem(container: ViewGroup, position: Int): Any {
        var binding = ItemImageSliderBinding.bind(LayoutInflater.from(container.context).inflate(R.layout.item_image_slider, container, false))
        with(binding){
            imageSliderVm = ItemImageSliderVM(sliders[position].imageBanner())
            executePendingBindings()
        }
        binding.img.setOnClickListener { DetailBannerActivity.startFromBannerDeal(container.context, sliders[position]) }
        container.addView(binding.root)
        return binding.root
    }

    override fun destroyItem(container: ViewGroup, position: Int, `object`: Any) {
        container.removeView(`object` as View)
    }

}