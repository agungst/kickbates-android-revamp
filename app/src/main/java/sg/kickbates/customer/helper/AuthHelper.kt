package sg.kickbates.customer.helper

import android.app.Activity

import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInClient
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.firebase.auth.AuthCredential
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.PhoneAuthOptions
import com.google.firebase.auth.PhoneAuthProvider
import sg.kickbates.customer.R
import java.util.concurrent.TimeUnit
import android.util.Log
/**
 * Created by simx on 04,September,2019
 */
object AuthHelper {

    fun getGoogleClient(context: Activity): GoogleSignInClient? {
        val gso = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
            .requestIdToken(context.getString(R.string.default_web_client_id))
            .requestEmail().build()
        return GoogleSignIn.getClient(context, gso)
    }

    fun firebaseAuth(): FirebaseAuth {

        return FirebaseAuth.getInstance()
    }
    fun authPhone(phone:String, activity: Activity, callback:PhoneAuthProvider.OnVerificationStateChangedCallbacks){
        //if (BuildConfig.DEBUG) fakeAuthOtp()
        val option = PhoneAuthOptions.newBuilder(firebaseAuth())
            .setPhoneNumber(phone)
            .setTimeout(60, TimeUnit.SECONDS)
            .setActivity(activity)
            .setCallbacks(callback)
            .build()
        PhoneAuthProvider.verifyPhoneNumber(option)
    }

    fun authPhoneReSend(phone:String, token:PhoneAuthProvider.ForceResendingToken?,  activity: Activity, callback:PhoneAuthProvider.OnVerificationStateChangedCallbacks){
        //if (BuildConfig.DEBUG) fakeAuthOtp()
        val option = PhoneAuthOptions.newBuilder(firebaseAuth())
            .setPhoneNumber(phone)
            .setTimeout(60, TimeUnit.SECONDS)
            .setActivity(activity)
            .setCallbacks(callback)

        if (token != null){ option.setForceResendingToken(token) }
        PhoneAuthProvider.verifyPhoneNumber(option.build())
    }

    fun singOut(){
        firebaseAuth().signOut()
    }
    private fun fakeAuthOtp(){
        val phoneNumber = "+16505554567"
        val smsCode = "123456"
        //val firebaseSetting = firebaseAuth().firebaseAuthSettings
        //firebaseSetting.forceRecaptchaFlowForTesting(true)
        //firebaseSetting.setAutoRetrievedSmsCodeForPhoneNumber(phoneNumber, smsCode)

    }

    fun singInWithPhone(credential: AuthCredential?): Boolean {
        var valid = false
        if (credential != null) {
            firebaseAuth().signInWithCredential(credential).addOnFailureListener {
                it.printStackTrace()
                valid = false
            }.addOnCompleteListener { task ->
                valid = if (task.isSuccessful){
                    Log.v("AuthHelper","singInWithPhone -> ${task.result.user?.phoneNumber}")
                    task.result.user != null
                } else {
                    false
                }
            }
        } else valid = false
        return valid
    }
}