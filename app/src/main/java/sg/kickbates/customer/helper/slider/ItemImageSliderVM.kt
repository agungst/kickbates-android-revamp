package sg.kickbates.customer.helper.slider

import androidx.databinding.BaseObservable
import androidx.databinding.Bindable
import androidx.databinding.ObservableField
import sg.kickbates.data.api.models.ResponseBanners


/**
 * Created by simx on 31,July,2019
 */
class ItemImageSliderVM(data:String?):BaseObservable() {
    @Bindable var image = ObservableField(data)
}