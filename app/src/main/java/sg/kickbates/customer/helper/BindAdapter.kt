package sg.kickbates.customer.helper

import android.text.TextWatcher
import android.view.View
import android.widget.EditText
import android.widget.ImageView
import androidx.databinding.BindingAdapter
import coil.api.load

import com.google.android.material.textfield.TextInputEditText
import com.google.android.material.textfield.TextInputLayout
import de.hdodenhof.circleimageview.CircleImageView
import sg.kickbates.customer.R

/**
 * Created by simx on 31,July,2019
 */
class BindAdapter {
    companion object {
        @BindingAdapter("bind:visibility") @JvmStatic fun setVisibility(view: View, value: Boolean) {
            view.visibility = if (value) View.VISIBLE else View.GONE
        }
        @BindingAdapter("bind:image_url") @JvmStatic
        fun loadImage(imageView: ImageView, url:String?){
            imageView.load(url){
                placeholder(R.drawable.placeholder)
                error(R.drawable.placeholder)
            }
        }
        @BindingAdapter("bind:image_url_card_small") @JvmStatic
        fun loadImageCardLogoSmall(imageView: ImageView, url:String?){
            imageView.load(url){
                placeholder(R.drawable.placeholder)
                error(R.drawable.placeholder)
                size(30 * 25)
            }
        }
        @BindingAdapter("bind:image_url_small") @JvmStatic
        fun loadImageSmall(imageView: ImageView, url:String){
            imageView.load(url){
                placeholder(R.drawable.placeholder)
                error(R.drawable.placeholder)
            }

        }
        @BindingAdapter("bind:image_custom") @JvmStatic
        fun loadImageCustom(imageView: CircleImageView, url:String?){
            imageView.load(url){
                placeholder(R.drawable.placeholder)
                error(R.drawable.placeholder)
            }

        }
        @BindingAdapter("bind:image_asset")
        @JvmStatic fun loadIntImage(imageView: ImageView, url:Int){
            imageView.load(url){
                placeholder(R.drawable.placeholder)
                error(R.drawable.placeholder)
            }
        }

        @BindingAdapter("bind:edit_text") @JvmStatic fun watchEditText(editText: EditText, watcher: TextWatcher){
            editText.addTextChangedListener(watcher)
        }
        @BindingAdapter("bind:text_input") @JvmStatic fun watchTextInputEditText(editText: TextInputEditText, watcher: TextWatcher){
            editText.addTextChangedListener(watcher)
        }

        @BindingAdapter("bind:error_input_layout") @JvmStatic fun errorLayoutInput(view: TextInputLayout, msg: String?){
            view.error  = msg
        }



    }


}